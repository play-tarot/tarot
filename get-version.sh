#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PACKAGE_VERSION=$(./git-version-gen .tarball-version \
		      | sed 's|/[[:digit:]]\.[[:digit:]]\.[[:digit:]]||g')
LIBTOOL_VERSION=$(./git-version-gen .tarball-version \
		      | sed 's|.*/\([[:digit:]]\.[[:digit:]]\.[[:digit:]]\).*|\1|g' \
		      | sed 's/\./:/g')

case $1 in
    package)
	echo -n "$PACKAGE_VERSION";;
    libtool)
	echo -n "$LIBTOOL_VERSION";;
    *)
	echo -n "Usage: ./get-version.sh (package|libtool)" ; exit 1;;
esac
