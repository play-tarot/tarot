#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo '<?xml version="1.0" encoding="utf-8"?>'
echo '<releases>'
for tag in $(git tag --sort=taggerdate | grep '^v[0-9]*\.[0-9]*\.[0-9]*/[0-9]*\.[0-9]*\.[0-9]*$' | tac)
do
    TAG_FILENAME=$(echo "$tag" | sed 's/\//_/g')
    TAG_CHARS=$(echo "$tag" | wc -c)
    TAG_HEADER=$(git tag -l -n $tag | tail -c +$TAG_CHARS | sed 's/\.[[:space:]]$//g')
    TAG_DATE=$(git log -1 --format=%ai $tag)
    TAG_DATE_DEB=$(LANG=C date --date="$TAG_DATE" "+%a, %d %b %Y %T %z")
    TAG_AUTHOR=$(git log -1 --format=%an $tag)
    TAG_EMAIL=$(git log -1 --format=%ae $tag)
    TAG_VERSION_FULL=$(echo "$tag" | tail -c +2)
    TAG_PROGRAM_VERSION=$(echo "$TAG_VERSION_FULL" | cut -d '/' -f 1)
    TAG_LIBTOOL_VERSION=$(echo "$TAG_VERSION_FULL" | cut -d '/' -f 2)

    echo "#+title: $TAG_HEADER" > "$TAG_FILENAME.org"
    echo "#+date: $TAG_DATE" >> "$TAG_FILENAME.org"
    echo "#+author: $TAG_AUTHOR" >> "$TAG_FILENAME.org"
    echo "#+email: $TAG_EMAIL" >> "$TAG_FILENAME.org"
    git tag -l -n999 $tag | tail -n +2 >> "$TAG_FILENAME.org"

    emacs --batch --file "$TAG_FILENAME.org" --eval "(org-html-export-to-html nil nil nil t '())" || exit 1
    echo "  <release version=\"$(echo $TAG_PROGRAM_VERSION)\" libtool=\"$(echo $TAG_LIBTOOL_VERSION)\" date=\"$TAG_DATE\" deb-date=\"$TAG_DATE_DEB\" author=\"$TAG_AUTHOR\" email=\"$TAG_EMAIL\">"
    echo "    <summary>"
    echo "$TAG_HEADER"
    echo "    </summary>"
    echo "    <description>"
    cat "$TAG_FILENAME.html"
    echo "    </description>"
    echo "  </release>"
    rm "$TAG_FILENAME.org" "$TAG_FILENAME.html"
done
echo '</releases>'
