<?xml version="1.0" encoding="UTF-8"?>
<!-- tarot implements the rules of the tarot game
Copyright (C) 2019, 2024  Vivien Kraus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:output method="xml" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template match="releases">
    <xsl:comment>Copyright (C) 2019 Vivien Kraus</xsl:comment>
    <component type="desktop-application">
      <id>eu.planete_kraus.Tarot</id>
      <metadata_license>CC0-1.0</metadata_license>
      <name>The Amazing Rules Of TAROT</name>
      <summary>Play a game of Tarot against the artificial intelligence</summary>
      <icon type="remote">https://play-tarot.frama.io/tarot/share/icons/hicolor/256x256/apps/eu.planete_kraus.Tarot.png</icon>
      <description>
	<p>
	  Tarot is a popular card game, in which a strong player plays
	  against the three others.  This application lets you play against
	  the AI.
	</p>
      </description>
      <url type="homepage">https://play-tarot.frama.io/tarot</url>
      <!-- <url type="help">https://play-tarot.frama.io/tarot/share/doc/tarot/tarot.html/index.html</url> -->
      <!-- <url type="contact">mailto://vivien@planete-kraus.eu</url> -->
      <screenshots>
	<screenshot type="default">
	  <caption>Playing tarot against the AI</caption>
	  <image type="source">https://play-tarot.frama.io/tarot/screenshot.png</image>
	</screenshot>
      </screenshots>
      <!-- <launchable type="desktop-id">eu.planete_kraus.Tarot.desktop</launchable> -->
      <provides>
	<binary>tarot</binary>
      </provides>
      <project_license>GPL-3.0+</project_license>
      <developer_name>Vivien Kraus</developer_name>
      <update_contact>vivien@planete_kraus.eu</update_contact>
      <launchable type="desktop-id">eu.planete_kraus.Tarot.desktop</launchable>
      <categories>
	<category>Game</category>
      </categories>
      <releases>
	<xsl:apply-templates />
      </releases>
      <translation type="gettext">tarot</translation>
      <content_rating type="oars-1.1">
	<content_attribute id="violence-cartoon">none</content_attribute>
	<content_attribute id="violence-fantasy">none</content_attribute>
	<content_attribute id="violence-realistic">none</content_attribute>
	<content_attribute id="violence-bloodshed">none</content_attribute>
	<content_attribute id="violence-sexual">none</content_attribute>
	<content_attribute id="violence-desecration">none</content_attribute>
	<content_attribute id="violence-slavery">none</content_attribute>
	<content_attribute id="violence-worship">none</content_attribute>
	<content_attribute id="drugs-alcohol">none</content_attribute>
	<content_attribute id="drugs-narcotics">none</content_attribute>
	<content_attribute id="drugs-tobacco">none</content_attribute>
	<content_attribute id="sex-nudity">none</content_attribute>
	<content_attribute id="sex-themes">none</content_attribute>
	<content_attribute id="sex-homosexuality">none</content_attribute>
	<content_attribute id="sex-prostitution">none</content_attribute>
	<content_attribute id="sex-adultery">none</content_attribute>
	<content_attribute id="sex-appearance">none</content_attribute>
	<content_attribute id="language-profanity">none</content_attribute>
	<content_attribute id="language-humor">none</content_attribute>
	<content_attribute id="language-discrimination">none</content_attribute>
	<content_attribute id="social-chat">none</content_attribute>
	<content_attribute id="social-info">none</content_attribute>
	<content_attribute id="social-audio">none</content_attribute>
	<content_attribute id="social-location">none</content_attribute>
	<content_attribute id="social-contacts">none</content_attribute>
	<content_attribute id="money-purchasing">none</content_attribute>
	<content_attribute id="money-gambling">none</content_attribute>
      </content_rating>
    </component>
  </xsl:template>

  <xsl:template match="release[not(starts-with(@version, '0.'))]">
    <release version="{@version}"
	     date="{@date}"
	     urgency="medium">
      <description>
	<xsl:apply-templates />
      </description>
    </release>
  </xsl:template>

  <xsl:template match="release[starts-with(@version, '0.')]">
    <xsl:comment>Release for version <xsl:value-of select="@version" /> ignored.</xsl:comment>
  </xsl:template>

  <xsl:template match="summary">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="description">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="p">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="ol">
    <ol>
      <xsl:apply-templates />
    </ol>
  </xsl:template>

  <xsl:template match="ul">
    <ul>
      <xsl:apply-templates />
    </ul>
  </xsl:template>

  <xsl:template match="li">
    <li>
      <xsl:apply-templates />
    </li>
  </xsl:template>
</xsl:stylesheet>
