<?xml version="1.0" encoding="UTF-8"?>
<!-- tarot implements the rules of the tarot game
Copyright (C) 2019  Vivien Kraus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:tarot="http://planete-kraus.eu/tarot"
		version="1.0">
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template match="tarot:tree">
    digraph G {
    node [shape=record];
    <xsl:apply-templates />
    }
  </xsl:template>

  <xsl:template match="tarot:node/tarot:node">
    <xsl:variable name="id" select="generate-id()" />
    <xsl:value-of select="$id" /> [label="<xsl:apply-templates select="tarot:stats" />"];
    <xsl:value-of select="generate-id(..)" /> -> <xsl:value-of select="$id" />;
    <xsl:apply-templates select="tarot:node" />
  </xsl:template>

  <xsl:template match="tarot:tree/tarot:node">
    <xsl:variable name="id" select="generate-id()" />
    <xsl:value-of select="$id" /> [label="<xsl:apply-templates select="tarot:stats" />" fillcolor="yellow" style="filled"];
    <xsl:apply-templates select="tarot:node" />
  </xsl:template>

  <xsl:template match="tarot:stats">
    <xsl:text>{</xsl:text>
    <xsl:apply-templates />
    <xsl:text>|{</xsl:text>
    <xsl:value-of select="@n-simulations" />
    <xsl:text>|</xsl:text>
    <xsl:value-of select="@sum-scores div @n-simulations" />
    <xsl:text>}</xsl:text>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:card">
    <xsl:text>play </xsl:text>
    <xsl:value-of select="@card" />
  </xsl:template>

  <xsl:template match="tarot:bid">
    <xsl:text>bid </xsl:text>
    <xsl:value-of select="@bid" />
  </xsl:template>
</xsl:stylesheet>
