#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019, 2020, 2024  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# HACKS!!!!
#
# Remove VALAFLAGS="--target-glib=2.50"

echo "Dir::Cache::Archives $APT_CACHE;" > /etc/apt/apt.conf.d/75gitlabcicache.conf
mkdir -p $APT_CACHE || exit 1
apt-get update
apt-get upgrade -y
apt-get install -y \
	appstream \
	autoconf \
	autoconf-archive \
	automake \
	autopoint \
	build-essential \
	check \
	curl \
	devscripts \
	emacs \
	emacs-goodies-el \
	frama-c-base \
	gcc \
	gettext \
	git \
	gobject-introspection \
	gperf \
	guile-2.2-dev \
	help2man \
	imagemagick \
	indent \
	jq \
	libcairo2-dev \
	libgirepository1.0-dev \
	libglib2.0-dev \
	libgtk-3-dev \
	libtool \
	libxml2-utils \
	make \
	nettle-dev \
	org-mode \
	pkg-config \
	r-cran-ggplot2 \
	r-cran-glmnet \
	r-cran-optparse \
	texinfo \
	texlive-full \
	valac \
	valgrind \
	wget \
	xsltproc

SOURCE_TOP=$(pwd)
rm -rf $SOURCE_TOP/dist || exit 1
rm -rf ../tarot-*.tar.gz ../tarot-*/ tarot-*.tar.gz
sh autogen.sh VALAFLAGS="--target-glib=2.50" || exit 1
./configure \
    CFLAGS="-Wall -Wextra -g $CFLAGS" \
    VALAFLAGS="--target-glib=2.50" \
    --prefix="$SOURCE_TOP/dist" \
    --enable-valgrind=yes \
    --enable-silent-rules=yes \
    --enable-static-analysis=yes \
    || (cat config.log ; exit 1) \
    || exit 1
make .version || exit 1
make -j || exit 1
make -j git-check || exit 1
make -j distcheck || exit 1
cd $SOURCE_TOP || exit 1
mkdir -p $SOURCE_TOP/dist || exit 1
cp $SOURCE_TOP/.version \
   $SOURCE_TOP/dist/version || exit 1
cp tarot-*.tar.gz $SOURCE_TOP/dist/ || exit 1

if test "x$CI_COMMIT_TAG" != "x"
then
    cd $SOURCE_TOP
    TARFILE=$(ls dist/tarot-*.tar.gz)
    mkdir -p /root/.ssh || exit 1
    echo "$SSH_KEY" > /root/.ssh/id_rsa || exit 1
    echo "$SSH_KEY_PUB" > /root/.ssh/id_rsa.pub || exit 1
    echo "$SSH_KNOWN_HOSTS" > /root/.ssh/known_hosts || exit 1
    chmod go-rwx /root/.ssh/id_rsa || exit 1
    git config --global user.email "vivien@planete-kraus.eu" || exit 1
    git config --global user.name "Gitlab CI for the tarot package" || exit 1
    wget -O /tmp/submit https://play-tarot.frama.io/tarot-releases/submit || exit 1
    chmod ugo+x /tmp/submit || exit 1
    /tmp/submit $TARFILE || exit 1
fi
