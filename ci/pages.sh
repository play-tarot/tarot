#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019, 2020, 2024  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo "Dir::Cache::Archives $APT_CACHE;" > /etc/apt/apt.conf.d/75gitlabcicache.conf
mkdir -p $APT_CACHE || exit 1
apt-get update
apt-get upgrade -y
apt-get install -y \
	build-essential \
	jq \
	libglib2.0-dev \
	nettle-dev \
	pkg-config \
	texinfo \
	valgrind \
	wget

VERSION=$(cat dist/version)

cp dist/tarot-$VERSION.tar.gz . || exit 1
tar xf tarot-$VERSION.tar.gz || exit 1
mkdir build || exit 1
cd build/
../tarot-$VERSION/configure \
    --prefix="$PWD/../public" \
    --enable-silent-rules=yes \
    --enable-valgrind=yes \
    --disable-program \
    CFLAGS="-g -fprofile-arcs -ftest-coverage" \
    || (cat config.log; exit 1)
make -j 16 install-dist_xlargeiconsDATA install-html || exit 1
cp ../tarot-$VERSION/screenshots/tarot.png ../public/screenshot.png || exit 1
make distclean || exit 1
cd .. || exit 1

cp tarot-$VERSION/index.html public/ || exit 1

if test "x$CI_PAGES_URL" = "x"
then
    export CI_PAGES_URL="https://play-tarot.frama.io/tarot"
fi

for link in $(wget -O- "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases" | jq -r '.[0].assets.links[].url')
do
    case $link in
	*/tarot-*.*.*.tar.gz)
	    wget -O public/tarot-latest.tar.gz "$link" || exit 1;;
	*)
	    echo "Unknown link '$link'"
    esac
done
