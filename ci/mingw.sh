#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019, 2020, 2024  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

(echo "keepcache=True" ; echo "cachedir=$DNF_CACHE") >> /etc/dnf/dnf.conf || exit 1
mkdir -p $DNF_CACHE || exit 1
dnf update -y || exit 1

cat > /etc/rpm/macros.image-language-conf <<EOF
%_install_langs all
EOF

dnf reinstall -y glibc-common || exit 1

dnf install -y \
    \
    mingw64-gcc \
    mingw64-nettle \
    mingw64-glib2 \
    mingw64-cairo \
    mingw64-gtk3 \
    mingw64-hicolor-icon-theme \
    \
    make \
    wget \
    ImageMagick \
    \
    mingw32-nsis \
    || exit 1

mkdir -p public || exit 1

echo "Build tarot"
VERSION=$(cat dist/version)

cp dist/tarot-$VERSION.tar.gz . || exit 1
tar xf tarot-$VERSION.tar.gz || exit 1
cd tarot-$VERSION
mingw64-configure \
    --prefix="/" \
    --exec-prefix="/" \
    --bindir="/bin" \
    --sysconfdir="/etc" \
    --libdir="/lib" \
    --includedir="/include" \
    --datarootdir="/share" \
    --datadir="/share" \
    --infodir="/share/info" \
    --mandir="/share/man" \
    --enable-runtime-prefix='TAROT_PREFIX' \
    USER_DATA_DIR_VARIABLE='LOCALAPPDATA' \
    --with-static-program-name=yes \
    HOST_SYSROOT="/usr/x86_64-w64-mingw32/sys-root/mingw" \
    --enable-textdomain-codeset="utf-8" \
    CFLAGS="-Wl,-subsystem,windows" \
    || exit 1
make -j 8 nsi || exit 1
cp tarot-setup.exe ../public/
make -j distclean || exit 1
cd ..
