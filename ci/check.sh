#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019, 2020  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo "Dir::Cache::Archives $APT_CACHE;" > /etc/apt/apt.conf.d/75gitlabcicache.conf
mkdir -p $APT_CACHE || exit 1
apt-get update
apt-get upgrade -y
apt-get install -y \
	build-essential \
	guile-2.2-dev \
	frama-c-base \
	libglib2.0-dev \
	make \
	nettle-dev \
	pkg-config \
	valgrind

VERSION=$(cat dist/version)

cp dist/tarot-$VERSION.tar.gz . || exit 1
tar xf tarot-$VERSION.tar.gz || exit 1
mkdir build || exit 1
cd build/
../tarot-$VERSION/configure \
    --enable-silent-rules=yes \
    --disable-valgrind \
    --disable-program \
    CFLAGS="-O3 -fsanitize=undefined -fsanitize=address" \
    || (cat config.log; exit 1)
make -j 16 check || exit 1
make distclean || exit 1
