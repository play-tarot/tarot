;; tarot implements the rules of the tarot game
;; Copyright (C) 2019  Vivien Kraus

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(setq org-confirm-babel-evaluate nil)
(setq org-export-use-babel t)
(setq org-babel-load-languages '((emacs-lisp . t) (scheme . t) (shell . t) (R . t)))
(setq geiser-default-implementation 'guile)
(setq org-html-htmlize-output-type 'css)
(setq ess-startup-directory "./")
(add-to-list 'load-path ".")
(require 'org)
(require 'ox-texinfo)
(require 'ox-html)
