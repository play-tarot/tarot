;; tarot implements the rules of the tarot game
;; Copyright (C) 2019, 2020, 2024  Vivien Kraus

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(specifications->manifest
 '(
   "appstream"
   "autoconf"
   "autoconf-archive"
   "automake"
   "bash"
   "bzip2"
   "cairo"
   "check"
   "coreutils"
   "curl"
   "diffutils"
   "emacs"
   "emacs-htmlize"
   "emacs-org"
   "findutils"
   "gawk"
   "gcc-toolchain"
   "gettext"
   "git"
   "glib:bin"
   "gobject-introspection"
   "grep"
   "gtk+"
   "guile"
   "gzip"
   "help2man"
   "imagemagick"
   "indent"
   "jq"
   "libtool"
   "libxml2"
   "libxslt"
   "make"
   "nettle"
   "nss-certs"
   "perl"
   "pkg-config"
   "r-ggplot2"
   "r-glmnet"
   "r-optparse"
   "sed"
   "tar"
   "texinfo"
   "texlive"
   "vala"
   "valgrind"
   "wget"
   "xz"
   ))
