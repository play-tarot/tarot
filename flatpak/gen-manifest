#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2019, 2020, 2024  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

URL="https://play-tarot.frama.io/tarot-releases/$1"

cat <<EOF
{
    "app-id": "eu.planete_kraus.Tarot",
    "runtime": "org.freedesktop.Platform",
    "sdk": "org.freedesktop.Sdk",
    "runtime-version": "23.08",

    "command": "tarot",
    "finish-args": [
	"--socket=x11",
	"--socket=wayland",
	"--share=ipc"
    ],
    "cleanup": [
	"/include",
	"*.a",
	"*.pc",
	"tarot/api.xml",
	"*.gir",
	"*.vapi",
	"*.typelib",
        "share/tarot/dataset"
    ],
    "modules": [
	{
	    "name": "tarot",
	    "buildsystem": "autotools",
	    "sources": [
		{
		    "type": "archive",
		    "url": "$URL",
		    "sha256": "GET_SHA256"
		}
	    ],
	    "config-opts": [
		"--enable-silent-rules=yes"
	    ]
	}
    ]
}

EOF
