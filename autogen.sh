#!/bin/sh
# tarot implements the rules of the tarot game
# Copyright (C) 2018, 2019  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

(echo "@setfilename tarot.info" \
     && echo '\bye') > doc/tarot.texi || exit 1
touch AUTHORS || exit 1
touch README NEWS || exit 1
export PATH="$PWD/.gnulib:$PATH"
if [ -d .gnulib ]
then
    git submodule update --init .gnulib || exit 1
fi
if [ -d src/libtarot/perceptron/julien ]
then
    git submodule update --init src/libtarot/perceptron/julien || exit 1
fi
gnulib-tool --libtool \
	    --import assert git-version-gen getopt-gnu setenv stdalign xalloc setlocale \
	    gitlog-to-changelog git-version-gen || exit 1
./gitlog-to-changelog > ChangeLog
touch README NEWS || exit 1
autoreconf -vif || exit 1
rm doc/tarot.texi || exit 1
rm AUTHORS || exit 1
rm README NEWS || exit 1
