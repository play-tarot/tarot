/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/perceptron_private.h>
#include <tarot/game_private.h>
#include <tarot/features_private.h>
#include <stdlib.h>
#include <assert.h>
#include "xalloc.h"
#include "gettext.h"
#define _(String) dgettext (PACKAGE, String)

TarotPerceptron *
tarot_perceptron_alloc (size_t n_hidden_layers, const size_t *hidden_sizes,
                        double learning_rate)
{
  TarotPerceptron *ret = xmalloc (sizeof (TarotPerceptron));
  perceptron_construct (ret, n_hidden_layers, hidden_sizes, learning_rate);
  return ret;
}

TarotPerceptron *
tarot_perceptron_static_default ()
{
  TarotPerceptron *ret = xmalloc (sizeof (TarotPerceptron));
  perceptron_construct_static_default (ret);
  return ret;
}

TarotPerceptron *
tarot_perceptron_dup (const TarotPerceptron * perceptron)
{
  TarotPerceptron *ret = xmalloc (sizeof (TarotPerceptron));
  perceptron_copy (ret, perceptron);
  return ret;
}

void
tarot_perceptron_set_learning_rate (TarotPerceptron * perceptron,
                                    double learning_rate)
{
  perceptron_set_learning_rate (perceptron, learning_rate);
}

void
tarot_perceptron_free (TarotPerceptron * perceptron)
{
  if (perceptron != NULL)
    {
      perceptron_destruct (perceptron);
    }
  free (perceptron);
}

void
tarot_perceptron_load (TarotPerceptron * perceptron, size_t start,
                       size_t n_weights, const double *parameters)
{
  perceptron_load (perceptron, start, n_weights, parameters);
}

size_t
tarot_perceptron_save (const TarotPerceptron * perceptron, size_t start,
                       size_t max, double *parameters)
{
  return perceptron_save (perceptron, start, max, parameters);
}

void
tarot_perceptron_save_alloc (const TarotPerceptron * perceptron, size_t *n,
                             double **data)
{
  *n = perceptron_save (perceptron, 0, 0, NULL);
  *data = xmalloc (*n * sizeof (double));
  if (perceptron_save (perceptron, 0, *n, *data) != *n)
    {
      assert (0);
    }
}

void
tarot_perceptron_learn (TarotPerceptron * perceptron, const TarotGame * base,
                        const TarotGameEvent * event, double final_score)
{
  perceptron_learn (perceptron, base, event, final_score);
}

void
tarot_perceptron_eval (TarotPerceptron * perceptron,
                       const TarotGame * base,
                       size_t n_candidates,
                       TarotGameEvent ** candidates,
                       size_t start, size_t max, double *scores)
{
  perceptron_eval (perceptron, base, n_candidates, candidates, start,
                   max, scores);
}

#include <tarot/perceptron_private_impl.h>
#include <tarot/game_private_impl.h>
#include <tarot/features_private_impl.h>
