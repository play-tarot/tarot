/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PERCEPTRON_PRIVATE_IMPL_INCLUDED
#define H_TAROT_PERCEPTRON_PRIVATE_IMPL_INCLUDED

#include <tarot/features_private.h>
#include <tarot/counter_private.h>
#include <tarot/game_event_private.h>
#include <tarot/cards_private.h>
#include <nettle/yarrow.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "xalloc.h"

#define PERCEPTRON_DIM_INPUT 63

#define PERCEPTRON_DIM_INPUT_BASE 36
#define PERCEPTRON_DIM_INPUT_ACTION 27

static inline void
perceptron_construct (TarotPerceptron * perceptron,
                      size_t n_hidden_layers,
                      const size_t *hidden_sizes, double learning_rate)
{
  size_t i;
  perceptron->n_layers = n_hidden_layers;
  perceptron->layer_sizes = xmalloc (n_hidden_layers * sizeof (size_t));
  for (i = 0; i < n_hidden_layers; i++)
    {
      perceptron->layer_sizes[i] = hidden_sizes[i];
    }
  perceptron->learning_rate = learning_rate;
  perceptron->jl =
    julien_alloc (PERCEPTRON_DIM_INPUT, 1, n_hidden_layers, hidden_sizes);
  perceptron->best_event.n = 78;
  perceptron->best_event.data = perceptron->event_data;
}

static inline void
perceptron_construct_static_default (TarotPerceptron * perceptron)
{
  static const size_t hidden_sizes[] = {
#include "perceptron_private_static_structure.h"
  };
  static const double weights[] = {
#include "perceptron_private_static_weights.h"
  };
  size_t n_hidden_layers = sizeof (hidden_sizes) / sizeof (hidden_sizes[0]);
  size_t n_weights = sizeof (weights) / sizeof (weights[0]);
  perceptron_construct (perceptron, n_hidden_layers, hidden_sizes, 1e-8);
  perceptron_load (perceptron, 0, n_weights, weights);
}

static inline void
perceptron_copy (TarotPerceptron * dest, const TarotPerceptron * source)
{
  size_t n_parameters;
  const double *source_parameters = julien_data (source->jl, &n_parameters);
  perceptron_construct (dest, source->n_layers, source->layer_sizes,
                        source->learning_rate);
  perceptron_load (dest, 0, n_parameters, source_parameters);
}

static inline void
perceptron_set_learning_rate (TarotPerceptron * perceptron,
                              double learning_rate)
{
  perceptron->learning_rate = learning_rate;
}

static inline void
perceptron_destruct (TarotPerceptron * perceptron)
{
  if (perceptron != NULL)
    {
      free (perceptron->layer_sizes);
      julien_free (perceptron->jl);
    }
}

static inline void
perceptron_load (TarotPerceptron * perceptron, size_t start, size_t n,
                 const double *parameters)
{
  julien_set_data (perceptron->jl, start, n, parameters);
}

static inline size_t
perceptron_save (const TarotPerceptron * perceptron, size_t start, size_t max,
                 double *parameters)
{
  size_t ret;
  const double *src = julien_data (perceptron->jl, &ret);
  size_t i;
  for (i = 0; i < max && i + start < ret; i++)
    {
      parameters[i] = src[i + start];
    }
  return ret;
}

static inline size_t
perceptron_encode_base (const TarotGame * state, size_t start, size_t max,
                        double *input)
{
  double my_input[36];
  size_t i;
  size_t f_n_players;
  int f_with_call;
  size_t f_trick;
  size_t f_position;
  int f_partner_known;
  TarotBid f_bid_minimum;
  size_t f_n_kings;
  size_t f_n_marriages;
  size_t f_n_extended_marriages;
  size_t f_n_queens;
  size_t f_n_knights;
  size_t f_n_shortest;
  size_t f_n_second_shortest;
  size_t f_n_second_longest;
  size_t f_n_longest;
  int f_has_petit;
  int f_has_excuse;
  int f_has_twentyone;
  size_t f_n_trumps;
  TarotNumber f_max_trump;
  TarotNumber f_min_trump;
  TarotNumber f_q1_trumps;
  TarotNumber f_q3_trumps;
  TarotNumber f_median_trumps;
  int f_team_attack;
  int f_team_defence;
  size_t f_n_trick_players_already_played;
  size_t f_n_allies_after_last_opponent;
  int f_play_called_suit;
  size_t f_play_n_times_lead;
  size_t f_play_suit_remaining;
  size_t f_play_suit_remaining_points;
  int f_ally_may_be_minor_master;
  int f_ennemy_may_be_minor_master;
  size_t f_n_trumps_remaining;
  int f_ally_may_be_trump_master;
  int f_ennemy_may_be_trump_master;
  size_t f_n_halfpoints_in_trick;
  size_t f_n_oudlers_in_trick;
  TarotSuit order[4];
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, state);
  features_variant (state, &f_n_players, &f_with_call);
  features_position (state, &f_trick, &f_position);
  features_partner_known (state, &f_partner_known);
  features_bid_minimum (state, &f_bid_minimum);
  features_count_faces (state, &f_n_kings, &f_n_marriages,
                        &f_n_extended_marriages, &f_n_queens, &f_n_knights);
  features_count_minor (state, &f_n_shortest, &f_n_second_shortest,
                        &f_n_second_longest, &f_n_longest, order);
  features_count_oudlers (state, &f_has_petit, &f_has_excuse,
                          &f_has_twentyone);
  features_count_trumps (state, &f_n_trumps, &f_max_trump, &f_min_trump,
                         &f_q1_trumps, &f_q3_trumps, &f_median_trumps);
  features_team (state, &f_team_attack, &f_team_defence);
  features_trick_players (state, &f_n_trick_players_already_played,
                          &f_n_allies_after_last_opponent);
  features_minor_suit (state, &counter, &f_play_called_suit,
                       &f_play_n_times_lead, &f_play_suit_remaining,
                       &f_play_suit_remaining_points,
                       &f_ally_may_be_minor_master,
                       &f_ennemy_may_be_minor_master);
  features_trump (state, &counter, &f_n_trumps_remaining,
                  &f_ally_may_be_trump_master, &f_ennemy_may_be_trump_master);
  features_points (state, &f_n_halfpoints_in_trick, &f_n_oudlers_in_trick);
  my_input[0] = (double) f_n_players;
  my_input[1] = (double) (!!f_with_call);
  my_input[2] = (double) f_trick;
  my_input[3] = (double) f_position;
  my_input[4] = (double) (!!f_partner_known);
  my_input[5] = (double) (f_bid_minimum - TAROT_PASS);
  my_input[6] = (double) f_n_kings;
  my_input[7] = (double) f_n_marriages;
  my_input[8] = (double) f_n_extended_marriages;
  my_input[9] = (double) f_n_queens;
  my_input[10] = (double) f_n_knights;
  my_input[11] = (double) f_n_shortest;
  my_input[12] = (double) f_n_second_shortest;
  my_input[13] = (double) f_n_second_longest;
  my_input[14] = (double) f_n_longest;
  my_input[15] = (double) (!!f_has_petit);
  my_input[16] = (double) (!!f_has_excuse);
  my_input[17] = (double) (!!f_has_twentyone);
  my_input[18] = (double) f_n_trumps;
  my_input[19] = (double) f_max_trump;
  my_input[20] = (double) f_min_trump;
  my_input[21] = (double) f_q1_trumps;
  my_input[22] = (double) f_q3_trumps;
  my_input[23] = (double) f_median_trumps;
  my_input[24] = (double) (!!f_team_attack);
  my_input[25] = (double) (!!f_team_defence);
  my_input[26] = (double) f_n_trick_players_already_played;
  my_input[27] = (double) f_n_allies_after_last_opponent;
  my_input[28] = (double) (!!f_play_called_suit);
  my_input[29] = (double) f_play_n_times_lead;
  my_input[30] = (double) f_play_suit_remaining;
  my_input[31] = (double) f_play_suit_remaining_points;
  my_input[32] = (double) (!!f_ally_may_be_minor_master);
  my_input[33] = (double) (!!f_ennemy_may_be_trump_master);
  my_input[34] = (double) f_n_halfpoints_in_trick;
  my_input[35] = (double) f_n_oudlers_in_trick;
  for (i = 0; i < max && i + start < sizeof (my_input) / sizeof (my_input[0]);
       i++)
    {
      input[i] = my_input[i + start];
    }
  return sizeof (my_input) / sizeof (my_input[0]);
}

static inline size_t
perceptron_encode_action (const TarotGame * state,
                          const TarotGameEvent * action, size_t start,
                          size_t max, double *input)
{
  double my_input[27];
  size_t i;
  TarotBid s_bid;
  int s_outbid;
  int s_call_shortest;
  int s_call_second_shortest;
  int s_call_second_longest;
  int s_call_longest;
  int s_call_myself;
  int s_call_marriage;
  size_t s_discard_queens;
  size_t s_discard_knights;
  size_t s_discard_jacks;
  size_t s_discard_junk;
  size_t s_discard_shortest;
  size_t s_discard_second_shortest;
  size_t s_discard_second_longest;
  size_t s_discard_longest;
  int s_lead_called_suit;
  size_t s_lead_suit_length;
  size_t s_lead_n_times_lead;
  int s_lead_trumps;
  size_t s_n_smaller_kept;
  size_t s_n_greater_nonface_kept;
  size_t s_n_points_kept;
  size_t s_n_greater_kept;
  int s_play_excuse;
  int s_trump_master;
  int s_trump_obvious_master;
  size_t s_halfpoints;
  int s_oudler;
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, state);
  strategy_bid (state, action, &s_bid);
  strategy_outbid (state, action, &s_outbid);
  strategy_call_by_length (state, action, &s_call_shortest,
                           &s_call_second_shortest,
                           &s_call_second_longest, &s_call_longest);
  strategy_call_myself (state, action, &s_call_myself);
  strategy_call_marriage (state, action, &s_call_marriage);
  strategy_discard_save (state, action, &s_discard_queens,
                         &s_discard_knights, &s_discard_jacks);
  strategy_discard_junk (state, action, &s_discard_junk);
  strategy_discard_by_length (state, action, &s_discard_shortest,
                              &s_discard_second_shortest,
                              &s_discard_second_longest, &s_discard_longest);
  strategy_lead (state, &counter, action, &s_lead_called_suit,
                 &s_lead_suit_length, &s_lead_n_times_lead, &s_lead_trumps);
  strategy_purity (state, action, &s_n_smaller_kept,
                   &s_n_greater_nonface_kept, &s_n_points_kept,
                   &s_n_greater_kept);
  strategy_excuse (state, action, &s_play_excuse);
  strategy_trump_master (state, &counter, action, &s_trump_master,
                         &s_trump_obvious_master);
  strategy_play_points (state, action, &s_halfpoints, &s_oudler);
  my_input[0] = (double) (s_bid - TAROT_PASS);
  my_input[1] = (double) s_outbid;
  my_input[2] = (double) (!!s_call_shortest);
  my_input[3] = (double) (!!s_call_second_shortest);
  my_input[4] = (double) (!!s_call_second_longest);
  my_input[5] = (double) (!!s_call_longest);
  my_input[6] = (double) (!!s_call_myself);
  my_input[7] = (double) (!!s_call_marriage);
  my_input[8] = (double) s_discard_queens;
  my_input[9] = (double) s_discard_knights;
  my_input[10] = (double) s_discard_jacks;
  my_input[11] = (double) s_discard_junk;
  my_input[12] = (double) s_discard_shortest;
  my_input[13] = (double) s_discard_second_shortest;
  my_input[14] = (double) s_discard_second_longest;
  my_input[15] = (double) s_discard_longest;
  my_input[16] = (double) (!!s_lead_called_suit);
  my_input[17] = (double) s_lead_suit_length;
  my_input[18] = (double) s_lead_n_times_lead;
  my_input[19] = (double) (!!s_lead_trumps);
  my_input[20] = (double) s_n_smaller_kept;
  my_input[21] = (double) s_n_greater_nonface_kept;
  my_input[22] = (double) (!!s_play_excuse);
  my_input[23] = (double) (!!s_trump_master);
  my_input[24] = (double) (!!s_trump_obvious_master);
  my_input[25] = (double) s_halfpoints;
  my_input[26] = (double) (!!s_oudler);
  for (i = 0; i < max && i + start < sizeof (my_input) / sizeof (my_input[0]);
       i++)
    {
      input[i] = my_input[i + start];
    }
  return sizeof (my_input) / sizeof (my_input[0]);
}

static inline double
perceptron_learn (TarotPerceptron * perceptron, const TarotGame * state,
                  const TarotGameEvent * action, double score)
{
  double input[PERCEPTRON_DIM_INPUT];
  if (perceptron_encode_base (state, 0, PERCEPTRON_DIM_INPUT_BASE, input) !=
      PERCEPTRON_DIM_INPUT_BASE)
    {
      assert (0);
    }
  if (perceptron_encode_action
      (state, action, 0, PERCEPTRON_DIM_INPUT_ACTION,
       &(input[PERCEPTRON_DIM_INPUT_BASE])) != PERCEPTRON_DIM_INPUT_ACTION)
    {
      assert (0);
    }
  return julien_learn (perceptron->jl, PERCEPTRON_DIM_INPUT, input, 1, &score,
                       perceptron->learning_rate);
}

static void
perceptron_eval (TarotPerceptron * perceptron, const TarotGame * state,
                 size_t n_candidates, TarotGameEvent ** candidates,
                 size_t start, size_t max, double *scores)
{
  double input[PERCEPTRON_DIM_INPUT];
  double output[1];
  size_t i;
  if (perceptron_encode_base (state, 0, PERCEPTRON_DIM_INPUT_BASE, input) !=
      PERCEPTRON_DIM_INPUT_BASE)
    {
      assert (0);
    }
  for (i = 0; i < max && i + start < n_candidates; i++)
    {
      if (perceptron_encode_action
          (state, candidates[i + start], 0, PERCEPTRON_DIM_INPUT_ACTION,
           &(input[PERCEPTRON_DIM_INPUT_BASE])) !=
          PERCEPTRON_DIM_INPUT_ACTION)
        {
          assert (0);
        }
      if (julien_predict
          (perceptron->jl, PERCEPTRON_DIM_INPUT, input, 1, 0, output) != 1)
        {
          assert (0);
        }
      scores[i] = output[0];
    }
}

#include <tarot/features_private_impl.h>
#include <tarot/counter_private_impl.h>
#include <tarot/game_event_private_impl.h>
#include <tarot/cards_private_impl.h>

#endif /* H_TAROT_PERCEPTRON_PRIVATE_IMPL_INCLUDED */
