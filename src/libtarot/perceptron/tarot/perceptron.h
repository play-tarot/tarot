/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PERCEPTRON_INCLUDED
#define H_TAROT_PERCEPTRON_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

  struct TarotGame;
  struct TarotPerceptron;
  typedef struct TarotPerceptron TarotPerceptron;

  /**
   * tarot_perceptron_alloc:
   * @hidden_sizes: (array length=n_hidden_layers):
   */
  TarotPerceptron *tarot_perceptron_alloc (size_t n_hidden_layers,
                                           const size_t *hidden_sizes,
                                           double learning_rate);

  /**
   * tarot_perceptron_static_default: (constructor):
   */
  TarotPerceptron *tarot_perceptron_static_default ();

  /**
   * tarot_perceptron_eval:
   * @candidates: (array length=n_candidates):
   * @scores: (array length=max):
   */
  void tarot_perceptron_eval (TarotPerceptron * perceptron,
                              const TarotGame * base, size_t n_candidates,
                              TarotGameEvent ** candidates, size_t start,
                              size_t max, double *scores);

  /**
   * tarot_perceptron_learn:
   */
  void tarot_perceptron_learn (TarotPerceptron * perceptron,
                               const TarotGame * base,
                               const TarotGameEvent * event,
                               double final_score);

  TarotPerceptron *tarot_perceptron_dup (const TarotPerceptron * perceptron);

  /**
   * tarot_perceptron_free:
   */
  void tarot_perceptron_free (TarotPerceptron * perceptron);

  void tarot_perceptron_set_learning_rate (TarotPerceptron * perceptron,
                                           double learning_rate);

  /**
   * tarot_perceptron_load:
   * @parameters: (array length=n_weights):
   */
  void tarot_perceptron_load (TarotPerceptron * perceptron, size_t start,
                              size_t n_weights, const double *parameters);

  /**
   * tarot_perceptron_save:
   * @parameters: (array length=max):
   */
  size_t tarot_perceptron_save (const TarotPerceptron * perceptron,
                                size_t start, size_t max, double *parameters);

  /**
   * tarot_perceptron_save_alloc:
   * @data: (array length=n) (out):
   */
  void tarot_perceptron_save_alloc (const TarotPerceptron * perceptron,
                                    size_t *n, double **data);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_PERCEPTRON_INCLUDED */
