/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PERCEPTRON_PRIVATE_INCLUDED
#define H_TAROT_PERCEPTRON_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void perceptron_construct (TarotPerceptron * perceptron,
                                    size_t n_hidden_layers,
                                    const size_t *hidden_sizes,
                                    double learning_rate);
  static void perceptron_construct_static_default (TarotPerceptron *
                                                   perceptron);
  static void perceptron_destruct (TarotPerceptron * perceptron);
  static void perceptron_copy (TarotPerceptron * dest,
                               const TarotPerceptron * source);

  static void perceptron_set_learning_rate (TarotPerceptron * perceptron,
                                            double learning_rate);

  static void perceptron_load (TarotPerceptron * perceptron, size_t start,
                               size_t n, const double *parameters);

  static size_t perceptron_save (const TarotPerceptron * perceptron,
                                 size_t start, size_t max,
                                 double *parameters);

  static size_t perceptron_encode_base (const TarotGame * base,
                                        size_t start,
                                        size_t max, double *input);

  static size_t perceptron_encode_action (const TarotGame * base,
                                          const TarotGameEvent * action,
                                          size_t start,
                                          size_t max, double *input);

  static double perceptron_learn (TarotPerceptron * perceptron,
                                  const TarotGame * state,
                                  const TarotGameEvent * action,
                                  double score);

  static void perceptron_eval (TarotPerceptron * perceptron,
                               const TarotGame * state,
                               size_t n_candidates,
                               TarotGameEvent ** candidates,
                               size_t start, size_t max, double *scores);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_PERCEPTRON_PRIVATE_INCLUDED */
