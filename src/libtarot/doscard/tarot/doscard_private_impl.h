/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tarot/doscard_private.h>

static inline void
doscard_3_generalize (TarotDoscard34 * doscard, TarotDoscardHandle * gen)
{
  gen->n_players = 3;
  gen->doscard = &(doscard->d);
  gen->dog.n_max = sizeof (doscard->dog.buffer) / sizeof (TarotCard);
  gen->dog.n = &(doscard->dog.n);
  gen->dog.buffer = doscard->dog.buffer;
  gen->discard.n_max = sizeof (doscard->discard.buffer) / sizeof (TarotCard);
  gen->discard.n = &(doscard->discard.n);
  gen->discard.buffer = doscard->discard.buffer;
}

static inline void
doscard_4_generalize (TarotDoscard34 * doscard, TarotDoscardHandle * gen)
{
  gen->n_players = 4;
  gen->doscard = &(doscard->d);
  gen->dog.n_max = sizeof (doscard->dog.buffer) / sizeof (TarotCard);
  gen->dog.n = &(doscard->dog.n);
  gen->dog.buffer = doscard->dog.buffer;
  gen->discard.n_max = sizeof (doscard->discard.buffer) / sizeof (TarotCard);
  gen->discard.n = &(doscard->discard.n);
  gen->discard.buffer = doscard->discard.buffer;
}

static inline void
doscard_5_generalize (TarotDoscard5 * doscard, TarotDoscardHandle * gen)
{
  gen->n_players = 5;
  gen->doscard = &(doscard->d);
  gen->dog.n_max = sizeof (doscard->dog.buffer) / sizeof (TarotCard);
  gen->dog.n = &(doscard->dog.n);
  gen->dog.buffer = doscard->dog.buffer;
  gen->discard.n_max = sizeof (doscard->discard.buffer) / sizeof (TarotCard);
  gen->discard.n = &(doscard->discard.n);
  gen->discard.buffer = doscard->discard.buffer;
}

static inline int
_doscard_get_has_started (const TarotDoscardHandle * doscard)
{
  return doscard->doscard->has_started;
}

static inline void
_doscard_set_has_started (TarotDoscardHandle * doscard, int value)
{
  doscard->doscard->has_started = value;
}

static inline TarotPlayer
_doscard_get_taker (const TarotDoscardHandle * doscard)
{
  return doscard->doscard->taker;
}

static inline void
_doscard_set_taker (TarotDoscardHandle * doscard, TarotPlayer value)
{
  doscard->doscard->taker = value;
}

static inline TarotBid
_doscard_get_contract (const TarotDoscardHandle * doscard)
{
  return doscard->doscard->contract;
}

static inline void
_doscard_set_contract (TarotDoscardHandle * doscard, TarotBid value)
{
  doscard->doscard->contract = value;
}

static inline int
_doscard_get_dog_done (const TarotDoscardHandle * doscard)
{
  return doscard->doscard->dog_done;
}

static inline void
_doscard_set_dog_done (TarotDoscardHandle * doscard, int value)
{
  doscard->doscard->dog_done = value;
}

static inline int
_doscard_get_discard_done (const TarotDoscardHandle * doscard)
{
  return doscard->doscard->discard_done;
}

static inline void
_doscard_set_discard_done (TarotDoscardHandle * doscard, int value)
{
  doscard->doscard->discard_done = value;
}

static inline void
doscard_initialize (TarotDoscardHandle * doscard, size_t n_players)
{
  doscard->n_players = n_players;
  _doscard_set_has_started (doscard, 0);
  _doscard_set_taker (doscard, 0);
  _doscard_set_contract (doscard, TAROT_PASS);
  _doscard_set_dog_done (doscard, 0);
  hand_set (&(doscard->dog), NULL, 0);
  _doscard_set_discard_done (doscard, 0);
  hand_set (&(doscard->discard), NULL, 0);
}

static inline int
doscard_copy (TarotDoscardHandle * dest, const TarotDoscardHandle * source)
{
  assert (dest->n_players == source->n_players);
  _doscard_set_has_started (dest, _doscard_get_has_started (source));
  _doscard_set_taker (dest, _doscard_get_taker (source));
  _doscard_set_contract (dest, _doscard_get_contract (source));
  _doscard_set_dog_done (dest, _doscard_get_dog_done (source));
  if (hand_copy (&(dest->dog), &(source->dog)) != 0)
    {
      assert (0);
    }
  _doscard_set_discard_done (dest, _doscard_get_discard_done (source));
  if (hand_copy (&(dest->discard), &(source->discard)) != 0)
    {
      assert (0);
    }
  return 0;
}

static inline int
doscard_start (TarotDoscardHandle * doscard)
{
  int error = _doscard_get_has_started (doscard);
  if (error == 0)
    {
      _doscard_set_has_started (doscard, 1);
      _doscard_set_dog_done (doscard, 0);
      _doscard_set_discard_done (doscard, 0);
    }
  return error;
}

static inline TarotBid
doscard_contract (const TarotDoscardHandle * doscard)
{
  return _doscard_get_contract (doscard);
}

static inline int
doscard_set_contract (TarotDoscardHandle * doscard, TarotBid contract)
{
  int error = (_doscard_get_has_started (doscard));
  if (error == 0)
    {
      _doscard_set_contract (doscard, contract);
    }
  return error;
}

static inline TarotPlayer
doscard_taker (const TarotDoscardHandle * doscard)
{
  return _doscard_get_taker (doscard);
}

static inline int
doscard_set_taker (TarotDoscardHandle * doscard, TarotPlayer taker)
{
  int error = (_doscard_get_has_started (doscard));
  if (error == 0)
    {
      _doscard_set_taker (doscard, taker);
    }
  return error;
}

static inline int
doscard_expects_dog (const TarotDoscardHandle * doscard)
{
  return (_doscard_get_has_started (doscard)
          && _doscard_get_contract (doscard) < TAROT_STRAIGHT_KEEP
          && !_doscard_get_dog_done (doscard));
}

static inline int
doscard_expects_discard (const TarotDoscardHandle * doscard)
{
  return (_doscard_get_has_started (doscard)
          && _doscard_get_contract (doscard) < TAROT_STRAIGHT_KEEP
          && _doscard_get_dog_done (doscard)
          && !_doscard_get_discard_done (doscard));
}

static inline int
doscard_check_dog (const TarotDoscardHandle * doscard, const TarotHand * dog)
{
  return (doscard_expects_dog (doscard)
          && hand_size (dog) == hand_max (&(doscard->dog)));
}

static inline int
doscard_all_non_oudler_trumps (const TarotHand * hand)
{
  size_t i;
  for (i = 0; i < *(hand->n); i++)
    {
      if (hand->buffer[i] <= TAROT_PETIT
          || hand->buffer[i] >= TAROT_TWENTYONE)
        {
          return 0;
        }
    }
  return 1;
}

static inline size_t
doscard_size (const TarotDoscardHandle * doscard)
{
  return hand_max (&(doscard->dog));
}

static inline int
doscard_check_partial_discard (const TarotDoscardHandle * doscard,
                               TarotPlayer player, const TarotHand * discard)
{
  return (doscard_expects_discard (doscard)
          && player == _doscard_get_taker (doscard)
          && (hand_size (discard) <=
              doscard_size (doscard))
          && doscard_all_non_oudler_trumps (discard));
}

static inline int
doscard_check_discard (const TarotDoscardHandle * doscard, TarotPlayer player,
                       const TarotHand * discard)
{
  return (doscard_expects_discard (doscard)
          && player == _doscard_get_taker (doscard)
          && (hand_size (discard) == doscard_size (doscard)));
}

static inline int
doscard_can_autoreveal (const TarotDoscardHandle * doscard, TarotHand * dog)
{
  if (doscard_expects_dog (doscard)
      && hand_size (&(doscard->dog)) == doscard_size (doscard))
    {
      return (hand_copy (dog, &(doscard->dog)) == 0);
    }
  return 0;
}

static inline int
doscard_reveal_dog (TarotDoscardHandle * doscard, const TarotHand * dog)
{
  int error = 0;
  if (doscard_check_dog (doscard, dog))
    {
      error = hand_copy (&(doscard->dog), dog);
      (void) error;
      assert (error == 0);
      _doscard_set_dog_done (doscard, 1);
      error = 0;
    }
  else
    {
      error = 1;
    }
  return error;
}

static inline int
doscard_set_partial_discard (TarotDoscardHandle * doscard,
                             const TarotHand * discard)
{
  TarotPlayer taker = _doscard_get_taker (doscard);
  int error = 0;
  if (doscard_check_partial_discard (doscard, taker, discard))
    {
      error = hand_copy (&(doscard->discard), discard);
      (void) error;
      assert (error == 0);
      _doscard_set_discard_done (doscard, 1);
      error = 0;
    }
  else
    {
      error = 1;
    }
  return error;
}

static inline int
doscard_set_discard (TarotDoscardHandle * doscard, const TarotHand * discard)
{
  TarotPlayer taker = _doscard_get_taker (doscard);
  int error = 0;
  if (doscard_check_discard (doscard, taker, discard))
    {
      error = hand_copy (&(doscard->discard), discard);
      (void) error;
      assert (error == 0);
      _doscard_set_discard_done (doscard, 1);
    }
  else
    {
      error = 1;
    }
  return error;
}

static inline int
doscard_add_unplayed_card (TarotDoscardHandle * doscard, TarotCard card)
{
  if (_doscard_get_discard_done (doscard)
      || _doscard_get_contract (doscard) >= TAROT_STRAIGHT_KEEP)
    {
      /* The unplayed card is for the discard */
      if (hand_insert (&(doscard->discard), card) != 0)
        {
          assert (0);
        }
      if (_doscard_get_contract (doscard) >= TAROT_STRAIGHT_KEEP)
        {
          /* And a fortiori for the dog */
          if (hand_insert (&(doscard->dog), card) != 0)
            {
              assert (0);
            }
        }
    }
  else
    {
      /* The card is just for the dog */
      if (hand_insert (&(doscard->dog), card) != 0)
        {
          assert (0);
        }
    }
  return 0;
}

static inline int
doscard_infer (TarotDoscardHandle * doscard, const TarotTricksHandle * tricks)
{
  if (!_doscard_get_has_started (doscard))
    {
      return 1;
    }
  if (hand_size (&(doscard->discard)) == doscard_size (doscard))
    {
      return 0;
    }
  if (hand_size (&(doscard->discard)) < doscard_size (doscard))
    {
      int seen[78];
      TarotCard i;
      size_t i_trick, n_tricks, current;
      size_t i_card;
      n_tricks = tricks_size (tricks);
      current = tricks_current (tricks);
      if (current != n_tricks)
        {
          return 1;
        }
      for (i = 0; i < 78; ++i)
        {
          seen[i] = 0;
        }
      for (i_trick = 0; i_trick < n_tricks; ++i_trick)
        {
          TarotTrickHandle *trick =
            (TarotTrickHandle *) & (tricks->tricks[i_trick]);
          assert (doscard->n_players == trick_n_cards (trick));
          for (i_card = 0; i_card < doscard->n_players; ++i_card)
            {
              i = trick_get_card (trick, i_card);
              if (seen[i] != 0)
                {
                  assert (0);
                }
              seen[i] = 1;
            }
        }
      hand_set (&(doscard->discard), NULL, 0);
      for (i = 0; i < 78; ++i)
        {
          if (!seen[i])
            {
              int error = 0;
              error = doscard_add_unplayed_card (doscard, i);
              (void) error;
              assert (error == 0);
            }
        }
      if (_doscard_get_contract (doscard) >= TAROT_STRAIGHT_KEEP)
        {
          assert (*(doscard->dog.n) == *(doscard->discard.n));
          assert (memcmp
                  (doscard->dog.buffer, doscard->discard.buffer,
                   *(doscard->dog.n) * sizeof (TarotCard)) == 0);
        }
      else
        {
          assert (_doscard_get_discard_done (doscard));
        }
      return 0;
    }
  return 1;
}

static inline int
doscard_dog_known (const TarotDoscardHandle * doscard)
{
  return _doscard_get_has_started (doscard)
    && _doscard_get_dog_done (doscard);
}

static inline int
doscard_discard_known (const TarotDoscardHandle * doscard)
{
  return (_doscard_get_has_started (doscard)
          && _doscard_get_discard_done (doscard));
}

static inline int
doscard_dog (const TarotDoscardHandle * doscard, TarotHand * dest)
{
  return hand_copy (dest, &(doscard->dog));
}

static inline int
doscard_discard (const TarotDoscardHandle * doscard, TarotHand * dest)
{
  return hand_copy (dest, &(doscard->discard));
}

static inline int
doscard_discard_public (const TarotDoscardHandle * doscard, TarotHand * dest)
{
  const TarotCard *buffer;
  size_t i;
  size_t n;
  int error = 0;
  n = *(doscard->discard.n);
  buffer = doscard->discard.buffer;
  hand_set (dest, NULL, 0);
  for (i = 0; i < n; ++i)
    {
      if (buffer[i] > TAROT_PETIT && buffer[i] < TAROT_TWENTYONE)
        {
          error = error || hand_insert (dest, buffer[i]);
        }
    }
  return error;
}

static inline int
doscard_points (const TarotDoscardHandle * doscard)
{
  return hand_points (&(doscard->discard));
}

static inline int
doscard_oudlers (const TarotDoscardHandle * doscard)
{
  return hand_oudlers (&(doscard->discard));
}
