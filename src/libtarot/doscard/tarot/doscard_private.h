/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_DOSCARD_PRIVATE_INCLUDED
#define H_TAROT_DOSCARD_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/hand_private.h>
#include <tarot/player.h>
#include <tarot/cards.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void doscard_3_generalize (TarotDoscard34 * doscard,
                                    TarotDoscardHandle * gen);
  static void doscard_4_generalize (TarotDoscard34 * doscard,
                                    TarotDoscardHandle * gen);
  static void doscard_5_generalize (TarotDoscard5 * doscard,
                                    TarotDoscardHandle * gen);
  static void doscard_initialize (TarotDoscardHandle * doscard,
                                  size_t n_players);
  static int doscard_copy (TarotDoscardHandle * dest,
                           const TarotDoscardHandle * source);
  static TarotBid doscard_contract (const TarotDoscardHandle * doscard);
  static int doscard_set_contract (TarotDoscardHandle * doscard,
                                   TarotBid contract);
  static TarotPlayer doscard_taker (const TarotDoscardHandle * doscard);
  static int doscard_set_taker (TarotDoscardHandle * doscard,
                                TarotPlayer taker);
  static int doscard_start (TarotDoscardHandle * doscard);
  static int doscard_expects_dog (const TarotDoscardHandle * doscard);
  static int doscard_expects_discard (const TarotDoscardHandle * doscard);
  static int doscard_check_dog (const TarotDoscardHandle * doscard,
                                const TarotHand * dog);
  static int doscard_check_partial_discard (const TarotDoscardHandle *
                                            doscard, TarotPlayer player,
                                            const TarotHand * discard);
  static int doscard_check_discard (const TarotDoscardHandle * doscard,
                                    TarotPlayer player,
                                    const TarotHand * discard);
  /* The dog is not done, but we know its cards since we had a full
     deal. */
  static int doscard_can_autoreveal (const TarotDoscardHandle * doscard,
                                     TarotHand * dog);
  static int doscard_reveal_dog (TarotDoscardHandle * doscard,
                                 const TarotHand * dog);
  static int doscard_set_partial_discard (TarotDoscardHandle * doscard,
                                          const TarotHand * discard);
  static int doscard_set_discard (TarotDoscardHandle * doscard,
                                  const TarotHand * discard);
  static int doscard_add_unplayed_card (TarotDoscardHandle * doscard,
                                        TarotCard card);
  static int doscard_infer (TarotDoscardHandle * doscard,
                            const TarotTricksHandle * tricks);
  static int doscard_dog_known (const TarotDoscardHandle * doscard);

  /* In fact, whether the discard is DONE, not fully known */
  static int doscard_discard_known (const TarotDoscardHandle * doscard);
  static int doscard_dog (const TarotDoscardHandle * doscard,
                          TarotHand * dest);
  static int doscard_discard (const TarotDoscardHandle * doscard,
                              TarotHand * dest);
  static int doscard_discard_public (const TarotDoscardHandle * doscard,
                                     TarotHand * dest);
  static int doscard_points (const TarotDoscardHandle * doscard);
  static int doscard_oudlers (const TarotDoscardHandle * doscard);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_DOSCARD_PRIVATE_INCLUDED */
