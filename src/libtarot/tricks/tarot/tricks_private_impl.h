/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tarot/tricks_private.h>

#ifndef H_TAROT_TRICKS_PRIVATE_IMPL_INCLUDED
#define H_TAROT_TRICKS_PRIVATE_IMPL_INCLUDED

static inline size_t
tricks_get_i_current (const TarotTricksHandle * tricks)
{
  return *(tricks->i_current);
}

static inline void
tricks_set_i_current (TarotTricksHandle * tricks, size_t value)
{
  *(tricks->i_current) = value;
}

static inline void
tricks_3_generalize (TarotTricks3 * tricks, TarotTricksHandle * gen)
{
  size_t i;
  gen->n_players = 3;
  gen->n_tricks = 24;
  gen->i_current = &(tricks->i_current);
  for (i = 0; i < 24; i++)
    {
      trick_3_generalize (&(tricks->tricks[i]), &(gen->tricks[i]));
    }
}

static inline void
tricks_4_generalize (TarotTricks4 * tricks, TarotTricksHandle * gen)
{
  size_t i;
  gen->n_players = 4;
  gen->n_tricks = 18;
  gen->i_current = &(tricks->i_current);
  for (i = 0; i < 18; i++)
    {
      trick_4_generalize (&(tricks->tricks[i]), &(gen->tricks[i]));
    }
}

static inline void
tricks_5_generalize (TarotTricks5 * tricks, TarotTricksHandle * gen)
{
  size_t i;
  gen->n_players = 5;
  gen->n_tricks = 15;
  gen->i_current = &(tricks->i_current);
  for (i = 0; i < 15; i++)
    {
      trick_5_generalize (&(tricks->tricks[i]), &(gen->tricks[i]));
    }
}

static inline void
tricks_initialize (TarotTricksHandle * tricks, size_t n_players)
{
  size_t i = 0;
  size_t n;
  switch (n_players)
    {
    case 3:
      n = 24;
      break;
    case 4:
      n = 18;
      break;
    case 5:
      n = 15;
      break;
    default:
      /* Invalid number of players */
      assert (0);
      break;
    }
  for (i = 0; i < n; ++i)
    {
      trick_initialize (&(tricks->tricks[i]), n_players);
    }
  trick_set_last (&(tricks->tricks[n - 1]), 1);
  tricks->n_players = n_players;
  tricks_set_i_current (tricks, 0);
}

static inline int
tricks_copy (TarotTricksHandle * dest, const TarotTricksHandle * source)
{
  size_t i;
  assert (dest->n_players == source->n_players);
  dest->n_tricks = source->n_tricks;
  tricks_set_i_current (dest, tricks_current (source));
  for (i = 0; i < source->n_tricks; ++i)
    {
      if (trick_copy (&(dest->tricks[i]), &(source->tricks[i])) != 0)
        {
          assert (0);
        }
    }
  return 0;
}

static inline size_t
tricks_size (const TarotTricksHandle * tricks)
{
  return tricks->n_tricks;
}

static inline size_t
tricks_current (const TarotTricksHandle * tricks)
{
  return tricks_get_i_current (tricks);
}

static inline size_t
tricks_count_remaining_cards (const TarotTricksHandle * tricks,
                              TarotPlayer who)
{
  size_t current = tricks_current (tricks);
  size_t max = tricks->n_tricks;
  if (current == max)
    {
      return 0;
    }
  if (trick_has_played (&(tricks->tricks[current]), who))
    {
      return max - current - 1;
    }
  else
    {
      return max - current;
    }
  assert (0);
  return 0;
}

static inline int
tricks_has_next (const TarotTricksHandle * tricks)
{
  return (tricks_get_i_current (tricks) < tricks->n_tricks
          &&
          (trick_n_cards
           (&(tricks->tricks[tricks_get_i_current (tricks)])) <
           tricks->n_players));
}

static inline TarotPlayer
tricks_next (const TarotTricksHandle * tricks)
{
  return trick_next (&(tricks->tricks[tricks_get_i_current (tricks)]));
}

static inline int
tricks_points (const TarotTricksHandle * tricks, TarotPlayer taker)
{
  int ret = 0;
  size_t i = 0;
  size_t i_current = tricks_get_i_current (tricks);
  for (i = 0; i < i_current; ++i)
    {
      if (trick_taker (&(tricks->tricks[i])) == taker)
        {
          ret += trick_points (&(tricks->tricks[i]));
        }
    }
  return ret;
}

static inline int
tricks_oudlers (const TarotTricksHandle * tricks, TarotPlayer taker)
{
  int ret = 0;
  size_t i = 0;
  size_t i_current = tricks_get_i_current (tricks);
  for (i = 0; i < i_current; ++i)
    {
      if (trick_taker (&(tricks->tricks[i])) == taker)
        {
          ret += trick_oudlers (&(tricks->tricks[i]));
        }
    }
  return ret;
}

static inline int
tricks_is_in_team (TarotPlayer taker, TarotPlayer partner, TarotPlayer needle)
{
  return (needle == taker || needle == partner);
}

static inline int
tricks_pab_check_normal (const TarotTricksHandle * tricks, TarotPlayer taker,
                         TarotPlayer partner)
{
  const TarotTrickHandle *last = &(tricks->tricks[tricks->n_tricks - 1]);
  int has_petit = trick_has_petit (last);
  TarotPlayer last_taker = trick_taker (last);
  int taker_is_attacker = tricks_is_in_team (taker, partner, last_taker);
  if (has_petit && taker_is_attacker)
    {
      return +1;
    }
  else if (has_petit)
    {
      return -1;
    }
  return 0;
}

static inline TarotPlayer
tricks_owner_of_petit_in_penultimate (const TarotTricksHandle * tricks)
{
  const TarotTrickHandle *trick = &(tricks->tricks[tricks->n_tricks - 2]);
  size_t pos = 0;
  int error = 0;
  error = trick_locate (trick, TAROT_PETIT, &pos);
  assert (error == 0);
  (void) error;
  return trick_get_player (trick, pos);
}

static inline int
  tricks_owner_of_petit_in_penultimate_has_played_excuse_in_last
  (const TarotTricksHandle * tricks)
{
  const TarotTrickHandle *last = &(tricks->tricks[tricks->n_tricks - 1]);
  TarotPlayer oopip = tricks_owner_of_petit_in_penultimate (tricks);
  TarotCard card_of_oopip = trick_get_card_of (last, oopip);
  return card_of_oopip == TAROT_EXCUSE;
}

static inline int
tricks_pab (const TarotTricksHandle * tricks, TarotPlayer taker,
            TarotPlayer partner)
{
  const TarotTrickHandle *last, *penultimate;
  assert (tricks_get_i_current (tricks) >= tricks->n_tricks);
  assert (tricks->n_tricks >= 2);
  last = &(tricks->tricks[tricks->n_tricks - 1]);
  penultimate = &(tricks->tricks[tricks->n_tricks - 2]);
  if (trick_taken_by_excuse (last)
      && trick_has_petit (penultimate)
      &&
      tricks_owner_of_petit_in_penultimate_has_played_excuse_in_last (tricks))
    {
      TarotPlayer slammer = trick_taker (last);
      if (tricks_is_in_team (taker, partner, slammer))
        {
          return +1;
        }
      else
        {
          return -1;
        }
    }
  return tricks_pab_check_normal (tricks, taker, partner);
}

static inline int
tricks_slam (const TarotTricksHandle * tricks, TarotPlayer taker,
             TarotPlayer partner)
{
  size_t i = 0;
  int attack = 1, defence = 1;
  for (i = 0; i < tricks_get_i_current (tricks); ++i)
    {
      if (tricks_is_in_team
          (taker, partner, trick_taker (&(tricks->tricks[i]))))
        {
          defence = 0;
        }
      else
        {
          attack = 0;
        }
    }
  return attack - defence;
}

static inline int
tricks_locate (const TarotTricksHandle * tricks, TarotCard needle,
               size_t *i_trick, size_t *position)
{
  int error = 0;
  size_t i;
  size_t i_current = tricks_get_i_current (tricks);
  for (i = 0; i <= i_current && i < tricks->n_tricks; ++i)
    {
      error = trick_locate (&(tricks->tricks[i]), needle, position);
      if (error == 0)
        {
          *i_trick = i;
          return 0;
        }
    }
  return 1;
}

static inline int
tricks_set_leader (TarotTricksHandle * tricks, TarotPlayer leader)
{
  return trick_set_leader (&(tricks->tricks[0]), leader);
}

static inline int
tricks_next_is_slamming (const TarotTricksHandle * tricks, TarotPlayer taker,
                         TarotPlayer partner)
{
  TarotPlayer next = tricks_next (tricks);
  int slam_team;
  slam_team = tricks_slam (tricks, taker, partner);
  if (tricks_is_in_team (taker, partner, next))
    {
      return slam_team > 0;
    }
  else
    {
      return slam_team < 0;
    }
  return 0;
}

int
tricks_add (TarotTricksHandle * tricks, TarotPlayer taker,
            TarotPlayer partner, TarotCard card)
{
  int force = 0;
  int error = 0;
  TarotTrickHandle *current;
  size_t i_current = tricks_get_i_current (tricks);
  if (card == TAROT_EXCUSE && i_current + 1 == tricks->n_tricks
      && tricks_next_is_slamming (tricks, taker, partner))
    {
      force = 1;
    }
  current = &(tricks->tricks[i_current]);
  error = trick_add (current, card, force);
  if (error == 0 && trick_n_cards (current) == tricks->n_players)
    {
      if (i_current + 1 < tricks->n_tricks)
        {
          TarotPlayer taker = trick_taker (current);
          error = trick_set_leader (&(tricks->tricks[i_current + 1]), taker);
        }
      tricks_set_i_current (tricks, i_current + 1);
    }
  return error;
}

#endif /* not H_TAROT_TRICKS_PRIVATE_IMPL_INCLUDED */
