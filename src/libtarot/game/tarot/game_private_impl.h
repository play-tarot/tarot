/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020, 2024  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_PRIVATE_IMPL_INCLUDED
#define H_TAROT_GAME_PRIVATE_IMPL_INCLUDED

#include <tarot/game_private.h>
#include <tarot/cards_private.h>
#include <tarot/game_event_private.h>
#include <tarot/hands_private.h>
#include <tarot/bids_private.h>
#include <tarot/decls_private.h>
#include <tarot/call_private.h>
#include <tarot/doscard_private.h>
#include <tarot/handfuls_private.h>
#include <tarot/tricks_private.h>
#include <tarot/trick_private.h>
#include <tarot/counter_private.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct TarotGameHandle
{
  TarotGameOptions opt;
  TarotPlayer *dealt;
  TarotStep *step;
  TarotHandsHandle hands;
  TarotBids *bids;
  TarotDecls *decls;
  TarotCall *call;
  TarotDoscardHandle doscard;
  TarotHandfuls *handfuls;
  TarotTricksHandle tricks;
  int *has_duplicate_deal;
  TarotPlayer *duplicate_deal;
  int *has_duplicate_taker;
  TarotPlayer *duplicate_taker;
  int *has_duplicate_call;
  TarotCard *duplicate_call;
};

typedef struct TarotGameHandle TarotGameHandle;

static void
game_generalize (TarotGame * game, TarotGameHandle * handle)
{
  memcpy (&(handle->opt), &(game->opt), sizeof (TarotGameOptions));
  handle->has_duplicate_deal = &(game->has_duplicate_deal);
  handle->duplicate_deal = game->duplicate_deal;
  handle->has_duplicate_taker = &(game->has_duplicate_taker);
  handle->duplicate_taker = &(game->duplicate_taker);
  handle->has_duplicate_call = &(game->has_duplicate_call);
  handle->duplicate_call = &(game->duplicate_call);
  switch (game->opt.n_players)
    {
    case 3:
      handle->dealt = &(game->u.p3.dealt);
      handle->step = &(game->u.p3.step);
      hands_3_generalize (&(game->u.p3.hands), &(handle->hands));
      handle->bids = &(game->u.p3.bids);
      handle->decls = &(game->u.p3.decls);
      handle->call = NULL;
      doscard_3_generalize (&(game->u.p3.doscard), &(handle->doscard));
      handle->handfuls = &(game->u.p3.handfuls);
      tricks_3_generalize (&(game->u.p3.tricks), &(handle->tricks));
      break;
    case 4:
      handle->dealt = &(game->u.p4.dealt);
      handle->step = &(game->u.p4.step);
      hands_4_generalize (&(game->u.p4.hands), &(handle->hands));
      handle->bids = &(game->u.p4.bids);
      handle->decls = &(game->u.p4.decls);
      handle->call = NULL;
      doscard_4_generalize (&(game->u.p4.doscard), &(handle->doscard));
      handle->handfuls = &(game->u.p4.handfuls);
      tricks_4_generalize (&(game->u.p4.tricks), &(handle->tricks));
      break;
    case 5:
      if (game->opt.with_call)
        {
          handle->dealt = &(game->u.p5c.dealt);
          handle->step = &(game->u.p5c.step);
          hands_5_generalize (&(game->u.p5c.hands), &(handle->hands));
          handle->bids = &(game->u.p5c.bids);
          handle->decls = &(game->u.p5c.decls);
          handle->call = &(game->u.p5c.call);
          doscard_5_generalize (&(game->u.p5c.doscard), &(handle->doscard));
          handle->handfuls = &(game->u.p5c.handfuls);
          tricks_5_generalize (&(game->u.p5c.tricks), &(handle->tricks));
        }
      else
        {
          handle->dealt = &(game->u.p5.dealt);
          handle->step = &(game->u.p5.step);
          hands_5_generalize (&(game->u.p5.hands), &(handle->hands));
          handle->bids = &(game->u.p5.bids);
          handle->decls = &(game->u.p5.decls);
          handle->call = NULL;
          doscard_5_generalize (&(game->u.p5.doscard), &(handle->doscard));
          handle->handfuls = &(game->u.p5.handfuls);
          tricks_5_generalize (&(game->u.p5.tricks), &(handle->tricks));
        }
      break;
    default:
      assert (0);
    }
}

static inline int game_step_advance (TarotGame * game);

static inline void
game_set_dealt (TarotGameHandle * handle, int dealt)
{
  *(handle->dealt) = dealt;
}

static inline void
game_set_step (TarotGameHandle * handle, TarotStep step)
{
  *(handle->step) = step;
}

static inline void
game_set_duplicate_deal (TarotGameHandle * handle, size_t n,
                         const TarotPlayer * owner)
{
  size_t i;
  assert (n == 78);
  *(handle->has_duplicate_deal) = 1;
  for (i = 0; i < n; i++)
    {
      handle->duplicate_deal[i] = owner[i];
    }
}

static inline void
game_set_duplicate_taker (TarotGameHandle * handle, TarotPlayer taker)
{
  *(handle->has_duplicate_taker) = 1;
  *(handle->duplicate_taker) = taker;
}

static inline void
game_set_duplicate_call (TarotGameHandle * handle, TarotCard call)
{
  *(handle->has_duplicate_call) = 1;
  *(handle->duplicate_call) = call;
}

static inline int
game_get_dealt (TarotGameHandle * handle)
{
  return *(handle->dealt);
}

static inline TarotStep
game_get_step (const TarotGameHandle * handle)
{
  return *(handle->step);
}

/* Returns NULL if no duplicate deal */
static inline const TarotPlayer *
game_get_duplicate_deal (const TarotGameHandle * handle, size_t *n)
{
  if (*(handle->has_duplicate_deal))
    {
      *n = 78;
      return handle->duplicate_deal;
    }
  return NULL;
}

static inline const TarotPlayer *
game_get_duplicate_taker (const TarotGameHandle * handle)
{
  if (*(handle->has_duplicate_taker))
    {
      return handle->duplicate_taker;
    }
  return NULL;
}

static inline const TarotCard *
game_get_duplicate_call (const TarotGameHandle * handle)
{
  if (*(handle->has_duplicate_call))
    {
      return handle->duplicate_call;
    }
  return NULL;
}

static inline void
game_initialize (TarotGame * game, size_t n_players, int with_call)
{
  TarotGameHandle handle;
  game->opt.n_players = n_players;
  game->opt.with_call = with_call;
  game_generalize (game, &handle);
  game_set_dealt (&handle, n_players);
  game_set_step (&handle, TAROT_SETUP);
  hands_initialize (&(handle.hands), n_players);
  bids_initialize (handle.bids);
  decls_initialize (handle.decls, n_players);
  if (with_call)
    {
      call_initialize (handle.call);
    }
  doscard_initialize (&(handle.doscard), n_players);
  handfuls_initialize (handle.handfuls, n_players);
  tricks_initialize (&(handle.tricks), n_players);
  *(handle.has_duplicate_deal) = 0;
  *(handle.has_duplicate_taker) = 0;
  *(handle.has_duplicate_call) = 0;
}

static inline void
game_copy (TarotGame * dest, const TarotGame * source)
{
  TarotGameHandle hdest, hsource;
  size_t n;
  const TarotPlayer *owners;
  const TarotPlayer *taker;
  const TarotPlayer *call;
  game_initialize (dest, source->opt.n_players, source->opt.with_call);
  game_generalize (dest, &hdest);
  game_generalize ((TarotGame *) source, &hsource);
  game_set_dealt (&hdest, game_get_dealt (&hsource));
  game_set_step (&hdest, game_get_step (&hsource));
  hands_copy (&(hdest.hands), &(hsource.hands));
  if (bids_copy (hdest.bids, hsource.bids) != 0)
    {
      assert (0);
    }
  if (decls_copy (hdest.decls, hsource.decls) != 0)
    {
      assert (0);
    }
  if (hsource.opt.with_call)
    {
      if (call_copy (hdest.call, hsource.call) != 0)
        {
          assert (0);
        }
    }
  if (doscard_copy (&(hdest.doscard), &(hsource.doscard)) != 0)
    {
      assert (0);
    }
  if (handfuls_copy (hdest.handfuls, hsource.handfuls) != 0)
    {
      assert (0);
    }
  if (tricks_copy (&(hdest.tricks), &(hsource.tricks)) != 0)
    {
      assert (0);
    }
  if ((owners = game_get_duplicate_deal (&hsource, &n)) != NULL)
    {
      assert (n == 78);
      game_set_duplicate_deal (&hdest, n, owners);
    }
  else
    {
      *(hdest.has_duplicate_deal) = 0;
    }
  if ((taker = game_get_duplicate_taker (&hsource)) != NULL)
    {
      game_set_duplicate_taker (&hdest, *taker);
    }
  else
    {
      *(hdest.has_duplicate_taker) = 0;
    }
  if ((call = game_get_duplicate_call (&hsource)) != NULL)
    {
      game_set_duplicate_call (&hdest, *call);
    }
  else
    {
      *(hdest.has_duplicate_call) = 0;
    }
}

static inline TarotGameError
game_copy_as (TarotGame * dest, const TarotGame * source, TarotPlayer who)
{
  TarotGame rebuilt;
  TarotGameIterator i;
  const TarotGameEvent *e;
  /* The number of players and call will be overwritten */
  game_initialize (&rebuilt, 4, 0);
  game_iterator_setup (&i, source);
  while ((e = game_iterator_next_value (&i)) != NULL)
    {
      TarotPlayer dealt;
      size_t n;
      if (event_get_deal (e, &dealt, &n, 0, 0, NULL) == TAROT_EVENT_OK
          || event_get_deal_all (e, &n, 0, 0, NULL) == TAROT_EVENT_OK)
        {
          TarotGameEvent my_deal;
          unsigned int data[78];
          TarotCard my_hand[78];
          if (game_get_deal_of (source, who, &n, 0, 78, my_hand) !=
              TAROT_GAME_OK)
            {
              return TAROT_GAME_NA;
            }
          event_init_deal (&my_deal, who, n, my_hand, 78, data);
          if (game_add_event (&rebuilt, &my_deal) != TAROT_GAME_OK)
            {
              assert (0);
            }
        }
      else if (event_get_discard (e, &n, 0, 0, NULL) == TAROT_EVENT_OK)
        {
          TarotGameEvent my_discard;
          unsigned int data[6];
          TarotCard my_cards[6];
          TarotPlayer taker;
          if (game_get_taker (source, &taker) != TAROT_GAME_OK)
            {
              assert (0);
            }
          if (taker == who)
            {
              if (game_get_full_discard (source, &n, 0, 6, my_cards) !=
                  TAROT_GAME_OK)
                {
                  return TAROT_GAME_NA;
                }
            }
          else
            {
              if (game_get_public_discard (source, &n, 0, 6, my_cards) !=
                  TAROT_GAME_OK)
                {
                  return TAROT_GAME_NA;
                }
            }
          event_init_discard (&my_discard, n, my_cards, 6, data);
          if (game_add_event (&rebuilt, &my_discard) != TAROT_GAME_OK)
            {
              assert (0);
            }
        }
      else if (game_add_event (&rebuilt, e) != TAROT_GAME_OK)
        {
          assert (0);
        }
    }
  game_copy (dest, &rebuilt);
  return TAROT_GAME_OK;
}

static inline TarotStep
game_step (const TarotGame * game)
{
  TarotGameHandle handle;
  game_generalize ((TarotGame *) game, &handle);
  return game_get_step (&handle);
}

static inline TarotGameError
game_duplicate_set_deal (TarotGame * game, size_t n_owners,
                         const TarotPlayer * owners)
{
  TarotGameHandle handle;
  TarotStep step;
  game_generalize ((TarotGame *) game, &handle);
  step = game_get_step (&handle);
  if (step == TAROT_SETUP || step == TAROT_DEAL)
    {
      game_set_duplicate_deal (&handle, n_owners, owners);
      if (step == TAROT_DEAL)
        {
          int err = game_step_advance (game);
          if (err != 0)
            {
              return TAROT_GAME_INVEV;
            }
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_TOOLATE;
}

static inline TarotGameError
game_duplicate_set_taker (TarotGame * game, TarotPlayer taker)
{
  TarotGameHandle handle;
  TarotStep step;
  size_t n_bids;
  int has_bids;
  game_generalize ((TarotGame *) game, &handle);
  step = game_get_step (&handle);
  has_bids = (game_get_bids (game, &n_bids, 0, 0, NULL) == TAROT_GAME_OK);
  if (step == TAROT_BIDS)
    {
      assert (has_bids);
    }
  if (step == TAROT_SETUP || step == TAROT_DEAL
      || (step == TAROT_BIDS && n_bids == 0))
    {
      game_set_duplicate_taker (&handle, taker);
      if (step == TAROT_BIDS)
        {
          int err = game_step_advance (game);
          if (err != 0)
            {
              return TAROT_GAME_INVEV;
            }
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_TOOLATE;
}

static inline TarotGameError
game_duplicate_set_call (TarotGame * game, TarotCard call)
{
  TarotGameHandle handle;
  TarotStep step;
  game_generalize ((TarotGame *) game, &handle);
  step = game_get_step (&handle);
  if (step == TAROT_SETUP || step == TAROT_DEAL || step == TAROT_BIDS
      || step == TAROT_DECLS || step == TAROT_CALL)
    {
      game_set_duplicate_call (&handle, call);
      if (step == TAROT_CALL)
        {
          int err = game_step_advance (game);
          if (err != 0)
            {
              return TAROT_GAME_INVEV;
            }
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_TOOLATE;
}

static inline void
game_iterator_setup (TarotGameIterator * iterator, const TarotGame * game)
{
  iterator->handle = game;
  iterator->step = TAROT_SETUP;
}

static inline size_t
game_n_players (const TarotGame * game)
{
  return game->opt.n_players;
}

static inline int
game_with_call (const TarotGame * game)
{
  return game->opt.with_call;
}

static inline TarotGameError
game_get_next (const TarotGame * game, TarotPlayer * next)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  switch (game_get_step (&h))
    {
    case TAROT_SETUP:
      return TAROT_GAME_NA;
    case TAROT_DEAL:
      return TAROT_GAME_NA;
    case TAROT_BIDS:
      assert (bids_has_next (h.bids, h.opt.n_players));
      *next = bids_next (h.bids);
      return TAROT_GAME_OK;
    case TAROT_DECLS:
      assert (decls_has_next (h.decls, h.opt.n_players));
      *next = decls_next (h.decls);
      return TAROT_GAME_OK;
    case TAROT_CALL:
      assert (call_waiting (h.call));
      *next = bids_taker (h.bids);
      return TAROT_GAME_OK;
    case TAROT_DOG:
      assert (doscard_expects_dog (&(h.doscard)));
      return TAROT_GAME_NA;
    case TAROT_DISCARD:
      assert (doscard_expects_discard (&(h.doscard)));
      *next = bids_taker (h.bids);
      return TAROT_GAME_OK;
    case TAROT_TRICKS:
      assert (handfuls_has_next (h.handfuls)
              || tricks_has_next (&(h.tricks)));
      if (handfuls_has_next (h.handfuls))
        {
          assert (handfuls_next (h.handfuls, h.opt.n_players)
                  == tricks_next (&(h.tricks)));
        }
      *next = tricks_next (&(h.tricks));
      return TAROT_GAME_OK;
    case TAROT_END:
      return TAROT_GAME_NA;
    default:
      assert (0);
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_main_player (const TarotGame * game, TarotPlayer * main)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  *main = game_get_dealt (&h);
  if (*main >= game->opt.n_players)
    {
      return TAROT_GAME_NA;
    }
  return TAROT_GAME_OK;
}

static inline int
game_get_deal_of_aux (const TarotGameHandle * game, TarotPlayer p,
                      TarotHand * dest)
{
  int error = 0;
  error = error || (hands_known (&(game->hands), p) == 0);
  hand_copy (dest, &(game->hands.hands[p]));
  if (error == 0)
    {
      unsigned int i_trick, current_trick;
      current_trick = tricks_current (&(game->tricks));
      for (i_trick = 0; i_trick < current_trick; i_trick++)
        {
          const TarotTrickHandle *trick = &(game->tricks.tricks[i_trick]);
          TarotCard played = trick_get_card_of (trick, p);
          error = error || hand_insert (dest, played);
        }
      if (current_trick < game->tricks.n_tricks)
        {
          const TarotTrickHandle *current =
            &(game->tricks.tricks[current_trick]);
          if (trick_has_played (current, p))
            {
              TarotCard played = trick_get_card_of (current, p);
              error = error || hand_insert (dest, played);
            }
        }
      if (bids_has_taker (game->bids, game->opt.n_players)
          && bids_taker (game->bids) == p
          && bids_discard_allowed (game->bids, game->opt.n_players)
          && doscard_discard_known (&(game->doscard)))
        {
          hand_swap (dest, &(game->doscard.discard), &(game->doscard.dog));
        }
    }
  return error;
}

static inline TarotGameError
game_get_deal_all (const TarotGame * game, size_t *n, size_t start,
                   size_t max, TarotPlayer * owners)
{
  TarotGameError ret = TAROT_GAME_NA;
  TarotPlayer buffer[78];
  TarotCard owned_cards[24];
  size_t n_owned;
  size_t i = 0;
  size_t n_players = game_n_players (game);
  *n = sizeof (buffer) / sizeof (buffer[0]);
  for (i = 0; i < sizeof (buffer) / sizeof (buffer[0]); i++)
    {
      buffer[i] = n_players;
    }
  for (i = 0; i < n_players; i++)
    {
      size_t j;
      ret =
        game_get_deal_of (game, i, &n_owned, 0,
                          sizeof (owned_cards) / sizeof (owned_cards[0]),
                          owned_cards);
      if (ret != TAROT_GAME_OK)
        {
          break;
        }
      for (j = 0; j < n_owned; j++)
        {
          TarotCard owned = owned_cards[j];
          buffer[owned] = i;
        }
    }
  for (i = 0; i < max && i + start < sizeof (buffer) / sizeof (*buffer); i++)
    {
      owners[i] = buffer[i + start];
    }
  return ret;
}

static inline void
query_hand (const TarotHand * hand, size_t *n, size_t start, size_t max,
            TarotCard * dest)
{
  size_t n_available = *(hand->n);
  if (start > n_available)
    {
      start = n_available;
    }
  if (start + max > n_available)
    {
      max = n_available - start;
    }
  *n = n_available;
  assert (start + max <= n_available);
  assert (start + max <= *(hand->n));
  if (max != 0)
    {
      memcpy (dest, hand->buffer + start, max * sizeof (TarotCard));
    }
}

static inline TarotGameError
game_get_deal_of (const TarotGame * game, TarotPlayer who, size_t *n,
                  size_t start, size_t max, TarotCard * deal)
{
  TarotHand24 hand = {.n = 0 };
  TarotHand hand_handle = {
    .n = &(hand.n),
    .n_max = sizeof (hand.buffer) / sizeof (TarotCard),
    .buffer = hand.buffer
  };
  TarotGameHandle handle;
  TarotGameError ret;
  game_generalize ((TarotGame *) game, &handle);
  if (game_get_deal_of_aux (&handle, who, &hand_handle) != 0)
    {
      ret = TAROT_GAME_NA;
    }
  else
    {
      size_t my_n;
      assert (hand_handle.n_max == 24);
      assert (*(hand_handle.n) <= hand_handle.n_max);
      assert (*(hand_handle.n) <= 24);
      query_hand (&hand_handle, &my_n, start, max, deal);
      assert (my_n <= hand_handle.n_max);
      *n = my_n;
      ret = TAROT_GAME_OK;
    }
  return ret;
}

static inline int
game_get_cards_aux (const TarotGameHandle * game, TarotPlayer who,
                    TarotHand * dest)
{
  int error = hands_known (&(game->hands), who) == 0;
  if (error == 0)
    {
      hand_copy (dest, &(game->hands.hands[who]));
    }
  return error;
}

static inline size_t
game_count_cards_aux (const TarotGameHandle * game, TarotPlayer who)
{
  return tricks_count_remaining_cards (&(game->tricks), who);
}

static inline TarotGameError
game_get_cards (const TarotGame * game, TarotPlayer who, size_t *n,
                size_t start, size_t max, TarotCard * cards)
{
  TarotHand24 hand;
  TarotHand hand_handle = {
    .n = &hand.n,
    .n_max = sizeof (hand.buffer) / sizeof (TarotCard),
    .buffer = hand.buffer
  };
  TarotGameHandle h;
  TarotGameError ret;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_cards_aux (&h, who, &hand_handle) != 0)
    {
      ret = TAROT_GAME_NA;
      *n = game_count_cards_aux (&h, who);
    }
  else
    {
      assert (*(hand_handle.n) == hand.n);
      assert (*(hand_handle.n) <= 24);
      assert (hand_handle.n_max == 24);
      query_hand (&hand_handle, n, start, max, cards);
      ret = TAROT_GAME_OK;
    }
  return ret;
}

static inline TarotGameError
game_get_bids (const TarotGame * game, size_t *n, TarotPlayer start,
               size_t max, TarotBid * bids)
{
  TarotGameHandle h;
  TarotGameError ret = TAROT_GAME_NA;
  *n = 0;
  game_generalize ((TarotGame *) game, &h);
  if (h.bids->has_started)
    {
      *n = h.bids->n_bids;
      if (start > *n)
        {
          start = *n;
        }
      if (start + max > *n)
        {
          max = *n - start;
        }
      if (max != 0)
        {
          memcpy (bids, h.bids->bids + start, max * sizeof (TarotBid));
        }
      ret = TAROT_GAME_OK;
    }
  return ret;
}

static inline TarotGameError
game_get_taker (const TarotGame * game, TarotPlayer * taker)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  *taker = bids_taker (h.bids);
  if (bids_has_taker (h.bids, h.opt.n_players))
    {
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_declarations (const TarotGame * game, size_t *n, TarotPlayer start,
                       size_t max, int *decls)
{
  TarotGameHandle h;
  size_t i;
  game_generalize ((TarotGame *) game, &h);
  *n = 0;
  if (!(bids_has_taker (h.bids, h.opt.n_players)))
    {
      return TAROT_GAME_NA;
    }
  if (game_get_step (&h) >= TAROT_DECLS)
    {
      if (decls_has_next (h.decls, h.opt.n_players))
        {
          *n = decls_next (h.decls);
        }
      else
        {
          *n = game->opt.n_players;
        }
    }
  else
    {
      return TAROT_GAME_NA;
    }
  for (i = 0; i < max && i + start < *n; i++)
    {
      decls[i] = 0;
      if (decls_has_declarant (h.decls, h.opt.n_players)
          && decls_declarant (h.decls) == i + start)
        {
          decls[i] = 1;
        }
    }
  return TAROT_GAME_OK;
}

static inline TarotGameError
game_get_declarant (const TarotGame * game, TarotPlayer * declarant)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) < TAROT_DECLS)
    {
      *declarant = 0;
      return TAROT_GAME_NA;
    }
  if (decls_has_next (h.decls, h.opt.n_players))
    {
      *declarant = decls_next (h.decls);
      return TAROT_GAME_NA;
    }
  if (decls_has_declarant (h.decls, h.opt.n_players))
    {
      *declarant = decls_declarant (h.decls);
      return TAROT_GAME_OK;
    }
  *declarant = game->opt.n_players;
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_call (const TarotGame * game, TarotCard * card)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  TarotGameError ret;
  if (game->opt.with_call && call_card_known (h.call))
    {
      *card = call_card (h.call);
      ret = TAROT_GAME_OK;
    }
  else
    {
      ret = TAROT_GAME_NA;
    }
  return ret;
}

static inline TarotGameError
game_get_partner (const TarotGame * game, TarotPlayer * partner)
{
  TarotGameHandle h;
  TarotGameError ret;
  game_generalize ((TarotGame *) game, &h);
  if (game->opt.with_call && call_partner_known (h.call))
    {
      *partner = call_partner (h.call);
      assert (*partner < game->opt.n_players);
      ret = TAROT_GAME_OK;
    }
  else
    {
      game_get_taker (game, partner);
      ret = TAROT_GAME_NA;
    }
  return ret;
}

static inline TarotGameError
game_get_dog (const TarotGame * game, size_t *n, size_t start, size_t max,
              TarotCard * cards)
{
  TarotHand6 dog;
  TarotHand hand = {
    .n = &dog.n,
    .n_max = 6,
    .buffer = dog.buffer
  };
  TarotGameHandle h;
  TarotGameError ret = TAROT_GAME_NA;
  game_generalize ((TarotGame *) game, &h);
  dog.n = 0;
  if (doscard_dog_known (&(h.doscard)))
    {
      if (doscard_dog (&(h.doscard), &hand) != 0)
        {
          assert (0);
        }
      ret = TAROT_GAME_OK;
    }
  assert (*(hand.n) == dog.n);
  assert (*(hand.n) <= 6);
  assert (hand.n_max == 6);
  query_hand (&hand, n, start, max, cards);
  return ret;
}

static inline TarotGameError
game_get_full_discard (const TarotGame * game, size_t *n, size_t start,
                       size_t max, TarotCard * cards)
{
  TarotHand6 h6;
  TarotHand hand = {
    .n = &(h6.n),
    .n_max = 6,
    .buffer = h6.buffer
  };
  TarotGameHandle h;
  TarotGameError ret = TAROT_GAME_NA;
  game_generalize ((TarotGame *) game, &h);
  h6.n = 0;
  if (doscard_discard_known (&(h.doscard)))
    {
      if (doscard_discard (&(h.doscard), &hand) != 0)
        {
          assert (0);
        }
      ret = TAROT_GAME_OK;
    }
  assert (*(hand.n) == h6.n);
  assert (*(hand.n) <= 6);
  assert (hand.n_max == 6);
  query_hand (&hand, n, start, max, cards);
  return ret;
}

static inline TarotGameError
game_get_public_discard (const TarotGame * game, size_t *n, size_t start,
                         size_t max, TarotCard * cards)
{
  TarotHand6 h6;
  TarotHand hand = {
    .n = &(h6.n),
    .n_max = 6,
    .buffer = h6.buffer
  };
  TarotGameHandle h;
  TarotGameError ret = TAROT_GAME_NA;
  game_generalize ((TarotGame *) game, &h);
  h6.n = 0;
  if (doscard_discard_known (&(h.doscard)))
    {
      if (doscard_discard_public (&(h.doscard), &hand) != 0)
        {
          assert (0);
        }
      ret = TAROT_GAME_OK;
    }
  size_t my_n;
  assert (hand.n_max <= 6);
  assert (*(hand.n) <= 6);
  query_hand (&hand, &my_n, start, max, cards);
  assert (my_n <= hand.n_max);
  *n = my_n;
  return ret;
}

static inline TarotGameError
game_get_handful (const TarotGame * game, TarotPlayer p, int *size, size_t *n,
                  size_t start, size_t max, TarotCard * cards)
{
  TarotHand24 h24;
  TarotHand hand = {
    .n = &h24.n,
    .n_max = 24,
    .buffer = h24.buffer
  };
  TarotGameHandle h;
  TarotGameError ret = TAROT_GAME_NA;
  game_generalize ((TarotGame *) game, &h);
  h24.n = 0;
  *size = 0;
  if (handfuls_has_handful (h.handfuls, p, h.opt.n_players))
    {
      *size = handfuls_handful_size_of (h.handfuls, p, h.opt.n_players);
      if (handfuls_handful_of (h.handfuls, p, &hand) != 0)
        {
          assert (0);
        }
      ret = TAROT_GAME_OK;
    }
  size_t my_n;
  assert (hand.n_max <= 24);
  assert (*(hand.n) <= 24);
  query_hand (&hand, &my_n, start, max, cards);
  assert (my_n <= hand.n_max);
  *n = my_n;
  return ret;
}

static inline size_t
game_n_tricks (const TarotGame * game)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  return h.tricks.n_tricks;
}

static inline TarotGameError
game_get_current_trick (const TarotGame * game, size_t *current)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  *current = tricks_current (&(h.tricks));
  if (game_get_step (&h) == TAROT_TRICKS
      || (game_get_step (&h) == TAROT_END
          && bids_contract (h.bids) != TAROT_PASS))
    {
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_lead_suit (const TarotGame * game, size_t i_trick,
                    TarotSuit * lead_suit)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  game_generalize ((TarotGame *) game, &h);
  trick = &(h.tricks.tricks[i_trick]);
  if (trick_has_lead_suit (trick))
    {
      *lead_suit = trick_lead_suit (trick);
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_max_trump (const TarotGame * game, size_t i_trick,
                    TarotNumber * max_trump)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  game_generalize ((TarotGame *) game, &h);
  trick = &(h.tricks.tricks[i_trick]);
  *max_trump = 0;
  if (trick_has_lead_suit (trick))
    {
      *max_trump = trick_max_trump (trick);
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_trick_leader (const TarotGame * game, size_t i_trick,
                       TarotPlayer * leader)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  game_generalize ((TarotGame *) game, &h);
  trick = &(h.tricks.tricks[i_trick]);
  if ((game_get_step (&h) == TAROT_TRICKS
       || game_get_step (&h) == TAROT_END) && trick_has_leader (trick))
    {
      *leader = trick_leader (trick);
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_trick_taker (const TarotGame * game, size_t i_trick,
                      TarotPlayer * taker)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  game_generalize ((TarotGame *) game, &h);
  trick = &(h.tricks.tricks[i_trick]);
  *taker = h.opt.n_players;
  if (trick_has_leader (trick))
    {
      *taker = trick_taker (trick);
      if (trick_n_cards (trick) == h.opt.n_players)
        {
          return TAROT_GAME_OK;
        }
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_trick_cards (const TarotGame * game, size_t i_trick,
                      size_t *n_cards,
                      size_t start, size_t max, TarotGameCardAnalysis * cards)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  size_t i;
  game_generalize ((TarotGame *) game, &h);
  if ((game_get_step (&h) == TAROT_TRICKS
       || game_get_step (&h) == TAROT_END)
      && i_trick <= *(h.tricks.i_current) && i_trick < h.tricks.n_tricks)
    {
      size_t my_n_cards;
      trick = &(h.tricks.tricks[i_trick]);
      my_n_cards = trick->trick->n_cards;
      if (start > my_n_cards)
        {
          start = my_n_cards;
        }
      for (i = 0; i < my_n_cards - start && i < max; i++)
        {
          TarotPlayer p = (trick_leader (trick) + i);
          if (p >= game->opt.n_players)
            {
              p -= game->opt.n_players;
            }
          cards[i].card = trick_get_card (trick, i + start);
          cards[i].cannot_follow = trick_cannot_follow (trick, p);
          cards[i].cannot_overtrump = trick_cannot_overtrump (trick, p);
          cards[i].cannot_undertrump = trick_cannot_undertrump (trick, p);
        }
      *n_cards = my_n_cards;
      return TAROT_GAME_OK;
    }
  *n_cards = 0;
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_one_trick_card (const TarotGame * game, size_t i_trick, size_t i,
                         TarotGameCardAnalysis * out)
{
  size_t n_cards;
  TarotGameCardAnalysis my_out;
  TarotGameError base =
    game_get_trick_cards (game, i_trick, &n_cards, i, 1, &my_out);
  if (base == TAROT_GAME_OK && n_cards <= i)
    {
      base = TAROT_GAME_NA;
    }
  if (base == TAROT_GAME_OK)
    {
      *out = my_out;
    }
  return base;
}

static inline TarotGameError
game_get_points_in_trick (const TarotGame * game, size_t i_trick,
                          unsigned int *halfpoints, unsigned int *oudlers)
{
  TarotGameHandle h;
  const TarotTrickHandle *trick;
  game_generalize ((TarotGame *) game, &h);
  *halfpoints = 0;
  *oudlers = 0;
  if (game_get_step (&h) == TAROT_TRICKS && i_trick <= *(h.tricks.i_current))
    {
      trick = &(h.tricks.tricks[i_trick]);
      *halfpoints = trick_points (trick);
      *oudlers = trick_oudlers (trick);
      if (trick_n_cards (trick) == h.opt.n_players)
        {
          return TAROT_GAME_OK;
        }
    }
  return TAROT_GAME_NA;
}

static inline int
game_base_score (const TarotGameHandle * game)
{
  int ret = 0, hp = 0, od = 0, bar = 0, hfs, declarant, slammer;
  TarotPlayer taker = bids_taker (game->bids);
  TarotPlayer partner = (game->opt.with_call
                         && (call_partner_known (game->call))
                         ? call_partner (game->call) : taker);
  static const int bars[4] = { 56, 51, 41, 36 };
  if (game_get_step (game) != TAROT_END
      || bids_contract (game->bids) == TAROT_PASS)
    {
      return 0;
    }
  hp = tricks_points (&(game->tricks), taker);
  od = tricks_oudlers (&(game->tricks), taker);
  if (partner != taker)
    {
      hp += tricks_points (&(game->tricks), partner);
      od += tricks_oudlers (&(game->tricks), partner);
    }
  if (bids_discard_counted (game->bids, game->opt.n_players))
    {
      hp += doscard_points (&(game->doscard));
      od += doscard_oudlers (&(game->doscard));
    }
  assert (od >= 0 && od < 4);
  bar = 2 * bars[od];
  if (hp >= bar)
    {
      int diff = hp - bar;
      ret = (diff - diff / 2) + 25;
      hfs = +1;
    }
  else
    {
      int diff = bar - hp;
      ret = -(diff - diff / 2) - 25;
      hfs = -1;
    }
  ret += 10 * tricks_pab (&(game->tricks), taker, partner);
  ret *= bids_multiplier (game->bids, game->opt.n_players);
  ret += hfs * handfuls_bonus (game->handfuls, game->opt.n_players);
  if (decls_has_declarant (game->decls, game->opt.n_players))
    {
      TarotPlayer d = decls_declarant (game->decls);
      if (d == taker || d == partner)
        {
          declarant = +1;
        }
      else
        {
          declarant = -1;
        }
    }
  else
    {
      declarant = 0;
    }
  slammer = tricks_slam (&(game->tricks), taker, partner);
  if (declarant != 0 && slammer != 0)
    {
      ret += slammer * 400;
    }
  else if (declarant != 0)
    {
      ret -= declarant * 200;
    }
  else if (slammer != 0)
    {
      ret += declarant * 200;
    }
  return ret;
}

static inline TarotGameError
game_get_scores (const TarotGame * game, size_t *n, size_t start,
                 size_t max, int *scores)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  *n = 0;
  if (game_get_step (&h) == TAROT_END)
    {
      size_t i;
      size_t n_players = h.opt.n_players;
      int base = game_base_score (&h);
      TarotPlayer taker, partner;
      *n = n_players;
      game_get_taker (game, &taker);
      game_get_partner (game, &partner);
      if (start > n_players)
        {
          start = n_players;
        }
      if (start + max > n_players)
        {
          max = n_players - start;
        }
      for (i = 0; i < max && i + start < n_players; i++)
        {
          TarotPlayer p = i + start;
          if (p == partner || p == taker)
            {
              if (p == partner && p == taker)
                {
                  scores[i] = (n_players - 1) * base;
                }
              else if (p == partner)
                {
                  scores[i] = base;
                }
              else
                {
                  scores[i] = 2 * base;
                }
            }
          else
            {
              scores[i] = -base;
            }
        }
      if (start == 0 && max >= n_players)
        {
          int sum = 0;
          for (i = 0; i < n_players; i++)
            {
              sum += scores[i];
            }
          assert (sum == 0);
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_bid (const TarotGame * game,
                   TarotPlayer * player, TarotBid * mini)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_BIDS)
    {
      *player = bids_next (h.bids);
      *mini = bids_contract (h.bids) + 1;
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_decl (const TarotGame * game,
                    TarotPlayer * player, int *allowed)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_DECLS)
    {
      *player = decls_next (h.decls);
      *allowed = 1;
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_call (const TarotGame * game, TarotPlayer * player,
                    TarotNumber * mini)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_CALL)
    {
      *player = bids_taker (h.bids);
      *mini = TAROT_JACK;
      if (hands_known (&(h.hands), *player))
        {
          *mini = hand_hint_call (&(h.hands.hands[*player]));
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_discard_aux (const TarotGame * game,
                           TarotPlayer * player, size_t *n,
                           TarotHand * prio, TarotHand * additional)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_DISCARD
      && doscard_expects_discard (&(h.doscard)))
    {
      const TarotHand *dog = &(h.doscard.dog);
      *player = bids_taker (h.bids);
      hand_set (prio, NULL, 0);
      hand_set (additional, NULL, 0);
      *n = hand_max (dog);
      if (hands_known (&(h.hands), *player))
        {
          const TarotHand *takers_hand = &(h.hands.hands[*player]);
          if (hand_hint_discard (takers_hand, dog, prio, additional) != 0)
            {
              return TAROT_GAME_NA;
            }
        }
      return TAROT_GAME_OK;
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_discard (const TarotGame * game, TarotPlayer * player,
                       size_t *n_cards, size_t *n_prio, size_t start_prio,
                       size_t max_prio, TarotCard * prio,
                       size_t *n_additional, size_t start_additional,
                       size_t max_additional, TarotCard * additional)
{
  TarotCard buffer_prio[78];
  TarotCard buffer_additional[78];
  size_t real_n_prio = 0, real_n_additional = 0;
  TarotHand h_prio = {
    .n = &real_n_prio,
    .n_max = 78,
    .buffer = buffer_prio
  };
  TarotHand h_additional = {
    .n = &real_n_additional,
    .n_max = 78,
    .buffer = buffer_additional
  };
  TarotGameError ret =
    game_get_hint_discard_aux (game, player, n_cards, &h_prio, &h_additional);
  if (ret != TAROT_GAME_OK)
    {
      real_n_prio = 0;
      real_n_additional = 0;
    }
  query_hand (&h_prio, n_prio, start_prio, max_prio, prio);
  query_hand (&h_additional, n_additional, start_additional, max_additional,
              additional);
  return ret;
}

static inline TarotGameError
game_get_hint_handful_aux (const TarotGame * game,
                           TarotPlayer * player, size_t *n_simple,
                           size_t *n_double, size_t *n_triple,
                           TarotHand * prio, TarotHand * additional)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_TRICKS && handfuls_has_next (h.handfuls))
    {
      unsigned int n[4] = { 0 };
      handfuls_sizes (h.opt.n_players, n);
      *n_simple = n[1];
      *n_double = n[2];
      *n_triple = n[3];
      *player = handfuls_next (h.handfuls, h.opt.n_players);
      hand_set (prio, NULL, 0);
      hand_set (additional, NULL, 0);
      if (hands_known (&(h.hands), *player))
        {
          const TarotHand *next_hand = &(h.hands.hands[*player]);
          if (bids_discard_allowed (h.bids, h.opt.n_players)
              && doscard_discard_known (&(h.doscard))
              && *player == bids_taker (h.bids))
            {
              const TarotHand *discard = &(h.doscard.discard);
              if (hand_hint_handful_with_discard
                  (next_hand, discard, prio, additional) != 0)
                {
                  return TAROT_GAME_NA;
                }
              return TAROT_GAME_OK;
            }
          else
            {
              if (hand_hint_handful_without_discard
                  (next_hand, prio, additional) != 0)
                {
                  return TAROT_GAME_NA;
                }
              return TAROT_GAME_OK;
            }
        }
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_handful (const TarotGame * game, TarotPlayer * player,
                       size_t *n_simple, size_t *n_double, size_t *n_triple,
                       size_t *n_prio, size_t start_prio, size_t max_prio,
                       TarotCard * prio, size_t *n_additional,
                       size_t start_additional, size_t max_additional,
                       TarotCard * additional)
{
  TarotCard buffer_prio[78];
  TarotCard buffer_additional[78];
  size_t real_n_prio = 0, real_n_additional = 0;
  TarotHand h_prio = {
    .n = &real_n_prio,
    .n_max = 78,
    .buffer = buffer_prio
  };
  TarotHand h_additional = {
    .n = &real_n_additional,
    .n_max = 78,
    .buffer = buffer_additional
  };
  TarotGameError ret =
    game_get_hint_handful_aux (game, player, n_simple, n_double, n_triple,
                               &h_prio, &h_additional);
  if (ret != TAROT_GAME_OK)
    {
      real_n_prio = 0;
      real_n_additional = 0;
    }
  query_hand (&h_prio, n_prio, start_prio, max_prio, prio);
  query_hand (&h_additional, n_additional, start_additional, max_additional,
              additional);
  return ret;
}

static inline TarotGameError
game_get_hint_card_aux (const TarotGame * game,
                        TarotPlayer * player, TarotHand * playable)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (game_get_step (&h) == TAROT_TRICKS)
    {
      *player = tricks_next (&(h.tricks));
      hand_set (playable, NULL, 0);
      if (hands_known (&(h.hands), *player))
        {
          const TarotHand *next_hand = &(h.hands.hands[*player]);
          size_t current = tricks_current (&(h.tricks));
          const TarotTrickHandle *current_trick = &(h.tricks.tricks[current]);
          if (trick_has_lead_suit (current_trick))
            {
              TarotSuit lead_suit = trick_lead_suit (current_trick);
              TarotNumber max_trump = trick_max_trump (current_trick);
              assert (max_trump <= 21);
              if (hand_hint_card_with_lead (next_hand, lead_suit, max_trump,
                                            playable) != 0)
                {
                  return TAROT_GAME_NA;
                }
            }
          else if (h.opt.with_call && tricks_current (&(h.tricks)) == 0)
            {
              TarotCard c = call_card (h.call);
              if (hand_hint_card_without_lead_first (next_hand, c, playable)
                  != 0)
                {
                  return TAROT_GAME_NA;
                }
            }
          else
            {
              if (hand_hint_card_without_lead_not_first (next_hand, playable)
                  != 0)
                {
                  return TAROT_GAME_NA;
                }
            }
          return TAROT_GAME_OK;
        }
    }
  return TAROT_GAME_NA;
}

static inline TarotGameError
game_get_hint_card (const TarotGame * game, TarotPlayer * player,
                    size_t *n_playable, size_t start_playable,
                    size_t max_playable, TarotCard * playable)
{
  TarotCard buffer[78];
  size_t real_n = 0;
  TarotHand h = {
    .n = &real_n,
    .n_max = 78,
    .buffer = buffer
  };
  TarotGameError ret = game_get_hint_card_aux (game, player, &h);
  if (ret != TAROT_GAME_OK)
    {
      real_n = 0;
    }
  query_hand (&h, n_playable, start_playable, max_playable, playable);
  return ret;
}

static inline int
game_deal_check_number_of_cards (const TarotGameHandle * game,
                                 const TarotPlayer * owners)
{
  size_t n_players = game->opt.n_players;
  TarotPlayer p;
  for (p = 0; p < n_players; p++)
    {
      size_t n_owned = 0;
      TarotCard c;
      for (c = 0; c < 78; c++)
        {
          if (owners[c] == p)
            {
              n_owned++;
            }
        }
      if (n_owned != game->hands.hands[p].n_max)
        {
          /* Player p has been given n_owned cards, not the expected
           * number */
          return 0;
        }
    }
  return 1;
}

static inline int
game_deal_has_petit_sec (const TarotPlayer * owners)
{
  TarotCard other_trump;
  int ret = 1;
  for (other_trump = TAROT_PETIT + 1; other_trump < 78; ++other_trump)
    {
      ret = ret && (owners[TAROT_PETIT] != owners[other_trump]);
    }
  return ret;
}

static inline int
game_check_deal_all (const TarotGameHandle * game, const TarotPlayer * owners)
{
  return ((game_get_step (game) == TAROT_DEAL
           || game_get_step (game) == TAROT_SETUP)
          && game_deal_check_number_of_cards (game, owners)
          && !game_deal_has_petit_sec (owners));
}

static inline int
game_check_deal (const TarotGameHandle * game,
                 TarotPlayer myself, const TarotHand * cards)
{
  return ((game_get_step (game) == TAROT_DEAL
           || game_get_step (game) == TAROT_SETUP)
          && myself < game->opt.n_players
          && hand_size (cards) == game->hands.hands[0].n_max
          && !hand_has_petit_sec (cards));
}

static inline int
game_check_bid (const TarotGameHandle * game, TarotBid bid)
{
  return (game_get_step (game) == TAROT_BIDS
          && bids_check (game->bids, bid, game->opt.n_players));
}

static inline int
game_check_decl (const TarotGameHandle * game, int decl)
{
  return (game_get_step (game) == TAROT_DECLS
          && decls_check (game->decls, decl, game->opt.n_players));
}

static inline int
game_check_call (const TarotGame * game_for_counter,
                 const TarotGameHandle * game, TarotCard call)
{
  int ok = (game_get_step (game) == TAROT_CALL);
  TarotNumber n;
  TarotSuit s;
  ok = ok && (decompose (call, &n, &s) == 0);
  if (ok)
    {
      TarotPlayer taker = bids_taker (game->bids);
      const TarotHand *takers_hand = &(game->hands.hands[taker]);
      if (hands_known (&(game->hands), taker))
        {
          ok = hand_check_call (takers_hand, n);
        }
      else
        {
          TarotNumber superior;
          TarotCounter validator;
          counter_load_from_game (&validator, game_for_counter);
          /* Check that this is a face and that the taker may own all
           * superior faces */
          if (n < TAROT_JACK || n > TAROT_KING || s == TAROT_TRUMPS)
            {
              ok = 0;
            }
          for (superior = n + 1; superior <= TAROT_KING; superior++)
            {
              TarotCard superior_card;
              if (of (superior, s, &superior_card) != 0)
                {
                  assert (0);
                }
              if (!counter_may_own (&validator, 1, &taker, superior_card))
                {
                  ok = 0;
                }
            }
        }
    }
  return ok;
}

static inline int
game_check_dog (const TarotGameHandle * game, const TarotHand * dog)
{
  int not_in_hands;
  size_t i = 0;
  TarotPlayer owner;
  size_t n = *(dog->n);
  for (i = 0; i < n; i++)
    {
      not_in_hands = hands_locate (&(game->hands), dog->buffer[i], &owner);
      if (not_in_hands)
        {
          /* This card may be in the dog */
        }
      else
        {
          return 0;
        }
    }
  return (game_get_step (game) == TAROT_DOG
          && doscard_check_dog (&(game->doscard), dog));
}

static inline int
game_check_discard (const TarotGame * game_for_counter,
                    const TarotGameHandle * game, const TarotHand * discard)
{
  int ok = (game_get_step (game) == TAROT_DISCARD);
  TarotPlayer taker = bids_taker (game->bids);
  if (ok)
    {
      const TarotHand *takers_hand = &(game->hands.hands[taker]);
      if (hands_known (&(game->hands), taker))
        {
          const TarotHand *dog = &(game->doscard.dog);
          ok = (hand_check_discard (takers_hand, dog, discard)
                && doscard_check_discard (&(game->doscard), taker, discard));
        }
      else
        {
          ok =
            doscard_check_partial_discard (&(game->doscard), taker, discard);
        }
    }
  else if (ok)
    {
      size_t i_card;
      TarotCounter validator;
      counter_load_from_game (&validator, game_for_counter);
      for (i_card = 0; i_card < *(discard->n); i_card++)
        {
          if (!counter_may_own
              (&validator, 1, &taker, discard->buffer[i_card]))
            {
              ok = 0;
            }
        }
    }
  return ok;
}

static inline int
game_check_handful (const TarotGame * game_for_counter,
                    const TarotGameHandle * game, const TarotHand * handful)
{
  size_t i_current = tricks_current (&(game->tricks));
  int ok = (game_get_step (game) == TAROT_TRICKS && i_current == 0);
  TarotPlayer next = tricks_next (&(game->tricks));
  if (ok)
    {
      ok = handfuls_check (game->handfuls, next, handful,
                           game->opt.n_players);
    }
  if (ok && hands_known (&(game->hands), next))
    {
      TarotPlayer taker = bids_taker (game->bids);
      const TarotHand *next_hand = &(game->hands.hands[next]);
      if (bids_discard_allowed (game->bids, game->opt.n_players)
          && doscard_discard_known (&(game->doscard)) && taker == next)
        {
          const TarotHand *discard = &(game->doscard.discard);
          ok = hand_check_handful_with_discard (next_hand, discard, handful);
        }
      else
        {
          ok = hand_check_handful_without_discard (next_hand, handful);
        }
    }
  else if (ok)
    {
      size_t i_card;
      TarotCounter validator;
      counter_load_from_game (&validator, game_for_counter);
      for (i_card = 0; i_card < *(handful->n); i_card++)
        {
          if (!counter_may_own
              (&validator, 1, &next, handful->buffer[i_card]))
            {
              ok = 0;
            }
        }
    }
  return ok;
}

static inline int
game_check_card_full_aux (const TarotGame * game_for_counter,
                          const TarotGameHandle * game, TarotCard played,
                          int *cnf, TarotSuit * lead_suit, int *cno,
                          TarotNumber * max_trump, int *cnu)
{
  size_t i_current = tricks_current (&(game->tricks));
  int ok = (game_get_step (game) == TAROT_TRICKS);
  TarotPlayer next = tricks_next (&(game->tricks));
  const TarotTrickHandle *current = &(game->tricks.tricks[i_current]);
  if (ok && hands_known (&(game->hands), next))
    {
      const TarotHand *next_hand = &(game->hands.hands[next]);
      if (trick_has_lead_suit (current))
        {
          ok =
            hand_check_card_with_lead (next_hand,
                                       trick_lead_suit (current),
                                       trick_max_trump (current), played);
        }
      else if (i_current == 0 && game->opt.with_call)
        {
          ok =
            hand_check_card_without_lead_first (next_hand,
                                                call_card
                                                (game->call), played);
        }
      else
        {
          ok = hand_check_card_without_lead_not_first (next_hand, played);
        }
    }
  else if (ok)
    {
      /* We need to check if this card does not violate the rules.
       * This is subtle: what if the player trumps clubs but later it
       * is found that she has clubs?  For this we need a counter.
       * Fortunately, the counter API does not call this function
       * during its construction. */
      TarotCounter validator;
      TarotCard call;
      counter_initialize (&validator);
      counter_load_from_game (&validator, game_for_counter);
      ok = (counter_may_own (&validator, 1, &next, played));
      if (game_get_call (game_for_counter, &call) == TAROT_GAME_OK)
        {
          size_t current_trick;
          if (game_get_current_trick (game_for_counter, &current_trick) ==
              TAROT_GAME_OK)
            {
              if (current_trick == 0)
                {
                  TarotCard first_trick_lead_suit;
                  if (game_get_lead_suit
                      (game_for_counter, current_trick,
                       &first_trick_lead_suit) == TAROT_GAME_NA)
                    {
                      /* We do not allow cards of the call suit,
                       * except the called card itself */
                      TarotNumber played_number;
                      TarotCard played_suit;
                      TarotNumber call_number;
                      TarotCard call_suit;
                      if (decompose (call, &call_number, &call_suit) != 0)
                        {
                          assert (0);
                        }
                      if (decompose (played, &played_number, &played_suit) ==
                          0)
                        {
                          if (played_suit == call_suit
                              && played_number != call_number)
                            {
                              ok = 0;
                            }
                        }
                    }
                }
            }
        }
    }
  if (ok)
    {
      assert (played < 78);
      trick_check_card_full (current, played, cnf, lead_suit, cno,
                             max_trump, cnu);
    }
  return ok;
}

static inline int
game_check_card (const TarotGame * game, TarotCard played,
                 int *cnf, TarotSuit * lead_suit, int *cno,
                 TarotNumber * max_trump, int *cnu)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  assert (played < 78);
  return game_check_card_full_aux (game, &h, played, cnf, lead_suit, cno,
                                   max_trump, cnu);
}

static void
copy_cards (TarotHand * dest, size_t n, const TarotCard * buffer)
{
  size_t i = 0;
  *(dest->n) = 0;
  for (i = 0; i < n; i++)
    {
      if (hand_insert (dest, buffer[i]) != 0)
        {
          assert (0);
        }
    }
}

static inline int
game_check_event_aux (const TarotGame * game_not_generalized,
                      const TarotGameHandle * game,
                      const TarotGameEvent * event)
{
  size_t n_players, n_in_hand = 0;
  int with_call, decl;
  int error = 0;
  TarotPlayer myself;
  TarotPlayer owners[78];
  TarotHand hand;
  TarotBid bid;
  TarotCard card;
  TarotCard buffer[78];
  TarotCard event_data[78];
  size_t n_event_data;
  hand.n = &n_in_hand;
  hand.n_max = 78;
  hand.buffer = buffer;
  switch (event_type (event))
    {
    case TAROT_SETUP_EVENT:
      error =
        (event_get_setup (event, &n_players, &with_call) != TAROT_EVENT_OK);
      assert (error == 0);
      return ((game_get_step (game) == TAROT_SETUP)
              && n_players >= 3 && n_players <= 5
              && (!with_call || n_players == 5));
    case TAROT_DEAL_EVENT:
      if (event_get_deal (event, &myself, &n_event_data, 0, 78, event_data) !=
          TAROT_EVENT_OK)
        {
          assert (0);
        }
      copy_cards (&hand, n_event_data, event_data);
      return game_check_deal (game, myself, &hand);
    case TAROT_DEAL_ALL_EVENT:
      if (event_get_deal_all (event, &n_players, 0, 78, owners) !=
          TAROT_EVENT_OK)
        {
          assert (0);
        }
      return (n_players == 78 && game_check_deal_all (game, owners));
    case TAROT_BID_EVENT:
      error = event_get_bid (event, &bid);
      assert (error == 0);
      return game_check_bid (game, bid);
    case TAROT_DECL_EVENT:
      error = event_get_decl (event, &decl);
      assert (error == 0);
      return game_check_decl (game, decl);
    case TAROT_CALL_EVENT:
      error = event_get_call (event, &card);
      assert (error == 0);
      return game_check_call (game_not_generalized, game, card);
    case TAROT_DOG_EVENT:
      if (event_get_dog (event, &n_event_data, 0, 78, event_data) !=
          TAROT_EVENT_OK)
        {
          assert (0);
        }
      copy_cards (&hand, n_event_data, event_data);
      return game_check_dog (game, &hand);
    case TAROT_DISCARD_EVENT:
      if (event_get_discard (event, &n_event_data, 0, 78, event_data) !=
          TAROT_EVENT_OK)
        {
          assert (0);
        }
      copy_cards (&hand, n_event_data, event_data);
      return game_check_discard (game_not_generalized, game, &hand);
    case TAROT_HANDFUL_EVENT:
      if (event_get_handful (event, &n_event_data, 0, 78, event_data) !=
          TAROT_EVENT_OK)
        {
          assert (0);
        }
      copy_cards (&hand, n_event_data, event_data);
      return game_check_handful (game_not_generalized, game, &hand);
    case TAROT_CARD_EVENT:
      error = event_get_card (event, &card);
      assert (error == TAROT_EVENT_OK);
      assert (card < 78);
      {
        int cnf;
        TarotSuit lead_suit;
        int cno;
        TarotNumber max_trump;
        int cnu;
        return game_check_card_full_aux (game_not_generalized, game, card,
                                         &cnf, &lead_suit, &cno, &max_trump,
                                         &cnu);
      }
    }
  (void) error;
  return 0;
}

static inline int
game_check_event (const TarotGame * game, const TarotGameEvent * event)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  return game_check_event_aux (game, &h, event);
}

static inline TarotHand
game_create_hand (size_t n_max, size_t *n, TarotCard * buffer)
{
  TarotHand ret;
  ret.n_max = n_max;
  ret.n = n;
  ret.buffer = buffer;
  return ret;
}

static inline int game_add_deal_all (TarotGame * game_not_generalized,
                                     TarotGameHandle * game,
                                     const TarotPlayer * owners);
static inline int game_add_bid (TarotGame * game_not_generalized,
                                TarotGameHandle * game, TarotBid bid);
static inline int game_add_call (TarotGame * game_not_generalized,
                                 TarotGameHandle * game, TarotCard call);

static inline int
game_step_advance (TarotGame * game_not_generalized)
{
  TarotGameHandle game_handle;
  TarotGameHandle *game = &game_handle;
  game_generalize (game_not_generalized, game);
  switch (game_get_step (game))
    {
    case TAROT_SETUP:
      game_set_step (game, TAROT_DEAL);
      return game_step_advance (game_not_generalized);
      break;
    case TAROT_DEAL:
      do
        {
          TarotPlayer i;
          int ok = 0;
          const TarotPlayer *owners;
          size_t n_owners;
          owners = game_get_duplicate_deal (game, &n_owners);
          if (owners != NULL)
            {
              assert (n_owners == 78);
              return game_add_deal_all (game_not_generalized, game, owners);
            }
          for (i = 0; i < game->opt.n_players; ++i)
            {
              if (hands_known (&(game->hands), i))
                {
                  ok = 1;
                }
            }
          if (ok)
            {
              int error = 0;
              error = bids_start (game->bids);
              assert (error == 0);
              (void) error;
              game_set_step (game, TAROT_BIDS);
              return game_step_advance (game_not_generalized);
            }
        }
      while (0);
      break;
    case TAROT_BIDS:
      if (bids_has_next (game->bids, game->opt.n_players))
        {
          const TarotPlayer *forced_taker = game_get_duplicate_taker (game);
          if (forced_taker != NULL)
            {
              if (*forced_taker == bids_next (game->bids))
                {
                  return game_add_bid (game_not_generalized, game,
                                       TAROT_PUSH);
                }
              else
                {
                  return game_add_bid (game_not_generalized, game,
                                       TAROT_PASS);
                }
            }
        }
      else
        {
          if (bids_has_taker (game->bids, game->opt.n_players))
            {
              int error = 0;
              error = decls_start (game->decls);
              assert (error == 0);
              (void) error;
              game_set_step (game, TAROT_DECLS);
            }
          else
            {
              game_set_step (game, TAROT_END);
            }
        }
      break;
    case TAROT_DECLS:
      if (!decls_has_next (game->decls, game->opt.n_players))
        {
          TarotBid contract = bids_contract (game->bids);
          TarotPlayer taker = bids_taker (game->bids);
          if (game->opt.with_call)
            {
              int error = 0;
              error = call_start (game->call);
              assert (error == 0);
              (void) error;
              game_set_step (game, TAROT_CALL);
              return game_step_advance (game_not_generalized);
            }
          else
            {
              TarotPlayer leader = 0;
              int error = 0;
              if (decls_has_declarant (game->decls, game->opt.n_players))
                {
                  leader = decls_declarant (game->decls);
                }
              error = error
                || doscard_set_contract (&(game->doscard), contract);
              error = error || doscard_set_taker (&(game->doscard), taker);
              error = error || doscard_start (&(game->doscard));
              if (bids_discard_allowed (game->bids, game->opt.n_players))
                {
                  game_set_step (game, TAROT_DOG);
                  return game_step_advance (game_not_generalized);
                }
              else
                {
                  error = error
                    || handfuls_set_leader (game->handfuls, leader);
                  error = error || handfuls_start (game->handfuls);
                  error = error
                    || tricks_set_leader (&(game->tricks), leader);
                  game_set_step (game, TAROT_TRICKS);
                }
              assert (error == 0);
              (void) error;
            }
        }
      break;
    case TAROT_CALL:
      if (call_waiting (game->call))
        {
          const TarotCard *forced_call = game_get_duplicate_call (game);
          if (forced_call != NULL)
            {
              return game_add_call (game_not_generalized, game, *forced_call);
            }
        }
      else
        {
          TarotBid contract = bids_contract (game->bids);
          TarotPlayer taker = bids_taker (game->bids);
          int error = 0;
          error = error || doscard_set_contract (&(game->doscard), contract);
          assert (error == 0);
          error = error || doscard_set_taker (&(game->doscard), taker);
          assert (error == 0);
          error = error || doscard_start (&(game->doscard));
          assert (error == 0);
          if (bids_discard_allowed (game->bids, game->opt.n_players))
            {
              game_set_step (game, TAROT_DOG);
              return game_step_advance (game_not_generalized);
            }
          else
            {
              TarotPlayer leader = 0;
              if (decls_has_declarant (game->decls, game->opt.n_players))
                {
                  leader = decls_declarant (game->decls);
                }
              error = error || handfuls_set_leader (game->handfuls, leader);
              error = error || handfuls_start (game->handfuls);
              error = error || tricks_set_leader (&(game->tricks), leader);
              assert (error == 0);
              (void) error;
              game_set_step (game, TAROT_TRICKS);
            }
        }
      break;
    case TAROT_DOG:
      if (!doscard_expects_dog (&(game->doscard)))
        {
          game_set_step (game, TAROT_DISCARD);
        }
      break;
    case TAROT_DISCARD:
      if (!doscard_expects_discard (&(game->doscard)))
        {
          TarotPlayer leader = 0;
          int error = 0;
          if (decls_has_declarant (game->decls, game->opt.n_players))
            {
              leader = decls_declarant (game->decls);
            }
          error = error || handfuls_set_leader (game->handfuls, leader);
          error = error || handfuls_start (game->handfuls);
          error = error || tricks_set_leader (&(game->tricks), leader);
          assert (error == 0);
          (void) error;
          game_set_step (game, TAROT_TRICKS);
        }
      break;
    case TAROT_TRICKS:
      if (!tricks_has_next (&(game->tricks)))
        {
          TarotPlayer p;
          size_t n_zero = 0;
          TarotHand empty_hand = game_create_hand (0, &n_zero, NULL);
          if (doscard_infer (&(game->doscard), &(game->tricks)) != 0)
            {
              return 1;
            }
          for (p = 0; p < game->opt.n_players; p++)
            {
              int set_empty = 0;
              set_empty = hands_set (&(game->hands), p, &empty_hand);
              if (set_empty != 0)
                {
                  return 1;
                }
            }
          if (game->opt.with_call)
            {
              const TarotCard *buffer_discard;
              size_t n_discard;
              size_t i;
              buffer_discard = game->doscard.discard.buffer;
              n_discard = *(game->doscard.discard.n);
              for (i = 0; i < n_discard; i++)
                {
                  TarotCard c = buffer_discard[i];
                  if (call_doscard_card (game->call, c) != 0)
                    {
                      return 1;
                    }
                }
            }
          game_set_step (game, TAROT_END);
        }
      break;
    default:
      break;
    }
  return 0;
}

static inline int
game_add_deal_all (TarotGame * game_not_generalized, TarotGameHandle * game,
                   const TarotPlayer * owners)
{
  int error = (game_check_deal_all (game, owners) == 0);
  TarotCard i;
  if (error == 0)
    {
      *(game->has_duplicate_deal) = 0;
      game_set_dealt (game, game->opt.n_players);
      error = error || hands_set_all (&(game->hands), owners);
      error = error || game_step_advance (game_not_generalized);
      for (i = 0; i < 78; i++)
        {
          if (owners[i] >= game->opt.n_players)
            {
              error = error
                || doscard_add_unplayed_card (&(game->doscard), i);
            }
        }
      assert (error == 0);
    }
  return error;
}

static inline int
game_add_deal (TarotGame * game_not_generalized, TarotGameHandle * game,
               TarotPlayer myself, const TarotHand * cards)
{
  int error = (game_check_deal (game, myself, cards) == 0);
  if (error == 0)
    {
      *(game->has_duplicate_deal) = 0;
      game_set_dealt (game, myself);
      error = error || hands_set (&(game->hands), myself, cards);
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
    }
  return error;
}

static inline int
game_add_bid (TarotGame * game_not_generalized, TarotGameHandle * game,
              TarotBid next)
{
  int error = (game_check_bid (game, next) == 0);
  if (error == 0)
    {
      error = error || bids_add (game->bids, next, game->opt.n_players);
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
      *(game->has_duplicate_taker) = 0;
    }
  return error;
}

static inline int
game_add_decl (TarotGame * game_not_generalized, TarotGameHandle * game,
               int next)
{
  int error = (game_check_decl (game, next) == 0);
  if (error == 0)
    {
      error = error || decls_add (game->decls, next, game->opt.n_players);
      assert (error == 0);
      error = game_step_advance (game_not_generalized);
    }
  return error;
}

static inline int
game_add_call (TarotGame * game_not_generalized, TarotGameHandle * game,
               TarotCard call)
{
  int error = (game_check_call (game_not_generalized, game, call) == 0);
  if (error == 0)
    {
      TarotPlayer i;
      TarotHand *h;
      *(game->has_duplicate_call) = 0;
      error = error || call_add (game->call, bids_taker (game->bids), call);
      for (i = 0; i < game->opt.n_players; ++i)
        {
          if (hands_known (&(game->hands), i))
            {
              size_t i_card;
              h = &(game->hands.hands[i]);
              for (i_card = 0; i_card < *(h->n); ++i_card)
                {
                  TarotCard c = h->buffer[i_card];
                  error = error || call_owned_card (game->call, i, c);
                }
            }
        }
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
    }
  return error;
}

static inline int
game_add_dog (TarotGame * game_not_generalized, TarotGameHandle * game,
              const TarotHand * dog)
{
  int error = (game_check_dog (game, dog) == 0);
  if (error == 0)
    {
      error = error || doscard_reveal_dog (&(game->doscard), dog);
      if (game->opt.with_call)
        {
          size_t i_card;
          for (i_card = 0; i_card < *(dog->n); ++i_card)
            {
              TarotCard c = dog->buffer[i_card];
              error = error || call_doscard_card (game->call, c);
            }
        }
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
    }
  return error;
}

static inline int
game_can_autoreveal_aux (const TarotGame * game, TarotHand * dog)
{
  TarotGameHandle h;
  game_generalize ((TarotGame *) game, &h);
  if (doscard_can_autoreveal (&(h.doscard), dog))
    {
      return game_check_dog (&h, dog);
    }
  return 0;
}

static inline int
game_can_autoreveal (const TarotGame * game, size_t *n_dog, size_t start,
                     size_t max, TarotCard * dog)
{
  TarotCard buffer[6];
  size_t n = 0;
  TarotHand h = {
    .n = &n,
    .n_max = sizeof (buffer) / sizeof (buffer[0]),
    .buffer = buffer
  };
  int ret = game_can_autoreveal_aux (game, &h);
  assert (n <= sizeof (buffer) / sizeof (buffer[0]));
  *n_dog = n;
  if (start > n)
    {
      start = n;
    }
  if (start + max > n)
    {
      max = n - start;
    }
  if (max != 0)
    {
      memcpy (dog, buffer + start, max * sizeof (TarotCard));
    }
  return ret;
}

static inline int
game_add_discard (TarotGame * game_not_generalized, TarotGameHandle * game,
                  const TarotHand * discard)
{
  TarotPlayer taker;
  TarotHand *takers_hand;
  TarotHand *dog;
  int error = (game_check_discard (game_not_generalized, game, discard) == 0);
  if (error == 0)
    {
      TarotPlayer i;
      taker = bids_taker (game->bids);
      takers_hand = &(game->hands.hands[taker]);
      dog = &(game->doscard.dog);
      if (hands_known (&(game->hands), taker))
        {
          error = error || doscard_set_discard (&(game->doscard), discard);
          error = error || hand_swap (takers_hand, dog, discard);
        }
      else
        {
          error = error
            || doscard_set_partial_discard (&(game->doscard), discard);
        }
      if (game->opt.with_call)
        {
          for (i = 0; i < game->opt.n_players; ++i)
            {
              if (hands_known (&(game->hands), i))
                {
                  size_t i_card;
                  for (i_card = 0; i_card < *(discard->n); ++i_card)
                    {
                      TarotCard c = discard->buffer[i_card];
                      error = error || call_doscard_card (game->call, c);
                    }
                }
            }
        }
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
    }
  return error;
}

static inline int
game_add_handful (TarotGame * game_not_generalized, TarotGameHandle * game,
                  const TarotHand * handful)
{
  int error = (game_check_handful (game_not_generalized, game, handful) == 0);
  if (error == 0)
    {
      error = error
        || handfuls_add (game->handfuls, handful, game->opt.n_players);
      error = error || game_step_advance (game_not_generalized);
    }
  return error;
}

static inline TarotPlayer
get_partner_or (const TarotGameHandle * game, TarotPlayer taker)
{
  if (game->opt.with_call && call_partner_known (game->call))
    {
      return call_partner (game->call);
    }
  return taker;
}

static inline int
game_add_card_aux (TarotGame * game_not_generalized, TarotGameHandle * game,
                   TarotCard card)
{
  int cnf;
  TarotSuit lead_suit;
  int cno;
  TarotNumber max_trump;
  int cnu;
  int check =
    game_check_card_full_aux (game_not_generalized, game, card, &cnf,
                              &lead_suit, &cno, &max_trump,
                              &cnu);
  int error = (check == 0);
  if (error == 0)
    {
      TarotPlayer taker = bids_taker (game->bids);
      TarotPlayer partner = get_partner_or (game, taker);
      TarotPlayer next = tricks_next (&(game->tricks));
      error = error || tricks_add (&(game->tricks), taker, partner, card);
      assert (error == 0);
      if (game->opt.with_call && !call_partner_known (game->call))
        {
          error = error || call_trick_card (game->call, next, card);
          assert (error == 0);
        }
      error = error || game_step_advance (game_not_generalized);
      assert (error == 0);
      error = error || handfuls_card (game->handfuls, game->opt.n_players);
      assert (error == 0);
      hands_remove (&(game->hands), next, card);
      assert (error == 0);
      if (tricks_count_remaining_cards (&(game->tricks), next) == 0)
        {
          TarotHand empty;
          size_t n = 0;
          empty.n = &n;
          empty.n_max = 0;
          if (hands_known (&(game->hands), next) == 0)
            {
              if (hands_set (&(game->hands), next, &empty) != 0)
                {
                  assert (0);
                }
            }
        }
    }
  return error;
}

static inline int
game_add_event_aux (TarotGame * game, const TarotGameEvent * event)
{
  size_t n_players;
  int with_call;
  int decl;
  int error = 0;
  TarotPlayer myself;
  TarotPlayer *owners;
  TarotHand hand;
  TarotBid bid;
  TarotCard card;
  size_t hand_n = 0;
  TarotCard buffer[78];
  TarotGameHandle h;
  hand.n = &hand_n;
  hand.n_max = 78;
  hand.buffer = buffer;
  game_generalize (game, &h);
  if (game_check_event (game, event) == 0)
    {
      return 1;
    }
  assert (error == 0);
  switch (event_type (event))
    {
    case TAROT_SETUP_EVENT:
      do
        {
          /* Back up the duplicaté options! */
          int has_duplicate_deal = game->has_duplicate_deal;
          int has_duplicate_taker = game->has_duplicate_taker;
          int has_duplicate_call = game->has_duplicate_call;
          if (event_get_setup (event, &n_players, &with_call) !=
              TAROT_EVENT_OK)
            {
              assert (0);
            }
          /* game_initialize forgets the duplicaté options, but it
           * does not overwrite them! */
          game_initialize (game, n_players, with_call);
          /* So we can restore the duplicaté options */
          game->has_duplicate_deal = has_duplicate_deal;
          game->has_duplicate_taker = has_duplicate_taker;
          game->has_duplicate_call = has_duplicate_call;
          game_generalize (game, &h);
        }
      while (0);
      return game_step_advance (game);
    case TAROT_DEAL_EVENT:
      copy_cards (&hand, event->n, event->data);
      myself = event->u.deal;
      return game_add_deal (game, &h, myself, &hand);
    case TAROT_DEAL_ALL_EVENT:
      n_players = event->n;
      owners = event->data;
      return (n_players == 78 && game_add_deal_all (game, &h, owners));
    case TAROT_BID_EVENT:
      error = event_get_bid (event, &bid);
      assert (error == 0);
      return game_add_bid (game, &h, bid);
    case TAROT_DECL_EVENT:
      error = event_get_decl (event, &decl);
      assert (error == 0);
      return game_add_decl (game, &h, decl);
    case TAROT_CALL_EVENT:
      error = event_get_call (event, &card);
      assert (error == 0);
      return game_add_call (game, &h, card);
    case TAROT_DOG_EVENT:
      copy_cards (&hand, event->n, event->data);
      return game_add_dog (game, &h, &hand);
    case TAROT_DISCARD_EVENT:
      copy_cards (&hand, event->n, event->data);
      return game_add_discard (game, &h, &hand);
    case TAROT_HANDFUL_EVENT:
      copy_cards (&hand, event->n, event->data);
      return game_add_handful (game, &h, &hand);
    case TAROT_CARD_EVENT:
      error = event_get_card (event, &card);
      assert (error == 0);
      return game_add_card_aux (game, &h, card);
    }
  (void) error;
  return 0;
}

static inline TarotGameError
game_add_event (TarotGame * game, const TarotGameEvent * event)
{
  int has_error = game_add_event_aux (game, event);
  if (has_error)
    {
      return TAROT_GAME_INVEV;
    }
  return TAROT_GAME_OK;
}

static inline int
game_assign_missing_card (TarotGame * game, TarotCard card, TarotPlayer owner)
{
  TarotGameHandle h;
  game_generalize (game, &h);
  if (owner < game->opt.n_players)
    {
      size_t n_remaining = tricks_count_remaining_cards (&(h.tricks), owner);
      if (doscard_dog_known (&(h.doscard))
          && !doscard_discard_known (&(h.doscard))
          && hand_has (&(h.doscard.dog), card))
        {
          /* This is a special case: if we're between the dog and the
             discard, and this is a card of the dog, then in fact we
             give it to the dog */
          return game_assign_missing_card (game, card, h.opt.n_players);
        }
      if (hands_assign_missing_card
          (&(h.hands), card, owner, n_remaining) != 0)
        {
          return 1;
        }
      if (h.opt.with_call && call_card_known (h.call)
          && !call_partner_known (h.call))
        {
          if (call_trick_card (h.call, owner, card) != 0)
            {
              assert (0);
            }
        }
    }
  else
    {
      if (doscard_add_unplayed_card (&(h.doscard), card) != 0)
        {
          return 1;
        }
      if (h.opt.with_call && call_card_known (h.call)
          && !call_partner_known (h.call))
        {
          if (call_doscard_card (h.call, card) != 0)
            {
              assert (0);
            }
        }
    }
  /* If we're doing one assignment, then the others will follow; so we
     shift from the deal to one player, to the full deal. */
  *(h.dealt) = h.opt.n_players;
  return 0;
}

static inline int
game_iterator_has_player (const TarotGameIterator * iterator,
                          TarotPlayer * next)
{
  TarotPlayer leader;
  size_t n_players = game_n_players (iterator->handle);
  switch (iterator->step)
    {
    case TAROT_SETUP:
      return 0;
    case TAROT_DEAL:
      return 0;
    case TAROT_BIDS:
      *next = iterator->i_player;
      return 1;
    case TAROT_DECLS:
      *next = iterator->i_player;
      return 1;
    case TAROT_CALL:
      if (game_get_taker (iterator->handle, next) != TAROT_GAME_OK)
        {
          assert (0);
        }
      return 1;
    case TAROT_DOG:
      return 0;
    case TAROT_DISCARD:
      if (game_get_taker (iterator->handle, next) != TAROT_GAME_OK)
        {
          assert (0);
        }
      return 1;
    case TAROT_TRICKS:
      if (game_get_trick_leader (iterator->handle, iterator->i_trick, &leader)
          != TAROT_GAME_OK)
        {
          assert (0);
        }
      *next = (leader + (iterator->i_player)) % n_players;
      return 1;
    case TAROT_END:
      return 0;
    }
  return 0;
}

static inline const TarotGameEvent *
game_iterator_next_value (TarotGameIterator * iterator)
{
  TarotCard cards_buffer[78];
  TarotPlayer player_buffer[78];
  size_t n_owners;
  TarotHand cards;
  size_t n_cards;
  TarotBid bid;
  int declaration;
  TarotCard card;
  TarotPlayer next;
  TarotPlayer main_player;
  int has_main_player = 0;
  TarotPlayer taker;
  int has_taker = 0;
  TarotBid contract;
  int has_contract = 0;
  size_t n_bids;
  const TarotGameEvent *ret = NULL;
  cards.n_max = 78;
  cards.n = &n_cards;
  cards.buffer = cards_buffer;
  has_main_player =
    (game_get_main_player (iterator->handle, &main_player) == TAROT_GAME_OK);
  has_taker = (game_get_taker (iterator->handle, &taker) == TAROT_GAME_OK);
  has_contract = (has_taker
                  &&
                  (game_get_bids
                   (iterator->handle, &n_bids, taker, 1,
                    &contract) == TAROT_GAME_OK) && n_bids > taker);
  switch (iterator->step)
    {
    case TAROT_SETUP:
      event_init_setup (&(iterator->last_event),
                        game_n_players (iterator->handle),
                        game_with_call (iterator->handle));
      iterator->step = TAROT_DEAL;
      ret = &(iterator->last_event);
      break;
    case TAROT_DEAL:
      if (has_main_player)
        {
          if (game_get_deal_of
              (iterator->handle, main_player, cards.n, 0, cards.n_max,
               cards.buffer) != TAROT_GAME_OK)
            {
              assert (0);
            }
          event_init_deal (&(iterator->last_event), main_player, n_cards,
                           cards_buffer,
                           sizeof (iterator->event_data) /
                           sizeof (iterator->event_data[0]),
                           iterator->event_data);
          iterator->step = TAROT_BIDS;
          iterator->i_player = 0;
          ret = &(iterator->last_event);
        }
      else if (game_get_deal_all (iterator->handle, &n_owners, 0, 78,
                                  player_buffer) == TAROT_GAME_OK)
        {
          event_init_deal_all (&(iterator->last_event), n_owners,
                               player_buffer,
                               sizeof (iterator->event_data) /
                               sizeof (iterator->event_data[0]),
                               iterator->event_data);
          iterator->step = TAROT_BIDS;
          iterator->i_player = 0;
          ret = &(iterator->last_event);
        }
      break;
    case TAROT_BIDS:
      next = iterator->i_player;
      {
        if (game_get_bids (iterator->handle, &n_bids, next, 1, &bid) ==
            TAROT_GAME_OK && n_bids > next)
          {
            event_init_bid (&(iterator->last_event), bid);
            (iterator->i_player)++;
            if (bid == TAROT_DOUBLE_KEEP)
              {
                iterator->i_player = game_n_players (iterator->handle);
              }
            if (iterator->i_player >= game_n_players (iterator->handle))
              {
                iterator->step = TAROT_DECLS;
                iterator->i_player = 0;
              }
            ret = &(iterator->last_event);
          }
      }
      break;
    case TAROT_DECLS:
      next = iterator->i_player;
      {
        size_t n_decls;
        if (game_get_declarations (iterator->handle, &n_decls, next, 1,
                                   &declaration)
            == TAROT_GAME_OK && n_decls > next)
          {
            event_init_decl (&(iterator->last_event), declaration);
            (iterator->i_player)++;
            if (declaration)
              {
                iterator->i_player = game_n_players (iterator->handle);
              }
            if (iterator->i_player >= game_n_players (iterator->handle))
              {
                assert (has_taker);
                assert (has_contract);
                if (game_with_call (iterator->handle))
                  {
                    iterator->step = TAROT_CALL;
                  }
                else if (contract < TAROT_STRAIGHT_KEEP)
                  {
                    iterator->step = TAROT_DOG;
                  }
                else
                  {
                    iterator->step = TAROT_TRICKS;
                    iterator->i_trick = 0;
                    iterator->i_player = 0;
                    iterator->handful_done = 0;
                  }
              }
            ret = &(iterator->last_event);
          }
      }
      break;
    case TAROT_CALL:
      if (game_get_call (iterator->handle, &card) == TAROT_GAME_OK)
        {
          event_init_call (&(iterator->last_event), card);
          assert (has_contract);
          if (contract < TAROT_STRAIGHT_KEEP)
            {
              iterator->step = TAROT_DOG;
            }
          else
            {
              iterator->step = TAROT_TRICKS;
              iterator->i_trick = 0;
              iterator->i_player = 0;
              iterator->handful_done = 0;
            }
          ret = &(iterator->last_event);
        }
      break;
    case TAROT_DOG:
      if (game_get_dog
          (iterator->handle, cards.n, 0, cards.n_max,
           cards.buffer) == TAROT_GAME_OK)
        {
          event_init_dog (&(iterator->last_event), n_cards, cards_buffer,
                          sizeof (iterator->event_data) /
                          sizeof (iterator->event_data[0]),
                          iterator->event_data);
          iterator->step = TAROT_DISCARD;
          ret = &(iterator->last_event);
        }
      break;
    case TAROT_DISCARD:
      assert (has_taker);
      {
        int ok = 0;
        if ((has_main_player && main_player == taker) || !has_main_player)
          {
            if (game_get_full_discard (iterator->handle, cards.n,
                                       0, cards.n_max,
                                       cards.buffer) == TAROT_GAME_OK)
              {
                ok = 1;
              }
          }
        else
          {
            if (game_get_public_discard (iterator->handle,
                                         cards.n, 0,
                                         cards.n_max,
                                         cards.buffer) == TAROT_GAME_OK)
              {
                ok = 1;
              }
          }
        if (ok)
          {
            event_init_discard (&(iterator->last_event), n_cards,
                                cards_buffer,
                                sizeof (iterator->event_data) /
                                sizeof (iterator->event_data[0]),
                                iterator->event_data);
            iterator->step = TAROT_TRICKS;
            iterator->i_trick = 0;
            iterator->i_player = 0;
            iterator->handful_done = 0;
            ret = &(iterator->last_event);
          }
      }
      break;
    case TAROT_TRICKS:
      if (iterator->i_trick == 0 && !iterator->handful_done)
        {
          int size;
          TarotPlayer leader, p;
          size_t n_players = game_n_players (iterator->handle);
          if (game_get_trick_leader (iterator->handle, 0, &leader)
              != TAROT_GAME_OK)
            {
              assert (0);
            }
          p = (leader + (iterator->i_player)) % n_players;
          n_cards = 0;
          if (game_get_handful (iterator->handle,
                                p, &size, cards.n,
                                0, cards.n_max,
                                cards.buffer) == TAROT_GAME_OK)
            {
              event_init_handful (&(iterator->last_event), n_cards,
                                  cards_buffer,
                                  sizeof (iterator->event_data) /
                                  sizeof (iterator->event_data[0]),
                                  iterator->event_data);
              iterator->handful_done = 1;
              ret = &(iterator->last_event);
            }
          else
            {
              iterator->handful_done = 1;
              ret = game_iterator_next_value (iterator);
            }
        }
      else
        {
          TarotGameCardAnalysis c;
          if (game_get_one_trick_card (iterator->handle,
                                       iterator->i_trick,
                                       iterator->i_player,
                                       &c) == TAROT_GAME_OK)
            {
              size_t n_players = game_n_players (iterator->handle);
              event_init_card (&(iterator->last_event), c.card);
              iterator->i_player += 1;
              iterator->handful_done = 0;
              ret = &(iterator->last_event);
              if (iterator->i_player >= n_players)
                {
                  size_t n_tricks = game_n_tricks (iterator->handle);
                  iterator->i_trick += 1;
                  iterator->i_player = 0;
                  if (iterator->i_trick >= n_tricks)
                    {
                      iterator->step = TAROT_END;
                    }
                }
            }
        }
      break;
    case TAROT_END:
      break;
    }
  return ret;
}

#include <tarot/hands_private_impl.h>
#include <tarot/bids_private_impl.h>
#include <tarot/decls_private_impl.h>
#include <tarot/call_private_impl.h>
#include <tarot/doscard_private_impl.h>
#include <tarot/handfuls_private_impl.h>
#include <tarot/tricks_private_impl.h>
#include <tarot/trick_private_impl.h>
#include <tarot/game_event_private_impl.h>
#include <tarot/counter_private_impl.h>

#endif /* H_TAROT_GAME_PRIVATE_IMPL_INCLUDED */
