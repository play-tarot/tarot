/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/game_private.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "xalloc.h"

TarotGame *
tarot_game_alloc (void)
{
  TarotGame *game = xmalloc (sizeof (TarotGame));
  game_initialize (game, 4, 0);
  return game;
}

TarotGame *
tarot_game_dup (const TarotGame * source)
{
  TarotGame *ret = tarot_game_alloc ();
  tarot_game_copy (ret, source);
  return ret;
}

void
tarot_game_free (TarotGame * game)
{
  free (game);
}

void
tarot_game_copy (TarotGame * dest, const TarotGame * source)
{
  game_copy (dest, source);
}

TarotGameError
tarot_game_copy_as (TarotGame * dest, const TarotGame * source,
                    TarotPlayer who)
{
  return game_copy_as (dest, source, who);
}

TarotStep
tarot_game_step (const TarotGame * game)
{
  return game_step (game);
}

TarotGameError
tarot_game_duplicate_set_deal (TarotGame * game, size_t n_owners,
                               const TarotPlayer * owners)
{
  return game_duplicate_set_deal (game, n_owners, owners);
}

TarotGameError
tarot_game_duplicate_set_taker (TarotGame * game, TarotPlayer taker)
{
  return game_duplicate_set_taker (game, taker);
}

TarotGameError
tarot_game_duplicate_set_call (TarotGame * game, TarotCard call)
{
  return game_duplicate_set_call (game, call);
}

TarotGameIterator *
tarot_game_iterator (const TarotGame * game)
{
  TarotGameIterator *ret = xmalloc (sizeof (TarotGameIterator));
  game_iterator_setup (ret, game);
  return ret;
}

TarotGameIterator *
tarot_game_iterator_dup (const TarotGameIterator * iterator)
{
  /* Not implemented yet */
  (void) iterator;
  return NULL;
}

void
tarot_game_iterator_free (TarotGameIterator * iterator)
{
  free (iterator);
}

size_t
tarot_game_n_players (const TarotGame * game)
{
  return game_n_players (game);
}

int
tarot_game_with_call (const TarotGame * game)
{
  return game_with_call (game);
}

TarotGameError
tarot_game_get_next (const TarotGame * game, TarotPlayer * next)
{
  return game_get_next (game, next);
}

TarotGameError
tarot_game_get_main_player (const TarotGame * game, TarotPlayer * main)
{
  return game_get_main_player (game, main);
}

TarotGameError
tarot_game_get_deal_all (const TarotGame * game, size_t *n, size_t start,
                         size_t max, TarotPlayer * owners)
{
  return game_get_deal_all (game, n, start, max, owners);
}

TarotGameError
tarot_game_get_deal_all_alloc (const TarotGame * game,
                               size_t *n, TarotPlayer ** owners)
{
  size_t my_n;
  TarotGameError ret = game_get_deal_all (game, &my_n, 0, 0, NULL);
  *n = 0;
  *owners = NULL;
  if (ret == TAROT_GAME_OK)
    {
      *n = my_n;
      *owners = xmalloc (my_n * sizeof (TarotPlayer));
      ret = game_get_deal_all (game, &my_n, 0, my_n, *owners);
      assert (ret == TAROT_GAME_OK);
      assert (my_n == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_deal_of (const TarotGame * game, TarotPlayer who, size_t *n,
                        size_t start, size_t max, TarotCard * deal)
{
  return game_get_deal_of (game, who, n, start, max, deal);
}

TarotGameError
tarot_game_get_deal_of_alloc (const TarotGame * game, TarotPlayer who,
                              size_t *n, TarotCard ** cards)
{
  size_t my_n;
  TarotGameError ret = game_get_deal_of (game, who, &my_n, 0, 0, NULL);
  *n = 0;
  *cards = NULL;
  if (ret == TAROT_GAME_OK)
    {
      *n = my_n;
      *cards = xmalloc (my_n * sizeof (TarotCard));
      ret = game_get_deal_of (game, who, &my_n, 0, my_n, *cards);
      assert (ret == TAROT_GAME_OK);
      assert (my_n == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_cards (const TarotGame * game, TarotPlayer who, size_t *n,
                      size_t start, size_t max, TarotCard * cards)
{
  return game_get_cards (game, who, n, start, max, cards);
}

TarotGameError
tarot_game_get_cards_alloc (const TarotGame * game, TarotPlayer who,
                            size_t *n, TarotCard ** cards)
{
  size_t my_n;
  TarotGameError ret = game_get_cards (game, who, &my_n, 0, 0, NULL);
  *n = 0;
  *cards = NULL;
  if (ret == TAROT_GAME_OK)
    {
      *n = my_n;
      *cards = xmalloc (my_n * sizeof (TarotCard));
      ret = game_get_cards (game, who, &my_n, 0, my_n, *cards);
      assert (ret == TAROT_GAME_OK);
      assert (my_n == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_bids (const TarotGame * game, size_t *n, TarotPlayer start,
                     size_t max, TarotBid * bids)
{
  return game_get_bids (game, n, start, max, bids);
}

TarotGameError
tarot_game_get_bids_alloc (const TarotGame * game, size_t *n,
                           TarotBid ** bids)
{
  TarotGameError ret = game_get_bids (game, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *bids = xmalloc (*n * sizeof (TarotBid));
      ret2 = game_get_bids (game, &n2, 0, *n, *bids);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_taker (const TarotGame * game, TarotPlayer * taker)
{
  return game_get_taker (game, taker);
}

TarotGameError
tarot_game_get_declarations (const TarotGame * game, size_t *n,
                             TarotPlayer start, size_t max, int *decls)
{
  return game_get_declarations (game, n, start, max, decls);
}

TarotGameError
tarot_game_get_declarations_alloc (const TarotGame * game, size_t *n,
                                   int **declarations)
{
  TarotGameError ret = game_get_declarations (game, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *declarations = xmalloc (*n * sizeof (int));
      ret2 = game_get_declarations (game, &n2, 0, *n, *declarations);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_declarant (const TarotGame * game, TarotPlayer * declarant)
{
  return game_get_declarant (game, declarant);
}

TarotGameError
tarot_game_get_call (const TarotGame * game, TarotCard * card)
{
  return game_get_call (game, card);
}

TarotGameError
tarot_game_get_partner (const TarotGame * game, TarotPlayer * partner)
{
  return game_get_partner (game, partner);
}

TarotGameError
tarot_game_get_dog (const TarotGame * game, size_t *n, size_t start,
                    size_t max, TarotCard * cards)
{
  return game_get_dog (game, n, start, max, cards);
}

TarotGameError
tarot_game_get_dog_alloc (const TarotGame * game, size_t *n,
                          TarotCard ** cards)
{
  size_t my_n;
  TarotGameError ret = game_get_dog (game, &my_n, 0, 0, NULL);
  *n = 0;
  *cards = NULL;
  if (ret == TAROT_GAME_OK)
    {
      *n = my_n;
      *cards = xmalloc (my_n * sizeof (TarotCard));
      ret = game_get_dog (game, &my_n, 0, my_n, *cards);
      assert (ret == TAROT_GAME_OK);
      assert (my_n == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_full_discard (const TarotGame * game, size_t *n, size_t start,
                             size_t max, TarotCard * cards)
{
  return game_get_full_discard (game, n, start, max, cards);
}

TarotGameError
tarot_game_get_full_discard_alloc (const TarotGame * game, size_t *n,
                                   TarotCard ** cards)
{
  TarotGameError ret = game_get_full_discard (game, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *cards = xmalloc (*n * sizeof (TarotCard));
      ret2 = game_get_full_discard (game, &n2, 0, *n, *cards);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_public_discard (const TarotGame * game, size_t *n,
                               size_t start, size_t max, TarotCard * cards)
{
  return game_get_public_discard (game, n, start, max, cards);
}

TarotGameError
tarot_game_get_public_discard_alloc (const TarotGame * game, size_t *n,
                                     TarotCard ** cards)
{
  TarotGameError ret = game_get_public_discard (game, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *cards = xmalloc (*n * sizeof (TarotCard));
      ret2 = game_get_public_discard (game, &n2, 0, *n, *cards);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_handful (const TarotGame * game, TarotPlayer p, int *size,
                        size_t *n_cards, size_t start, size_t max,
                        TarotCard * cards)
{
  return game_get_handful (game, p, size, n_cards, start, max, cards);
}

TarotGameError
tarot_game_get_handful_alloc (const TarotGame * game, TarotPlayer p,
                              int *size, size_t *n, TarotCard ** cards)
{
  TarotGameError ret = game_get_handful (game, p, size, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *cards = xmalloc (*n * sizeof (TarotCard));
      ret2 = game_get_handful (game, p, size, &n2, 0, *n, *cards);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

size_t
tarot_game_n_tricks (const TarotGame * game)
{
  return game_n_tricks (game);
}

TarotGameError
tarot_game_get_current_trick (const TarotGame * game, size_t *current)
{
  return game_get_current_trick (game, current);
}

TarotGameError
tarot_game_get_lead_suit (const TarotGame * game, size_t i_trick,
                          TarotSuit * lead_suit)
{
  return game_get_lead_suit (game, i_trick, lead_suit);
}

TarotGameError
tarot_game_get_max_trump (const TarotGame * game, size_t i_trick,
                          TarotNumber * max_trump)
{
  return game_get_max_trump (game, i_trick, max_trump);
}

TarotGameError
tarot_game_get_trick_leader (const TarotGame * game, size_t i_trick,
                             TarotPlayer * leader)
{
  return game_get_trick_leader (game, i_trick, leader);
}

TarotGameError
tarot_game_get_trick_taker (const TarotGame * game, size_t i_trick,
                            TarotPlayer * taker)
{
  return game_get_trick_taker (game, i_trick, taker);
}

TarotGameError
tarot_game_get_trick_cards (const TarotGame * game, size_t i_trick,
                            size_t *n_cards, size_t start, size_t max,
                            TarotGameCardAnalysis * cards)
{
  return game_get_trick_cards (game, i_trick, n_cards, start, max, cards);
}

TarotGameError
tarot_game_get_trick_cards_alloc (const TarotGame * game, size_t i_trick,
                                  size_t *n_cards,
                                  TarotGameCardAnalysis ** cards)
{
  size_t my_n;
  TarotGameError ret =
    game_get_trick_cards (game, i_trick, &my_n, 0, 0, NULL);
  *n_cards = 0;
  *cards = NULL;
  if (ret == TAROT_GAME_OK)
    {
      *n_cards = my_n;
      *cards = xmalloc (my_n * sizeof (TarotGameCardAnalysis));
      ret = game_get_trick_cards (game, i_trick, &my_n, 0, my_n, *cards);
      assert (ret == TAROT_GAME_OK);
      assert (my_n == *n_cards);
    }
  return ret;
}

TarotGameError
tarot_game_get_points_in_trick (const TarotGame * game, size_t i_trick,
                                unsigned int *halfpoints,
                                unsigned int *oudlers)
{
  return game_get_points_in_trick (game, i_trick, halfpoints, oudlers);
}

TarotGameError
tarot_game_get_scores (const TarotGame * game, size_t *n, size_t start,
                       size_t max, int *scores)
{
  return game_get_scores (game, n, start, max, scores);
}

TarotGameError
tarot_game_get_scores_alloc (const TarotGame * game, size_t *n, int **scores)
{
  TarotGameError ret = game_get_scores (game, n, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *scores = xmalloc (*n * sizeof (int));
      ret2 = game_get_scores (game, &n2, 0, *n, *scores);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

TarotGameError
tarot_game_get_hint_bid (const TarotGame * game, TarotPlayer * player,
                         TarotBid * mini)
{
  return game_get_hint_bid (game, player, mini);
}

TarotGameError
tarot_game_get_hint_decl (const TarotGame * game, TarotPlayer * player,
                          int *allowed)
{
  return game_get_hint_decl (game, player, allowed);
}

TarotGameError
tarot_game_get_hint_call (const TarotGame * game, TarotPlayer * player,
                          TarotNumber * mini)
{
  return game_get_hint_call (game, player, mini);
}

TarotGameError
tarot_game_get_hint_discard (const TarotGame * game, TarotPlayer * player,
                             size_t *n_cards, size_t *n_prio,
                             size_t start_prio, size_t max_prio,
                             TarotCard * prio, size_t *n_additional,
                             size_t start_additional, size_t max_additional,
                             TarotCard * additional)
{
  return game_get_hint_discard (game, player, n_cards, n_prio, start_prio,
                                max_prio, prio, n_additional,
                                start_additional, max_additional, additional);
}

TarotGameError
tarot_game_get_hint_discard_alloc (const TarotGame * game,
                                   TarotPlayer * player, size_t *n_cards,
                                   size_t *n_prio, TarotCard ** prio,
                                   size_t *n_additional,
                                   TarotCard ** additional)
{
  TarotGameError ret =
    game_get_hint_discard (game, player, n_cards, n_prio, 0, 0, NULL,
                           n_additional, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t np2, na2;
      TarotGameError ret2;
      *prio = xmalloc (*n_prio * sizeof (TarotCard));
      *additional = xmalloc (*n_additional * sizeof (TarotCard));
      ret2 =
        game_get_hint_discard (game, player, n_cards, &np2, 0, *n_prio,
                               *prio, &na2, 0, *n_additional, *additional);
      (void) ret2;
      (void) np2;
      (void) na2;
      assert (ret2 == ret);
      assert (np2 == *n_prio);
      assert (na2 == *n_additional);
    }
  return ret;
}

TarotGameError
tarot_game_get_hint_handful (const TarotGame * game, TarotPlayer * player,
                             size_t *n_simple, size_t *n_double,
                             size_t *n_triple, size_t *n_prio,
                             size_t start_prio, size_t max_prio,
                             TarotCard * prio, size_t *n_additional,
                             size_t start_additional, size_t max_additional,
                             TarotCard * additional)
{
  return game_get_hint_handful (game, player, n_simple, n_double, n_triple,
                                n_prio, start_prio, max_prio, prio,
                                n_additional, start_additional,
                                max_additional, additional);
}

TarotGameError
tarot_game_get_hint_handful_alloc (const TarotGame * game,
                                   TarotPlayer * player, size_t *n_simple,
                                   size_t *n_double, size_t *n_triple,
                                   size_t *n_prio, TarotCard ** prio,
                                   size_t *n_additional,
                                   TarotCard ** additional)
{
  TarotGameError ret =
    game_get_hint_handful (game, player, n_simple, n_double, n_triple,
                           n_prio, 0, 0, NULL, n_additional, 0, 0,
                           NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t np2, na2;
      TarotGameError ret2;
      *prio = xmalloc (*n_prio * sizeof (TarotCard));
      *additional = xmalloc (*n_additional * sizeof (TarotCard));
      ret2 =
        game_get_hint_handful (game, player, n_simple, n_double,
                               n_triple, &np2, 0, *n_prio, *prio, &na2,
                               0, *n_additional, *additional);
      (void) ret2;
      (void) np2;
      (void) na2;
      assert (ret2 == ret);
      assert (np2 == *n_prio);
      assert (na2 == *n_additional);
    }
  return ret;
}

TarotGameError
tarot_game_get_hint_card (const TarotGame * game, TarotPlayer * player,
                          size_t *n, size_t start, size_t max,
                          TarotCard * playable)
{
  return game_get_hint_card (game, player, n, start, max, playable);
}

TarotGameError
tarot_game_get_hint_card_alloc (const TarotGame * game, TarotPlayer * player,
                                size_t *n_playable, TarotCard ** playable)
{
  TarotGameError ret =
    game_get_hint_card (game, player, n_playable, 0, 0, NULL);
  if (ret == TAROT_GAME_OK)
    {
      size_t n2;
      TarotGameError ret2;
      *playable = xmalloc (*n_playable * sizeof (TarotCard));
      ret2 =
        game_get_hint_card (game, player, &n2, 0, *n_playable, *playable);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n_playable);
    }
  return ret;
}

int
tarot_game_check_event (const TarotGame * game, const TarotGameEvent * event)
{
  return game_check_event (game, event);
}

int
tarot_game_check_card (const TarotGame * game, TarotCard card,
                       int *cannot_follow, TarotSuit * lead_suit,
                       int *cannot_overtrump, TarotNumber * max_trump,
                       int *cannot_undertrump)
{
  return game_check_card (game, card, cannot_follow, lead_suit,
                          cannot_overtrump, max_trump, cannot_undertrump);
}

TarotGameError
tarot_game_add_event (TarotGame * game, const TarotGameEvent * event)
{
  return game_add_event (game, event);
}

int
tarot_game_can_autoreveal (const TarotGame * game, size_t *n_dog,
                           size_t start, size_t max, TarotCard * dog)
{
  return game_can_autoreveal (game, n_dog, start, max, dog);
}

int
tarot_game_can_autoreveal_alloc (const TarotGame * game, size_t *n,
                                 TarotCard ** cards)
{
  int ret = game_can_autoreveal (game, n, 0, 0, NULL);
  if (ret)
    {
      size_t n2;
      int ret2;
      *cards = xmalloc (*n * sizeof (TarotCard));
      ret2 = game_can_autoreveal (game, &n2, 0, *n, *cards);
      (void) ret2;
      (void) n2;
      assert (ret2 == ret);
      assert (n2 == *n);
    }
  return ret;
}

int
tarot_game_assign_missing_card (TarotGame * game, TarotCard card,
                                TarotPlayer owner)
{
  return game_assign_missing_card (game, card, owner);
}

const TarotGameEvent *
tarot_game_iterator_next_value (TarotGameIterator * iterator)
{
  return game_iterator_next_value (iterator);
}

#include <tarot/game_private_impl.h>
