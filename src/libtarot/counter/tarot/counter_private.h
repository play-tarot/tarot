/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_COUNTER_PRIVATE_INCLUDED
#define H_TAROT_COUNTER_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/cards.h>
#include <tarot/player.h>
#include <tarot/game.h>
#include <tarot/game_event.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

#define TAROT_COUNTER_CARD_OWNER_UNKNOWN ((TarotPlayer) (-1))
#define TAROT_COUNTER_CARD_OWNER_PLAYED ((TarotPlayer) (-2))
#define TAROT_COUNTER_CARD_OWNER_DISCARDED ((TarotPlayer) (-3))

  static void counter_initialize (TarotCounter * counter);
  static void counter_copy (TarotCounter * dest, const TarotCounter * source);
  static size_t counter_n_cards_remaining (const TarotCounter * counter,
                                           TarotPlayer who);
  static size_t counter_n_cards_doscard (const TarotCounter * counter);
  static int counter_may_own (const TarotCounter * counter, size_t n,
                              const TarotPlayer * who, TarotCard card);
  static int counter_may_be_in_doscard (const TarotCounter * counter,
                                        TarotCard c);
  static void counter_load_from_game (TarotCounter * counter,
                                      const TarotGame * game);

  static TarotImputationError counter_and_game_impute (TarotCounter * counter,
                                                       TarotGame * game,
                                                       size_t seed_size,
                                                       const void *seed);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_COUNTER_PRIVATE_INCLUDED */
