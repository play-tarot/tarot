/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_COUNTER_INCLUDED
#define H_TAROT_COUNTER_INCLUDED

#include <tarot/cards.h>
#include <tarot/player.h>
#include <tarot/game.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotCounter;
  typedef struct TarotCounter TarotCounter;

  /**
   * tarot_counter_alloc: (constructor)
   */
  TarotCounter *tarot_counter_alloc (void);
  TarotCounter *tarot_counter_dup (const TarotCounter * source);
  void tarot_counter_free (TarotCounter * counter);

  void tarot_counter_copy (TarotCounter * dest, const TarotCounter * source);
  size_t tarot_counter_n_cards_remaining (const TarotCounter * counter,
                                          TarotPlayer who);
  size_t tarot_counter_n_cards_doscard (const TarotCounter * counter);

  /**
   * tarot_counter_may_own:
   * @who: (array length=n_query):
   * Returns: (type boolean):
   */
  int tarot_counter_may_own (const TarotCounter * counter, size_t n_query,
                             const TarotPlayer * who, TarotCard card);

  /**
   * tarot_counter_may_be_in_doscard:
   * Returns: (type boolean):
   */
  int tarot_counter_may_be_in_doscard (const TarotCounter * counter,
                                       TarotCard c);

  void tarot_counter_load_from_game (TarotCounter * counter,
                                     const TarotGame * game);

  typedef enum
  {
    TAROT_IMPUTATION_OK = 0,
    TAROT_IMPUTATION_FAILED
  } TarotImputationError;

  /**
   * tarot_counter_and_game_impute:
   * @seed: (array length=seed_size) (element-type char):
   */
  TarotImputationError tarot_counter_and_game_impute (TarotCounter * counter,
                                                      TarotGame * game,
                                                      size_t seed_size,
                                                      const void *seed);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_COUNTER_INCLUDED */
