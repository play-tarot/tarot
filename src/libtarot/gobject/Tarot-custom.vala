// tarot implements the rules of the tarot game
// Copyright (C) 2019  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace Tarot {
	public struct Card : uint {
		public static Tarot.Card parse (string text, out unowned string end);
		public static Tarot.Card parse_c (string text, out unowned string end);
		public size_t to_string (string dest);
		public size_t to_string_c (string dest);
		public string to_string_alloc ();
		public string to_string_c_alloc ();
	}
	public struct Player : uint {
		public static Tarot.Player parse (string text, out unowned string end);
		public static Tarot.Player parse_c (string text, out unowned string end);
		public size_t to_string (string dest);
		public size_t to_string_c (string dest);
		public string to_string_alloc ();
		public string to_string_c_alloc ();
	}
}