# tarot implements the rules of the tarot game
# Copyright (C) 2017, 2018, 2019  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

EXTRA_DIST += \
  %reldir%/tarot-gobject.h.in \
  %reldir%/tarot-gobject.c.in \
  %reldir%/tarot-enums-stamp

INDENTED += %reldir%/tarot-gobject.h.in %reldir%/tarot-gobject.c.in
BUILT_SOURCES += \
  %reldir%/tarot-gobject.h \
  %reldir%/tarot-gobject.c
MAINTAINERCLEANFILES += \
  $(srcdir)/%reldir%/tarot-enums-stamp \
  $(srcdir)/%reldir%/tarot-gobject.h \
  $(srcdir)/%reldir%/tarot-gobject.c

INTROSPECTED_SOURCES += %reldir%/tarot-gobject.c

glib_mkenums_verbose = $(glib_mkenums_verbose_@AM_V@)
glib_mkenums_verbose_ = $(glib_mkenums_verbose_@AM_DEFAULT_V@)
glib_mkenums_verbose_0 = @echo "GLIBMKENUMS $@";

$(srcdir)/%reldir%/tarot-enums-stamp: %reldir%/tarot-gobject.h.in %reldir%/tarot-gobject.c.in $(ENUM_HEADER_FILES)
	@rm -f $(srcdir)/%reldir%/tarot-enums-temp
	@touch $(srcdir)/%reldir%/tarot-enums-temp
	$(glib_mkenums_verbose) cd $(srcdir) \
	  && $(GLIB_MKENUMS)  \
	    --template %reldir%/tarot-gobject.h.in \
	    $(ENUM_HEADER_FILES) \
	    > %reldir%/tarot-gobject.h \
	  && $(GLIB_MKENUMS)  \
	    --template %reldir%/tarot-gobject.c.in \
	    $(ENUM_HEADER_FILES) \
	    > %reldir%/tarot-gobject.c
	@mv -f $(srcdir)/%reldir%/tarot-enums-temp $(srcdir)/%reldir%/tarot-enums-stamp

$(srcdir)/%reldir%/tarot-gobject.h $(srcdir)/%reldir%/tarot-gobject.c: %reldir%/tarot-enums-stamp
	@dry=; for f in x $$MAKEFLAGS; do \
          case $$f in \
            *=*|--*);; \
            *n*) dry=:;; \
          esac; \
        done; \
        if test -f $@; then :; else \
          $$dry trap 'rm -rf $(srcdir)/%reldir%/tarot-enums-lock $(srcdir)/%reldir%/tarot-enums-stamp' 1 2 13 15; \
          if $$dry mkdir $(srcdir)/%reldir%/tarot-enums-lock 2>/dev/null; then \
            $$dry rm -f $(srcdir)/%reldir%/tarot-enums-stamp; \
            $(MAKE) $(AM_MAKEFLAGS) $(srcdir)/%reldir%/tarot-enums-stamp; \
            $$dry rmdir $(srcdir)/%reldir%/tarot-enums-lock; \
          else \
            while test -d $(srcdir)/%reldir%/tarot-enums-lock && test -z "$$dry"; do \
              sleep 1; \
            done; \
            $$dry test -f $(srcdir)/%reldir%/tarot-enums-stamp; exit $$?; \
          fi; \
        fi
