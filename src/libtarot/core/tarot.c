/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "gettext.h"
#include <tarot/features_private.h>
#include "xalloc.h"
#include "gettext.h"
#define _(String) dgettext (PACKAGE, String)

#ifdef __FRAMAC__
#undef alignof
#define alignof(x) (16)
#endif /* __FRAMAC__ */

static int libtarot_initialized = 0;

char *tarot_libtarot_program_name = PACKAGE_NAME;

static char *libtarot_localedir = NULL;
static char *libtarot_datadir = NULL;
static char *libtarot_pkgdatadir = NULL;

int tarot_libtarot_with_guile = HAVE_GUILE;

static void
tarot_set_localedir (const char *localedir)
{
  free (libtarot_localedir);
  libtarot_localedir = strdup (localedir);
}

void
tarot_set_datadir (const char *datadir)
{
  free (libtarot_datadir);
  free (libtarot_pkgdatadir);
  libtarot_datadir = strdup (datadir);
  libtarot_pkgdatadir = xmalloc (strlen (libtarot_datadir)
                                 + strlen ("/") + strlen (PACKAGE) + 1);
  strcpy (libtarot_pkgdatadir, libtarot_datadir);
  strcat (libtarot_pkgdatadir, "/");
  strcat (libtarot_pkgdatadir, PACKAGE);
}

int
tarot_init (const char *localedir)
{
  assert (libtarot_initialized == 0);
  tarot_set_localedir (localedir);
  bindtextdomain (PACKAGE, libtarot_localedir);
  libtarot_initialized = 1;
  return 0;
}

void
tarot_quit (void)
{
  libtarot_initialized = 0;
}

const char *
tarot_libtarot_version (void)
{
  static const char *effective_version = LIBTAROT_VERSION_INFO;
  return effective_version;
}

#include <tarot/features_private_impl.h>
