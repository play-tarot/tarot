/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_INCLUDED
#define H_TAROT_INCLUDED

#include <tarot/bid.h>
#include <tarot/cards.h>
#include <tarot/player.h>
#include <tarot/step.h>
#include <tarot/game_event.h>
#include <tarot/game.h>
#include <tarot/counter.h>
#include <tarot/layout.h>
#include <tarot/features.h>
#include <tarot/solo.h>
#include <tarot/perceptron.h>
#include <tarot/ai.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  void tarot_set_datadir (const char *datadir);

  int tarot_init (const char *localedir);
  void tarot_quit (void);

  /**
   * tarot_libtarot_version:
   */
  const char *tarot_libtarot_version (void);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */
#endif                          /* not H_TAROT_INCLUDED */
