/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_HANDFULS_PRIVATE_INCLUDED
#define H_TAROT_HANDFULS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/hand_private.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void handfuls_initialize (TarotHandfuls * handfuls,
                                   size_t n_players);
  static int handfuls_copy (TarotHandfuls * dest,
                            const TarotHandfuls * source);
  static TarotPlayer handfuls_leader (const TarotHandfuls * handfuls);
  static int handfuls_set_leader (TarotHandfuls * handfuls,
                                  TarotPlayer leader);
  static int handfuls_start (TarotHandfuls * handfuls);
  static void handfuls_sizes (size_t n_players, unsigned int *n);
  static size_t handfuls_n_decisions (const TarotHandfuls * handfuls);
  static int handfuls_has_handful (const TarotHandfuls * handfuls,
                                   TarotPlayer p, size_t n_players);
  static int handfuls_has_next (const TarotHandfuls * handfuls);
  static TarotPlayer handfuls_next (const TarotHandfuls * handfuls,
                                    size_t n_players);
  static int handfuls_handful_of (const TarotHandfuls * handfuls,
                                  TarotPlayer p, TarotHand * dest);
  static int handfuls_handful_size_of (const TarotHandfuls * handfuls,
                                       TarotPlayer p, size_t n_players);
  static int handfuls_handful_size (const TarotHandfuls * handfuls, size_t i,
                                    size_t n_players);
  static int handfuls_bonus (const TarotHandfuls * handfuls,
                             size_t n_players);
  static int handfuls_check (const TarotHandfuls * handfuls, TarotPlayer p,
                             const TarotHand * handful, size_t n_players);
  static int handfuls_add (TarotHandfuls * handfuls,
                           const TarotHand * handful, size_t n_players);
  static int handfuls_card (TarotHandfuls * handfuls, size_t n_players);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_HANDFULS_PRIVATE_INCLUDED */
