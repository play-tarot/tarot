/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/ai_private.h>
#include <stdlib.h>
#include <assert.h>
#include "xalloc.h"
#include "gettext.h"
#define _(String) dgettext (PACKAGE, String)

TarotAi *
tarot_ai_alloc (void *user_data,
                void (*eval) (void *, const TarotGame *, size_t,
                              TarotGameEvent **, size_t, size_t, double *),
                void (*learn) (void *, const TarotGame *,
                               const TarotGameEvent *, double),
                void *(*dup) (void *), void (*destruct) (void *))
{
  TarotAi *ret = xmalloc (sizeof (TarotAi));
  ai_construct (ret, user_data, eval, learn, dup, destruct);
  return ret;
}

static inline void
eval_noop (void *state, const TarotGame * base, size_t n_candidates,
           TarotGameEvent ** candidates, size_t start, size_t max,
           double *scores)
{
  (void) state;
  (void) base;
  (void) n_candidates;
  (void) candidates;
  (void) start;
  (void) max;
  (void) scores;
}

static inline void
learn_noop (void *state, const TarotGame * base, const TarotGameEvent * event,
            double final_score)
{
  (void) state;
  (void) base;
  (void) event;
  (void) final_score;
}

static inline void *
dup_noop (void *state)
{
  (void) state;
  return NULL;
}

static inline void
destruct_noop (void *state)
{
  (void) state;
}

TarotAi *
tarot_ai_dup (const TarotAi * ai)
{
  TarotAi *ret =
    tarot_ai_alloc (NULL, eval_noop, learn_noop, dup_noop, destruct_noop);
  ai_copy (ret, ai);
  return ret;
}

TarotAi *
tarot_ai_alloc_perceptron ()
{
  return tarot_ai_alloc (tarot_perceptron_static_default (),
                         ai_perceptron_eval, ai_perceptron_learn,
                         ai_perceptron_dup, ai_perceptron_destruct);
}

TarotAi *
tarot_ai_alloc_random (size_t seed_size, const void *seed)
{
  struct yarrow256_ctx *rng = xmalloc (sizeof (struct yarrow256_ctx));
  yarrow256_init (rng, 0, NULL);
  yarrow256_seed (rng, seed_size, seed);
  return tarot_ai_alloc (rng, ai_random_eval, ai_random_learn,
                         ai_random_dup, ai_random_destruct);
}

TarotAi *
tarot_ai_alloc_fuzzy (double fuzziness, size_t seed_size, const void *seed,
                      const TarotAi * base)
{
  TarotAi *ret =
    tarot_ai_alloc (NULL, eval_noop, learn_noop, dup_noop, destruct_noop);
  ai_construct_fuzzy (ret, fuzziness, seed_size, seed, base);
  return ret;
}

void
tarot_ai_free (TarotAi * ai)
{
  ai_destruct (ai);
  free (ai);
}

void
tarot_ai_eval (TarotAi * ai, const TarotGame * base, size_t n_candidates,
               TarotGameEvent ** candidates, size_t start, size_t max,
               double *scores)
{
  ai_eval (ai, base, n_candidates, candidates, start, max, scores);
}

void
tarot_ai_learn (TarotAi * ai, const TarotGame * base,
                const TarotGameEvent * event, double final_score)
{
  ai_learn (ai, base, event, final_score);
}

TarotPlayer
tarot_ai_strongest_player (TarotAi * ai, size_t n_players, int with_call,
                           size_t n_cards, const TarotPlayer * owners,
                           double *confidence, TarotCard * call)
{
  return ai_strongest_player (ai, n_players, with_call, n_cards, owners,
                              confidence, call);
}

const TarotGameEvent *
tarot_ai_best (TarotAi * ai, const TarotGame * state, double *score)
{
  return ai_best (ai, state, score);
}

double
tarot_ai_validate (TarotAi * ai, const TarotGame * validation_game)
{
  return ai_validate (ai, validation_game);
}

#include <tarot/ai_private_impl.h>
