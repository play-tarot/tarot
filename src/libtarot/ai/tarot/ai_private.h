/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_AI_PRIVATE_INCLUDED
#define H_TAROT_AI_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void ai_construct (TarotAi * ai, void *user_data,
                            void (*eval) (void *, const TarotGame *, size_t,
                                          TarotGameEvent **, size_t, size_t,
                                          double *), void (*learn) (void *,
                                                                    const
                                                                    TarotGame
                                                                    *,
                                                                    const
                                                                    TarotGameEvent
                                                                    *,
                                                                    double),
                            void *(*dup) (void *), void (*destruct) (void *));

  static void ai_copy (TarotAi * ai, const TarotAi * source);

  static void ai_destruct (TarotAi * ai);

  static void ai_random_eval (void *state, const TarotGame * base,
                              size_t n_candidates,
                              TarotGameEvent ** candidates, size_t start,
                              size_t max, double *scores);

  static void ai_random_learn (void *state, const TarotGame * base,
                               const TarotGameEvent * event,
                               double final_score);

  static void *ai_random_dup (void *state);

  static void ai_random_destruct (void *state);

  static void ai_perceptron_eval (void *state, const TarotGame * base,
                                  size_t n_candidates,
                                  TarotGameEvent ** candidates, size_t start,
                                  size_t max, double *scores);

  static void ai_perceptron_learn (void *state, const TarotGame * base,
                                   const TarotGameEvent * event,
                                   double final_score);

  static void *ai_perceptron_dup (void *state);

  static void ai_perceptron_destruct (void *state);

  static void ai_construct_fuzzy (TarotAi * ai, double fuzziness,
                                  size_t seed_size, const void *seed,
                                  const TarotAi * base);

  static void ai_eval (TarotAi * ai, const TarotGame * base,
                       size_t n_candidates, TarotGameEvent ** candidates,
                       size_t start, size_t max, double *scores);

  static void ai_learn (TarotAi * ai, const TarotGame * base,
                        const TarotGameEvent * event, double final_score);

  static TarotPlayer ai_strongest_player (TarotAi * ai, size_t n_players,
                                          int with_call, size_t n_cards,
                                          const TarotPlayer * owners,
                                          double *confidence,
                                          TarotCard * call);

  static const TarotGameEvent *ai_best (TarotAi * ai, const TarotGame * state,
                                        double *score);

  static double ai_validate (TarotAi * ai, const TarotGame * validation_game);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_AI_PRIVATE_INCLUDED */
