/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020, 2024  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_AI_PRIVATE_IMPL_INCLUDED
#define H_TAROT_AI_PRIVATE_IMPL_INCLUDED

#include <tarot/ai_private.h>
#include <tarot/game_event_private.h>
#include <tarot/game_private.h>
#include <tarot/cards_private.h>
#include <nettle/yarrow.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "xalloc.h"

static inline void
ai_construct (TarotAi * ai, void *user_data,
              void (*eval) (void *, const TarotGame *, size_t,
                            TarotGameEvent **, size_t, size_t, double *),
              void (*learn) (void *, const TarotGame *,
                             const TarotGameEvent *, double),
              void *(*dup) (void *), void (*destruct) (void *))
{
  ai->user_data = user_data;
  ai->eval = eval;
  ai->learn = learn;
  ai->dup = dup;
  ai->destruct = destruct;
  ai->best_event.data = ai->event_data;
  ai->best_event.n = 78;
}

static inline void
ai_copy_fresh (TarotAi * ai, const TarotAi * source)
{
  ai_construct (ai, source->dup (source->user_data), source->eval,
                source->learn, source->dup, source->destruct);
}

static inline void
ai_copy (TarotAi * ai, const TarotAi * source)
{
  ai_destruct (ai);
  ai_copy_fresh (ai, source);
}

static inline void
ai_destruct (TarotAi * ai)
{
  if (ai != NULL)
    {
      ai->destruct (ai->user_data);
    }
}

static inline void
ai_random_eval (void *state, const TarotGame * base, size_t n_candidates,
                TarotGameEvent ** candidates, size_t start, size_t max,
                double *scores)
{
  size_t max_size_t = (size_t) -1;
  double maximum_value = max_size_t;
  const double amplitude = 100;
  size_t value;
  uint8_t buffer[sizeof (value)];
  size_t i;
  struct yarrow256_ctx *prng = (struct yarrow256_ctx *) state;
  (void) base;
  (void) candidates;
  for (i = 0; i < max && i + start < n_candidates; i++)
    {
      double unif;
      yarrow256_random (prng, sizeof (buffer), buffer);
      memcpy (&value, buffer, sizeof (buffer));
      unif = value / maximum_value;
      scores[i] = (unif * 2 * amplitude) - amplitude;
    }
}

static inline void
ai_random_learn (void *state, const TarotGame * base,
                 const TarotGameEvent * event, double final_score)
{
  (void) state;
  (void) base;
  (void) event;
  (void) final_score;
}

static inline void *
ai_random_dup (void *state)
{
  struct yarrow256_ctx *prng = (struct yarrow256_ctx *) state;
  struct yarrow256_ctx *ret = xmalloc (sizeof (struct yarrow256_ctx));
  /* According to the public header in nettle, there are no pointers
   * in struct yarrow256_ctx */
  memcpy (ret, prng, sizeof (struct yarrow256_ctx));
  return ret;
}

static inline void
ai_random_destruct (void *state)
{
  free (state);
}

static inline void
ai_perceptron_eval (void *state, const TarotGame * base, size_t n_candidates,
                    TarotGameEvent ** candidates, size_t start, size_t max,
                    double *scores)
{
  TarotPerceptron *perceptron = (TarotPerceptron *) state;
  tarot_perceptron_eval (perceptron, base, n_candidates, candidates, start,
                         max, scores);
}

static inline void
ai_perceptron_learn (void *state, const TarotGame * base,
                     const TarotGameEvent * event, double final_score)
{
  TarotPerceptron *perceptron = (TarotPerceptron *) state;
  tarot_perceptron_learn (perceptron, base, event, final_score);
}

static inline void *
ai_perceptron_dup (void *state)
{
  TarotPerceptron *perceptron = (TarotPerceptron *) state;
  return tarot_perceptron_dup (perceptron);
}

static inline void
ai_perceptron_destruct (void *state)
{
  TarotPerceptron *perceptron = (TarotPerceptron *) state;
  tarot_perceptron_free (perceptron);
}

struct fuzzy_state
{
  TarotAi base;
  double fuzziness;
  struct yarrow256_ctx prng;
};

static inline void
ai_fuzzy_eval (void *state, const TarotGame * base,
               size_t n_candidates, TarotGameEvent ** candidates,
               size_t start, size_t max, double *scores)
{
  size_t i;
  struct fuzzy_state *ai = (struct fuzzy_state *) state;
  double max_random = (double) ((size_t) (-1));
  double flt_random;
  size_t random_value;
  uint8_t random_buffer[sizeof (random_value)];
  double fuzziness = ai->fuzziness;
  ai_eval (&(ai->base), base, n_candidates, candidates, start, max, scores);
  yarrow256_random (&(ai->prng), sizeof (random_buffer), random_buffer);
  memcpy (&random_value, random_buffer, sizeof (random_value));
  flt_random = random_value / max_random;
  /* If we are evaluating bids, then the fuzziness should be reduced
   * because each player having a chance of bidding at random is
   * another chance of the taker being not correct. */
  if (game_step (base) == TAROT_BIDS || game_step (base) == TAROT_DECLS)
    {
      fuzziness /= game_n_players (base);
    }
  /* If the behavior is to play randomly, shuffle the scores. */
  if (flt_random < fuzziness)
    {
      size_t size_shuffled = 1;
      size_t position = 0;
      for (size_shuffled = 1;
           size_shuffled < max && size_shuffled + start < n_candidates;
           size_shuffled++)
        {
          uint8_t position_buffer[sizeof (position)];
          double hold = scores[size_shuffled];
          yarrow256_random (&(ai->prng), sizeof (position_buffer),
                            position_buffer);
          memcpy (&position, position_buffer, sizeof (position));
          position %= (size_shuffled + 1);
          scores[size_shuffled] = scores[position];
          scores[position] = hold;
        }
    }
  /* In any case, add a faint noise so that we don't always play
   * hearts then clubs then diamonds etc */
  for (i = 0; i < max && i + start < n_candidates; i++)
    {
      double noise;
      yarrow256_random (&(ai->prng), sizeof (random_buffer), random_buffer);
      memcpy (&random_value, random_buffer, sizeof (random_value));
      flt_random = random_value / max_random;
      noise = ((flt_random - 0.5) * 2) * 1e-6;
      scores[i] += noise;
    }
}

static inline void
ai_fuzzy_learn (void *state, const TarotGame * base,
                const TarotGameEvent * event, double final_score)
{
  struct fuzzy_state *ai = (struct fuzzy_state *) state;
  ai_learn (&(ai->base), base, event, final_score);
}

static inline void *
ai_fuzzy_dup (void *state)
{
  struct fuzzy_state *ai = (struct fuzzy_state *) state;
  struct fuzzy_state *ret = xmalloc (sizeof (struct fuzzy_state));
  ai_copy_fresh (&(ret->base), &(ai->base));
  ret->fuzziness = ai->fuzziness;
  memcpy (&(ret->prng), &(ai->prng), sizeof (struct yarrow256_ctx));
  return ret;
}

static inline void
ai_fuzzy_destruct (void *state)
{
  struct fuzzy_state *ai = (struct fuzzy_state *) state;
  ai_destruct (&(ai->base));
  free (ai);
}

static inline void
ai_construct_fuzzy (TarotAi * ai, double fuzziness, size_t seed_size,
                    const void *seed, const TarotAi * base)
{
  struct fuzzy_state *state = xmalloc (sizeof (struct fuzzy_state));
  ai_copy_fresh (&(state->base), base);
  state->fuzziness = fuzziness;
  yarrow256_init (&(state->prng), 0, NULL);
  yarrow256_seed (&(state->prng), seed_size, seed);
  ai_construct (ai, state, ai_fuzzy_eval, ai_fuzzy_learn,
                ai_fuzzy_dup, ai_fuzzy_destruct);
}

static inline void
ai_eval (TarotAi * ai, const TarotGame * base, size_t n_candidates,
         TarotGameEvent ** candidates, size_t start, size_t max,
         double *scores)
{
  ai->eval (ai->user_data, base, n_candidates, candidates, start, max,
            scores);
}

static inline void
ai_learn (TarotAi * ai, const TarotGame * base, const TarotGameEvent * event,
          double final_score)
{
  ai->learn (ai->user_data, base, event, final_score);
}

static inline double
ai_strongest_player_try_bid (TarotAi * ai, const TarotGame * game)
{
  /* Return the maximum predicted score for taking */
  static TarotBid candidate_bids[] = {
    TAROT_PUSH
  };
  static const size_t n_candidates =
    sizeof (candidate_bids) / sizeof (candidate_bids[0]);
  double scores[n_candidates];
  TarotGameEvent candidates[n_candidates];
  TarotGameEvent *pointers[n_candidates];
  size_t i;
  size_t best = 0;
  for (i = 0; i < n_candidates; i++)
    {
      candidates[i].t = TAROT_BID_EVENT;
      candidates[i].u.bid = candidate_bids[i];
      pointers[i] = &(candidates[i]);
    }
  ai_eval (ai, game, n_candidates, pointers, 0, n_candidates, scores);
  for (i = 1; i < n_candidates; i++)
    {
      if (scores[i] > scores[best])
        {
          best = i;
        }
    }
  return scores[best];
}

static inline double
ai_strongest_player_eval_player (TarotAi * ai, size_t n_players,
                                 int with_call, size_t n_cards,
                                 const TarotPlayer * owners, TarotPlayer who)
{
  TarotGame game;
  TarotGameEvent deal_event;
  unsigned int my_owners[78];
  size_t i;
  assert (n_cards == 78);
  /* Adapt the owners so that the cards of who are now the cards of
   * P1 */
  for (i = 0; i < n_cards; i++)
    {
      if (owners[i] >= n_players)
        {
          my_owners[i] = n_players;
        }
      else
        {
          size_t position = (owners[i] + n_players - who) % n_players;
          my_owners[i] = position;
        }
    }
  deal_event.t = TAROT_DEAL_ALL_EVENT;
  deal_event.n = 78;
  deal_event.data = my_owners;
  game_initialize (&game, n_players, with_call);
  if (game_add_event (&game, &deal_event) != TAROT_GAME_OK)
    {
      /* Petit sec */
      return -DBL_MAX;
    }
  return ai_strongest_player_try_bid (ai, &game);
}

static inline double
ai_strongest_player_taker (TarotAi * ai, size_t n_players, int with_call,
                           size_t n_cards, const TarotPlayer * owners,
                           TarotPlayer * taker)
{
  double score[5];
  size_t i, best = 0;
  assert (n_players <= 5);
  assert (n_players != 0);
  for (i = 0; i < n_players; i++)
    {
      score[i] =
        ai_strongest_player_eval_player (ai, n_players, with_call, n_cards,
                                         owners, i);
    }
  for (i = 1; i < n_players; i++)
    {
      if (score[i] > score[best])
        {
          best = i;
        }
    }
  *taker = best;
  return score[best];
}

static inline int
ai_strongest_player_apply_bids (size_t n_players, size_t n_cards,
                                const TarotPlayer * owners, TarotPlayer taker,
                                TarotGame * game)
{
  TarotPlayer i;
  TarotGameEvent deal_event;
  unsigned int my_owners[78];
  assert (n_cards == 78);
  for (i = 0; i < n_cards; i++)
    {
      my_owners[i] = owners[i];
    }
  deal_event.t = TAROT_DEAL_ALL_EVENT;
  deal_event.n = n_cards;
  deal_event.data = my_owners;
  if (game_add_event (game, &deal_event) != TAROT_GAME_OK)
    {
      return 1;
    }
  for (i = 0; i < n_players; i++)
    {
      TarotBid bid = TAROT_PASS;
      TarotGameEvent bid_event;
      if (i == taker)
        {
          bid = TAROT_PUSH;
        }
      bid_event.t = TAROT_BID_EVENT;
      bid_event.u.bid = bid;
      if (game_add_event (game, &bid_event) != TAROT_GAME_OK)
        {
          assert (0);
        }
    }
  for (i = 0; i < n_players; i++)
    {
      TarotGameEvent decl_event;
      decl_event.t = TAROT_DECL_EVENT;
      decl_event.u.decl = 0;
      if (game_add_event (game, &decl_event) != TAROT_GAME_OK)
        {
          assert (0);
        }
    }
  return 0;
}

static inline TarotCard
ai_strongest_player_select_call (TarotAi * ai, const TarotGame * game)
{
  TarotCard candidates[16];
  TarotGameEvent candidate_events[16];
  TarotGameEvent *pointers[16];
  double scores[16];
  size_t n_candidates = 0, i_suit, i_candidate;
  TarotNumber mini, n;
  size_t best = 0;
  static const TarotSuit suits[] = {
    TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES
  };
  static const size_t n_suits = sizeof (suits) / sizeof (suits[0]);
  TarotPlayer next;
  if (game_get_hint_call (game, &next, &mini) != TAROT_GAME_OK)
    {
      assert (0);
    }
  for (n = mini; n <= TAROT_KING; n++)
    {
      assert (n_candidates < (sizeof (candidates) / sizeof (candidates[0])));
      for (i_suit = 0; i_suit < n_suits; i_suit++)
        {
          if (of (n, suits[i_suit], &(candidates[n_candidates])) != 0)
            {
              assert (0);
            }
          candidate_events[n_candidates].t = TAROT_CALL_EVENT;
          candidate_events[n_candidates].u.call = candidates[n_candidates];
          pointers[n_candidates] = &(candidate_events[n_candidates]);
          n_candidates++;
        }
    }
  ai_eval (ai, game, n_candidates, pointers, 0, 16, scores);
  assert (n_candidates != 0);
  for (i_candidate = 1; i_candidate < n_candidates; i_candidate++)
    {
      if (scores[i_candidate] > scores[best])
        {
          best = i_candidate;
        }
    }
  return candidates[best];
}

static inline TarotPlayer
ai_strongest_player (TarotAi * ai, size_t n_players, int with_call,
                     size_t n_cards, const TarotPlayer * owners,
                     double *confidence, TarotCard * call)
{
  TarotGame game;
  TarotPlayer taker;
  game_initialize (&game, n_players, with_call);
  *confidence =
    ai_strongest_player_taker (ai, n_players, with_call, n_cards, owners,
                               &taker);
  if (with_call)
    {
      int error =
        ai_strongest_player_apply_bids (n_players, n_cards, owners, taker,
                                        &game);
      if (!error)
        {
          *call = ai_strongest_player_select_call (ai, &game);
        }
    }
  return taker;
}

static inline void
ai_save_event (TarotAi * ai, const TarotGameEvent * event)
{
  event_init_copy (&(ai->best_event), event,
                   sizeof (ai->event_data) / sizeof (ai->event_data[0]),
                   ai->event_data);
}

static inline void
ai_best_consider_bids (TarotAi * ai, const TarotGame * state,
                       TarotBid minimum)
{
  TarotBid candidates[5];
  TarotGameEvent events[5];
  TarotGameEvent *pointers[5];
  double scores[5];
  size_t i, n;
  TarotBid added;
  candidates[0] = TAROT_PASS;
  for (n = 1, added = minimum; n < 5 && added <= TAROT_DOUBLE_KEEP;
       n++, added++)
    {
      candidates[n] = added;
    }
  for (i = 0; i < n; i++)
    {
      event_init_bid (&(events[i]), candidates[i]);
      pointers[i] = &(events[i]);
    }
  ai_eval (ai, state, n, pointers, 0, 5, scores);
  for (i = 0; i < n; i++)
    {
      if (ai->best_score == -DBL_MAX || scores[i] > ai->best_score)
        {
          ai_save_event (ai, &(events[i]));
          ai->best_score = scores[i];
        }
    }
}

static inline void
ai_best_consider_decls (TarotAi * ai, const TarotGame * state, int allowed)
{
  int candidates[2];
  TarotGameEvent events[2];
  TarotGameEvent *pointers[2];
  double scores[2];
  size_t i, n = 1;
  candidates[0] = 0;
  if (allowed)
    {
      candidates[1] = 1;
      n = 2;
    }
  for (i = 0; i < n; i++)
    {
      event_init_decl (&(events[i]), candidates[i]);
      pointers[i] = &(events[i]);
    }
  ai_eval (ai, state, n, pointers, 0, 2, scores);
  for (i = 0; i < n; i++)
    {
      if (ai->best_score == -DBL_MAX || scores[i] > ai->best_score)
        {
          ai_save_event (ai, &(events[i]));
          ai->best_score = scores[i];
        }
    }
}

static inline void
ai_best_consider_calls (TarotAi * ai, const TarotGame * state,
                        TarotNumber mini)
{
  TarotCard candidates[16];
  TarotGameEvent events[16];
  TarotGameEvent *pointers[16];
  double scores[16];
  size_t i, n = 0, i_suit;
  TarotNumber number;
  static const TarotSuit suits[] = {
    TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES
  };
  static const size_t n_suits = sizeof (suits) / sizeof (suits[0]);
  assert (mini + 4 > TAROT_KING);
  for (number = mini; number <= TAROT_KING; number++)
    {
      for (i_suit = 0; i_suit < n_suits; i_suit++)
        {
          if (of (number, suits[i_suit], &(candidates[n++])) != 0)
            {
              assert (0);
            }
        }
    }
  for (i = 0; i < n; i++)
    {
      event_init_call (&(events[i]), candidates[i]);
      pointers[i] = &(events[i]);
    }
  ai_eval (ai, state, n, pointers, 0, 16, scores);
  for (i = 0; i < n; i++)
    {
      if (ai->best_score == -DBL_MAX || scores[i] > ai->best_score)
        {
          ai_save_event (ai, &(events[i]));
          ai->best_score = scores[i];
        }
    }
}

static inline void
ai_best_consider_discards_draw (struct yarrow256_ctx *prng, size_t n_total,
                                size_t n_prio, const TarotCard * prio,
                                size_t n_additional,
                                const TarotCard * additional,
                                TarotGameEvent * e,
                                size_t n_data, unsigned int *data)
{
  TarotCard selected[6];
  size_t n_selected = 0;
  TarotCard set[78];
  size_t n_set = n_prio;
  assert (n_total <= 6);
  memcpy (set, prio, n_prio * sizeof (TarotCard));
  if (n_prio <= n_total)
    {
      for (n_selected = 0; n_selected < n_prio; n_selected++)
        {
          selected[n_selected] = prio[n_selected];
        }
      memcpy (set, additional, n_additional * sizeof (TarotCard));
      n_set = n_additional;
    }
  for (; n_selected < n_total; n_selected++)
    {
      TarotCard c;
      size_t position;
      uint8_t position_data[sizeof (position)];
      yarrow256_random (prng, sizeof (position_data), position_data);
      memcpy (&position, position_data, sizeof (position));
      position %= n_set;
      c = set[position];
      set[position] = set[--n_set];
      selected[n_selected] = c;
    }
  event_init_discard (e, n_selected, selected, n_data, data);
}

static inline void
ai_best_consider_discards_draw_many (size_t n_total, size_t n_prio,
                                     const TarotCard * prio,
                                     size_t n_additional,
                                     const TarotCard * additional,
                                     size_t n_draws, TarotGameEvent ** draws,
                                     size_t *n_data, unsigned int **data)
{
  unsigned char seed[78] = { 0 };
  size_t i;
  struct yarrow256_ctx generator;
  for (i = 0; i < n_prio; i++)
    {
      seed[prio[i]] = 1;
    }
  for (i = 0; i < n_additional; i++)
    {
      seed[additional[i]] = 2;
    }
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, sizeof (seed), seed);
  for (i = 0; i < n_draws; i++)
    {
      ai_best_consider_discards_draw (&generator, n_total, n_prio, prio,
                                      n_additional, additional, draws[i],
                                      n_data[i], data[i]);
    }
}

static inline void
ai_best_consider_discards (TarotAi * ai, const TarotGame * state,
                           size_t n_total, size_t n_prio,
                           const TarotCard * prio, size_t n_additional,
                           const TarotCard * additional)
{
  TarotGameEvent events[20];
  TarotGameEvent *pointers[20];
  size_t n_data[20];
  unsigned int event_data[20 * 6];
  double scores[20];
  size_t i;
  unsigned int *event_data_pp[20];
  for (i = 0; i < 20; i++)
    {
      n_data[i] = 6;
      event_data_pp[i] = &(event_data[i * 6]);
      pointers[i] = &(events[i]);
    }
  ai_best_consider_discards_draw_many (n_total, n_prio, prio, n_additional,
                                       additional, 20, pointers, n_data,
                                       event_data_pp);
  ai_eval (ai, state, 20, pointers, 0, 20, scores);
  for (i = 0; i < 20; i++)
    {
      if (ai->best_score == -DBL_MAX || scores[i] > ai->best_score)
        {
          ai_save_event (ai, &(events[i]));
          ai->best_score = scores[i];
        }
    }
}

static inline void
ai_best_consider_cards (TarotAi * ai, const TarotGame * state, size_t n_cards,
                        const TarotCard * playable)
{
  TarotCard candidates[78];
  TarotGameEvent events[78];
  TarotGameEvent *pointers[78];
  double scores[78];
  size_t i;
  assert (n_cards <= 78);
  for (i = 0; i < n_cards; i++)
    {
      candidates[i] = playable[i];
      event_init_card (&(events[i]), candidates[i]);
      pointers[i] = &(events[i]);
    }
  ai_eval (ai, state, n_cards, pointers, 0, 78, scores);
  for (i = 0; i < n_cards; i++)
    {
      if (ai->best_score == -DBL_MAX || scores[i] > ai->best_score)
        {
          ai_save_event (ai, &(events[i]));
          ai->best_score = scores[i];
        }
    }
}

static inline void
ai_best_consider_possible (TarotAi * ai, const TarotGame * state)
{
  TarotBid hint_bid;
  int hint_decl;
  TarotNumber hint_call;
  size_t n_cards, n_prio, n_additional;
  TarotCard prio[78], additional[78];
  TarotCard playable[78];
  TarotPlayer next;
  ai->best_score = -DBL_MAX;
  if (game_get_hint_bid (state, &next, &hint_bid) == TAROT_GAME_OK)
    {
      ai_best_consider_bids (ai, state, hint_bid);
    }
  else if (game_get_hint_decl (state, &next, &hint_decl) == TAROT_GAME_OK)
    {
      /* WARNING: we disable declarations! */
      int actually_allowed = 0;
      ai_best_consider_decls (ai, state, actually_allowed && hint_decl);
    }
  else if (game_get_hint_call (state, &next, &hint_call) == TAROT_GAME_OK)
    {
      ai_best_consider_calls (ai, state, hint_call);
    }
  else
    if (game_get_hint_discard
        (state, &next, &n_cards, &n_prio, 0, 78, prio, &n_additional, 0, 78,
         additional) == TAROT_GAME_OK)
    {
      assert (n_prio <= 78);
      assert (n_additional <= 78);
      ai_best_consider_discards (ai, state, n_cards, n_prio, prio,
                                 n_additional, additional);
    }
  else if (game_get_hint_card (state, &next, &n_cards, 0, 78, playable) ==
           TAROT_GAME_OK)
    {
      assert (n_cards <= 78);
      ai_best_consider_cards (ai, state, n_cards, playable);
    }
}

static inline const TarotGameEvent *
ai_best (TarotAi * ai, const TarotGame * state, double *score)
{
  TarotPlayer next;
  if (game_get_next (state, &next) == TAROT_GAME_OK)
    {
      TarotGame as_next;
      game_initialize (&as_next, 4, 0);
      ai->best_score = -DBL_MAX;
      if (game_copy_as (&as_next, state, next) == TAROT_GAME_OK)
        {
          ai_best_consider_possible (ai, &as_next);
          if (ai->best_score != -DBL_MAX)
            {
              *score = ai->best_score;
              return &(ai->best_event);
            }
        }
    }
  return NULL;
}

static double
ai_validate_player (TarotAi * ai, const TarotGame * validation_game,
                    TarotPlayer who)
{
  TarotGame rebuilt;
  const TarotGameEvent *event;
  TarotGameIterator i;
  int actual_score;
  size_t n_players;
  double sum = 0;
  double n = 0;
  game_initialize (&rebuilt, 4, 0);
  game_iterator_setup (&i, validation_game);
  if (game_get_scores (validation_game, &n_players, who, 1, &actual_score) !=
      TAROT_GAME_OK)
    {
      return sum / n;
    }
  while ((event = game_iterator_next_value (&i)) != NULL)
    {
      TarotPlayer next;
      if (game_get_next (&rebuilt, &next) == TAROT_GAME_OK)
        {
          if (next == who)
            {
              double prediction;
              double diff;
              ai_eval (ai, &rebuilt, 1, (TarotGameEvent **) & event, 0, 1,
                       &prediction);
              diff = prediction - actual_score;
              sum += diff * diff;
              n += 1;
            }
        }
      if (game_add_event (&rebuilt, event) != TAROT_GAME_OK)
        {
          assert (0);
        }
    }
  return sqrt (sum / n);
}

static double
ai_validate (TarotAi * ai, const TarotGame * validation_game)
{
  double sum = 0, n = 0;
  size_t n_players = game_n_players (validation_game);
  TarotPlayer i;
  for (i = 0; i < n_players; i++)
    {
      sum += ai_validate_player (ai, validation_game, i);
      n += 1;
    }
  return sum / n;
}

#include <tarot/game_event_private_impl.h>
#include <tarot/game_private_impl.h>
#include <tarot/cards_private_impl.h>
#endif /* H_TAROT_AI_PRIVATE_IMPL_INCLUDED */
