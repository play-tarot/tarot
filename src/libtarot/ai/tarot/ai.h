/* tarot implements the rules of the tarot game
 * Copyright (C) 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_AI_INCLUDED
#define H_TAROT_AI_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

  struct TarotAi;
  typedef struct TarotAi TarotAi;

  /**
   * tarot_ai_alloc: (skip)
   */
  TarotAi *tarot_ai_alloc (void *user_data,
                           void (*eval) (void *user_data,
                                         const TarotGame * base,
                                         size_t n_candidates,
                                         TarotGameEvent ** candidates,
                                         size_t start, size_t max,
                                         double *scores),
                           void (*learn) (void *user_data,
                                          const TarotGame * base,
                                          const TarotGameEvent * event,
                                          double final_score),
                           void *(*dup) (void *user_data),
                           void (*destruct) (void *user_data));

  /**
   * tarot_ai_dup:
   */
  TarotAi *tarot_ai_dup (const TarotAi * ai);

  /**
   * tarot_ai_alloc_perceptron: (constructor)
   */
  TarotAi *tarot_ai_alloc_perceptron ();

  /**
   * tarot_ai_alloc_random: (constructor)
   * @seed: (array length=seed_size) (element-type char):
   */
  TarotAi *tarot_ai_alloc_random (size_t seed_size, const void *seed);

  /**
   * tarot_ai_alloc_fuzzy: (constructor)
   * @seed: (array length=seed_size) (element-type char):
   */
  TarotAi *tarot_ai_alloc_fuzzy (double fuzziness, size_t seed_size,
                                 const void *seed, const TarotAi * base);

  void tarot_ai_free (TarotAi * ai);

  /**
   * tarot_ai_eval:
   * @candidates: (transfer none) (array length=n_candidates):
   * @scores: (array length=max):
   */
  void tarot_ai_eval (TarotAi * ai, const TarotGame * base,
                      size_t n_candidates, TarotGameEvent ** candidates,
                      size_t start, size_t max, double *scores);

  /**
   * tarot_ai_learn:
   */
  void tarot_ai_learn (TarotAi * ai, const TarotGame * base,
                       const TarotGameEvent * event, double final_score);

  /**
   * tarot_ai_strongest_player:
   * @owners: (array length=n_cards):
   * @confidence: (out):
   * @call: (out):
   */
  TarotPlayer tarot_ai_strongest_player (TarotAi * ai, size_t n_players,
                                         int with_call, size_t n_cards,
                                         const TarotPlayer * owners,
                                         double *confidence,
                                         TarotCard * call);

  /**
   * tarot_ai_best:
   * @score: (out):
   */
  const TarotGameEvent *tarot_ai_best (TarotAi * ai, const TarotGame * state,
                                       double *score);

  /**
   * tarot_ai_validate:
   */
  double tarot_ai_validate (TarotAi * ai, const TarotGame * validation_game);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_AI_INCLUDED */
