/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED
#define H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED

#include <tarot/game_event_private.h>
#include <assert.h>
#include <stdlib.h>
#include <nettle/yarrow.h>

static inline TarotGameEventT
event_type (const TarotGameEvent * event)
{
  return event->t;
}

static inline void
event_init_setup (TarotGameEvent * event, size_t n_players, int with_call)
{
  event->t = TAROT_SETUP_EVENT;
  event->u.setup.n_players = n_players;
  event->u.setup.with_call = with_call;
  event->n = 0;
  event->data = NULL;
}

static inline void
event_init_deal (TarotGameEvent * event, TarotPlayer myself, size_t n_cards,
                 const TarotCard * cards, size_t n_data, unsigned int *data)
{
  size_t i;
  assert (n_data >= n_cards);
  event->t = TAROT_DEAL_EVENT;
  event->u.deal = myself;
  event->n = n_cards;
  event->data = data;
  for (i = 0; i < n_cards; i++)
    {
      event->data[i] = cards[i];
    }
}

static inline void
event_init_deal_all (TarotGameEvent * event, size_t n_owners,
                     const TarotPlayer * owners, size_t n_data,
                     unsigned int *data)
{
  size_t i;
  assert (n_data >= n_owners);
  event->t = TAROT_DEAL_ALL_EVENT;
  event->n = n_owners;
  event->data = data;
  for (i = 0; i < n_owners; i++)
    {
      event->data[i] = owners[i];
    }
}

static inline void
event_init_deal_all_random (TarotGameEvent * event, size_t n_players,
                            size_t seed_size, const uint8_t * seed,
                            size_t n_data, unsigned int *data)
{
  size_t n_cards;
  unsigned int i;
  TarotPlayer owners[78];
  struct yarrow256_ctx generator;
  switch (n_players)
    {
    case 3:
      n_cards = 24;
      break;
    case 4:
      n_cards = 18;
      break;
    case 5:
      n_cards = 15;
      break;
    default:
      assert (0);
    }
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, seed_size, seed);
  for (i = 0; i < 78; i++)
    {
      /* Give the hearts to the first player, the clubs to the
       * next... */
      owners[i] = i / n_cards;
    }
  /* Shuffle */
  for (i = 1; i < 78; i++)
    {
      unsigned char random_buffer[sizeof (size_t)];
      size_t random_number;
      yarrow256_random (&generator, sizeof (size_t), random_buffer);
      memcpy (&random_number, random_buffer, sizeof (size_t));
      if (1)
        {
          unsigned int pos = random_number % (i + 1);
          TarotPlayer hold = owners[pos];
          owners[pos] = owners[i];
          owners[i] = hold;
        }
    }
  event_init_deal_all (event, 78, owners, n_data, data);
}

static inline void
event_init_bid (TarotGameEvent * event, TarotBid bid)
{
  event->t = TAROT_BID_EVENT;
  event->u.bid = bid;
  event->n = 0;
  event->data = NULL;
}

static inline void
event_init_decl (TarotGameEvent * event, int slam)
{
  event->t = TAROT_DECL_EVENT;
  event->u.decl = slam;
  event->n = 0;
  event->data = NULL;
}

static inline void
event_init_call (TarotGameEvent * event, TarotCard card)
{
  event->t = TAROT_CALL_EVENT;
  event->u.call = card;
  event->n = 0;
  event->data = NULL;
}

static inline void
event_init_dog (TarotGameEvent * event, size_t n_cards,
                const TarotCard * cards, size_t n_data, unsigned int *data)
{
  size_t i;
  assert (n_data >= n_cards);
  event->t = TAROT_DOG_EVENT;
  event->n = n_cards;
  event->data = data;
  for (i = 0; i < n_cards; i++)
    {
      event->data[i] = cards[i];
    }
}

static inline void
event_init_discard (TarotGameEvent * event, size_t n_cards,
                    const TarotCard * cards, size_t n_data,
                    unsigned int *data)
{
  size_t i;
  assert (n_data >= n_cards);
  event->t = TAROT_DISCARD_EVENT;
  event->n = n_cards;
  event->data = data;
  for (i = 0; i < n_cards; i++)
    {
      event->data[i] = cards[i];
    }
}

static inline void
event_init_handful (TarotGameEvent * event, size_t n_cards,
                    const TarotCard * cards, size_t n_data,
                    unsigned int *data)
{
  size_t i;
  assert (n_data >= n_cards);
  event->t = TAROT_HANDFUL_EVENT;
  event->n = n_cards;
  event->data = data;
  for (i = 0; i < n_cards; i++)
    {
      event->data[i] = cards[i];
    }
}

static inline void
event_init_card (TarotGameEvent * event, TarotCard card)
{
  event->t = TAROT_CARD_EVENT;
  event->u.card = card;
  event->n = 0;
  event->data = NULL;
}

static inline void
event_init_copy (TarotGameEvent * event, const TarotGameEvent * source,
                 size_t n_data, unsigned int *data)
{
  size_t source_n_data;
  const unsigned int *source_data = event_get_data (source, &source_n_data);
  size_t i;
  assert (n_data >= source_n_data);
  event->t = source->t;
  event->u = source->u;
  event->n = source_n_data;
  event->data = data;
  for (i = 0; i < source_n_data; i++)
    {
      event->data[i] = source_data[i];
    }
}

static const unsigned int *
event_get_data (const TarotGameEvent * event, size_t *n_data)
{
  *n_data = event->n;
  return event->data;
}

static inline TarotGameEventError
event_get_setup (const TarotGameEvent * event, size_t *n_players,
                 int *with_call)
{
  if (event->t == TAROT_SETUP_EVENT)
    {
      *n_players = event->u.setup.n_players;
      *with_call = event->u.setup.with_call;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_deal (const TarotGameEvent * event, TarotPlayer * myself,
                size_t *n_cards, size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DEAL_EVENT);
  if (ok)
    {
      size_t i;
      *myself = event->u.deal;
      for (i = 0; i < max && i + start < event->n; i++)
        {
          cards[i] = event->data[i + start];
        }
      *n_cards = event->n;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_deal_all (const TarotGameEvent * event, size_t *n_owners,
                    size_t start, size_t max, TarotPlayer * players)
{
  int ok = (event->t == TAROT_DEAL_ALL_EVENT);
  if (ok)
    {
      size_t i;
      for (i = 0; i < max && i + start < event->n; i++)
        {
          players[i] = event->data[i + start];
        }
      *n_owners = event->n;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_bid (const TarotGameEvent * event, TarotBid * bid)
{
  int ok = (event->t == TAROT_BID_EVENT);
  if (ok)
    {
      *bid = event->u.bid;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_decl (const TarotGameEvent * event, int *decl)
{
  int ok = (event->t == TAROT_DECL_EVENT);
  if (ok)
    {
      *decl = event->u.decl;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_call (const TarotGameEvent * event, TarotCard * call)
{
  int ok = (event->t == TAROT_CALL_EVENT);
  if (ok)
    {
      *call = event->u.call;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_dog (const TarotGameEvent * event, size_t *n_cards, size_t start,
               size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DOG_EVENT);
  if (ok)
    {
      size_t i;
      for (i = 0; i < max && i + start < event->n; i++)
        {
          cards[i] = event->data[i + start];
        }
      *n_cards = event->n;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_discard (const TarotGameEvent * event, size_t *n_cards,
                   size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_DISCARD_EVENT);
  if (ok)
    {
      size_t i;
      for (i = 0; i < max && i + start < event->n; i++)
        {
          cards[i] = event->data[i + start];
        }
      *n_cards = event->n;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_handful (const TarotGameEvent * event, size_t *n_cards,
                   size_t start, size_t max, TarotCard * cards)
{
  int ok = (event->t == TAROT_HANDFUL_EVENT);
  if (ok)
    {
      size_t i;
      for (i = 0; i < max && i + start < event->n; i++)
        {
          cards[i] = event->data[i + start];
        }
      *n_cards = event->n;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

static inline TarotGameEventError
event_get_card (const TarotGameEvent * event, TarotCard * card)
{
  int ok = (event->t == TAROT_CARD_EVENT);
  if (ok)
    {
      *card = event->u.card;
      return TAROT_EVENT_OK;
    }
  return TAROT_EVENT_ERRTP;
}

#endif /* not H_TAROT_GAME_EVENT_PRIVATE_IMPL_INCLUDED */
