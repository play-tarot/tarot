/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_EVENT_INCLUDED
#define H_TAROT_GAME_EVENT_INCLUDED

#include <tarot/bid.h>
#include <tarot/player.h>
#include <tarot/step.h>
#include <tarot/cards.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotGameEvent;
  typedef struct TarotGameEvent TarotGameEvent;

  typedef enum
  {
    TAROT_SETUP_EVENT = 0,
    TAROT_DEAL_EVENT,
    TAROT_DEAL_ALL_EVENT,
    TAROT_BID_EVENT,
    TAROT_DECL_EVENT,
    TAROT_CALL_EVENT,
    TAROT_DOG_EVENT,
    TAROT_DISCARD_EVENT,
    TAROT_HANDFUL_EVENT,
    TAROT_CARD_EVENT
  } TarotGameEventT;

  typedef enum
  {
    TAROT_EVENT_OK = 0,

    /*
     * Could not query the data; the type is incorrect.
     */
    TAROT_EVENT_ERRTP
  } TarotGameEventError;

  /**
   * tarot_game_event_type:
   * @event: the event.
   * Returns: its type.
   */
  TarotGameEventT tarot_game_event_type (const TarotGameEvent * event);

  /**
   * tarot_game_event_alloc_setup: (constructor)
   * @with_call: (type boolean):
   */
  TarotGameEvent *tarot_game_event_alloc_setup (size_t n_players,
                                                int with_call);

  /**
   * tarot_game_event_alloc_deal: (constructor)
   * @cards: (array length=n_cards):
   */
  TarotGameEvent *tarot_game_event_alloc_deal (TarotPlayer myself,
                                               size_t n_cards,
                                               const TarotCard * cards);

  /**
   * tarot_game_event_alloc_deal_all: (constructor)
   * @owners: (array length=n_owners):
   */
  TarotGameEvent *tarot_game_event_alloc_deal_all
    (size_t n_owners, const TarotPlayer * owners);

  /**
   * tarot_game_event_alloc_deal_all_random: (constructor)
   * @seed: (array length=seed_size):
   */
  TarotGameEvent *tarot_game_event_alloc_deal_all_random (size_t n_players,
                                                          size_t seed_size,
                                                          const char *seed);

  /**
   * tarot_game_event_alloc_bid: (constructor)
   */
  TarotGameEvent *tarot_game_event_alloc_bid (TarotBid bid);

  /**
   * tarot_game_event_alloc_decl: (constructor)
   * @decl: (type boolean):
   */
  TarotGameEvent *tarot_game_event_alloc_decl (int decl);

  /**
   * tarot_game_event_alloc_call: (constructor)
   */
  TarotGameEvent *tarot_game_event_alloc_call (TarotCard call);

  /**
   * tarot_game_event_alloc_dog: (constructor)
   * @cards: (array length=n_cards):
   */
  TarotGameEvent *tarot_game_event_alloc_dog (size_t n_cards,
                                              const TarotCard * cards);

  /**
   * tarot_game_event_alloc_discard: (constructor)
   * @cards: (array length=n_cards):
   */
  TarotGameEvent *tarot_game_event_alloc_discard (size_t n_cards,
                                                  const TarotCard * cards);

  /**
   * tarot_game_event_alloc_handful: (constructor)
   * @cards: (array length=n_cards):
   */
  TarotGameEvent *tarot_game_event_alloc_handful (size_t n_cards,
                                                  const TarotCard * cards);

  /**
   * tarot_game_event_alloc_card: (constructor)
   */
  TarotGameEvent *tarot_game_event_alloc_card (TarotCard call);

  /**
   * tarot_game_event_dup: (constructor)
   */
  TarotGameEvent *tarot_game_event_dup (const TarotGameEvent * source);
  void tarot_game_event_free (TarotGameEvent * game_event);

  /**
   * tarot_game_event_get_setup:
   * @n_players: (out):
   * @with_call: (out) (type boolean):
   */
  TarotGameEventError tarot_game_event_get_setup (const TarotGameEvent *
                                                  event, size_t *n_players,
                                                  int *with_call);

  /**
   * tarot_game_event_get_deal:
   * @myself: (out):
   * @n_cards: (out):
   * @cards_out: (array length=max):
   */
  TarotGameEventError tarot_game_event_get_deal (const TarotGameEvent * event,
                                                 TarotPlayer * myself,
                                                 size_t *n_cards,
                                                 size_t start,
                                                 size_t max,
                                                 TarotCard * cards_out);

  /**
   * tarot_game_event_get_deal_alloc:
   * @whom: (out):
   * @cards: (out) (array length=n_cards):
   */
  TarotGameEventError tarot_game_event_get_deal_alloc
    (const TarotGameEvent * event,
     TarotPlayer * whom, size_t *n_cards, TarotCard ** cards);

  /**
   * tarot_game_event_get_deal_all:
   * @n_owners: (out):
   * @owners_out: (array length=max):
   */
  TarotGameEventError tarot_game_event_get_deal_all (const TarotGameEvent *
                                                     event, size_t *n_owners,
                                                     size_t start, size_t max,
                                                     TarotPlayer *
                                                     owners_out);

  /**
   * tarot_game_event_get_deal_all_alloc:
   * @owners: (out) (array length=n_owners):
   */
  TarotGameEventError tarot_game_event_get_deal_all_alloc
    (const TarotGameEvent * event, size_t *n_owners, TarotPlayer ** owners);

  /**
   * tarot_game_event_get_bid:
   * @bid: (out):
   */
  TarotGameEventError tarot_game_event_get_bid (const TarotGameEvent * event,
                                                TarotBid * bid);

  /**
   * tarot_game_event_get_decl:
   * @decl: (out) (type boolean):
   */
  TarotGameEventError tarot_game_event_get_decl (const TarotGameEvent * event,
                                                 int *decl);

  /**
   * tarot_game_event_get_call:
   * @call: (out):
   */
  TarotGameEventError tarot_game_event_get_call (const TarotGameEvent * event,
                                                 TarotCard * call);

  /**
   * tarot_game_event_get_dog:
   * @n_cards: (out):
   * @cards_out: (array length=max):
   */
  TarotGameEventError tarot_game_event_get_dog (const TarotGameEvent * event,
                                                size_t *n_cards,
                                                size_t start,
                                                size_t max,
                                                TarotCard * cards_out);

  /**
   * tarot_game_event_get_dog_alloc:
   * @cards: (out) (array length=n_cards):
   */
  TarotGameEventError tarot_game_event_get_dog_alloc
    (const TarotGameEvent * event, size_t *n_cards, TarotCard ** cards);

  /**
   * tarot_game_event_get_discard:
   * @n_cards: (out):
   * @cards_out: (array length=max):
   */
  TarotGameEventError tarot_game_event_get_discard (const TarotGameEvent *
                                                    event, size_t *n_cards,
                                                    size_t start, size_t max,
                                                    TarotCard * cards_out);

  /**
   * tarot_game_event_get_discard_alloc:
   * @cards: (out) (array length=n_cards):
   */
  TarotGameEventError tarot_game_event_get_discard_alloc
    (const TarotGameEvent * event, size_t *n_cards, TarotCard ** cards);

  /**
   * tarot_game_event_get_handful:
   * @n_cards: (out):
   * @cards_out: (array length=max):
   */
  TarotGameEventError tarot_game_event_get_handful (const TarotGameEvent *
                                                    event, size_t *n_cards,
                                                    size_t start, size_t max,
                                                    TarotCard * cards_out);

  /**
   * tarot_game_event_get_handful_alloc:
   * @cards: (out) (array length=n_cards):
   */
  TarotGameEventError tarot_game_event_get_handful_alloc
    (const TarotGameEvent * event, size_t *n_cards, TarotCard ** cards);

  /**
   * tarot_game_event_get_card:
   * @card: (out):
   */
  TarotGameEventError tarot_game_event_get_card (const TarotGameEvent * event,
                                                 TarotCard * card);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_GAME_EVENT_INCLUDED */
