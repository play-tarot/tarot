/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_EVENT_PRIVATE_INCLUDED
#define H_TAROT_GAME_EVENT_PRIVATE_INCLUDED

#include <tarot/game_event.h>
#include <tarot_private.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */
  static TarotGameEventT event_type (const TarotGameEvent * event);
  static void event_init_setup (TarotGameEvent * event, size_t n_players,
                                int with_call);
  static void event_init_deal (TarotGameEvent * event, TarotPlayer myself,
                               size_t n_cards, const TarotCard * cards,
                               size_t n_data, unsigned int *data);
  static void event_init_deal_all (TarotGameEvent * event, size_t n_owners,
                                   const TarotPlayer * owners, size_t n_data,
                                   unsigned int *data);
  static void event_init_deal_all_random (TarotGameEvent * event,
                                          size_t n_players, size_t seed_size,
                                          const uint8_t * seed, size_t n_data,
                                          unsigned int *data);
  static void event_init_bid (TarotGameEvent * event, TarotBid bid);
  static void event_init_decl (TarotGameEvent * event, int decl);
  static void event_init_call (TarotGameEvent * event, TarotCard call);
  static void event_init_dog (TarotGameEvent * event, size_t n_cards,
                              const TarotCard * cards, size_t n_data,
                              unsigned int *data);
  static void event_init_discard (TarotGameEvent * event, size_t n_cards,
                                  const TarotCard * cards, size_t n_data,
                                  unsigned int *data);
  static void event_init_handful (TarotGameEvent * event, size_t n_cards,
                                  const TarotCard * cards, size_t n_data,
                                  unsigned int *data);
  static void event_init_card (TarotGameEvent * event, TarotCard card);
  static void event_init_copy (TarotGameEvent * event,
                               const TarotGameEvent * source, size_t n_data,
                               unsigned int *data);

  static const unsigned int *event_get_data (const TarotGameEvent * event,
                                             size_t *n_data);

  static TarotGameEventError event_get_setup (const TarotGameEvent * event,
                                              size_t *n_players,
                                              int *with_call);
  static TarotGameEventError event_get_deal (const TarotGameEvent * event,
                                             TarotPlayer * myself,
                                             size_t *n_cards, size_t start,
                                             size_t max, TarotCard * cards);
  static TarotGameEventError event_get_deal_all (const TarotGameEvent * event,
                                                 size_t *n_owners,
                                                 size_t start, size_t max,
                                                 TarotPlayer * owners);
  static TarotGameEventError event_get_bid (const TarotGameEvent * event,
                                            TarotBid * bid);
  static TarotGameEventError event_get_decl (const TarotGameEvent * event,
                                             int *decl);
  static TarotGameEventError event_get_call (const TarotGameEvent * event,
                                             TarotCard * call);
  static TarotGameEventError event_get_dog (const TarotGameEvent * event,
                                            size_t *n_cards, size_t start,
                                            size_t max, TarotCard * cards);
  static TarotGameEventError event_get_discard (const TarotGameEvent * event,
                                                size_t *n_cards, size_t start,
                                                size_t max,
                                                TarotCard * cards);
  static TarotGameEventError event_get_handful (const TarotGameEvent * event,
                                                size_t *n_cards, size_t start,
                                                size_t max,
                                                TarotCard * cards);
  static TarotGameEventError event_get_card (const TarotGameEvent * event,
                                             TarotCard * card);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_GAME_EVENT_PRIVATE_INCLUDED */
