/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/game_event_private.h>
#include <string.h>
#include <stdlib.h>
#include "xalloc.h"
#include <assert.h>

#ifdef __FRAMAC__
#undef alignof
#define alignof(x) (16)
#endif /* __FRAMAC__ */

TarotGameEventT
tarot_game_event_type (const TarotGameEvent * event)
{
  return event_type (event);
}

TarotGameEvent *
tarot_game_event_alloc_setup (size_t n_players, int with_call)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  event_init_setup (ret, n_players, with_call);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_deal (TarotPlayer myself, size_t n_cards,
                             const TarotCard * cards)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (n_cards * sizeof (unsigned int));
  event_init_deal (ret, myself, n_cards, cards, n_cards, data);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_deal_all (size_t n_owners, const TarotPlayer * owners)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (n_owners * sizeof (unsigned int));
  event_init_deal_all (ret, n_owners, owners, n_owners, data);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_deal_all_random (size_t n_players, size_t seed_size,
                                        const char *seed)
{
  uint8_t *seed_u8 = xmalloc (seed_size * sizeof (uint8_t));
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (78 * sizeof (unsigned int));
  memcpy (seed_u8, seed, seed_size);
  event_init_deal_all_random (ret, n_players, seed_size, seed_u8, 78, data);
  free (seed_u8);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_bid (TarotBid bid)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  event_init_bid (ret, bid);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_decl (int decl)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  event_init_decl (ret, decl);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_call (TarotCard call)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  event_init_call (ret, call);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_dog (size_t n_cards, const TarotCard * cards)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (n_cards * sizeof (unsigned int));
  event_init_dog (ret, n_cards, cards, n_cards, data);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_discard (size_t n_cards, const TarotCard * cards)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (n_cards * sizeof (unsigned int));
  event_init_discard (ret, n_cards, cards, n_cards, data);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_handful (size_t n_cards, const TarotCard * cards)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  unsigned int *data = xmalloc (n_cards * sizeof (unsigned int));
  event_init_handful (ret, n_cards, cards, n_cards, data);
  return ret;
}

TarotGameEvent *
tarot_game_event_alloc_card (TarotCard call)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  event_init_card (ret, call);
  return ret;
}

TarotGameEvent *
tarot_game_event_dup (const TarotGameEvent * source)
{
  TarotGameEvent *ret = xmalloc (sizeof (TarotGameEvent));
  size_t n_data;
  unsigned int *data = NULL;
  event_get_data (source, &n_data);
  if (n_data != 0)
    {
      data = xmalloc (n_data * sizeof (unsigned int));
    }
  event_init_copy (ret, source, n_data, data);
  return ret;
}

void
tarot_game_event_free (TarotGameEvent * game_event)
{
  if (game_event != NULL)
    {
      free (game_event->data);
    }
  free (game_event);
}

TarotGameEventError
tarot_game_event_get_setup (const TarotGameEvent * event,
                            size_t *n_players, int *with_call)
{
  return event_get_setup (event, n_players, with_call);
}

TarotGameEventError
tarot_game_event_get_deal (const TarotGameEvent * event,
                           TarotPlayer * myself,
                           size_t *n_cards,
                           size_t start, size_t max, TarotCard * cards)
{
  return event_get_deal (event, myself, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_deal_alloc (const TarotGameEvent * event,
                                 TarotPlayer * whom, size_t *n_cards,
                                 TarotCard ** cards)
{
  TarotGameEventError ret = event_get_deal (event, whom, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_deal (event, whom, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_deal_all (const TarotGameEvent * event,
                               size_t *n_owners,
                               size_t start, size_t max, TarotPlayer * owners)
{
  return event_get_deal_all (event, n_owners, start, max, owners);
}

TarotGameEventError
tarot_game_event_get_deal_all_alloc (const TarotGameEvent * event,
                                     size_t *n_owners, TarotPlayer ** owners)
{
  TarotGameEventError ret = event_get_deal_all (event, n_owners, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_owners_2;
      *owners = xmalloc (*n_owners * sizeof (TarotCard));
      ret2 = event_get_deal_all (event, &n_owners_2, 0, *n_owners, *owners);
      assert (ret2 == ret);
      assert (*n_owners == n_owners_2);
      (void) ret;
      (void) n_owners_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_bid (const TarotGameEvent * event, TarotBid * bid)
{
  return event_get_bid (event, bid);
}

TarotGameEventError
tarot_game_event_get_decl (const TarotGameEvent * event, int *decl)
{
  return event_get_decl (event, decl);
}

TarotGameEventError
tarot_game_event_get_call (const TarotGameEvent * event, TarotCard * call)
{
  return event_get_call (event, call);
}

TarotGameEventError
tarot_game_event_get_dog (const TarotGameEvent * event,
                          size_t *n_cards,
                          size_t start, size_t max, TarotCard * cards)
{
  return event_get_dog (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_dog_alloc (const TarotGameEvent * event, size_t *n_cards,
                                TarotCard ** cards)
{
  TarotGameEventError ret = event_get_dog (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_dog (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_discard (const TarotGameEvent * event,
                              size_t *n_cards,
                              size_t start, size_t max, TarotCard * cards)
{
  return event_get_discard (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_discard_alloc (const TarotGameEvent * event,
                                    size_t *n_cards, TarotCard ** cards)
{
  TarotGameEventError ret = event_get_discard (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_discard (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_handful (const TarotGameEvent * event,
                              size_t *n_cards,
                              size_t start, size_t max, TarotCard * cards)
{
  return event_get_handful (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_handful_alloc (const TarotGameEvent * event,
                                    size_t *n_cards, TarotCard ** cards)
{
  TarotGameEventError ret = event_get_handful (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_handful (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_card (const TarotGameEvent * event, TarotCard * card)
{
  return event_get_card (event, card);
}

#include <tarot/game_event_private_impl.h>
