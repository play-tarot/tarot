/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_LAYOUT_INCLUDED
#define H_TAROT_LAYOUT_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotPosition
  {
    double x;
    double y;
    double z;
    double scale;
    double angle;
  };
  typedef struct TarotPosition TarotPosition;

  struct TarotCollision
  {
    double x;
    double y;
    size_t card_index;
  };
  typedef struct TarotCollision TarotCollision;

  /**
   * tarot_layout_hand:
   * @position_out: (array length=max):
   */
  size_t tarot_layout_hand (double view_w, double view_h, double card_w,
                            double card_h, size_t n_cards, size_t max,
                            size_t start, TarotPosition * position_out);

  /**
   * tarot_layout_trick:
   * @position_out: (array length=max):
   */
  size_t tarot_layout_trick (double view_w, double view_h, double card_w,
                             double card_h, size_t n_players, size_t max,
                             size_t start, TarotPosition * position_out);

  /**
   * tarot_layout_center:
   * @position_out: (array length=max):
   */
  size_t tarot_layout_center (double view_w, double view_h, double card_w,
                              double card_h, size_t n_cards, size_t max,
                              size_t start, TarotPosition * position_out);

  /**
   * tarot_layout_hand_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_hand_alloc (double view_w, double view_h, double card_w,
                                double card_h, size_t n_cards,
                                size_t *n_positions,
                                TarotPosition ** positions);

  /**
   * tarot_layout_trick_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_trick_alloc (double view_w, double view_h, double card_w,
                                 double card_h, size_t n_players,
                                 size_t *n_positions,
                                 TarotPosition ** positions);

  /**
   * tarot_layout_center_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_center_alloc (double view_w, double view_h, double card_w,
                                  double card_h, size_t n_cards,
                                  size_t *n_positions,
                                  TarotPosition ** positions);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_LAYOUT_INCLUDED */
