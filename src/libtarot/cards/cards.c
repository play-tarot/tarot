/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/cards_private.h>
#include <stdlib.h>
#include <assert.h>

#ifndef __FRAMAC__
#include "xalloc.h"
#endif /* __FRAMAC__ */

/*@
  requires \valid(c);
  assigns *c;

  behavior compute_card_ok: 
  assumes of_ok (n, s);
  ensures *c == of (n, s);
  ensures \result == 0;

  behavior compute_card_fail:
  assumes !of_ok (n, s);
  ensures \result != 0;

  complete behaviors;
  disjoint behaviors;
*/
int
tarot_of (TarotNumber n, TarotSuit s, TarotCard * c)
{
  return of (n, s, c);
}

/*@
  requires \valid(n);
  requires \valid(s);
  requires \separated (n, s);
  assigns *n, *s;

  behavior decompose_ok:
  assumes (c < TAROT_EXCUSE);
  ensures composition_valid: of_ok (*n, *s);
  ensures composition_correct: of (*n, *s) == c;
  ensures \result == 0;

  behavior decompose_fail:
  assumes (c >= TAROT_EXCUSE);
  ensures \result != 0;

  complete behaviors;
  disjoint behaviors;
*/
int
tarot_decompose (TarotCard c, TarotNumber * n, TarotSuit * s)
{
  return decompose (c, n, s);
}

#ifndef __FRAMAC__
TarotCard
tarot_string_to_card (const char *text)
{
  size_t n_read = 0;
  TarotCard ret = card_parse (text, &n_read);
  if (n_read != 0 && text[n_read] == '\0')
    {
      return ret;
    }
  return (TarotCard) (-1);
}

TarotCard
tarot_string_to_card_c (const char *text)
{
  size_t n_read = 0;
  TarotCard ret = card_parse_c (text, &n_read);
  if (n_read != 0 && text[n_read] == '\0')
    {
      return ret;
    }
  return (TarotCard) (-1);
}

TarotCard
tarot_card_parse (const char *text, char **end)
{
  size_t n_read = 0;
  TarotCard c = card_parse (text, &n_read);
  if (end != NULL)
    {
      *end = (char *) text + n_read;
    }
  return c;
}

TarotCard
tarot_card_parse_c (const char *text, char **end)
{
  size_t n_read = 0;
  TarotCard c = card_parse_c (text, &n_read);
  if (end != NULL)
    {
      *end = (char *) text + n_read;
    }
  return c;
}

size_t
tarot_card_to_string (TarotCard c, size_t max, char *dest)
{
  return card_to_string (c, max, dest);
}

char *
tarot_card_to_string_alloc (TarotCard p)
{
  size_t n = card_to_string (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t check = card_to_string (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}

size_t
tarot_card_to_string_c (TarotCard c, size_t max, char *dest)
{
  return card_to_string_c (c, max, dest);
}

char *
tarot_card_to_string_c_alloc (TarotCard p)
{
  size_t n = card_to_string_c (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t check = card_to_string_c (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}
#endif /* not __FRAMAC__ */

#include <tarot/cards_private_impl.h>
