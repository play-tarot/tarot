/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020, 2024  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

int
main ()
{
  char buffer_too_short[1];
  char buffer_just_right[3];
  char buffer_large_enough[4];
  size_t needed;
  needed = tarot_card_to_string (TAROT_EXCUSE, 0, NULL);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 1, buffer_too_short);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_too_short[0] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 3, buffer_just_right);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_just_right[2] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 4, buffer_large_enough);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_large_enough[3] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  if (strcmp (buffer_large_enough, "EXC") != 0)
    {
      fprintf (stderr, "The cards test failed: buffer set to garbage.\n");
      abort ();
    }
  if (tarot_string_to_card ("EXC") != TAROT_EXCUSE)
    {
      fprintf (stderr, "The cards test failed: the excuse is %u != %u.\n",
               tarot_string_to_card ("EXC"), TAROT_EXCUSE);
      abort ();
    }
  if (tarot_string_to_card ("AH") != 0)
    {
      fprintf (stderr,
               "The cards test failed: the ace of hearts is %u != %u.\n",
               tarot_string_to_card ("AH"), 0);
      abort ();
    }
  if (tarot_string_to_card ("garbage") < 78)
    {
      fprintf (stderr, "The cards test failed: garbage recognized as %u.\n",
               tarot_string_to_card ("garbage"));
      abort ();
    }
  int i;
  TarotNumber number;
  TarotSuit suit;
  int seen_as_trumps[21] = { 0 };
  int seen_as_minor[14][4] = { 0 };
  /* Check that decompose is surjective */
  for (i = 0; i < 77; i++)
    {
      if (tarot_decompose (i, &number, &suit) != 0)
	{
	  abort ();
	}
      if (suit == TAROT_TRUMPS)
	{
	  if (number < 1 || number > 21)
	    {
	      abort ();
	    }
	  seen_as_trumps[number - 1] = 1;
	}
      else
	{
	  if (suit != TAROT_HEARTS
	      && suit != TAROT_CLUBS
	      && suit != TAROT_DIAMONDS
	      && suit != TAROT_SPADES)
	    {
	      abort ();
	    }
	  if (number < 1 || number > TAROT_KING)
	    {
	      abort ();
	    }
	  seen_as_minor[number - 1][suit] = 1;
	}
    }
  for (i = 0; i < 21; i++)
    {
      if (!seen_as_trumps[i])
	{
	  abort ();
	}
    }
  for (i = 0; i < 4; i++)
    {
      int j;
      for (j = 0; j < 14; j++)
	{
	  if (!seen_as_minor[j][i])
	    {
	      abort ();
	    }
	}
    }
  /* Check that of (decompose (.)) is the identity */
  for (i = 0; i < 77; i++)
    {
      TarotCard rebuilt;
      if (tarot_decompose (i, &number, &suit) != 0)
	{
	  abort ();
	}
      if (tarot_of (number, suit, &rebuilt) != 0)
	{
	  abort ();
	}
      assert (i >= 0);
      assert (i < 77);
      /* Casting i to an unsigned int is fine. */
      if (rebuilt != i)
	{
	  abort ();
	}
    }
  /* Since decompose is surjective and of o decompose = identity, then
   * of is the inverse of decompose. */
  if (tarot_decompose (TAROT_EXCUSE, &number, &suit) == 0)
    {
      abort ();
    }
  fprintf (stderr, "Cards test: OK\n");
  return 0;
}
