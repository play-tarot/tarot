/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_CARDS_PRIVATE_INCLUDED
#define H_TAROT_CARDS_PRIVATE_INCLUDED
#include <stddef.h>
#include <string.h>
#include <tarot/cards.h>
#define N_(String) (String)
#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

#define TAROT_AH 0
#define TAROT_2H 1
#define TAROT_3H 2
#define TAROT_4H 3
#define TAROT_5H 4
#define TAROT_6H 5
#define TAROT_7H 6
#define TAROT_8H 7
#define TAROT_9H 8
#define TAROT_10H 9
#define TAROT_JH 10
#define TAROT_CH 11
#define TAROT_QH 12
#define TAROT_KH 13
#define TAROT_AC 14
#define TAROT_2C 15
#define TAROT_3C 16
#define TAROT_4C 17
#define TAROT_5C 18
#define TAROT_6C 19
#define TAROT_7C 20
#define TAROT_8C 21
#define TAROT_9C 22
#define TAROT_10C 23
#define TAROT_JC 24
#define TAROT_CC 25
#define TAROT_QC 26
#define TAROT_KC 27
#define TAROT_AD 28
#define TAROT_2D 29
#define TAROT_3D 30
#define TAROT_4D 31
#define TAROT_5D 32
#define TAROT_6D 33
#define TAROT_7D 34
#define TAROT_8D 35
#define TAROT_9D 36
#define TAROT_10D 37
#define TAROT_JD 38
#define TAROT_CD 39
#define TAROT_QD 40
#define TAROT_KD 41
#define TAROT_AS 42
#define TAROT_2S 43
#define TAROT_3S 44
#define TAROT_4S 45
#define TAROT_5S 46
#define TAROT_6S 47
#define TAROT_7S 48
#define TAROT_8S 49
#define TAROT_9S 50
#define TAROT_10S 51
#define TAROT_JS 52
#define TAROT_CS 53
#define TAROT_QS 54
#define TAROT_KS 55
#define TAROT_1T TAROT_PETIT
#define TAROT_2T 57
#define TAROT_3T 58
#define TAROT_4T 59
#define TAROT_5T 60
#define TAROT_6T 61
#define TAROT_7T 62
#define TAROT_8T 63
#define TAROT_9T 64
#define TAROT_10T 65
#define TAROT_11T 66
#define TAROT_12T 67
#define TAROT_13T 68
#define TAROT_14T 69
#define TAROT_15T 70
#define TAROT_16T 71
#define TAROT_17T 72
#define TAROT_18T 73
#define TAROT_19T 74
#define TAROT_20T 75
#define TAROT_21T TAROT_TWENTYONE
#define TAROT_EXC TAROT_EXCUSE

  /*@
     logic boolean of_ok_minor (TarotNumber n) = (n >= 1 && n <= TAROT_KING);
   */

  /*@
     logic boolean of_ok_trump (TarotNumber n) = (n >= 1 && n <= 21);
   */

  /*@ 
     logic boolean of_ok (TarotNumber n, TarotSuit s) =
     (s == TAROT_TRUMPS && of_ok_trump (n)) || (s < TAROT_TRUMPS && of_ok_minor (n));
   */

  /*@
     logic TarotCard of (TarotNumber n, TarotSuit s) =
     (TarotCard) (of_ok (n, s) ? s * TAROT_KING + n - 1 : -1);
   */

  /*@
     requires \valid(c);
     assigns *c;

     behavior compute_card_ok: 
     assumes of_ok (n, s);
     ensures *c == of (n, s);
     ensures \result == 0;

     behavior compute_card_fail:
     assumes !of_ok (n, s);
     ensures \result != 0;

     complete behaviors;
     disjoint behaviors;
   */
  static int of (TarotNumber n, TarotSuit s, TarotCard * c);

  /*@
     requires \valid(n);
     requires \valid(s);
     requires \separated (n, s);
     assigns *n, *s;

     behavior decompose_ok:
     assumes (c < TAROT_EXCUSE);
     ensures composition_valid: of_ok (*n, *s);
     ensures composition_correct: of (*n, *s) == c;
     ensures \result == 0;

     behavior decompose_fail:
     assumes (c >= TAROT_EXCUSE);
     ensures \result != 0;

     complete behaviors;
     disjoint behaviors;
   */
  static int decompose (TarotCard c, TarotNumber * n, TarotSuit * s);

#ifndef __FRAMAC__
  static TarotCard card_parse (const char *text, size_t *n_read);
  static TarotCard card_parse_c (const char *text, size_t *n_read);
  static size_t card_to_string (TarotCard c, size_t max, char *dest);
  static size_t card_to_string_c (TarotCard c, size_t max, char *dest);
#endif                          /* not __FRAMAC__ */

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_CARDS_PRIVATE_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
