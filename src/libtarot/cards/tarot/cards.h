/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_CARDS_INCLUDED
#define H_TAROT_CARDS_INCLUDED
#include <stddef.h>
#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /**
   * TarotCard:
   * A card is an integer between 0 and 78. 
   */
  typedef unsigned int TarotCard;

  /**
   * TAROT_ACE:
   * The ace is the other number for the 1, except for trumps. 
   */
#define TAROT_ACE 1

  /**
   * TAROT_JACK:
   * The jack is the smallest face card. 
   */
#define TAROT_JACK 11

  /**
   * TAROT_KNIGHT:
   * Face card between jack and queen. 
   */
#define TAROT_KNIGHT 12

  /**
   * TAROT_QUEEN:
   * Face card between knight and king. 
   */
#define TAROT_QUEEN 13

  /**
   * TAROT_KING:
   * The strongest face card. 
   */
#define TAROT_KING 14

  /**
   * TAROT_PETIT:
   */
#define TAROT_PETIT 56

  /**
   * TAROT_TWENTYONE:
   */
#define TAROT_TWENTYONE 76

  /**
   * TAROT_EXCUSE:
   * 77 is always the excuse.
   */
#define TAROT_EXCUSE 77

  /**
   * TarotNumber:
   * A number is an integer between 1 and TAROT_KING + 1 or 22. 
   */
  typedef unsigned int TarotNumber;

  /**
   * TarotSuit:
   * Enumeration of tarot suits. 
   */
  typedef enum
  {
    TAROT_HEARTS,
    TAROT_CLUBS,
    TAROT_DIAMONDS,
    TAROT_SPADES,
    TAROT_TRUMPS
  } TarotSuit;

  /**
   * tarot_of:
   * @number: the number.
   * @suit: the suit.
   * @card: (out): set to the n of s.
   * Returns: whether there were an error.
  */
  int tarot_of (TarotNumber number, TarotSuit suit, TarotCard * card);

  /**
   * tarot_decompose:
   * @c: the card.
   * @n: (out): set to the number of @c.
   * @s: (out): set to the suit of @c.
   * Returns: whether there were an error.
  */
  int tarot_decompose (TarotCard c, TarotNumber * n, TarotSuit * s);
  TarotCard tarot_string_to_card (const char *text);
  TarotCard tarot_string_to_card_c (const char *text);

#ifndef __FRAMAC__
  /**
   * tarot_card_parse:
   * @text:
   * @end: (out) (nullable) (transfer none):
   * Returns: the parsed card.
   */
  TarotCard tarot_card_parse (const char *text, char **end);

  /**
   * tarot_card_parse_c:
   * @text:
   * @end: (out) (nullable) (transfer none):
   * Returns: the parsed card.
   */
  TarotCard tarot_card_parse_c (const char *text, char **end);

  /**
   * tarot_card_to_string:
   * @dest_out: (array length=max): where to represent the card.
   */
  size_t tarot_card_to_string (TarotCard c, size_t max, char *dest_out);

  /**
   * tarot_card_to_string_alloc: (rename-to tarot_card_to_string)
   */
  char *tarot_card_to_string_alloc (TarotCard c);

  /**
   * tarot_card_to_string_c:
   * @dest_out: (array length=max): where to represent the card.
   */
  size_t tarot_card_to_string_c (TarotCard c, size_t max, char *dest_out);

  /**
   * tarot_card_to_string_c_alloc: (rename-to tarot_card_to_string_c)
   */
  char *tarot_card_to_string_c_alloc (TarotCard c);
#endif                          /* not __FRAMAC__ */

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_CARDS_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
