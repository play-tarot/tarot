/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020, 2024  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED
#define H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED
#include <stddef.h>
#include <tarot/cards_private.h>
#include <string.h>
#include <assert.h>
#ifndef __FRAMAC__
#include <gettext.h>
#define _(String) dgettext (PACKAGE, String)
#else /* __FRAMAC__ */
#define _(String) (String)
#endif /* __FRAMAC__ */

static inline int
of (TarotNumber n, TarotSuit s, TarotCard * c)
{
  unsigned int suit_order;
  if (s == TAROT_TRUMPS)
    {
      if (n == 0 || n > 21)
        {
          /* N of trumps, where N is incorrect */
          /*@ assert !of_ok_trump (n); */
          return 1;
        }
      /*@ assert of_ok (n, s); */
      /*@ assert s * TAROT_KING == TAROT_PETIT; */
      *c = TAROT_PETIT + n - 1;
      return 0;
    }
  else if (n == 0 || n > TAROT_KING)
    {
      /* N of <minor>, where N is incorrect */
      /*@ assert !of_ok_minor (n); */
      return 1;
    }
  switch (s)
    {
    case TAROT_HEARTS:
      suit_order = 0;
      break;
    case TAROT_CLUBS:
      suit_order = 1;
      break;
    case TAROT_DIAMONDS:
      suit_order = 2;
      break;
    case TAROT_SPADES:
      suit_order = 3;
      break;
    default:
      /* Incorrect suit value */
      return 1;
    }
  /*@ assert of_ok (n, s); */
  /*@ assert suit_order == s; */
  *c = suit_order * TAROT_KING + n - 1;
  return 0;
}

static inline int
decompose (TarotCard c, TarotNumber * n, TarotSuit * s)
{
  const TarotSuit suits[4] = {
    /* In the same order as above */
    TAROT_HEARTS,
    TAROT_CLUBS,
    TAROT_DIAMONDS,
    TAROT_SPADES,
  };
  if (c >= 77)
    {
      return 1;
    }
  if (c >= TAROT_PETIT)
    {
      *s = TAROT_TRUMPS;
      *n = c - TAROT_PETIT + 1;
      /*@ assert of_ok_trump (*n); */
      /*@ assert of_ok (*n, *s); */
      /*@ assert c == TAROT_TRUMPS * TAROT_KING + *n - 1; */
      return 0;
    }
  /*@ assert c < 56; */
  /*@ assert c < TAROT_KING * 4; */
  /*@ assert c / TAROT_KING < 4; */
  *s = suits[c / TAROT_KING];
  /*@ assert *s < TAROT_TRUMPS; */
  /*@ assert *s == c / TAROT_KING; */
  *n = c % TAROT_KING + 1;
  /*@ assert *n >= 1; */
  /*@ assert *n <= TAROT_KING; */
  /*@ assert of_ok_minor (*n); */
  /*@ assert of_ok (*n, *s); */
  /*@ assert c == *s * TAROT_KING + *n - 1; */
  return 0;
}

#ifndef __FRAMAC__

static inline void
get_card_name (TarotCard i, char *dest)
{
  const char *card_names[] = {
    N_("AH"), N_("2H"), N_("3H"), N_("4H"), N_("5H"),
    N_("6H"), N_("7H"), N_("8H"), N_("9H"), N_("10H"),
    N_("JH"), N_("CH"), N_("QH"), N_("KH"), N_("AC"),
    N_("2C"), N_("3C"), N_("4C"), N_("5C"), N_("6C"),
    N_("7C"), N_("8C"), N_("9C"), N_("10C"), N_("JC"),
    N_("CC"), N_("QC"), N_("KC"), N_("AD"), N_("2D"),
    N_("3D"), N_("4D"), N_("5D"), N_("6D"), N_("7D"),
    N_("8D"), N_("9D"), N_("10D"), N_("JD"), N_("CD"),
    N_("QD"), N_("KD"), N_("AS"), N_("2S"), N_("3S"),
    N_("4S"), N_("5S"), N_("6S"), N_("7S"), N_("8S"),
    N_("9S"), N_("10S"), N_("JS"), N_("CS"), N_("QS"),
    N_("KS"), N_("1T"), N_("2T"), N_("3T"), N_("4T"),
    N_("5T"), N_("6T"), N_("7T"), N_("8T"), N_("9T"),
    N_("10T"), N_("11T"), N_("12T"), N_("13T"), N_("14T"),
    N_("15T"), N_("16T"), N_("17T"), N_("18T"), N_("19T"),
    N_("20T"), N_("21T"), N_("EXC")
  };
  assert (strlen (card_names[i]) <= 3);
  strcpy (dest, card_names[i]);
  dest[3] = '\0';
}

static inline int
recognize (const char *text, const char *keyword, size_t *n_read)
{
  size_t text_len = strlen (text);
  size_t keyword_len = strlen (keyword);
  if (keyword_len <= text_len && strncmp (text, keyword, keyword_len) == 0)
    {
      *n_read = keyword_len;
      return 1;
    }
  *n_read = 0;
  return 0;
}

static inline TarotCard
card_parse (const char *text, size_t *n_read)
{
  TarotCard i;
  for (i = 0; i < 78; i++)
    {
      char name[4];
      const char *candidate;
      get_card_name (i, name);
      candidate = _(name);
      if (recognize (text, candidate, n_read))
        {
          return i;
        }
    }
  *n_read = 0;
  return (TarotCard) (-1);
}

static inline TarotCard
card_parse_c (const char *text, size_t *n_read)
{
  TarotCard i;
  size_t text_len = strlen (text);
  for (i = 0; i < 78; i++)
    {
      char name[4];
      size_t name_len;
      get_card_name (i, name);
      name_len = strlen (name);
      if (name_len <= text_len && strncmp (text, name, name_len) == 0)
        {
          *n_read = name_len;
          return i;
        }
    }
  *n_read = 0;
  return (TarotCard) (-1);
}

static inline size_t
card_to_string (TarotCard c, size_t max, char *dest)
{
  const char *to_write;
  size_t required;
  char name[4];
  if (c >= 78)
    {
      if (max != 0)
        {
          dest[0] = '\0';
        }
      return 0;
    }
  get_card_name (c, name);
  to_write = _(name);
  required = strlen (to_write);
  if (max > required)
    {
      assert (max != 0);
      assert (dest != NULL);
      memcpy (dest, to_write, required);
      dest[required] = '\0';
    }
  else if (max != 0)
    {
      assert (dest != NULL);
      memcpy (dest, to_write, max - 1);
      dest[max - 1] = '\0';
    }
  return required;
}

static inline size_t
card_to_string_c (TarotCard c, size_t max, char *dest)
{
  size_t required;
  char to_write[4];
  if (c >= 78)
    {
      if (max != 0)
        {
          dest[0] = '\0';
        }
      return 0;
    }
  get_card_name (c, to_write);
  required = strlen (to_write);
  if (max > required)
    {
      assert (max != 0);
      assert (dest != NULL);
      memcpy (dest, to_write, required);
      dest[required] = '\0';
    }
  else if (max != 0)
    {
      assert (dest != NULL);
      memcpy (dest, to_write, max - 1);
      dest[max - 1] = '\0';
    }
  return required;
}

#endif /* not __FRAMAC__ */

#endif /* not H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED */
