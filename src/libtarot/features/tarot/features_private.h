/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_FEATURES_PRIVATE_INCLUDED
#define H_TAROT_FEATURES_PRIVATE_INCLUDED

#include <tarot_private.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */
  static void features_variant (const TarotGame * game, size_t *n_players,
                                int *with_call);
  static void features_position (const TarotGame * game, size_t *i_trick,
                                 size_t *position);
  static void features_partner_known (const TarotGame * game, int *known);
  static void features_bid_minimum (const TarotGame * game,
                                    TarotBid * minimum);
  static void features_count_faces (const TarotGame * game,
                                    size_t *n_kings, size_t *n_marriages,
                                    size_t *n_extended_marriages,
                                    size_t *n_queens, size_t *n_knights);
  static void features_count_minor (const TarotGame * game,
                                    size_t *n_shortest,
                                    size_t *n_second_shortest,
                                    size_t *n_second_longest,
                                    size_t *n_longest,
                                    TarotSuit * suit_order);
  static void features_count_oudlers (const TarotGame * game,
                                      int *has_petit,
                                      int *has_excuse, int *has_twentyone);
  static void features_count_trumps (const TarotGame * game,
                                     size_t *n_trumps,
                                     TarotNumber * max_trump,
                                     TarotNumber * min_trump,
                                     TarotNumber * q1_trumps,
                                     TarotNumber * q3_trumps,
                                     TarotNumber * median_trumps);
  static void strategy_bid (const TarotGame * game,
                            const TarotGameEvent * event, TarotBid * bid);
  static void strategy_outbid (const TarotGame * game,
                               const TarotGameEvent * event, int *outbid);
  static void strategy_call_by_length (const TarotGame * game,
                                       const TarotGameEvent * event,
                                       int *shortest,
                                       int *second_shortest,
                                       int *second_longest, int *longest);
  static void strategy_call_myself (const TarotGame * game,
                                    const TarotGameEvent * event,
                                    int *myself);
  static void strategy_call_marriage (const TarotGame * game,
                                      const TarotGameEvent * event,
                                      int *will_own_marriage);
  static void strategy_discard_save (const TarotGame * game,
                                     const TarotGameEvent * event,
                                     size_t *queens,
                                     size_t *knights, size_t *jacks);
  static void strategy_discard_junk (const TarotGame * game,
                                     const TarotGameEvent * event,
                                     size_t *lesser);
  static void strategy_discard_by_length (const TarotGame * game,
                                          const TarotGameEvent * event,
                                          size_t *shortest,
                                          size_t *second_shortest,
                                          size_t *second_longest,
                                          size_t *longest);
  static void features_team (const TarotGame * game,
                             int *attack, int *defence);
  static void features_trick_players (const TarotGame * game,
                                      size_t *n_already_played,
                                      size_t *n_allies_after_last_opponent);
  static void features_minor_suit (const TarotGame * game,
                                   const TarotCounter * counter,
                                   int *called_suit,
                                   size_t *n_times_lead,
                                   size_t *n_remaining_cards,
                                   size_t *n_remaining_points,
                                   int *ally_may_be_master,
                                   int *ennemy_may_be_master);
  static void features_trump (const TarotGame * game,
                              const TarotCounter * counter,
                              size_t *n_remaining,
                              int *ally_may_be_master,
                              int *ennemy_may_be_master);
  static void features_points (const TarotGame * game,
                               size_t *n_halfpoints, size_t *n_oudlers);
  static void strategy_lead (const TarotGame * game,
                             const TarotCounter * counter,
                             const TarotGameEvent * card,
                             int *called_suit,
                             size_t *suit_length,
                             size_t *n_times_lead, int *trumps);
  static void strategy_purity (const TarotGame * game,
                               const TarotGameEvent * event,
                               size_t *n_smaller_kept,
                               size_t *n_greater_nonface_kept,
                               size_t *n_points_kept, size_t *n_greater_kept);
  static void strategy_excuse (const TarotGame * game,
                               const TarotGameEvent * event, int *excuse);
  static void strategy_trump_master (const TarotGame * game,
                                     const TarotCounter * counter,
                                     const TarotGameEvent * card,
                                     int *actually_master,
                                     int *obvious_master);
  static void strategy_play_points (const TarotGame * game,
                                    const TarotGameEvent * card,
                                    size_t *n_halfpoints, int *oudler);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_FEATURES_PRIVATE_INCLUDED */
