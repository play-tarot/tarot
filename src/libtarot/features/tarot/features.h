/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_FEATURES_INCLUDED
#define H_TAROT_FEATURES_INCLUDED

#include <stddef.h>
#include <tarot/bid.h>
#include <tarot/cards.h>

#ifdef __cplusplus
extern "C"
{
#endif

  struct TarotGame;

  struct TarotGameEvent;

  /* General-purpose features */

  /**
   * tarot_features_variant:
   * @game: (type TarotGame):
   * @n_players: (out):
   * @with_call: (type boolean) (out):
   */
  void tarot_features_variant (const struct TarotGame *game,
                               size_t *n_players, int *with_call);

  /**
   * tarot_features_position:
   * @game: (type TarotGame):
   * @i_trick: (out): the number of tricks played so far
   * @position: (out): the number of players that play before me (for
   * bids, decls, or for the current trick)
   */
  void tarot_features_position (const struct TarotGame *game, size_t *i_trick,
                                size_t *position);

  /**
   * tarot_features_partner_known:
   * @game: (type TarotGame):
   * @known: (out) (type boolean):
   */
  void tarot_features_partner_known (const struct TarotGame *game,
                                     int *known);

  /* Features specific to bidding */

  /**
   * tarot_features_bid_minimum:
   * @game: (type TarotGame):
   * @minimum: (out):
   */
  void tarot_features_bid_minimum (const struct TarotGame *game,
                                   TarotBid * minimum);

  /**
   * tarot_features_count_faces:
   * @game: (type TarotGame):
   * @n_kings: (out):
   * @n_marriages: (out):
   * @n_extended_marriages: (out):
   * @n_queens: (out):
   * @n_knights: (out):
   */
  void tarot_features_count_faces (const struct TarotGame *game,
                                   size_t *n_kings,
                                   size_t *n_marriages,
                                   size_t *n_extended_marriages,
                                   size_t *n_queens, size_t *n_knights);

  /**
   * tarot_features_count_minor:
   * @game: (type TarotGame):
   * @n_shortest: (out):
   * @n_second_shortest: (out):
   * @n_second_longest: (out):
   * @n_longest: (out):
   */
  void tarot_features_count_minor (const struct TarotGame *game,
                                   size_t *n_shortest,
                                   size_t *n_second_shortest,
                                   size_t *n_second_longest,
                                   size_t *n_longest);

  /**
   * tarot_features_count_oudlers:
   * @game: (type TarotGame):
   * @has_petit: (out) (type boolean):
   * @has_excuse: (out) (type boolean):
   * @has_twentyone: (out) (type boolean):
   */
  void tarot_features_count_oudlers (const struct TarotGame *game,
                                     int *has_petit,
                                     int *has_excuse, int *has_twentyone);

  /**
   * tarot_features_count_trumps:
   * @game: (type TarotGame):
   * @n_trumps: (out):
   * @max_trump: (out):
   * @min_trump: (out):
   * @q1_trumps: (out):
   * @q3_trumps: (out):
   * @median_trumps: (out):
   */
  void tarot_features_count_trumps (const struct TarotGame *game,
                                    size_t *n_trumps,
                                    TarotNumber * max_trump,
                                    TarotNumber * min_trump,
                                    TarotNumber * q1_trumps,
                                    TarotNumber * q3_trumps,
                                    TarotNumber * median_trumps);

  /**
   * tarot_strategy_bid:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @bid: (out):
   */
  void tarot_strategy_bid (const struct TarotGame *game,
                           const struct TarotGameEvent *event,
                           TarotBid * bid);

  /**
   * tarot_strategy_outbid:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @outbid: (out):
   */
  void tarot_strategy_outbid (const struct TarotGame *game,
                              const struct TarotGameEvent *event,
                              int *outbid);

  /**
   * tarot_strategy_call_by_length:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @shortest: (out) (type boolean):
   * @second_shortest: (out) (type boolean):
   * @second_longest: (out) (type boolean):
   * @longest: (out) (type boolean):
   */
  void tarot_strategy_call_by_length (const struct TarotGame *game,
                                      const struct TarotGameEvent *event,
                                      int *shortest,
                                      int *second_shortest,
                                      int *second_longest, int *longest);

  /**
   * tarot_strategy_call_myself:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @myself: (out) (type boolean):
   */
  void tarot_strategy_call_myself (const struct TarotGame *game,
                                   const struct TarotGameEvent *event,
                                   int *myself);

  /**
   * tarot_strategy_call_marriage:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @will_own_marriage: (out) (type boolean):
   */
  void tarot_strategy_call_marriage (const struct TarotGame *game,
                                     const struct TarotGameEvent *event,
                                     int *will_own_marriage);

  /**
   * tarot_strategy_discard_save:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @n_queens_saved: (out): number of queens for which we don't plan to own the king
   * @n_knights_saved: (out):
   * @n_jacks_saved: (out):
   */
  void tarot_strategy_discard_save (const struct TarotGame *game,
                                    const struct TarotGameEvent *event,
                                    size_t *n_queens_saved,
                                    size_t *n_knights_saved,
                                    size_t *n_jacks_saved);

  /**
   * tarot_strategy_discard_junk:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @n_lesser_cards_left_behind: (out):
   */
  void tarot_strategy_discard_junk (const struct TarotGame *game,
                                    const struct TarotGameEvent *event,
                                    size_t *n_lesser_cards_left_behind);

  /**
   * tarot_strategy_discard_by_length:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @from_shortest: (out): number of cards discarded from the shortest suit with the dog
   * @from_second_shortest: (out):
   * @from_second_longest: (out):
   * @from_longest: (out):
   */
  void tarot_strategy_discard_by_length (const struct TarotGame *game,
                                         const struct TarotGameEvent *event,
                                         size_t *from_shortest,
                                         size_t *from_second_shortest,
                                         size_t *from_second_longest,
                                         size_t *from_longest);

  /**
   * tarot_features_team:
   * @game: (type TarotGame):
   * @attack: (out) (type boolean): whether we may be an attacker
   * @defence: (out) (type boolean): whether we may be a defendent
   */
  void tarot_features_team (const struct TarotGame *game,
                            int *attack, int *defence);

  /**
   * tarot_features_trick_players:
   * @game: (type TarotGame):
   * @n_already_played: (out):
   * @n_allies_after_last_opponent: (out):
   */
  void tarot_features_trick_players (const struct TarotGame *game,
                                     size_t *n_already_played,
                                     size_t *n_allies_after_last_opponent);

  /**
   * tarot_features_minor_suit:
   * @game: (type TarotGame):
   * @ally_may_be_master: (out) (type boolean): whether noone can play
   * a stronger card of the minor suit than the current taker and the
   * current taker is an ally, or whether an ally who has still not
   * played yet may play the master card of the minor suit.  Same for
   * trumps, but we don't consider the possibility that someone
   * trumps.  That's why we have counted how many cards of the suit 
   * are remaining.
   * @ennemy_may_be_master: (out) (type boolean):
   * @called_suit: (out) (type boolean):
   * @n_times_lead: (out):
   * @n_remaining_cards: (out):
   * @n_remaining_points: (out):
   */
  void tarot_features_minor_suit (const struct TarotGame *game,
                                  int *called_suit,
                                  size_t *n_times_lead,
                                  size_t *n_remaining_cards,
                                  size_t *n_remaining_points,
                                  int *ally_may_be_master,
                                  int *ennemy_may_be_master);

  /**
   * tarot_features_trump:
   * @game: (type TarotGame):
   * @n_remaining: (out):
   * @ally_may_be_master: (out) (type boolean):
   * @ennemy_may_be_master: (out) (type boolean):
   */
  void tarot_features_trump (const struct TarotGame *game,
                             size_t *n_remaining,
                             int *ally_may_be_master,
                             int *ennemy_may_be_master);

  /**
   * tarot_features_points:
   * @game: (type TarotGame):
   * @n_halfpoints: (out):
   * @n_oudlers: (out):
   */
  void tarot_features_points (const struct TarotGame *game,
                              size_t *n_halfpoints, size_t *n_oudlers);

  /**
   * tarot_strategy_lead:
   * @game: (type TarotGame):
   * @card: (type TarotGameEvent):
   * @called_suit: (out) (type boolean):
   * @suit_length: (out):
   * @n_times_lead: (out):
   * @trumps: (out) (type boolean):
   */
  void tarot_strategy_lead (const struct TarotGame *game,
                            const struct TarotGameEvent *card,
                            int *called_suit,
                            size_t *suit_length,
                            size_t *n_times_lead, int *trumps);

  /**
   * tarot_strategy_purity:
   * @game: (type TarotGame):
   * @event: (type TarotGameEvent):
   * @n_smaller_kept: (out):
   * @n_greater_nonface_kept: (out):
   * @n_points_kept: (out):
   * @n_greater_kept: (out):
   */
  void tarot_strategy_purity (const struct TarotGame *game,
                              const struct TarotGameEvent *event,
                              size_t *n_smaller_kept,
                              size_t *n_greater_nonface_kept,
                              size_t *n_points_kept, size_t *n_greater_kept);

  /**
   * tarot_strategy_excuse:
   * @game: (type TarotGame):
   * @card: (type TarotGameEvent):
   * @excuse: (out) (type boolean):
   */
  void tarot_strategy_excuse (const struct TarotGame *game,
                              const struct TarotGameEvent *card, int *excuse);

  /**
   * tarot_strategy_trump_master:
   * @game: (type TarotGame):
   * @card: (type TarotGameEvent):
   * @actually_master: (out) (type boolean):
   * @obvious_master: (out) (type boolean):
   */
  void tarot_strategy_trump_master (const struct TarotGame *game,
                                    const struct TarotGameEvent *card,
                                    int *actually_master,
                                    int *obvious_master);

  /**
   * tarot_strategy_play_points:
   * @game: (type TarotGame):
   * @card: (type TarotGameEvent):
   * @n_halfpoints: (out):
   * @oudler: (out) (type bool):
   */
  void tarot_strategy_play_points (const struct TarotGame *game,
                                   const struct TarotGameEvent *card,
                                   size_t *n_halfpoints, int *oudler);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_FEATURES_INCLUDED */
