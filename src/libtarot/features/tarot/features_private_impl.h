/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_FEATURES_PRIVATE_IMPL_INCLUDED
#define H_TAROT_FEATURES_PRIVATE_IMPL_INCLUDED

#include <tarot/game_private.h>
#include <tarot/game_event_private.h>
#include <tarot/cards_private.h>
#include <tarot/counter_private.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <float.h>
#include <errno.h>
#include "xalloc.h"

static inline void
features_variant (const TarotGame * game, size_t *n_players, int *with_call)
{
  *n_players = game_n_players (game);
  *with_call = game_with_call (game);
}

static inline void
features_position (const TarotGame * game, size_t *i_trick, size_t *position)
{
  TarotPlayer myself, leader;
  *i_trick = 0;
  if (game_get_main_player (game, &myself) != TAROT_GAME_OK)
    {
      *position = 0;
    }
  else
    {
      *position = (size_t) myself;
    }
  if (game_get_current_trick (game, i_trick) != TAROT_GAME_OK)
    {
      *i_trick = 0;
    }
  if (game_get_trick_leader (game, *i_trick, &leader) == TAROT_GAME_OK)
    {
      if (leader > *position)
        {
          *position += game_n_players (game);
        }
      assert (leader <= *position);
      *position -= leader;
    }
}

static inline void
features_partner_known (const TarotGame * game, int *known)
{
  TarotPlayer partner;
  *known = 0;
  if (!game_with_call (game)
      || game_get_partner (game, &partner) == TAROT_GAME_OK)
    {
      *known = 1;
    }
}

static inline void
features_bid_minimum (const TarotGame * game, TarotBid * minimum)
{
  TarotPlayer taker;
  size_t n_bids;
  game_get_taker (game, &taker);
  if (taker < game_n_players (game)
      && game_get_bids (game, &n_bids, taker, 1, minimum) == TAROT_GAME_OK)
    {
      if (n_bids == 0)
        {
          *minimum = TAROT_PASS;
        }
      else
        {
          /* minimum is set */
        }
    }
  else
    {
      *minimum = TAROT_PASS;
    }
}

static inline size_t
features_get_my_cards (const TarotGame * game, size_t max, TarotCard * cards)
{
  TarotPlayer myself;
  if (game_get_next (game, &myself) == TAROT_GAME_OK)
    {
      size_t n_cards;
      if (game_get_cards (game, myself, &n_cards, 0, max, cards) ==
          TAROT_GAME_OK)
        {
          size_t n_dog;
          TarotCard dog[6];
          TarotPlayer taker;
          game_get_taker (game, &taker);
          if (game_step (game) == TAROT_DOG
              && taker == myself
              && game_get_dog (game, &n_dog, 0, 6, dog) == TAROT_GAME_OK)
            {
              size_t i = 0;
              assert (n_dog <= 6);
              for (i = 0; i < n_dog; i++)
                {
                  if (n_cards < max)
                    {
                      cards[n_cards] = dog[i];
                    }
                  n_cards++;
                }
            }
          return n_cards;
        }
    }
  return 0;
}

static inline void
features_count_faces (const TarotGame * game,
                      size_t *n_kings,
                      size_t *n_marriages,
                      size_t *n_extended_marriages,
                      size_t *n_queens, size_t *n_knights)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  int has_knight[4] = { 0, 0, 0, 0 };
  int has_queen[4] = { 0, 0, 0, 0 };
  int has_king[4] = { 0, 0, 0, 0 };
  *n_kings = 0;
  *n_marriages = 0;
  *n_extended_marriages = 0;
  *n_queens = 0;
  *n_knights = 0;
  assert (n_cards <= 78);
  for (i = 0; i < n_cards; i++)
    {
      TarotNumber number;
      TarotSuit suit;
      if (decompose (my_cards[i], &number, &suit) == 0
          && suit != TAROT_TRUMPS && number >= TAROT_KNIGHT)
        {
          size_t suit_index = (size_t) suit;
          assert (suit_index < 4);
          switch (number)
            {
            case TAROT_KNIGHT:
              assert (!has_knight[suit_index]);
              has_knight[suit_index] = 1;
              break;
            case TAROT_QUEEN:
              assert (!has_queen[suit_index]);
              has_queen[suit_index] = 1;
              break;
            case TAROT_KING:
              assert (!has_king[suit_index]);
              has_king[suit_index] = 1;
              break;
            default:
              assert (0);
            }
        }
    }
  for (i = 0; i < 4; i++)
    {
      *n_kings += has_king[i];
      *n_marriages += has_king[i] && has_queen[i];
      *n_extended_marriages += has_king[i] && has_queen[i] && has_knight[i];
      *n_queens += has_queen[i];
      *n_knights += has_knight[i];
    }
}

static inline void
features_count_minor (const TarotGame * game,
                      size_t *n_shortest,
                      size_t *n_second_shortest,
                      size_t *n_second_longest, size_t *n_longest,
                      TarotSuit * suit_order)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  size_t n_by_suit[4] = { 0, 0, 0, 0 };
  size_t size_sorted;
  *n_shortest = 0;
  *n_second_shortest = 0;
  *n_second_longest = 0;
  *n_longest = 0;
  suit_order[0] = TAROT_HEARTS;
  suit_order[1] = TAROT_CLUBS;
  suit_order[2] = TAROT_DIAMONDS;
  suit_order[3] = TAROT_SPADES;
  assert (n_cards <= 78);
  for (i = 0; i < n_cards; i++)
    {
      TarotNumber number;
      TarotSuit suit;
      if (decompose (my_cards[i], &number, &suit) == 0
          && suit != TAROT_TRUMPS)
        {
          size_t suit_index = (size_t) suit;
          assert (suit_index < 4);
          n_by_suit[suit_index]++;
        }
    }
  for (size_sorted = 1; size_sorted < 4; size_sorted++)
    {
      size_t inserted = size_sorted;
      while (inserted > 0 && n_by_suit[inserted - 1] > n_by_suit[inserted])
        {
          size_t n_inserted = n_by_suit[inserted];
          TarotSuit suit_inserted = suit_order[inserted];
          n_by_suit[inserted] = n_by_suit[inserted - 1];
          n_by_suit[inserted - 1] = n_inserted;
          suit_order[inserted] = suit_order[inserted - 1];
          suit_order[inserted - 1] = suit_inserted;
          inserted--;
        }
    }
  for (i = 0; i + 1 < 4; i++)
    {
      assert (n_by_suit[i] <= n_by_suit[i + 1]);
    }
  *n_shortest = n_by_suit[0];
  *n_second_shortest = n_by_suit[1];
  *n_second_longest = n_by_suit[2];
  *n_longest = n_by_suit[3];
}

static inline void
features_count_oudlers (const TarotGame * game,
                        int *has_petit, int *has_excuse, int *has_twentyone)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  *has_petit = 0;
  *has_excuse = 0;
  *has_twentyone = 0;
  for (i = 0; i < n_cards; i++)
    {
      if (my_cards[i] == TAROT_PETIT)
        {
          *has_petit = 1;
        }
      else if (my_cards[i] == TAROT_EXCUSE)
        {
          *has_excuse = 1;
        }
      else if (my_cards[i] == TAROT_TWENTYONE)
        {
          *has_twentyone = 1;
        }
    }
}

static inline void
features_count_trumps (const TarotGame * game,
                       size_t *n_trumps,
                       TarotNumber * max_trump,
                       TarotNumber * min_trump,
                       TarotNumber * q1_trumps,
                       TarotNumber * q3_trumps, TarotNumber * median_trump)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  TarotNumber my_trumps[21];
  size_t my_n_trumps = 0;
  *max_trump = 0;
  *min_trump = 0;
  *q1_trumps = 0;
  *q3_trumps = 0;
  *median_trump = 0;
  for (i = 0; i < n_cards; i++)
    {
      TarotNumber number;
      TarotSuit suit;
      if (decompose (my_cards[i], &number, &suit) == 0
          && suit == TAROT_TRUMPS)
        {
          assert (my_n_trumps < 21);
          if (my_n_trumps > 0)
            {
              assert (my_trumps[my_n_trumps - 1] < number);
            }
          my_trumps[my_n_trumps++] = number;
        }
    }
  *n_trumps = my_n_trumps;
  if (my_n_trumps != 0)
    {
      double size_d = (double) my_n_trumps;
      double size_q1 = size_d / 4;
      double size_med = size_d / 2;
      double size_q3 = 3 * size_d / 4;
      *max_trump = my_trumps[my_n_trumps - 1];
      *min_trump = my_trumps[0];
      *q1_trumps = my_trumps[(size_t) size_q1];
      *q3_trumps = my_trumps[(size_t) size_q3];
      *median_trump = my_trumps[(size_t) size_med];
    }
}

static inline void
strategy_bid (const TarotGame * game,
              const TarotGameEvent * event, TarotBid * bid)
{
  (void) game;
  if (event_get_bid (event, bid) != TAROT_EVENT_OK)
    {
      *bid = TAROT_PASS;
    }
}

static inline void
strategy_outbid (const TarotGame * game,
                 const TarotGameEvent * event, int *outbid)
{
  TarotBid minimum;
  TarotBid actual;
  TarotPlayer next;
  *outbid = 0;
  if (game_get_hint_bid (game, &next, &minimum) == TAROT_GAME_OK
      && event_get_bid (event, &actual) == TAROT_EVENT_OK)
    {
      *outbid = ((int) actual) - ((int) minimum);
    }
}

static inline void
strategy_call_by_length (const TarotGame * game,
                         const TarotGameEvent * event,
                         int *shortest,
                         int *second_shortest,
                         int *second_longest, int *longest)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  size_t n_by_suit[4] = { 0, 0, 0, 0 };
  size_t n_fewer = 0;
  size_t n_more = 0;
  TarotSuit call_suit;
  TarotNumber call_number;
  TarotCard call;
  *shortest = 0;
  *second_shortest = 0;
  *second_longest = 0;
  *longest = 0;
  assert (n_cards <= 78);
  if (event_get_call (event, &call) != TAROT_EVENT_OK)
    {
      return;
    }
  if (decompose (call, &call_number, &call_suit) != 0)
    {
      return;
    }
  if (call_suit == TAROT_TRUMPS)
    {
      return;
    }
  for (i = 0; i < n_cards; i++)
    {
      TarotNumber number;
      TarotSuit suit;
      if (decompose (my_cards[i], &number, &suit) == 0
          && suit != TAROT_TRUMPS)
        {
          size_t suit_index = (size_t) suit;
          assert (suit_index < 4);
          n_by_suit[suit_index]++;
        }
    }
  n_fewer = n_more = 0;
  for (i = 0; i < 4; i++)
    {
      if (n_by_suit[i] < n_by_suit[(size_t) call_suit])
        {
          n_fewer++;
        }
      if (n_by_suit[i] > n_by_suit[(size_t) call_suit])
        {
          n_more++;
        }
    }
  switch (n_fewer)
    {
    case 0:
      *shortest = 1;
      break;
    case 1:
      *second_shortest = 1;
      break;
    case 2:
      *second_longest = 1;
      break;
    case 3:
      *longest = 1;
      break;
    default:
      assert (0);
    }
  switch (n_more)
    {
    case 0:
      *longest = 1;
      break;
    case 1:
      *second_longest = 1;
      break;
    case 2:
      *second_shortest = 1;
      break;
    case 3:
      *shortest = 1;
      break;
    default:
      assert (0);
    }
}

static inline void
strategy_call_myself (const TarotGame * game,
                      const TarotGameEvent * event, int *myself)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  TarotCard call;
  *myself = 0;
  assert (n_cards <= 78);
  if (event_get_call (event, &call) != TAROT_EVENT_OK)
    {
      return;
    }
  for (i = 0; i < n_cards; i++)
    {
      if (my_cards[i] == call)
        {
          *myself = 1;
        }
    }
}

static inline void
strategy_call_marriage (const TarotGame * game,
                        const TarotGameEvent * event, int *will_own_marriage)
{
  TarotCard my_cards[78];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t i;
  TarotCard call;
  TarotNumber call_number;
  TarotSuit call_suit;
  TarotNumber spouse_number;
  TarotCard spouse;
  *will_own_marriage = 0;
  assert (n_cards <= 78);
  if (event_get_call (event, &call) != TAROT_EVENT_OK)
    {
      return;
    }
  if (decompose (call, &call_number, &call_suit) != 0)
    {
      return;
    }
  switch (call_number)
    {
    case TAROT_KING:
      spouse_number = TAROT_QUEEN;
      break;
    case TAROT_QUEEN:
      spouse_number = TAROT_KING;
      break;
    default:
      return;
    }
  if (of (spouse_number, call_suit, &spouse) != 0)
    {
      assert (0);
    }
  assert (spouse == call + 1 || spouse + 1 == call);
  for (i = 0; i < n_cards; i++)
    {
      if (my_cards[i] == spouse)
        {
          *will_own_marriage = 1;
        }
    }
}

/* For each of the 78 cards, set its status to 0 by default, or +1 if
   we own that card, or +2 if we do not own it, do not discard it and
   call it, or -1 if we own it and discard it. */
static inline void
features_get_status (const TarotGame * game,
                     const TarotGameEvent * event, int *status)
{
  size_t i = 0;
  TarotCard my_cards[78];
  TarotCard discard[6];
  size_t n_cards = features_get_my_cards (game, 78, my_cards);
  size_t n_discard;
  TarotCard call;
  for (i = 0; i < 78; i++)
    {
      status[i] = 0;
    }
  assert (n_cards <= 78);
  if (event_get_discard (event, &n_discard, 0, 6, discard) != TAROT_EVENT_OK)
    {
      return;
    }
  assert (n_discard <= 6);
  for (i = 0; i < n_cards; i++)
    {
      status[my_cards[i]] = +1;
    }
  if (game_get_call (game, &call) == TAROT_GAME_OK)
    {
      status[call] = +2;
    }
  for (i = 0; i < n_discard; i++)
    {
      status[discard[i]] = -1;
    }
}

static inline void
strategy_discard_save (const TarotGame * game,
                       const TarotGameEvent * event,
                       size_t *queens, size_t *knights, size_t *jacks)
{
  int status[78] = { 0 };
  static const TarotSuit minor_suits[4] =
    { TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES };
  size_t i;
  *queens = 0;
  *knights = 0;
  *jacks = 0;
  features_get_status (game, event, status);
  for (i = 0; i < 4; i++)
    {
      TarotSuit suit = minor_suits[i];
      TarotCard king, queen, knight, jack;
      if (of (TAROT_KING, suit, &king) != 0
          || of (TAROT_QUEEN, suit, &queen) != 0
          || of (TAROT_KNIGHT, suit, &knight) != 0
          || of (TAROT_JACK, suit, &jack) != 0)
        {
          assert (0);
        }
      if (status[king] == 0 && status[queen] < 0)
        {
          *queens += 1;
        }
      if ((status[king] == 0 || status[queen] == 0) && status[knight] < 0)
        {
          *knights += 1;
        }
      if ((status[king] == 0 || status[queen] == 0 || status[knight] == 0)
          && status[jack] < 0)
        {
          *jacks += 1;
        }
    }
}

static inline void
strategy_discard_junk (const TarotGame * game,
                       const TarotGameEvent * event, size_t *lesser)
{
  int status[78] = { 0 };
  static const TarotSuit minor_suits[4] =
    { TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES };
  size_t i;
  *lesser = 0;
  features_get_status (game, event, status);
  for (i = 0; i < 4; i++)
    {
      TarotSuit suit = minor_suits[i];
      TarotCard ace, ten, c;
      if (of (TAROT_ACE, suit, &ace) != 0 || of (10, suit, &ten) != 0)
        {
          assert (0);
        }
      /* Check if we discarded any minor card of that suit */
      for (c = ace; c <= ten && status[c] >= 0; c++);
      if (c <= ten)
        {
          for (c = ace; c <= ten && status[c] >= 0; c++)
            {
              if (status[c] > 0)
                {
                  *lesser += 1;
                }
            }
        }
    }
}

static inline void
strategy_discard_by_length (const TarotGame * game,
                            const TarotGameEvent * event,
                            size_t *shortest,
                            size_t *second_shortest,
                            size_t *second_longest, size_t *longest)
{
  TarotSuit suit_order[4];
  size_t i = 0;
  TarotCard discard[6];
  size_t n_discard;
  size_t n_shortest;
  size_t n_second_shortest;
  size_t n_second_longest;
  size_t n_longest;
  *shortest = 0;
  *second_shortest = 0;
  *second_longest = 0;
  *longest = 0;
  features_count_minor (game, &n_shortest, &n_second_shortest,
                        &n_second_longest, &n_longest, suit_order);
  if (event_get_discard (event, &n_discard, 0, 6, discard) != TAROT_EVENT_OK)
    {
      return;
    }
  assert (n_discard <= 6);
  for (i = 0; i < n_discard; i++)
    {
      TarotNumber n;
      TarotSuit s;
      if (decompose (discard[i], &n, &s) != 0)
        {
          assert (0);
        }
      if (s == suit_order[0])
        {
          *shortest += 1;
        }
      else if (s == suit_order[1])
        {
          *second_shortest += 1;
        }
      else if (s == suit_order[2])
        {
          *second_longest += 1;
        }
      else
        {
          assert (s == suit_order[3]);
          *longest += 1;
        }
    }
}

static inline void
get_teams (const TarotGame * game, int *attack)
{
  size_t n_players = game_n_players (game), i;
  TarotPlayer taker, partner;
  assert (n_players <= 5);
  game_get_taker (game, &taker);
  if (game_get_partner (game, &partner) != TAROT_GAME_OK)
    {
      partner = taker;
    }
  for (i = 0; i < n_players; i++)
    {
      attack[i] = 0;
    }
  attack[taker] = 1;
  attack[partner] = 1;
}

static inline void
features_team (const TarotGame * game, int *attack, int *defence)
{
  TarotPlayer myself;
  int teams[5];
  *attack = 0;
  *defence = 0;
  get_teams (game, teams);
  if (tarot_game_get_main_player (game, &myself) == TAROT_GAME_OK)
    {
      *attack = teams[myself];
      *defence = 1 - teams[myself];
    }
}

static inline void
features_trick_players (const TarotGame * game,
                        size_t *n_already_played,
                        size_t *n_allies_after_last_opponent)
{
  size_t i_trick;
  size_t n_players = game_n_players (game), i, n_cards;
  TarotPlayer leader, myself;
  int teams[5];
  TarotPlayer partner;
  *n_already_played = 0;
  *n_allies_after_last_opponent = 0;
  if (game_get_current_trick (game, &i_trick) != TAROT_GAME_OK
      || game_get_main_player (game, &myself) != TAROT_GAME_OK)
    {
      return;
    }
  if (game_get_trick_leader (game, i_trick, &leader) != TAROT_GAME_OK)
    {
      return;
    }
  assert (leader < n_players);
  if (game_get_trick_cards (game, i_trick, &n_cards, 0, 0, NULL) ==
      TAROT_GAME_OK)
    {
      *n_already_played = n_cards;
    }
  get_teams (game, teams);
  assert (n_players >= 3 && n_players <= 5);
  if (!game_with_call (game)
      || game_get_partner (game, &partner) == TAROT_GAME_OK)
    {
      size_t last_opponent;     /* Number of players that have played before
                                 * the last opponent */
      last_opponent = n_cards;
      for (i = n_cards + 1; i < n_players; i++)
        {
          TarotPlayer player = (leader + i) % n_players;
          int team = teams[player];
          int is_ally = (team == teams[myself]);
          if (!is_ally)
            {
              last_opponent = i;
            }
        }
      *n_allies_after_last_opponent = n_players - last_opponent - 1;
    }
}

static inline void
may_play_in_trick (const TarotCounter * counter,
                   size_t n_players,
                   TarotPlayer first_forced,
                   size_t n_forced,
                   const TarotCard * forced_cards,
                   TarotCard candidate, int *may_play)
{
  size_t i;
  assert (n_players <= 5);
  for (i = 0; i < n_players; i++)
    {
      TarotPlayer player = (i + first_forced) % n_players;
      if (i < n_forced)
        {
          may_play[player] = (candidate == forced_cards[i]);
        }
      else
        {
          may_play[player] = counter_may_own (counter, 1, &player, candidate);
        }
    }
}

static inline int
find_master (const TarotGame * game,
             const TarotCounter * counter,
             TarotCard least_candidate,
             TarotCard strongest_candidate, int *may_play)
{
  size_t n_players = game_n_players (game);
  size_t i_trick;
  size_t i_candidate;
  TarotGameCardAnalysis cards_full[5];
  TarotCard cards[5];
  size_t n_cards, i_card;
  TarotPlayer leader;
  if (game_get_current_trick (game, &i_trick) != TAROT_GAME_OK)
    {
      return 1;
    }
  if (game_get_trick_leader (game, i_trick, &leader) != TAROT_GAME_OK)
    {
      return 1;
    }
  if (game_get_trick_cards (game, i_trick, &n_cards, 0, 5, cards_full) !=
      TAROT_GAME_OK)
    {
      return 1;
    }
  for (i_card = 0; i_card < n_cards; i_card++)
    {
      cards[i_card] = cards_full[i_card].card;
    }
  for (i_candidate = 0;
       i_candidate < strongest_candidate - least_candidate + 1; i_candidate++)
    {
      TarotCard candidate = strongest_candidate - i_candidate;
      TarotPlayer i;
      may_play_in_trick (counter, n_players, leader, n_cards, cards,
                         candidate, may_play);
      for (i = 0; i < n_players; i++)
        {
          if (may_play[i])
            {
              return 0;
            }
        }
    }
  return 0;
}

static inline int
find_master_team (const TarotGame * game,
                  const TarotCounter * counter,
                  TarotCard least_candidate,
                  TarotCard strongest_candidate, int *ally, int *ennemy)
{
  int teams[5];
  int may_play[5] = { 0 };
  size_t i;
  TarotPlayer myself;
  get_teams (game, teams);
  *ally = 0;
  *ennemy = 0;
  if (find_master
      (game, counter, least_candidate, strongest_candidate, may_play) != 0)
    {
      return 1;
    }
  if (game_get_main_player (game, &myself) != 0)
    {
      return 1;
    }
  /* Since may_play has been initialized with extra zeros, this works
   * with 3 or 4 players too */
  for (i = 0; i < 5; i++)
    {
      if (may_play[i])
        {
          if (teams[i] == teams[myself])
            {
              *ally = 1;
            }
          else
            {
              *ennemy = 1;
            }
        }
    }
  return 0;
}

static inline int
get_called_suit (const TarotGame * game, TarotSuit * called)
{
  TarotCard call;
  if (game_get_call (game, &call) == TAROT_GAME_OK)
    {
      TarotNumber n;
      if (decompose (call, &n, called) != 0)
        {
          assert (0);
        }
      return 0;
    }
  return 1;
}

static inline size_t
count_times_lead (const TarotGame * game, TarotSuit suit)
{
  size_t current_trick;
  size_t trick;
  size_t ret = 0;
  if (game_get_current_trick (game, &current_trick) != TAROT_GAME_OK)
    {
      /* Fun fact: this is actually correct! */
      return 0;
    }
  for (trick = 0; trick <= current_trick; trick++)
    {
      TarotSuit lead_suit;
      if (game_get_lead_suit (game, trick, &lead_suit) == TAROT_GAME_OK)
        {
          ret += (suit == lead_suit);
        }
    }
  return ret;
}

static inline void
features_minor_suit (const TarotGame * game,
                     const TarotCounter * counter,
                     int *called_suit,
                     size_t *n_times_lead,
                     size_t *n_remaining_cards,
                     size_t *n_remaining_points,
                     int *ally_may_be_master, int *ennemy_may_be_master)
{
  TarotSuit the_called_suit, lead_suit;
  size_t i_trick;
  TarotCard first_of_lead_suit;
  TarotCard last_of_lead_suit;
  TarotCard card_of_lead_suit;
  size_t n_players = tarot_game_n_players (game);
  *called_suit = 0;
  *n_times_lead = 0;
  *n_remaining_cards = 0;
  *n_remaining_points = 0;
  *ally_may_be_master = 0;
  *ennemy_may_be_master = 0;
  assert (n_players <= 5);
  if (game_get_current_trick (game, &i_trick) != TAROT_GAME_OK)
    {
      return;
    }
  if (game_get_lead_suit (game, i_trick, &lead_suit) != TAROT_GAME_OK)
    {
      return;
    }
  if (get_called_suit (game, &the_called_suit) == 0)
    {
      *called_suit = (lead_suit == the_called_suit);
    }
  *n_times_lead = count_times_lead (game, lead_suit);
  if (of (1, lead_suit, &first_of_lead_suit) != 0)
    {
      assert (0);
    }
  if (lead_suit == TAROT_TRUMPS)
    {
      if (of (21, lead_suit, &last_of_lead_suit) != 0)
        {
          assert (0);
        }
    }
  else
    {
      if (of (TAROT_KING, lead_suit, &last_of_lead_suit) != 0)
        {
          assert (0);
        }
    }
  for (card_of_lead_suit = first_of_lead_suit;
       card_of_lead_suit <= last_of_lead_suit; card_of_lead_suit++)
    {
      TarotPlayer current_owner =
        counter->current_owner_of[card_of_lead_suit];
      static const size_t points[78] = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,
        1
      };
      if (current_owner == TAROT_COUNTER_CARD_OWNER_PLAYED
          || current_owner == TAROT_COUNTER_CARD_OWNER_DISCARDED)
        {
          /* This card is not available */
        }
      else
        {
          *n_remaining_cards += 1;
          *n_remaining_points += points[card_of_lead_suit];
        }
    }
  assert (*n_remaining_points <= 37);
  if (find_master_team
      (game, counter, first_of_lead_suit, last_of_lead_suit,
       ally_may_be_master, ennemy_may_be_master) != 0)
    {
      *ally_may_be_master = 0;
      *ennemy_may_be_master = 0;
    }
}

static inline void
features_trump (const TarotGame * game,
                const TarotCounter * counter,
                size_t *n_remaining,
                int *ally_may_be_master, int *ennemy_may_be_master)
{
  TarotCard first = TAROT_PETIT;
  TarotCard last = TAROT_TWENTYONE;
  TarotCard card;
  size_t n_players = tarot_game_n_players (game);
  *n_remaining = 0;
  *ally_may_be_master = 0;
  *ennemy_may_be_master = 0;
  assert (n_players <= 5);
  for (card = first; card <= last; card++)
    {
      TarotPlayer current_owner = counter->current_owner_of[card];
      if (current_owner == TAROT_COUNTER_CARD_OWNER_PLAYED
          || current_owner == TAROT_COUNTER_CARD_OWNER_DISCARDED)
        {
          /* This card is not available */
        }
      else
        {
          *n_remaining += 1;
        }
    }
  if (find_master_team
      (game, counter, first, last, ally_may_be_master,
       ennemy_may_be_master) != 0)
    {
      *ally_may_be_master = 0;
      *ennemy_may_be_master = 0;
    }
}

static inline void
features_points (const TarotGame * game,
                 size_t *n_halfpoints, size_t *n_oudlers)
{
  size_t n_cards;
  TarotGameCardAnalysis cards[5];
  size_t i_trick;
  size_t i_card;
  *n_oudlers = 0;
  *n_halfpoints = 0;
  if (game_get_current_trick (game, &i_trick) != TAROT_GAME_OK)
    {
      return;
    }
  if (game_get_trick_cards (game, i_trick, &n_cards, 0, 5, cards) !=
      TAROT_GAME_OK)
    {
      return;
    }
  assert (n_cards <= 5);
  for (i_card = 0; i_card < n_cards; i_card++)
    {
      static const unsigned int halfpoints[78] = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
        9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,
        1
      };
      static const int is_oudler[78] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
        0
      };
      *n_oudlers += is_oudler[cards[i_card].card];
      *n_halfpoints += halfpoints[cards[i_card].card];
    }
}

static inline void
strategy_lead (const TarotGame * game,
               const TarotCounter * counter,
               const TarotGameEvent * event,
               int *called_suit,
               size_t *suit_length, size_t *n_times_lead, int *trumps)
{
  TarotCard card;
  int is_decomposable;
  TarotSuit card_suit;
  TarotNumber card_number;
  (void) counter;
  *called_suit = 0;
  *suit_length = 0;
  *n_times_lead = 0;
  *trumps = 0;
  if (event_get_card (event, &card) != TAROT_EVENT_OK)
    {
      return;
    }
  is_decomposable = (decompose (card, &card_number, &card_suit) == 0);
  if (is_decomposable)
    {
      TarotCard my_cards[78];
      size_t n_my_cards =
        features_get_my_cards (game, sizeof (my_cards) / sizeof (*my_cards),
                               my_cards);
      size_t i_card;
      TarotCard first;
      TarotCard last;
      TarotNumber first_number = 1;
      TarotNumber last_number = TAROT_KING;
      size_t n_of_suit = 0;
      TarotSuit call_suit;
      if (card_suit == TAROT_TRUMPS)
        {
          last_number = 21;
        }
      if (of (first_number, card_suit, &first) != 0
          || of (last_number, card_suit, &last) != 0)
        {
          assert (0);
        }
      for (i_card = 0; i_card < n_my_cards; i_card++)
        {
          if (my_cards[i_card] >= first && my_cards[i_card] <= last)
            {
              n_of_suit++;
            }
        }
      if (get_called_suit (game, &call_suit) == 0)
        {
          *called_suit = (call_suit == card_suit);
        }
      *suit_length = n_of_suit;
      *n_times_lead = count_times_lead (game, card_suit);
      *trumps = (card_suit == TAROT_TRUMPS);
    }
}

static inline void
strategy_purity (const TarotGame * game,
                 const TarotGameEvent * event,
                 size_t *n_smaller_kept,
                 size_t *n_greater_nonface_kept,
                 size_t *n_points_kept, size_t *n_greater_kept)
{
  TarotCard card;
  int is_decomposable;
  TarotSuit card_suit;
  TarotNumber card_number;
  *n_smaller_kept = 0;
  *n_greater_nonface_kept = 0;
  *n_points_kept = 0;
  *n_greater_kept = 0;
  if (event_get_card (event, &card) != TAROT_EVENT_OK)
    {
      return;
    }
  is_decomposable = (decompose (card, &card_number, &card_suit) == 0);
  if (is_decomposable)
    {
      TarotCard my_cards[78];
      size_t n_my_cards =
        features_get_my_cards (game, sizeof (my_cards) / sizeof (*my_cards),
                               my_cards);
      size_t i_card;
      TarotCard first;
      TarotCard last_nonface;
      TarotCard last;
      TarotNumber first_number = 1;
      TarotNumber last_nonface_number = 10;
      TarotNumber last_number = TAROT_KING;
      size_t n_lesser = 0;
      size_t n_greater_nonface = 0;
      size_t n_greater = 0;
      size_t n_points = 0;
      if (card_suit == TAROT_TRUMPS)
        {
          last_nonface_number = 21;
          last_number = 21;
        }
      if (of (first_number, card_suit, &first) != 0
          || of (last_nonface_number, card_suit, &last_nonface) != 0
          || of (last_number, card_suit, &last) != 0)
        {
          assert (0);
        }
      for (i_card = 0; i_card < n_my_cards; i_card++)
        {
          if (my_cards[i_card] >= first && my_cards[i_card] < card)
            {
              n_lesser++;
            }
          if (my_cards[i_card] > card && my_cards[i_card] <= last_nonface)
            {
              n_greater_nonface++;
            }
          if (my_cards[i_card] > card && my_cards[i_card] <= last)
            {
              n_greater++;
            }
          if (my_cards[i_card] >= first && my_cards[i_card] <= last
              && my_cards[i_card] != card)
            {
              static const unsigned int halfpoints[78] = {
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
                9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,
                1
              };
              n_points += halfpoints[my_cards[i_card]];
            }
        }
      *n_smaller_kept = n_lesser;
      *n_greater_nonface_kept = n_greater_nonface;
      *n_greater_kept = n_greater;
      *n_points_kept = n_points;
    }
}

static inline void
strategy_excuse (const TarotGame * game,
                 const TarotGameEvent * event, int *excuse)
{
  TarotCard card;
  (void) game;
  *excuse = 0;
  if (event_get_card (event, &card) == TAROT_EVENT_OK)
    {
      *excuse = (card == TAROT_EXCUSE);
    }
}

static inline size_t
who_plays_after_me (const TarotGame * game, TarotPlayer * p)
{
  size_t i_trick, ret, i, n_cards, n_players = game_n_players (game);
  TarotPlayer leader;
  if (game_get_current_trick (game, &i_trick) != TAROT_GAME_OK)
    {
      return 0;
    }
  if (game_get_trick_leader (game, i_trick, &leader) != TAROT_GAME_OK)
    {
      return 0;
    }
  if (game_get_trick_cards (game, i_trick, &n_cards, 0, 0, NULL) !=
      TAROT_GAME_OK)
    {
      return 0;
    }
  ret = 0;
  for (i = n_cards + 1; i < n_players; i++)
    {
      assert (ret <= 5);
      p[ret++] = (leader + i) % n_players;
    }
  return ret;
}

static inline void
strategy_trump_master (const TarotGame * game,
                       const TarotCounter * counter,
                       const TarotGameEvent * event,
                       int *actually_master, int *obvious_master)
{
  TarotCard card, c;
  TarotNumber number, number_max_trump;
  TarotSuit suit;
  size_t n_players = game_n_players (game);
  TarotPlayer after_me[5];
  size_t n_after_me = who_plays_after_me (game, after_me);
  size_t current_trick;
  *actually_master = 0;
  *obvious_master = 0;
  if (event_get_card (event, &card) != TAROT_EVENT_OK)
    {
      return;
    }
  if (decompose (card, &number, &suit) != 0)
    {
      return;
    }
  if (suit != TAROT_TRUMPS)
    {
      return;
    }
  if (game_get_current_trick (game, &current_trick) != TAROT_GAME_OK)
    {
      return;
    }
  if (game_get_max_trump (game, current_trick, &number_max_trump) ==
      TAROT_GAME_OK)
    {
      if (number_max_trump > number)
        {
          /* Someone has already played better than us in this
           * trick. */
          return;
        }
    }
  *obvious_master = 1;
  *actually_master = 1;
  for (c = card + 1; c < TAROT_EXCUSE; c++)
    {
      if (counter->current_owner_of[c] < n_players)
        {
          *obvious_master = 0;
        }
      if (counter_may_own (counter, n_after_me, after_me, c))
        {
          *actually_master = 0;
          *obvious_master = 0;
        }
    }
}

static inline void
strategy_play_points (const TarotGame * game,
                      const TarotGameEvent * event,
                      size_t *n_halfpoints, int *oudler)
{
  TarotCard card;
  static const unsigned int halfpoints[78] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,
    1
  };
  static const int is_oudler[78] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
    0
  };
  (void) game;
  *n_halfpoints = 0;
  *oudler = 0;
  if (event_get_card (event, &card) != TAROT_EVENT_OK)
    {
      return;
    }
  *n_halfpoints = halfpoints[card];
  *oudler = is_oudler[card];
}

#include <tarot/game_private_impl.h>
#include <tarot/game_event_private_impl.h>
#include <tarot/cards_private_impl.h>
#include <tarot/counter_private_impl.h>

#endif /* H_TAROT_FEATURES_PRIVATE_IMPL_INCLUDED */
