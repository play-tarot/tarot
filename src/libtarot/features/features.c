/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/features_private.h>
#include <tarot/counter_private.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "xalloc.h"
#include "gettext.h"
#define _(String) dgettext (PACKAGE, String)

void
tarot_features_variant (const TarotGame * game, size_t *n_players,
                        int *with_call)
{
  features_variant (game, n_players, with_call);
}

void
tarot_features_position (const TarotGame * game, size_t *i_trick,
                         size_t *position)
{
  features_position (game, i_trick, position);
}

void
tarot_features_partner_known (const TarotGame * game, int *known)
{
  features_partner_known (game, known);
}

void
tarot_features_bid_minimum (const TarotGame * game, TarotBid * minimum)
{
  features_bid_minimum (game, minimum);
}

void
tarot_features_count_faces (const TarotGame * game, size_t *n_kings,
                            size_t *n_marriages,
                            size_t *n_extended_marriages,
                            size_t *n_queens, size_t *n_knights)
{
  features_count_faces (game, n_kings, n_marriages, n_extended_marriages,
                        n_queens, n_knights);
}

void
tarot_features_count_minor (const TarotGame * game,
                            size_t *n_shortest,
                            size_t *n_second_shortest,
                            size_t *n_second_longest, size_t *n_longest)
{
  TarotSuit order[4];
  features_count_minor (game, n_shortest, n_second_shortest,
                        n_second_longest, n_longest, order);
}

void
tarot_features_count_oudlers (const TarotGame * game,
                              int *has_petit,
                              int *has_excuse, int *has_twentyone)
{
  features_count_oudlers (game, has_petit, has_excuse, has_twentyone);
}

void
tarot_features_count_trumps (const TarotGame * game, size_t *n_trumps,
                             TarotNumber * max_trump,
                             TarotNumber * min_trump,
                             TarotNumber * q1_trumps,
                             TarotNumber * q3_trumps,
                             TarotNumber * median_trumps)
{
  features_count_trumps (game, n_trumps, max_trump, min_trump, q1_trumps,
                         q3_trumps, median_trumps);
}

void
tarot_strategy_bid (const TarotGame * game,
                    const TarotGameEvent * e, TarotBid * bid)
{
  strategy_bid (game, e, bid);
}

void
tarot_strategy_outbid (const TarotGame * game,
                       const TarotGameEvent * e, int *outbid)
{
  strategy_outbid (game, e, outbid);
}

void
tarot_strategy_call_by_length (const TarotGame * game,
                               const TarotGameEvent * event,
                               int *shortest,
                               int *second_shortest,
                               int *second_longest, int *longest)
{
  strategy_call_by_length (game, event, shortest, second_shortest,
                           second_longest, longest);
}

void
tarot_strategy_call_myself (const TarotGame * game,
                            const TarotGameEvent * event, int *myself)
{
  strategy_call_myself (game, event, myself);
}

void
tarot_strategy_call_marriage (const TarotGame * game,
                              const TarotGameEvent * event,
                              int *will_own_marriage)
{
  strategy_call_marriage (game, event, will_own_marriage);
}

void
tarot_strategy_discard_save (const TarotGame * game,
                             const TarotGameEvent * event,
                             size_t *queens, size_t *knights, size_t *jacks)
{
  strategy_discard_save (game, event, queens, knights, jacks);
}

void
tarot_strategy_discard_junk (const TarotGame * game,
                             const TarotGameEvent * event, size_t *lesser)
{
  strategy_discard_junk (game, event, lesser);
}

void
tarot_strategy_discard_by_length (const TarotGame * game,
                                  const TarotGameEvent * event,
                                  size_t *shortest,
                                  size_t *second_shortest,
                                  size_t *second_longest, size_t *longest)
{
  strategy_discard_by_length (game, event, shortest, second_shortest,
                              second_longest, longest);
}

void
tarot_features_team (const TarotGame * game, int *attack, int *defence)
{
  features_team (game, attack, defence);
}

void
tarot_features_trick_players (const TarotGame * game,
                              size_t *n_already_played,
                              size_t *n_allies_after_last_opponent)
{
  features_trick_players (game, n_already_played,
                          n_allies_after_last_opponent);
}

void
tarot_features_minor_suit (const TarotGame * game,
                           int *called_suit,
                           size_t *n_times_lead,
                           size_t *n_remaining_cards,
                           size_t *n_remaining_points,
                           int *ally_may_be_master, int *ennemy_may_be_master)
{
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, game);
  features_minor_suit (game, &counter, called_suit, n_times_lead,
                       n_remaining_cards, n_remaining_points,
                       ally_may_be_master, ennemy_may_be_master);
}

void
tarot_features_trump (const TarotGame * game,
                      size_t *n_remaining,
                      int *ally_may_be_master, int *ennemy_may_be_master)
{
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, game);
  features_trump (game, &counter, n_remaining, ally_may_be_master,
                  ennemy_may_be_master);
}

void
tarot_features_points (const TarotGame * game,
                       size_t *n_halfpoints, size_t *n_oudlers)
{
  features_points (game, n_halfpoints, n_oudlers);
}

void
tarot_strategy_lead (const TarotGame * game,
                     const TarotGameEvent * card,
                     int *called_suit,
                     size_t *suit_length, size_t *n_times_lead, int *trumps)
{
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, game);
  strategy_lead (game, &counter, card, called_suit, suit_length, n_times_lead,
                 trumps);
}

void
tarot_strategy_purity (const TarotGame * game,
                       const TarotGameEvent * card,
                       size_t *n_smaller_kept,
                       size_t *n_greater_nonface_kept,
                       size_t *n_points_kept, size_t *n_greater_kept)
{
  strategy_purity (game, card, n_smaller_kept, n_greater_nonface_kept,
                   n_points_kept, n_greater_kept);
}

void
tarot_strategy_excuse (const TarotGame * game,
                       const TarotGameEvent * card, int *is_excuse)
{
  strategy_excuse (game, card, is_excuse);
}

void
tarot_strategy_trump_master (const TarotGame * game,
                             const TarotGameEvent * card,
                             int *actually_master, int *obvious_master)
{
  TarotCounter counter;
  counter_initialize (&counter);
  counter_load_from_game (&counter, game);
  strategy_trump_master (game, &counter, card, actually_master,
                         obvious_master);
}

void
tarot_strategy_play_points (const TarotGame * game,
                            const TarotGameEvent * card,
                            size_t *n_halfpoints, int *oudler)
{
  strategy_play_points (game, card, n_halfpoints, oudler);
}

#include <tarot/features_private_impl.h>
#include <tarot/counter_private_impl.h>
