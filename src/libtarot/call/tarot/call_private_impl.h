/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tarot/call_private.h>

static int
_call_get_has_started (const TarotCall * call)
{
  return call->has_started;
}

static void
_call_set_has_started (TarotCall * call, int has_started)
{
  call->has_started = has_started;
}

static int
_call_get_has_card (const TarotCall * call)
{
  return call->has_card;
}

static void
_call_set_has_card (TarotCall * call, int has_card)
{
  call->has_card = has_card;
}

static int
_call_get_has_player (const TarotCall * call)
{
  return call->has_player;
}

static void
_call_set_has_player (TarotCall * call, int has_player)
{
  call->has_player = has_player;
}

static TarotCard
_call_get_call (const TarotCall * call)
{
  return call->call;
}

static void
_call_set_call (TarotCall * call, TarotCard value)
{
  call->call = value;
}

static TarotPlayer
_call_get_partner (const TarotCall * call)
{
  return call->partner;
}

static void
_call_set_partner (TarotCall * call, TarotPlayer partner)
{
  call->partner = partner;
}

static TarotPlayer
_call_get_taker (const TarotCall * call)
{
  return call->taker;
}

static void
_call_set_taker (TarotCall * call, TarotPlayer taker)
{
  call->taker = taker;
}

static void
call_initialize (TarotCall * call)
{
  _call_set_has_started (call, 0);
  _call_set_has_card (call, 0);
  _call_set_has_player (call, 0);
}

static int
call_copy (TarotCall * dest, const TarotCall * source)
{
  dest->has_started = source->has_started;
  dest->has_card = source->has_card;
  dest->has_player = source->has_player;
  if (source->has_card)
    {
      dest->call = source->call;
      dest->taker = source->taker;
    }
  if (source->has_player)
    {
      dest->partner = source->partner;
    }
  return 0;
}

static int
call_start (TarotCall * call)
{
  if (_call_get_has_started (call))
    {
      return 1;
    }
  _call_set_has_started (call, 1);
  return 0;
}

static int
call_waiting (const TarotCall * call)
{
  return _call_get_has_started (call) && !(_call_get_has_card (call));
}

static int
call_card_known (const TarotCall * call)
{
  return _call_get_has_started (call) && _call_get_has_card (call);
}

static int
call_partner_known (const TarotCall * call)
{
  return _call_get_has_started (call) && _call_get_has_player (call);
}

static TarotCard
call_card (const TarotCall * call)
{
  return _call_get_call (call);
}

static TarotPlayer
call_partner (const TarotCall * call)
{
  assert (call->has_started);
  assert (call->has_player);
  return _call_get_partner (call);
}

static int
call_add (TarotCall * call, TarotPlayer taker, TarotCard c)
{
  int error = 0;
  error = !(_call_get_has_started (call));
  if (error == 0)
    {
      _call_set_has_card (call, 1);
      _call_set_has_player (call, 0);
      _call_set_taker (call, taker);
      _call_set_call (call, c);
    }
  return error;
}

static int
call_owned_by (TarotCall * call, TarotPlayer owner, TarotCard c)
{
  if (!_call_get_has_started (call) || !call_card_known (call))
    {
      return 1;
    }
  if (c == _call_get_call (call))
    {
      _call_set_has_player (call, 1);
      _call_set_partner (call, owner);
    }
  return 0;
}

static int
call_owned_card (TarotCall * call, TarotPlayer owner, TarotCard c)
{
  return call_owned_by (call, owner, c);
}

static int
call_trick_card (TarotCall * call, TarotPlayer owner, TarotCard c)
{
  return call_owned_by (call, owner, c);
}

static int
call_doscard_card (TarotCall * call, TarotCard c)
{
  return call_owned_by (call, _call_get_taker (call), c);
}
