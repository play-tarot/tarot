/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_BIDS_PRIVATE_INCLUDED
#define H_TAROT_BIDS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <stddef.h>
#include <tarot/bid.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void bids_initialize (TarotBids * bids);
  static int bids_copy (TarotBids * dest, const TarotBids * source);
  static int bids_start (TarotBids * bids);
  static int bids_has_next (const TarotBids * bids, size_t n_players);
  static TarotPlayer bids_next (const TarotBids * bids);
  static int bids_has_taker (const TarotBids * bids, size_t n_players);
  static TarotPlayer bids_taker (const TarotBids * bids);
  static TarotBid bids_contract (const TarotBids * bids);
  static int bids_discard_allowed (const TarotBids * bids, size_t n_players);
  static int bids_discard_counted (const TarotBids * bids, size_t n_players);
  static int bids_multiplier (const TarotBids * bids, size_t n_players);
  static int bids_check (const TarotBids * bids, TarotBid next,
                         size_t n_players);
  static int bids_add (TarotBids * bids, TarotBid next, size_t n_players);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_BIDS_PRIVATE_INCLUDED */
