/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_TRICK_PRIVATE_INCLUDED
#define H_TAROT_TRICK_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/cards.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void trick_3_generalize (TarotTrick3 * trick,
                                  TarotTrickHandle * gen);
  static void trick_4_generalize (TarotTrick4 * trick,
                                  TarotTrickHandle * gen);
  static void trick_5_generalize (TarotTrick5 * trick,
                                  TarotTrickHandle * gen);
  static void trick_initialize (TarotTrickHandle * trick, size_t n_players);
  static int trick_copy (TarotTrickHandle * dest,
                         const TarotTrickHandle * source);
  static int trick_copy_until (TarotTrickHandle * dest,
                               const TarotTrickHandle * source,
                               TarotPlayer stop);
  static size_t trick_n_cards (const TarotTrickHandle * trick);
  static int trick_last (const TarotTrickHandle * trick);
  static int trick_has_leader (const TarotTrickHandle * trick);
  static TarotPlayer trick_leader (const TarotTrickHandle * trick);
  static TarotPlayer trick_taker (const TarotTrickHandle * trick);
  static TarotPlayer trick_next (const TarotTrickHandle * trick);
  static int trick_has_lead_suit (const TarotTrickHandle * trick);
  static TarotSuit trick_lead_suit (const TarotTrickHandle * trick);
  static TarotNumber trick_max_trump (const TarotTrickHandle * trick);
  static int trick_has_played (const TarotTrickHandle * trick,
                               TarotPlayer player);
  static TarotCard trick_get_card_of (const TarotTrickHandle * trick,
                                      TarotPlayer player);
  static TarotCard trick_get_card (const TarotTrickHandle * trick, size_t i);
  static TarotPlayer trick_get_player (const TarotTrickHandle * trick,
                                       size_t i);
  static size_t trick_get_position (const TarotTrickHandle * trick,
                                    TarotPlayer player);
  static int trick_cannot_follow (const TarotTrickHandle * trick,
                                  TarotPlayer p);
  static int trick_cannot_overtrump (const TarotTrickHandle * trick,
                                     TarotPlayer p);
  static int trick_cannot_undertrump (const TarotTrickHandle * trick,
                                      TarotPlayer p);
  static void trick_check_card_full (const TarotTrickHandle * trick,
                                     TarotCard played, int *cnf,
                                     TarotSuit * lead_suit, int *cno,
                                     TarotNumber * max_trump, int *cnu);
  static int trick_points (const TarotTrickHandle * trick);
  static int trick_oudlers (const TarotTrickHandle * trick);
  static int trick_has_petit (const TarotTrickHandle * trick);
  static int trick_taken_by_excuse (const TarotTrickHandle * trick);
  static int trick_locate (const TarotTrickHandle * trick, TarotCard needle,
                           size_t *OUTPUT);
  static int trick_set_last (TarotTrickHandle * trick, int last);
  static int trick_set_leader (TarotTrickHandle * trick, TarotPlayer leader);
  static int trick_add (TarotTrickHandle * trick, TarotCard card,
                        int force_taker);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_TRICK_PRIVATE_INCLUDED */
