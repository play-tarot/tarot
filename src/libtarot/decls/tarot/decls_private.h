/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_DECLS_PRIVATE_INCLUDED
#define H_TAROT_DECLS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void decls_initialize (TarotDecls * decls, size_t n_players);
  static int decls_copy (TarotDecls * dest, const TarotDecls * source);
  static int decls_start (TarotDecls * decls);
  static int decls_has_next (const TarotDecls * decls, size_t n_players);
  static TarotPlayer decls_next (const TarotDecls * decls);
  static int decls_has_declarant (const TarotDecls * decls, size_t n_players);
  static TarotPlayer decls_declarant (const TarotDecls * decls);
  static int decls_check (const TarotDecls * decls, int next,
                          size_t n_players);
  static int decls_add (TarotDecls * decls, int next, size_t n_players);
  static int decls_done (const TarotDecls * decls, size_t n_players);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_DECLS_PRIVATE_INCLUDED */
