/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_BID_PRIVATE_INCLUDED
#define H_TAROT_BID_PRIVATE_INCLUDED

#include "bid.h"

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /*@
     assigns \nothing;
     ensures \result == 0 || \result == 1;
   */
  static int bid_discard_allowed (TarotBid bid);

  /*@
     assigns \nothing;
     ensures \result == 0 || \result == 1;
   */
  static int bid_discard_counted (TarotBid bid);

  /*@
     requires \valid(OUTPUT);
     assigns *OUTPUT;
     ensures \result == 0 || \result == 1;
     ensures *OUTPUT == 0 || *OUTPUT == 1;
   */
  static int bid_check (TarotBid base, TarotBid newBid, int *OUTPUT);

  /*@
     ensures \result >= 1;
     assigns \nothing;
   */
  static int bid_multiplier (TarotBid bid);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_BID_PRIVATE_INCLUDED */
