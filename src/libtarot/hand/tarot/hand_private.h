/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_HAND_PRIVATE_INCLUDED
#define H_TAROT_HAND_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/cards.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static int hand_copy (TarotHand * dest, const TarotHand * src);
  static size_t hand_size (const TarotHand * set);
  static size_t hand_max (const TarotHand * set);
  static int hand_has_petit_sec (const TarotHand * set);
  static size_t hand_get (const TarotHand * set, size_t max_dest,
                          TarotCard * dest);
  static int hand_set (TarotHand * set, const TarotCard * cards, size_t n);
  static size_t hand_set_filter (TarotHand * set,
                                 const TarotCard * cards, TarotPlayer player);
  static int hand_insert (TarotHand * set, TarotCard c);
  static void hand_remove (TarotHand * set, TarotCard c);
  static int hand_has (const TarotHand * set, TarotCard c);
  static int hand_swap (TarotHand * set, const TarotHand * given,
                        const TarotHand * removed);
  static int hand_points (const TarotHand * set);
  static int hand_oudlers (const TarotHand * set);
  static TarotNumber hand_hint_call (const TarotHand * set);
  static int hand_check_call (const TarotHand * set, TarotNumber number);
  static int hand_hint_discard (const TarotHand * set,
                                const TarotHand * dog, TarotHand * prio,
                                TarotHand * additional);
  static int hand_check_discard (const TarotHand * set,
                                 const TarotHand * dog,
                                 const TarotHand * discard);
  static int hand_hint_handful_without_discard (const TarotHand * set,
                                                TarotHand * prio,
                                                TarotHand * additional);
  static int hand_hint_handful_with_discard (const TarotHand * cards,
                                             const TarotHand * discard,
                                             TarotHand * prio,
                                             TarotHand * additional);
  static int hand_check_handful_without_discard (const TarotHand * set,
                                                 const TarotHand * handful);
  static int hand_check_handful_with_discard (const TarotHand * set,
                                              const TarotHand * discard,
                                              const TarotHand * handful);
  static int hand_hint_card_with_lead (const TarotHand * cards,
                                       TarotSuit lead_suit,
                                       TarotNumber max_trump,
                                       TarotHand * playable);
  static int hand_hint_card_without_lead_first (const TarotHand * cards,
                                                TarotCard call,
                                                TarotHand * playable);
  static int hand_hint_card_without_lead_not_first (const TarotHand *
                                                    cards,
                                                    TarotHand * playable);
  static int hand_check_card_with_lead (const TarotHand * cards,
                                        TarotSuit lead_suit,
                                        TarotNumber max_trump,
                                        TarotCard played);
  static int hand_check_card_without_lead_first (const TarotHand *
                                                 cards, TarotCard call,
                                                 TarotCard played);
  static int hand_check_card_without_lead_not_first (const TarotHand *
                                                     cards, TarotCard played);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_HAND_PRIVATE_INCLUDED */
