/* tarot implements the rules of the tarot game
 * Copyright (C) 2017, 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tarot/hand_private.h>
#include <tarot/cards_private.h>
#include <assert.h>

static inline int
is_consistent (const TarotHand * hand)
{
  size_t i = 0;
  size_t n = *(hand->n);
  if (n > hand->n_max)
    {
      return 0;
    }
  for (i = 1; i < n; i++)
    {
      if (hand->buffer[i] <= hand->buffer[i - 1])
        {
          return 0;
        }
    }
  return 1;
}

static inline int
hand_copy (TarotHand * dest, const TarotHand * src)
{
  unsigned int i = 0, max = 0;
  assert (is_consistent (src));
  (void) &is_consistent;
  max = *(src->n);
  if (dest->n_max < max)
    {
      max = dest->n_max;
    }
  for (i = 0; i < max; i++)
    {
      dest->buffer[i] = src->buffer[i];
    }
  *(dest->n) = *(src->n);
  return 0;
}

static inline size_t
hand_size (const TarotHand * set)
{
  assert (is_consistent (set));
  return *(set->n);
}

static inline size_t
hand_max (const TarotHand * set)
{
  assert (is_consistent (set));
  return set->n_max;
}

static inline int
hand_has_petit_sec (const TarotHand * set)
{
  assert (is_consistent (set));
  if (hand_has (set, TAROT_PETIT) && !hand_has (set, TAROT_EXCUSE))
    {
      TarotCard i = TAROT_PETIT + 1;
      for (i = TAROT_PETIT + 1; i < TAROT_EXCUSE; ++i)
        {
          if (hand_has (set, i))
            {
              return 0;
            }
        }
      return 1;
    }
  return 0;
}

static inline size_t
hand_get (const TarotHand * set, size_t max_dest, TarotCard * dest)
{
  size_t n_copied = *(set->n);
  size_t i = 0;
  assert (is_consistent (set));
  if (max_dest < n_copied)
    {
      max_dest = n_copied;
    }
  for (i = 0; i < n_copied; ++i)
    {
      dest[i] = set->buffer[i];
    }
  return *(set->n);
}

static inline void
locate_aux (const TarotCard * buffer, size_t n, size_t *i, size_t *span,
            TarotCard card)
{
  if (n > 0 && buffer[*i] >= card)
    {
      *span = 0;
    }
  else if (n > 0 && buffer[*i + *span - 1] < card)
    {
      *i = *i + *span;
      *span = 0;
    }
  else
    {
      size_t middle = *i + *span / 2;
      size_t endpos = *i + *span;
      TarotCard m = buffer[middle];
      if (m == card)
        {
          *i = middle;
          *span = 0;
        }
      else if (m < card)
        {
          *i = middle + 1;
          /* The new end position is the same as before */
          *span = endpos - *i;
        }
      else
        {
          *i = *i + 1;
          *span = middle - *i;
        }
    }
}

static inline void
hand_locate (const TarotHand * set, TarotCard card, size_t *pos)
{
  size_t i = 0;
  size_t span = *(set->n);
  while (span > 0)
    {
      locate_aux (set->buffer, *(set->n), &i, &span, card);
    }
  *pos = i;
}

static inline int
hand_has (const TarotHand * set, TarotCard c)
{
  size_t pos;
  hand_locate (set, c, &pos);
  return (pos < *(set->n) && set->buffer[pos] == c);
}

static inline int
hand_insert (TarotHand * set, TarotCard card)
{
  size_t pos;
  hand_locate (set, card, &pos);
  if ((pos < *(set->n) && set->buffer[pos] != card) || (pos == *(set->n)))
    {
      if (*(set->n) >= set->n_max)
        {
          return 1;
        }
      else
        {
          size_t j = 0;
          for (j = *(set->n); j > pos; --j)
            {
              set->buffer[j] = set->buffer[j - 1];
            }
          set->buffer[pos] = card;
          *(set->n) = *(set->n) + 1;
        }
    }
  return 0;
}

static inline void
hand_remove (TarotHand * set, TarotCard card)
{
  size_t pos;
  hand_locate (set, card, &pos);
  if (pos < *(set->n) && set->buffer[pos] == card)
    {
      size_t j = 0;
      *(set->n) = *(set->n) - 1;
      for (j = pos; j < *(set->n); ++j)
        {
          set->buffer[j] = set->buffer[j + 1];
        }
    }
}

static inline int
hand_set (TarotHand * set, const TarotCard * cards, size_t n)
{
  size_t i;
  int ret = 0;
  *(set->n) = 0;
  for (i = 0; i < n; ++i)
    {
      ret = ret || hand_insert (set, cards[i]);
    }
  assert (is_consistent (set));
  return ret;
}

static inline size_t
hand_set_filter (TarotHand * set, const TarotCard * owner_of,
                 TarotPlayer player)
{
  TarotCard c;
  size_t added = 0;
  *(set->n) = 0;
  for (c = 0; c < 78; ++c)      /* FIXME: get the number of cards from the
                                   TarotDeal class */
    {
      if (owner_of[c] == player)
        {
          added++;
          hand_insert (set, c);
        }
    }
  assert (is_consistent (set));
  return added;
}

static inline int
hand_swap (TarotHand * set, const TarotHand * given,
           const TarotHand * removed)
{
  size_t i_given = 0, i_removed = 0;
  int ret = 0;
  assert (is_consistent (set));
  assert (is_consistent (given));
  assert (is_consistent (removed));
  /* Remove the cards from removed that are not in given */
  for (i_removed = 0; i_removed < *(removed->n); ++i_removed)
    {
      i_given = 0;
      while (i_given < *(given->n)
             && given->buffer[i_given] < removed->buffer[i_removed])
        {
          i_given++;
        }
      if (i_given < *(given->n)
          && given->buffer[i_given] == removed->buffer[i_removed])
        {
          /* Skip it */
        }
      else
        {
          hand_remove (set, removed->buffer[i_removed]);
        }
    }
  /* Add the cards from given that are not in removed */
  for (i_given = 0; i_given < *(given->n); ++i_given)
    {
      i_removed = 0;
      while (i_removed < *(removed->n)
             && removed->buffer[i_removed] < given->buffer[i_given])
        {
          i_removed++;
        }
      if (i_removed < *(removed->n)
          && removed->buffer[i_removed] == given->buffer[i_given])
        {
          /* Skip it */
        }
      else
        {
          ret = ret || hand_insert (set, given->buffer[i_given]);
        }
    }
  assert (is_consistent (set));
  return ret;
}

static inline int
hand_points (const TarotHand * set)
{
  int ret = 0;
  size_t i;
  static const int points[78] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 5, 7, 9,
    9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,
    9
  };
  assert (is_consistent (set));
  for (i = 0; i < *(set->n); i++)
    {
      assert (set->buffer[i] < 78);
      ret += points[set->buffer[i]];
    }
  return ret;
}

static inline int
hand_oudlers (const TarotHand * set)
{
  int ret = 0;
  size_t i;
  assert (is_consistent (set));
  for (i = 0; i < *(set->n); i++)
    {
      if (set->buffer[i] == TAROT_PETIT
          || set->buffer[i] == TAROT_TWENTYONE
          || set->buffer[i] == TAROT_EXCUSE)
        {
          ret++;
        }
    }
  return ret;
}

static inline TarotNumber
hand_hint_call (const TarotHand * set)
{
  TarotNumber mini;
  static const TarotSuit suits[4] =
    { TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES };
  size_t n_found, i;
  assert (is_consistent (set));
  mini = TAROT_KING;
  do
    {
      n_found = 0;
      for (i = 0; i < 4; ++i)
        {
          TarotCard c;
          of (mini, suits[i], &c);
          if (hand_has (set, c))
            {
              n_found++;
            }
        }
      if (n_found == 4)
        {
          mini = mini - 1;
        }
    }
  while (n_found == 4 && mini > TAROT_JACK);
  return mini;
}

static inline int
hand_check_call (const TarotHand * set, TarotNumber number)
{
  static const TarotSuit suits[4] =
    { TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES };
  size_t i;
  TarotNumber superior;
  assert (is_consistent (set));
  for (superior = number + 1; superior <= TAROT_KING; superior++)
    {
      for (i = 0; i < 4; ++i)
        {
          TarotCard c;
          of (superior, suits[i], &c);
          if (hand_has (set, c))
            {
              /* Continue! */
            }
          else
            {
              return 0;
            }
        }
    }
  return 1;
}

#define PRIO {								\
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,				\
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,				\
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,				\
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,				\
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	\
    0									\
  }

#define ADDITIONAL { \
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,				\
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,				\
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,				\
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,				\
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,	\
    0									\
  }

static inline int
hand_hint_discard (const TarotHand * set, const TarotHand * dog,
                   TarotHand * prio, TarotHand * additional)
{
  static const int discardable_prio[78] = PRIO;
  static const int discardable_additional[78] = ADDITIONAL;
  const TarotHand *sets[2];
  size_t i = 0, n_sets = 1, i_set;
  int error = 0;
  assert (is_consistent (set));
  assert (is_consistent (dog));
  *(prio->n) = 0;
  *(additional->n) = 0;
  sets[0] = set;
  sets[1] = dog;
  if (dog != NULL)
    {
      n_sets = 2;
    }
  for (i_set = 0; i_set < n_sets; i_set++)
    {
      const TarotHand *the_set = sets[i_set];
      for (i = 0; i < *(the_set->n); ++i)
        {
          if (discardable_prio[the_set->buffer[i]])
            {
              if (hand_insert (prio, the_set->buffer[i]) != 0)
                {
                  error = 1;
                }
            }
          else if (discardable_additional[the_set->buffer[i]])
            {
              if (hand_insert (additional, the_set->buffer[i]) != 0)
                {
                  error = 1;
                }
            }
        }
    }
  assert (is_consistent (prio));
  assert (is_consistent (additional));
  return error;
}

static inline int
hand_has_safe (const TarotHand * cards, TarotCard card)
{
  return (cards != NULL && hand_has (cards, card));
}

static inline int
hand_check_discard (const TarotHand * set, const TarotHand * dog,
                    const TarotHand * discard)
{
  static const int discardable_prio[78] = PRIO;
  static const int discardable_additional[78] = ADDITIONAL;
  size_t i;
  int has_additional;
  assert (is_consistent (set));
  assert (is_consistent (dog));
  assert (is_consistent (discard));
  /* Are all discarded owned? */
  for (i = 0; i < *(discard->n); ++i)
    {
      if (!hand_has (set, discard->buffer[i])
          && !hand_has_safe (dog, discard->buffer[i]))
        {
          /* discard->buffer[i] is not owned */
          return 0;
        }
    }
  /* Are all discarded discardable or additional? */
  for (i = 0; i < *(discard->n); ++i)
    {
      if (!discardable_prio[discard->buffer[i]]
          && !discardable_additional[discard->buffer[i]])
        {
          /* discard->buffer[i] is a king or an oudler. */
          return 0;
        }
    }
  /* If there are additional, are all prio discarded? */
  has_additional = 0;
  for (i = 0; i < *(discard->n); ++i)
    {
      has_additional = has_additional
        || (!discardable_prio[discard->buffer[i]]
            && discardable_additional[discard->buffer[i]]);
    }
  if (has_additional)
    {
      for (i = 0; i < *(set->n); ++i)
        {
          if (discardable_prio[set->buffer[i]]
              && !hand_has (discard, set->buffer[i]))
            {
              /* set->buffer[i] should be discarded. */
              return 0;
            }
        }
      if (dog != NULL)
        {
          for (i = 0; i < *(dog->n); ++i)
            {
              if (discardable_prio[dog->buffer[i]]
                  && !hand_has (discard, dog->buffer[i]))
                {
                  /* dog->buffer[i] should be discarded. */
                  return 0;
                }
            }
        }
    }
  return 1;
}

static inline int
hand_hint_handful_without_discard (const TarotHand * set,
                                   TarotHand * prio, TarotHand * additional)
{
  size_t i;
  int ret = 0;
  assert (is_consistent (set));
  *(prio->n) = 0;
  *(additional->n) = 0;
  for (hand_locate (set, TAROT_PETIT, &i); i < *(set->n); ++i)
    {
      TarotCard c = set->buffer[i];
      if (c == TAROT_EXCUSE)
        {
          ret = ret || hand_insert (additional, c);
        }
      else
        {
          ret = ret || hand_insert (prio, c);
        }
    }
  assert (is_consistent (prio));
  assert (is_consistent (additional));
  return ret;
}

static inline int
hand_hint_handful_with_discard (const TarotHand * set,
                                const TarotHand * discard,
                                TarotHand * prio, TarotHand * additional)
{
  size_t i;
  int ret = 0;
  assert (is_consistent (set));
  assert (is_consistent (discard));
  ret = hand_hint_handful_without_discard (set, prio, additional);
  for (hand_locate (discard, TAROT_PETIT, &i); i < *(discard->n); ++i)
    {
      TarotCard c = discard->buffer[i];
      /* If the excuse is in the discard, it means there were no
         actual discard (it was in the dog and stayed there). */
      assert (c != TAROT_EXCUSE);
      ret = ret || hand_insert (additional, c);
    }
  assert (is_consistent (prio));
  assert (is_consistent (additional));
  return ret;
}

static inline int
hand_check_handful (const TarotHand * set, const TarotHand * discard,
                    const TarotHand * handful)
{
  size_t i;
  int has_additional = 0;
  assert (is_consistent (set));
  if (discard != NULL)
    {
      assert (is_consistent (discard));
    }
  assert (is_consistent (handful));
  for (i = 0; i < *(handful->n); ++i)
    {
      if (handful->buffer[i] < TAROT_PETIT)
        {
          /* Cannot show a non-trump */
          return 0;
        }
      else if (handful->buffer[i] == TAROT_EXCUSE)
        {
          has_additional = 1;
          if (handful->buffer[i] == TAROT_EXCUSE
              && !hand_has (set, TAROT_EXCUSE))
            {
              /* Showing the excuse, but we don't have it. */
              return 0;
            }
        }
      else if (!hand_has (set, handful->buffer[i]))
        {
          has_additional = 1;
          if (discard == NULL || !hand_has (discard, handful->buffer[i]))
            {
              /* Showing handful->buffer[i], but it's not in the hand
                 nor in the discard! */
              return 0;
            }
        }
    }
  if (has_additional)
    {
      for (hand_locate (set, TAROT_PETIT, &i); i < *(set->n); ++i)
        {
          if (!hand_has (handful, set->buffer[i]))
            {
              /* set->buffer[i] should be shown before! */
              return 0;
            }
        }
    }
  return 1;
}

static inline int
hand_check_handful_without_discard (const TarotHand * set,
                                    const TarotHand * handful)
{
  assert (is_consistent (set));
  assert (is_consistent (handful));
  return hand_check_handful (set, NULL, handful);
}

static inline int
hand_check_handful_with_discard (const TarotHand * set,
                                 const TarotHand * discard,
                                 const TarotHand * handful)
{
  assert (is_consistent (set));
  assert (is_consistent (discard));
  assert (is_consistent (handful));
  return hand_check_handful (set, discard, handful);
}

static inline int
hand_insert_from (const TarotHand * source, TarotSuit suit,
                  TarotNumber start, TarotNumber end, TarotHand * dest)
{
  TarotCard first, last;
  size_t pos_start, pos_end, i;
  int ret = 0;
  if (start > end)
    {
      return 0;
    }
  assert (is_consistent (source));
  assert (is_consistent (dest));
  if (of (start, suit, &first) != 0)
    {
      assert (0);
    }
  if (of (end, suit, &last) != 0)
    {
      assert (0);
    }
  hand_locate (source, first, &pos_start);
  hand_locate (source, last + 1, &pos_end);
  for (i = pos_start; i < pos_end; ++i)
    {
      ret = ret || hand_insert (dest, source->buffer[i]);
    }
  assert (is_consistent (dest));
  return ret;
}

/*
 * The following functions will have an ok parameter that is set to 0
 * if no cards are playable, 1 otherwise.  They return 0 if playable
 * is large enough, 1 otherwise.
 */

static inline int
hand_follow (const TarotHand * cards, TarotSuit lead_suit,
             TarotHand * playable, int *ok)
{
  int ret = 0;
  assert (lead_suit != TAROT_TRUMPS);
  /* Otherwise use hand_overtrump */
  ret = hand_insert_from (cards, lead_suit, 1, TAROT_KING, playable);
  if (ret == 0)
    {
      *ok = (*(playable->n) != 0);
    }
  else
    {
      *ok = 0;
    }
  return ret;
}

static inline int
hand_overtrump (const TarotHand * cards, TarotNumber max_trump,
                TarotHand * playable, int *ok)
{
  int ret = 0;
  ret = hand_insert_from (cards, TAROT_TRUMPS, max_trump + 1, 21, playable);
  if (ret == 0)
    {
      *ok = (*(playable->n) != 0);
    }
  else
    {
      *ok = 0;
    }
  return ret;
}

static inline int
hand_undertrump (const TarotHand * cards, TarotHand * playable, int *ok)
{
  int ret = 0;
  ret = hand_insert_from (cards, TAROT_TRUMPS, 1, 21, playable);
  if (ret == 0)
    {
      *ok = (*(playable->n) != 0);
    }
  else
    {
      *ok = 0;
    }
  return ret;
}

static inline int
hand_add_excuse (const TarotHand * cards, TarotHand * playable)
{
  int ret = 0;
  if (hand_has (cards, TAROT_EXCUSE))
    {
      ret = hand_insert (playable, TAROT_EXCUSE);
    }
  return ret;
}

static inline int
hand_hint_card_with_lead (const TarotHand * cards, TarotSuit lead_suit,
                          TarotNumber max_trump, TarotHand * playable)
{
  int ok = 0;
  int ret = 0;
  assert (is_consistent (cards));
  *(playable->n) = 0;
  if (lead_suit != TAROT_TRUMPS)
    {
      ret = hand_follow (cards, lead_suit, playable, &ok);
      if (ret != 0)
        {
          return ret;
        }
    }
  if (!ok)
    {
      ret = hand_overtrump (cards, max_trump, playable, &ok);
      if (ret != 0)
        {
          return ret;
        }
    }
  if (!ok)
    {
      ret = hand_undertrump (cards, playable, &ok);
      if (ret != 0)
        {
          return ret;
        }
    }
  if (!ok)
    {
      /* Add all cards. */
      ret = hand_hint_card_without_lead_not_first (cards, playable);
      if (ret != 0)
        {
          return ret;
        }
    }
  ret = hand_add_excuse (cards, playable);
  if (ret != 0)
    {
      return ret;
    }
  assert (*(cards->n) == 0 || *(playable->n) != 0);
  assert (is_consistent (playable));
  return ret;
}

static inline int
hand_hint_card_without_lead_first (const TarotHand * cards,
                                   TarotCard call, TarotHand * playable)
{
  size_t i;
  TarotNumber number;
  TarotSuit suit;
  int ret = 0;
  assert (is_consistent (cards));
  *(playable->n) = 0;
  ret = decompose (call, &number, &suit);
  assert (ret == 0);
  for (i = 0; i < *(cards->n); i++)
    {
      int skip = 0;
      if (cards->buffer[i] != TAROT_EXCUSE)
        {
          TarotNumber n;
          TarotSuit s;
          int decomposed = 0;
          decomposed = decompose (cards->buffer[i], &n, &s);
          assert (decomposed == 0);
          (void) decomposed;
          skip = (suit == s && cards->buffer[i] != call);
        }
      if (!skip)
        {
          ret = ret || hand_insert (playable, cards->buffer[i]);
        }
    }
  assert (is_consistent (playable));
  return ret;
}

static inline int
hand_hint_card_without_lead_not_first (const TarotHand * cards,
                                       TarotHand * playable)
{
  return hand_copy (playable, cards);
}

static inline int
hand_has_from_suit (const TarotHand * cards, TarotSuit suit,
                    TarotNumber mini, TarotNumber maxi)
{
  TarotCard first, last;
  size_t pos_start, pos_end;
  if (mini > maxi)
    {
      /* Happens when we check whether we have some greater trump than
         the 21 */
      return 0;
    }
  if (of (mini, suit, &first) != 0)
    {
      assert (0);
    }
  if (of (maxi, suit, &last) != 0)
    {
      assert (0);
    }
  hand_locate (cards, first, &pos_start);
  hand_locate (cards, last + 1, &pos_end);
  return pos_start != pos_end;
}

static inline int
hand_check_card_with_lead (const TarotHand * cards, TarotSuit lead_suit,
                           TarotNumber max_trump, TarotCard played)
{
  TarotNumber number = 0;
  TarotSuit suit = 0;
  int may_undertrump, may_piss;
  assert (is_consistent (cards));
  if (!hand_has (cards, played))
    {
      /* No, we don't have this card. */
      return 0;
    }
  if (played == TAROT_EXCUSE)
    {
      /* Nothing to check there. */
      return 1;
    }
  decompose (played, &number, &suit);
  if (suit == lead_suit && lead_suit != TAROT_TRUMPS)
    {
      return 1;
    }
  may_undertrump =
    !hand_has_from_suit (cards, TAROT_TRUMPS, max_trump + 1, 21);
  if (suit == TAROT_TRUMPS && lead_suit == TAROT_TRUMPS)
    {
      return (number > max_trump || may_undertrump);
    }
  if (suit == TAROT_TRUMPS && lead_suit != TAROT_TRUMPS)
    {
      return ((number > max_trump || may_undertrump)
              && !hand_has_from_suit (cards, lead_suit, 1, TAROT_KING));
    }
  may_piss = !hand_has_from_suit (cards, TAROT_TRUMPS, 1, 21);
  if (suit != lead_suit && suit != TAROT_TRUMPS)
    {
      return !hand_has_from_suit (cards, lead_suit, 1, TAROT_KING)
        && may_piss;
    }
  return 0;
}

static inline int
hand_check_card_without_lead_first (const TarotHand * cards,
                                    TarotCard call, TarotCard played)
{
  TarotNumber number, call_number;
  TarotSuit suit, call_suit;
  int error_decompose = 0;
  assert (is_consistent (cards));
  if (!hand_has (cards, played))
    {
      /* No, we don't have this card. */
      return 0;
    }
  if (played == TAROT_EXCUSE
      || (played >= TAROT_PETIT && played <= TAROT_TWENTYONE)
      || played == call)
    {
      return 1;
    }
  error_decompose = error_decompose || decompose (played, &number, &suit);
  error_decompose = error_decompose
    || decompose (call, &call_number, &call_suit);
  if (error_decompose != 0)
    {
      return 1;
    }
  return (suit != call_suit);
}

static inline int
hand_check_card_without_lead_not_first (const TarotHand * cards,
                                        TarotCard played)
{
  assert (is_consistent (cards));
  return hand_has (cards, played);
}
