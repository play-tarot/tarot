/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/solo_private.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "xalloc.h"

static inline TarotSolo *
solo_alloc_aux (const TarotAi * ai)
{
  TarotSolo *ret = xmalloc (sizeof (TarotSolo));
  solo_initialize (ret, ai);
  return ret;
}

TarotSolo *
tarot_solo_alloc (void)
{
  TarotAi *perceptron = tarot_ai_alloc_perceptron ();
  TarotSolo *ret = solo_alloc_aux (perceptron);
  tarot_ai_free (perceptron);
  return ret;
}

static inline TarotSolo *
solo_alloc_fuzzy (double fuzziness, size_t seed_size, const void *seed,
                  const TarotAi * base)
{
  TarotAi *ai = tarot_ai_alloc_fuzzy (fuzziness, seed_size, seed, base);
  TarotSolo *ret = solo_alloc_aux (ai);
  tarot_ai_free (ai);
  return ret;
}

TarotSolo *
tarot_solo_alloc_fuzzy (double fuzziness, size_t seed_size, const void *seed,
                        const TarotAi * base)
{
  TarotSolo *ret;
  if (base == NULL)
    {
      TarotAi *perceptron = tarot_ai_alloc_perceptron ();
      ret = solo_alloc_fuzzy (fuzziness, seed_size, seed, perceptron);
      tarot_ai_free (perceptron);
    }
  else
    {
      ret = solo_alloc_fuzzy (fuzziness, seed_size, seed, base);
    }
  return ret;
}

TarotSolo *
tarot_solo_dup (const TarotSolo * source)
{
  TarotSolo *ret = tarot_solo_alloc ();
  tarot_solo_copy (ret, source);
  return ret;
}

void
tarot_solo_free (TarotSolo * solo)
{
  solo_destruct (solo);
  free (solo);
}

void
tarot_solo_copy (TarotSolo * dest, const TarotSolo * source)
{
  solo_copy (dest, source);
}

void
tarot_solo_copy_as (TarotSolo * dest, const TarotSolo * source,
                    TarotPlayer who)
{
  solo_copy_as (dest, source, who);
}

void
tarot_solo_get (const TarotSolo * solo, TarotGame * game)
{
  solo_get (solo, game);
}

TarotGame *
tarot_solo_get_alloc (const TarotSolo * solo)
{
  TarotGame *ret = tarot_game_alloc ();
  solo_get (solo, ret);
  return ret;
}

TarotGameError
tarot_solo_setup (TarotSolo * solo, size_t n_players, int with_call,
                  TarotPlayer myself, size_t n_owners,
                  const TarotPlayer * owners, TarotPlayer taker,
                  TarotCard call)
{
  return solo_setup (solo, n_players, with_call, myself, n_owners, owners,
                     taker, call);
}

TarotGameError
tarot_solo_setup_random (TarotSolo * solo, size_t n_players, int with_call,
                         size_t seed_size, const void *seed)
{
  return solo_setup_random (solo, n_players, with_call, seed_size, seed);
}

TarotGameError
tarot_solo_add (TarotSolo * solo, const TarotGameEvent * event)
{
  return solo_add (solo, event);
}

#include <tarot/solo_private_impl.h>
