/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_SOLO_PRIVATE_IMPL_INCLUDED
#define H_TAROT_SOLO_PRIVATE_IMPL_INCLUDED

#include <tarot/solo_private.h>
#include <tarot/game_private.h>
#include <tarot/game_event_private.h>
#include <tarot/features_private.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <nettle/yarrow.h>
#include <float.h>

static inline void solo_advance (TarotSolo * dest);

static inline void
solo_initialize (TarotSolo * solo, const TarotAi * ai)
{
  game_initialize (&(solo->game), 4, 0);
  solo->myself = 0;
  solo->ai = tarot_ai_dup (ai);
}

static inline void
solo_destruct (TarotSolo * solo)
{
  if (solo != NULL)
    {
      tarot_ai_free (solo->ai);
    }
}

static inline void
solo_copy (TarotSolo * dest, const TarotSolo * source)
{
  game_copy (&(dest->game), &(source->game));
  dest->myself = source->myself;
  tarot_ai_free (dest->ai);
  dest->ai = tarot_ai_dup (source->ai);
}

static inline void
solo_copy_as (TarotSolo * dest, const TarotSolo * source, TarotPlayer who)
{
  TarotGameError err = game_copy_as (&(dest->game), &(source->game), who);
  if (err != TAROT_GAME_OK)
    {
      assert (0);
    }
  dest->myself = who;
  tarot_ai_free (dest->ai);
  dest->ai = tarot_ai_dup (source->ai);
  solo_advance (dest);
}

static inline void
solo_get (const TarotSolo * solo, TarotGame * game)
{
  TarotGameError err = game_copy_as (game, &(solo->game), solo->myself);
  if (err != TAROT_GAME_OK)
    {
      assert (0);
    }
}

static inline TarotGameError
solo_setup (TarotSolo * solo, size_t n_players, int with_call,
            TarotPlayer myself, size_t n_owners, const TarotPlayer * owners,
            TarotPlayer taker, TarotCard call)
{
  /* To check whether these options are valid, we first run a game
   * with noone doing a slam.  If we can accept the call, then these
   * options are valid. */
  TarotGame check;
  TarotGameEvent setup_event;
  TarotGameEvent decl_event;
  size_t i;
  game_initialize (&check, 4, 0);
  if (game_duplicate_set_deal (&check, n_owners, owners) != TAROT_GAME_OK)
    {
      return TAROT_GAME_INVEV;
    }
  if (game_duplicate_set_taker (&check, taker) != TAROT_GAME_OK)
    {
      return TAROT_GAME_INVEV;
    }
  if (game_duplicate_set_call (&check, call) != TAROT_GAME_OK)
    {
      return TAROT_GAME_INVEV;
    }
  setup_event.t = TAROT_SETUP_EVENT;
  setup_event.u.setup.n_players = n_players;
  setup_event.u.setup.with_call = with_call;
  decl_event.t = TAROT_DECL_EVENT;
  decl_event.u.decl = 0;
  if (game_add_event (&check, &setup_event) != TAROT_GAME_OK)
    {
      return TAROT_GAME_INVEV;
    }
  for (i = 0; i < n_players; i++)
    {
      if (game_add_event (&check, &decl_event) != TAROT_GAME_OK)
        {
          return TAROT_GAME_INVEV;
        }
    }
  assert (game_step (&check) == TAROT_DOG);
  /* These options are correct */
  if (game_add_event (&(solo->game), &setup_event) != TAROT_GAME_OK)
    {
      return TAROT_GAME_TOOLATE;
    }
  if (game_duplicate_set_deal (&(solo->game), n_owners, owners) !=
      TAROT_GAME_OK)
    {
      assert (0);
    }
  if (game_duplicate_set_taker (&(solo->game), taker) != TAROT_GAME_OK)
    {
      assert (0);
    }
  if (game_duplicate_set_call (&(solo->game), call) != TAROT_GAME_OK)
    {
      assert (0);
    }
  assert (myself < n_players);
  solo->myself = myself;
  solo_advance (solo);
  return TAROT_GAME_OK;
}

static inline TarotGameError
solo_setup_random (TarotSolo * solo, size_t n_players, int with_call,
                   size_t seed_size, const unsigned char *seed)
{
  struct yarrow256_ctx generator;
  unsigned char next_seed[256];
  TarotPlayer best_owners[78];
  TarotPlayer best_taker;
  TarotCard best_call;
  double best_prediction = -DBL_MAX;
  size_t i;
  unsigned int is_taker;
  unsigned int other_player;
  TarotPlayer myself;
  unsigned char random_buffer[sizeof (unsigned int)];
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, seed_size, seed);
  for (i = 0; i < 10; i++)
    {
      TarotGameEvent ev;
      unsigned int ev_data[78];
      size_t j;
      TarotPlayer candidate_owners[78];
      size_t n_owners;
      TarotPlayer candidate_taker;
      TarotCard candidate_call = (TarotCard) (-1);
      double candidate_prediction;
      yarrow256_random (&generator, sizeof (next_seed), next_seed);
      event_init_deal_all_random (&ev, n_players, sizeof (next_seed),
                                  next_seed,
                                  sizeof (ev_data) / sizeof (ev_data[0]),
                                  ev_data);
      if (event_get_deal_all (&ev, &n_owners, 0,
                              sizeof (candidate_owners) /
                              sizeof (candidate_owners[0]),
                              candidate_owners) != TAROT_EVENT_OK)
        {
          assert (0);
        }
      candidate_taker =
        tarot_ai_strongest_player (solo->ai, n_players, with_call, n_owners,
                                   candidate_owners, &candidate_prediction,
                                   &candidate_call);
      if (candidate_prediction >= best_prediction)
        {
          for (j = 0; j < 78; j++)
            {
              best_owners[j] = candidate_owners[j];
            }
          best_taker = candidate_taker;
          best_call = candidate_call;
          best_prediction = candidate_prediction;
          if (candidate_prediction > 0)
            {
              i = 10;
            }
        }
    }
  yarrow256_random (&generator, sizeof (random_buffer), random_buffer);
  memcpy (&is_taker, random_buffer, sizeof (is_taker));
  other_player = is_taker / 2;
  is_taker = is_taker % 2;
  if (is_taker)
    {
      myself = best_taker;
    }
  else
    {
      myself = (other_player % n_players);
    }
  return solo_setup (solo, n_players, with_call, myself, 78, best_owners,
                     best_taker, best_call);
}

static inline TarotGameError
solo_add (TarotSolo * solo, const TarotGameEvent * event)
{
  TarotGameError err = game_add_event (&(solo->game), event);
  solo_advance (solo);
  return err;
}

static inline void
solo_advance (TarotSolo * solo)
{
  TarotPlayer next;
  int has_next = (game_get_next (&(solo->game), &next) == TAROT_GAME_OK);
  TarotCard dog[6];
  size_t n_dog;
  int my_score;
  size_t n_scores;
  if (has_next && next == solo->myself)
    {
      return;
    }
  else if (has_next)
    {
      double predicted_score;
      const TarotGameEvent *chosen =
        tarot_ai_best (solo->ai, &(solo->game), &predicted_score);
      if (game_add_event (&(solo->game), chosen) != TAROT_GAME_OK)
        {
          assert (0);
        }
      (void) predicted_score;
      solo_advance (solo);
    }
  else if (game_can_autoreveal (&(solo->game), &n_dog, 0, 6, dog))
    {
      TarotGameEvent dog_event;
      unsigned int ev_data[6];
      size_t j;
      dog_event.t = TAROT_DOG_EVENT;
      dog_event.n = n_dog;
      dog_event.data = ev_data;
      assert (n_dog <= 6);
      for (j = 0; j < 6; j++)
        {
          ev_data[j] = dog[j];
        }
      if (game_add_event (&(solo->game), &dog_event) != TAROT_GAME_OK)
        {
          assert (0);
        }
      solo_advance (solo);
    }
  else
    if (game_get_scores (&(solo->game), &n_scores, solo->myself, 1, &my_score)
        == TAROT_GAME_OK)
    {
      TarotGameIterator i;
      TarotGame rebuilt;
      const TarotGameEvent *next_event;
      game_initialize (&rebuilt, 4, 0);
      game_iterator_setup (&i, &(solo->game));
      while ((next_event = game_iterator_next_value (&i)) != NULL)
        {
          const double s = my_score;
          tarot_ai_learn (solo->ai, &rebuilt, next_event, s);
          if (game_add_event (&rebuilt, next_event) != TAROT_GAME_OK)
            {
              assert (0);
            }
        }
    }
}

#include <tarot/game_private_impl.h>
#include <tarot/game_event_private_impl.h>
#include <tarot/features_private_impl.h>
#endif /* H_TAROT_SOLO_PRIVATE_IMPL_INCLUDED */
