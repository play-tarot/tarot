/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_SOLO_PRIVATE_INCLUDED
#define H_TAROT_SOLO_PRIVATE_INCLUDED

#include <stddef.h>
#include <tarot_private.h>
#include <tarot/solo.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */
  static void solo_initialize (TarotSolo * solo, const TarotAi * ai);
  static void solo_destruct (TarotSolo * solo);
  static void solo_copy (TarotSolo * dest, const TarotSolo * source);
  static void solo_copy_as (TarotSolo * dest, const TarotSolo * source,
                            TarotPlayer who);
  static void solo_get (const TarotSolo * solo, TarotGame * game);
  static TarotGameError solo_setup (TarotSolo * solo, size_t n_players,
                                    int with_call, TarotPlayer myself,
                                    size_t n_owners,
                                    const TarotPlayer * owners,
                                    TarotPlayer taker, TarotCard call);
  static TarotGameError solo_setup_random (TarotSolo * solo, size_t n_players,
                                           int with_call, size_t seed_size,
                                           const unsigned char *seed);
  static TarotGameError solo_add (TarotSolo * solo,
                                  const TarotGameEvent * event);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_SOLO_PRIVATE_INCLUDED */
