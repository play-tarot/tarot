/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_SOLO_INCLUDED
#define H_TAROT_SOLO_INCLUDED

#include <stddef.h>
#include <tarot/game.h>
#include <tarot/player.h>
#include <tarot/cards.h>
#include <tarot/ai.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotSolo;
  typedef struct TarotSolo TarotSolo;

  /**
   * tarot_solo_alloc: (constructor)
   */
  TarotSolo *tarot_solo_alloc (void);

  /**
   * tarot_solo_alloc_fuzzy: (constructor)
   * @seed: (array length=seed_size) (element-type char):
   * @ai: (nullable):
   */
  TarotSolo *tarot_solo_alloc_fuzzy (double fuzziness, size_t seed_size,
                                     const void *seed, const TarotAi * ai);

  TarotSolo *tarot_solo_dup (const TarotSolo * source);
  void tarot_solo_free (TarotSolo * solo);

  void tarot_solo_copy (TarotSolo * dest, const TarotSolo * source);
  void tarot_solo_copy_as (TarotSolo * dest, const TarotSolo * source,
                           TarotPlayer who);
  void tarot_solo_get (const TarotSolo * solo, TarotGame * game);
  TarotGame *tarot_solo_get_alloc (const TarotSolo * solo);

  /**
   * tarot_solo_setup:
   * @owners: (array length=n_owners):
   * @with_call: (type boolean):
   */
  TarotGameError tarot_solo_setup (TarotSolo * solo, size_t n_players,
                                   int with_call, TarotPlayer myself,
                                   size_t n_owners,
                                   const TarotPlayer * owners,
                                   TarotPlayer taker, TarotCard call);

  /**
   * tarot_solo_setup_random:
   * @seed: (array length=seed_size) (type char):
   * @with_call: (type boolean):
   */
  TarotGameError tarot_solo_setup_random (TarotSolo * solo, size_t n_players,
                                          int with_call, size_t seed_size,
                                          const void *seed);
  TarotGameError tarot_solo_add (TarotSolo * solo,
                                 const TarotGameEvent * event);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_SOLO_INCLUDED */
