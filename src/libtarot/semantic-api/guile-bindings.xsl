<?xml version="1.0" encoding="UTF-8"?>
<!-- tarot implements the rules of the tarot game
Copyright (C) 2019  Vivien Kraus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template match="api">
    <xsl:text>#include &lt;config.h&gt;&#xA;</xsl:text>
    <xsl:text>#include &lt;tarot.h&gt;&#xA;</xsl:text>
    <xsl:text>#include &lt;libguile.h&gt;&#xA;</xsl:text>
    <xsl:text>#define DECLARE_ARRAY_ACCESSOR(type, ctype) static inline SCM scm_from_array_##type (size_t n, ctype *values) { size_t i; SCM ret = SCM_EOL; for (i = 0; i &lt; n; i++) ret = scm_cons (scm_from_##type (values[i]), ret); free (values); return scm_reverse (ret); } static inline ctype *scm_to_array_##type (size_t *n, SCM values) {size_t i; *n = scm_to_size_t (scm_length (values)); ctype *ret = scm_gc_malloc (*n * sizeof (ctype), &quot;tarot&quot;); for (i = 0; i &lt; *n &amp;&amp; scm_is_pair (values); i++, values = scm_cdr (values)) ret[i] = scm_to_##type (scm_car (values)); return ret;}&#xA;</xsl:text>
    <xsl:text>static inline SCM scm_from_narrow_string (const char *value) { size_t i = 0; size_t n = strlen (value); SCM ret = scm_c_make_bytevector (n); for (i = 0; i &lt; n; i++) scm_c_bytevector_set_x (ret, i, value[i]); return ret;}</xsl:text>
    <xsl:text>static inline char *scm_to_narrow_string (SCM value) { size_t n = scm_to_size_t (scm_bytevector_length (value)); char *ret = scm_gc_malloc (n + 1, &quot;a string&quot;); size_t i = 0; for (i = 0; i &lt; n; i++) ret[i] = scm_c_bytevector_ref (value, i); return ret; }</xsl:text>
    <xsl:text>&#xA;#define scm_to_unowned_pointer(ptr) scm_to_pointer (ptr)</xsl:text>
    <xsl:text>&#xA;#define scm_from_unowned_pointer(ptr) scm_from_pointer (ptr, NULL)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (char, char)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (bool, int)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (int, int)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (size_t, size_t)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (double, double)</xsl:text>
    <xsl:text>&#xA;DECLARE_ARRAY_ACCESSOR (unowned_pointer, void *)</xsl:text>
    <xsl:text>&#xA;#define scm_to_array_pointer(n, lst) (void *) scm_to_array_unowned_pointer (n, lst)</xsl:text>
    <xsl:text>&#xA;#define scm_from_array_pointer(n, ptr) scm_from_array_unowned_pointer (n, ptr)</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates mode="accessors" />
    <xsl:apply-templates />
    <xsl:text>void tarot_guile_init () { </xsl:text>
    <xsl:for-each select="descendant::method|descendant::malloc|descendant::dup|descendant::constructor">
      <xsl:text>scm_c_define_gsubr (&quot;</xsl:text>
      <xsl:value-of select="translate(@c-name, '_', '-')" />
      <xsl:text>&quot;, </xsl:text>
      <xsl:value-of select="count(parameter[@direction = 'in' and @size-placeholder = 'false']|instance-parameter)" />
      <xsl:text>, 0, 0, tarot_guile_</xsl:text>
      <xsl:value-of select="@c-name" />
      <xsl:text>);</xsl:text>
    </xsl:for-each>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="namespace">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="method|malloc|dup|constructor">
    <xsl:text>static SCM tarot_guile_</xsl:text><xsl:value-of select="@c-name" /><xsl:text> (</xsl:text>
    <xsl:for-each select="instance-parameter|parameter[@direction = 'in' and @size-placeholder = 'false']">
      <xsl:text>SCM g_</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text>, </xsl:text>
    </xsl:for-each>
    <xsl:text>) {</xsl:text>
    <xsl:text>&#xA;/* Declaration of the types for input parameters */&#xA;</xsl:text>
    <xsl:for-each select="instance-parameter|parameter[@direction = 'in']">
      <xsl:apply-templates mode="c-type" select="type" />
      <xsl:text> c_</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text>; </xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;/* Declaration of the type for the return value */&#xA;</xsl:text>
    <xsl:variable name="has-return-value">
      <xsl:choose>
	<xsl:when test="return/type/@kind = 'void' and return/type/@type = 'void'">
	  <xsl:text>no</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>yes</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$has-return-value = 'no'">
	<xsl:text>&#xA;/* There is no return value for this function */&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates mode="c-type" select="return/type" />
	<xsl:text> c_ret;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#xA;/* Declaration of the types for output parameters */&#xA;</xsl:text>
    <xsl:for-each select="parameter[@direction = 'out']">
      <xsl:apply-templates mode="c-type" select="type" />
      <xsl:text> c_</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text> = </xsl:text>
      <xsl:apply-templates mode="safe-value" select="type" />
      <xsl:text>; </xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;/* Declaration of the return value */&#xA;</xsl:text>
    <xsl:text>SCM g_ret = SCM_EOL;</xsl:text>
    <xsl:text>&#xA;/* Binding the input parameters */&#xA;</xsl:text>
    <xsl:for-each select="instance-parameter|parameter[@direction = 'in' and @size-placeholder = 'false']">
      <xsl:apply-templates mode="bind" select="type" />
    </xsl:for-each>
    <xsl:text>&#xA;/* Calling the function */&#xA;</xsl:text>
    <xsl:if test="$has-return-value = 'yes'">
      <xsl:text>c_ret = (</xsl:text>
      <xsl:apply-templates mode="c-type" select="return/type" />
      <xsl:text>) </xsl:text>
    </xsl:if>
    <xsl:value-of select="@c-name" />
    <xsl:text> (</xsl:text>
    <xsl:for-each select="instance-parameter|parameter">
      <xsl:if test="@direction = 'out'">
	<xsl:text>&amp;</xsl:text>
      </xsl:if>
      <xsl:text>c_</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text>, </xsl:text>
    </xsl:for-each>
    <xsl:text>);</xsl:text>
    <xsl:if test="$has-return-value = 'yes'">
      <xsl:text>&#xA;/* Pushing the return value */&#xA;</xsl:text>
      <xsl:apply-templates mode="push-return" select="return/type" />
    </xsl:if>
    <xsl:text>&#xA;/* Pushing the output parameters */&#xA;</xsl:text>
    <xsl:for-each select="parameter[@direction = 'out' or (type/@kind = 'array' and @const = 'no')]">
      <xsl:apply-templates mode="push" select="type" />
    </xsl:for-each>
    <xsl:text>&#xA;/* Returning */&#xA;</xsl:text>
    <xsl:text>if (scm_to_size_t (scm_length (g_ret)) == 0) { return SCM_UNDEFINED; }</xsl:text>
    <xsl:text>if (scm_to_size_t (scm_length (g_ret)) == 1) { return scm_car (g_ret); }</xsl:text>
    <xsl:text>return scm_reverse (g_ret);</xsl:text>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>

  <!-- 
       Computing the C type

These templates let us declare C variables to hold the input and output parameters.

  -->

  <xsl:template match="type[@kind = 'scalar' and @type = 'unsigned-integer']" mode="c-type">
    <xsl:text>unsigned int</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and (@type = 'boolean' or @type = 'integer')]" mode="c-type">
    <xsl:text>int</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'real']" mode="c-type">
    <xsl:text>double</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'size']" mode="c-type">
    <xsl:text>size_t</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'char']" mode="c-type">
    <xsl:text>char</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'address']" mode="c-type">
    <xsl:text>void *</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'enum']" mode="c-type">
    <xsl:variable name="type-name" select="@type" />
    <xsl:value-of select="/api/namespace/enumeration[@name = $type-name]/@c" />
  </xsl:template>

  <xsl:template match="type[@kind = 'class']" mode="c-type">
    <xsl:variable name="type-name" select="@type" />
    <xsl:value-of select="/api/namespace/class[@name = $type-name]/@c-name" />
    <xsl:text> *</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'array']" mode="c-type">
    <xsl:apply-templates mode="c-type" select="type" />
    <xsl:text>*</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="c-type">
    <xsl:variable name="type-name" select="@type" />
    <xsl:value-of select="/api/namespace/typedef[@name = $type-name]/@c" />
  </xsl:template>

  <xsl:template match="type[@kind = 'struct']" mode="c-type">
    <xsl:variable name="type-name" select="@type" />
    <xsl:value-of select="/api/namespace/struct[@name = $type-name]/@c-name" />
  </xsl:template>

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="c-type">
    <xsl:text>char *</xsl:text>
  </xsl:template>

  <xsl:template match="type" mode="c-type">
    <xsl:text>&#xA;#error &quot;No support for type of kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <!-- 
       Initializing output parameters with a safe value

In the library, some output parameters may not be always initialized,
for instance when the procedure encounters an error.  However, since
we are blindly copying all output parameter to scheme, we cannot leave
them uninitialized.

  -->

  <xsl:template match="type[@kind = 'scalar']" mode="safe-value">
    <xsl:text>0</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'enum']" mode="safe-value">
    <xsl:variable name="type-name" select="@type" />
    <xsl:value-of select="/api/namespace/enumeration[@name = $type-name]/member[1]/@c" />
  </xsl:template>

  <xsl:template match="type[@kind = 'class']" mode="safe-value">
    <xsl:text>NULL</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'array']" mode="safe-value">
    <xsl:text>NULL</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="safe-value">
    <xsl:variable name="type-name" select="@type" />
    <xsl:apply-templates mode="safe-value" select="/api/namespace/typedef[@name = $type-name]/type" />
  </xsl:template>

  <xsl:template match="type[@kind = 'struct']" mode="safe-value">
    <xsl:variable name="type-name" select="@type" />
    <xsl:text>{</xsl:text>
    <xsl:for-each select="/api/namespace/struct[@name = $type-name]/field">
      <xsl:text>.</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text> = </xsl:text>
      <xsl:apply-templates mode="safe-value" select="type" />
      <xsl:text>, </xsl:text>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="safe-value">
    <xsl:text>NULL</xsl:text>
  </xsl:template>

  <xsl:template match="type" mode="safe-value">
    <xsl:text>&#xA;#error &quot;No support for a safe value for kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <!-- 
       Binding the input parameters

The input parameters are bound from the Guile values.  If the guile value is a list, then the corresponding value in C is an allocated array.

  -->

  <xsl:template match="type[@kind = 'scalar' and @type = 'unsigned-integer']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_uint (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'integer']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_int (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'boolean']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_bool (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'real']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_double (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'size']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_size_t (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'char']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_char (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'address']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_pointer (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'enum']" mode="bind">
    <xsl:variable name="type-name" select="@type" />
    <xsl:variable name="parameter-name" select="../@name" />
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="$parameter-name" />
    <xsl:text> = scm_to_enum_</xsl:text>
    <xsl:value-of select="$type-name" />
    <xsl:text> (g_</xsl:text>
    <xsl:value-of select="$parameter-name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'class']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_pointer (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'array']" mode="bind">
    <xsl:variable name="array-parameter-name" select="../@name" />
    <xsl:variable name="size-parameter-label" select="@size-param" />
    <xsl:variable name="size-parameter-name" select="../../parameter[@label = $size-parameter-label]/@name" />
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="$array-parameter-name" />
    <xsl:text> = scm_to_array_</xsl:text>
    <xsl:for-each select="type">
      <xsl:choose>
	<xsl:when test="@kind = 'scalar' and @type = 'unsigned-integer'">
	  <xsl:text>uint</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'integer'">
	  <xsl:text>int</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'boolean'">
	  <xsl:text>bool</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'real'">
	  <xsl:text>double</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'size'">
	  <xsl:text>size_t</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'char'">
	  <xsl:text>char</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'enum'">
	  <xsl:text>enum_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'class'">
	  <xsl:text>pointer</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'typedef'">
	  <xsl:text>typedef_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'struct'">
	  <xsl:text>struct_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>&#xA;#error &quot;Cannot bind input elements of kind </xsl:text>
	  <xsl:value-of select="@kind" />
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="@type" />
	  <xsl:text>&quot;&#xA;</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>(&amp;</xsl:text>
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="$size-parameter-name" />
    <xsl:text>, g_</xsl:text>
    <xsl:value-of select="$array-parameter-name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_typedef_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'struct']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_struct_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="bind">
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text> = scm_to_narrow_string (g_</xsl:text>
    <xsl:value-of select="../@name" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="type" mode="bind">
    <xsl:text>&#xA;#error &quot;No support for binding an input parameter of kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="clean" />

  <xsl:template match="type[@kind = 'struct']" mode="clean" />

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="clean">
    <!-- We allocate the strings with the garbage collector. -->
  </xsl:template>

  <xsl:template match="type" mode="clean">
    <xsl:text>&#xA;#error &quot;No support for cleaning an input parameter of kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <!-- 

       Pushing the return value on the return stack

When the return value is known, we transfer it from the C side to the
Scheme side.  The return value is in fact a stack; for each output
parameter and modified array, we will push that value on the stack.
At the end of each function, the stack is reversed.

  -->
  
  <xsl:template match="type[@kind = 'scalar' and @type = 'unsigned-integer']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_uint (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'integer']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_int (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'boolean']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_bool (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'real']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_double (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'size']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_size_t (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'char']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_char (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'address']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_pointer (c_ret, NULL), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'enum']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_enum_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'class']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_class_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_ret, </xsl:text>
    <xsl:choose>
      <xsl:when test="../@transfer = 'full'">
	<xsl:text>1</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	0
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'array']" mode="push-return">
    <xsl:variable name="size-parameter-label" select="@size-param" />
    <xsl:variable name="size-parameter-name" select="../../parameter[@label = $size-parameter-label]/@name" />
    <xsl:text>g_ret = scm_cons (scm_from_array_</xsl:text>
    <xsl:for-each select="type">
      <xsl:choose>
	<xsl:when test="@kind = 'scalar' and @type = 'unsigned-integer'">
	  <xsl:text>uint</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'integer'">
	  <xsl:text>int</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'boolean'">
	  <xsl:text>bool</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'real'">
	  <xsl:text>double</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'size'">
	  <xsl:text>size_t</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'char'">
	  <xsl:text>char</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'enum'">
	  <xsl:text>enum_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'class'">
	  <xsl:text>pointer</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'typedef'">
	  <xsl:text>typedef_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'struct'">
	  <xsl:text>struct_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>&#xA;#error &quot;Cannot bind return elements of kind </xsl:text>
	  <xsl:value-of select="@kind" />
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="@type" />
	  <xsl:text>&quot;&#xA;</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>(</xsl:text>
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="$size-parameter-name" />
    <xsl:text>, c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_typedef_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'struct']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_struct_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_ret), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="push-return">
    <xsl:text>g_ret = scm_cons (scm_from_narrow_string (c_ret), g_ret);</xsl:text>
    <xsl:if test="../@transfer = 'full'">
      <xsl:text>free (c_ret);</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="type" mode="push-return">
    <xsl:text>&#xA;#error &quot;No support for returning a kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <!-- 

       Pushing the output values on the return stack

This is very similar to the return value, but we are examining named
output parameters.

  -->
  
  <xsl:template match="type[@kind = 'scalar' and @type = 'unsigned-integer']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_uint (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'integer']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_int (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'boolean']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_bool (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'real']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_double (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'size']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_size_t (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'char']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_char (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'scalar' and @type = 'address']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_pointer (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>, NULL), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'enum']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_enum_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'class']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_class_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="../@transfer = 'full'">
	<xsl:text>1</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	0
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'array']" mode="push">
    <xsl:variable name="size-parameter-label" select="@size-param" />
    <xsl:variable name="size-parameter-name" select="../../parameter[@label = $size-parameter-label]/@name" />
    <xsl:text>g_ret = scm_cons (scm_from_array_</xsl:text>
    <xsl:for-each select="type">
      <xsl:choose>
	<xsl:when test="@kind = 'scalar' and @type = 'unsigned-integer'">
	  <xsl:text>uint</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'integer'">
	  <xsl:text>int</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'boolean'">
	  <xsl:text>bool</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'real'">
	  <xsl:text>double</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'size'">
	  <xsl:text>size_t</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'scalar' and @type = 'char'">
	  <xsl:text>char</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'enum'">
	  <xsl:text>enum_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'class'">
	  <xsl:text>pointer</xsl:text>
	</xsl:when>
	<xsl:when test="@kind = 'typedef'">
	  <xsl:text>typedef_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:when test="@kind = 'struct'">
	  <xsl:text>struct_</xsl:text>
	  <xsl:value-of select="@type" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>&#xA;#error &quot;Cannot bind output elements of kind </xsl:text>
	  <xsl:value-of select="@kind" />
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="@type" />
	  <xsl:text>&quot;&#xA;</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>(</xsl:text>
    <xsl:text>c_</xsl:text>
    <xsl:value-of select="$size-parameter-name" />
    <xsl:text>, c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'typedef']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_typedef_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'struct']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_struct_</xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text> (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
  </xsl:template>

  <xsl:template match="type[@kind = 'string' and @type = 'char']" mode="push">
    <xsl:text>g_ret = scm_cons (scm_from_narrow_string (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>), g_ret);</xsl:text>
    <xsl:if test="../@transfer = 'full'">
      <xsl:text>free (c_</xsl:text><xsl:value-of select="../@name" /><xsl:text>);</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="type" mode="push">
    <xsl:text>&#xA;#error &quot;No support for output parameter of kind </xsl:text>
    <xsl:value-of select="@kind" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@type" />
    <xsl:text>&quot;&#xA;</xsl:text>
  </xsl:template>

  <!-- 

       Implementing the accessors

These functions convert from and to Scheme.

  -->

  <xsl:template match="typedef[type/@kind = 'scalar' and type/@type = 'unsigned-integer']" mode="accessors">
    <xsl:text>static inline SCM scm_from_typedef_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (</xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text> value) { return scm_from_uint (value); }</xsl:text>
    
    <xsl:text>static inline </xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text> scm_to_typedef_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (SCM value) { return scm_to_uint (value); }</xsl:text>

    <xsl:text>DECLARE_ARRAY_ACCESSOR (typedef_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="enumeration" mode="accessors">
    <xsl:text>static inline SCM scm_from_enum_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (</xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text> value) { switch (value) {</xsl:text>
    <xsl:for-each select="member">
      <xsl:text>case </xsl:text>
      <xsl:value-of select="@c" />
      <xsl:text>: return scm_from_utf8_symbol (&quot;</xsl:text>
      <xsl:value-of select="@nickname" />
      <xsl:text>&quot;); break;</xsl:text>
    </xsl:for-each>
    <xsl:text>} abort ();}</xsl:text>
    
    <xsl:text>static inline </xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text> scm_to_enum_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (SCM value) { if (0) { }</xsl:text>
    <xsl:for-each select="member">
      <xsl:text>else if (scm_to_bool (scm_eq_p (value, scm_from_utf8_symbol (&quot;</xsl:text>
      <xsl:value-of select="@nickname" />
      <xsl:text>&quot;)))) { return </xsl:text>
      <xsl:value-of select="@c" />
      <xsl:text>;} </xsl:text>
    </xsl:for-each>
    <xsl:text>scm_throw (scm_from_utf8_symbol ("invalid-argument"), SCM_EOL);</xsl:text>
    <xsl:text>abort ();</xsl:text>
    <xsl:text>}</xsl:text>

    <xsl:text>DECLARE_ARRAY_ACCESSOR (enum_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@c" />
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="class" mode="accessors">
    <xsl:text>static void free_class_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (void *data) { </xsl:text>
    <xsl:for-each select="free">
      <xsl:value-of select="@c-name" />
    </xsl:for-each>
    <xsl:text> ((</xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text> *) data); } </xsl:text>
    <xsl:text>static inline SCM scm_from_class_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (const </xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text> *value, int transfer) { if (transfer) { return scm_from_pointer ((void *) value, free_class_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>);} return scm_from_pointer ((void *) value, NULL); }</xsl:text>
  </xsl:template>

  <xsl:template match="struct" mode="accessors">
    <xsl:text>static inline SCM scm_from_struct_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (</xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text> value) { return scm_list_n (</xsl:text>
    <xsl:for-each select="field">
      <xsl:text>scm_from_</xsl:text>
      <xsl:choose>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'real'">
	  <xsl:text>double</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'size'">
	  <xsl:text>size_t</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'integer'">
	  <xsl:text>int</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'typedef'">
	  <xsl:text>typedef_</xsl:text>
	  <xsl:value-of select="type/@type" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>#error &quot;Unknown field kind </xsl:text>
	  <xsl:value-of select="type/@kind" />
	  <xsl:text>, type </xsl:text>
	  <xsl:value-of select="type/@type" />
	  <xsl:text>&quot;</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:text> (value.</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text> ), </xsl:text>
    </xsl:for-each>
    <xsl:text>SCM_UNDEFINED); }</xsl:text>
    
    <xsl:text>static inline </xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text> scm_to_struct_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text> (SCM value) {</xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text> ret;</xsl:text>
    <xsl:for-each select="field">
      <xsl:text>ret.</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text> = scm_to_</xsl:text>
      <xsl:choose>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'real'">
	  <xsl:text>double</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'size'">
	  <xsl:text>size_t</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'scalar' and type/@type = 'integer'">
	  <xsl:text>int</xsl:text>
	</xsl:when>
	<xsl:when test="type/@kind = 'typedef'">
	  <xsl:text>typedef_</xsl:text>
	  <xsl:value-of select="type/@type" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>#error &quot;Unknown field kind </xsl:text>
	  <xsl:value-of select="type/@kind" />
	  <xsl:text>, type </xsl:text>
	  <xsl:value-of select="type/@type" />
	  <xsl:text>&quot;</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:text> (scm_car (value)); value = scm_cdr (value);</xsl:text>
    </xsl:for-each>
    <xsl:text>return ret;}</xsl:text>

    <xsl:text>DECLARE_ARRAY_ACCESSOR (struct_</xsl:text>
    <xsl:value-of select="@name" />
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@c-name" />
    <xsl:text>)</xsl:text>
  </xsl:template>
</xsl:stylesheet>
