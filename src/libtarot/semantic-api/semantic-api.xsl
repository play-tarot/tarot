<?xml version="1.0" encoding="UTF-8"?>
<!-- tarot implements the rules of the tarot game
Copyright (C) 2019  Vivien Kraus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:c="http://www.gtk.org/introspection/c/1.0"
		xmlns:glib="http://www.gtk.org/introspection/glib/1.0"
		xmlns:gir="http://www.gtk.org/introspection/core/1.0"
		version="1.0">
  <xsl:output method="xml" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template match="gir:repository">
    <api>
      <xsl:apply-templates />
    </api>
  </xsl:template>

  <xsl:template match="gir:namespace">
    <namespace name="{@c:symbol-prefixes}">
      <xsl:apply-templates />
    </namespace>
  </xsl:template>

  <xsl:template match="gir:alias">
    <typedef name="{@name}" c="{@c:type}">
      <xsl:apply-templates />
    </typedef>
  </xsl:template>
  
  <xsl:template match="gir:enumeration">
    <enumeration name="{@name}" c="{@c:type}">
      <xsl:apply-templates />
    </enumeration>
  </xsl:template>

  <xsl:template match="gir:member">
    <member name="{@name}" value="{@value}" c="{@c:identifier}" nickname="{@glib:nick}" />
  </xsl:template>

  <xsl:template match="gir:record[boolean(gir:field)]">
    <struct name="{@name}" c-name="{@c:type}">
      <xsl:apply-templates />
    </struct>
  </xsl:template>

  <xsl:template match="gir:field">
    <field name="{@name}">
      <xsl:apply-templates />
    </field>
  </xsl:template>

  <xsl:template match="gir:record">
    <class name="{@name}" c-name="{@c:type}">
      <xsl:apply-templates />
    </class>
  </xsl:template>

  <xsl:template match="gir:function[@name = 'construct']">
    <constructor c-name="{@c:identifier}">
      <xsl:apply-templates />
    </constructor>
  </xsl:template>

  <xsl:template match="gir:function[starts-with (@name, 'construct_')]">
    <constructor name="{substring (@name, string-length ('construct_') + 1)}" c-name="{@c:identifier}">
      <xsl:apply-templates />
    </constructor>
  </xsl:template>

  <xsl:template match="gir:method[@name = 'destruct']">
    <destructor c-name="{@c:identifier}">
      <xsl:apply-templates />
    </destructor>
  </xsl:template>

  <xsl:template match="gir:method[@name = 'dup']">
    <dup c-name="{@c:identifier}">
      <xsl:apply-templates />
    </dup>
  </xsl:template>

  <xsl:template match="gir:method[@name = 'free']">
    <free c-name="{@c:identifier}">
      <xsl:apply-templates />
    </free>
  </xsl:template>

  <xsl:template match="gir:function | gir:method">
    <xsl:if test="not(boolean(@moved-to))">
      <method name="{@name}" c-name="{@c:identifier}">
	<xsl:apply-templates />
      </method>
    </xsl:if>
  </xsl:template>

  <xsl:template match="gir:constructor">
    <malloc name="{@name}" c-name="{@c:identifier}">
      <xsl:apply-templates />
    </malloc>
  </xsl:template>

  <!-- Convert from complex GLib types to semantic types -->
  <xsl:template match="gir:type[@name = 'gboolean']">
    <type kind="scalar" type="boolean" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'gint']">
    <type kind="scalar" type="integer" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'guint']">
    <type kind="scalar" type="unsigned-integer" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'gdouble']">
    <type kind="scalar" type="real" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'gsize']">
    <type kind="scalar" type="size" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'gchar' or @name = 'utf8']">
    <xsl:choose>
      <xsl:when test="@name = 'utf8' and (@c:type = 'char*' or @c:type = 'const char*' or @c:type = 'char**')">
	<type kind="string" type="char" />
      </xsl:when>
      <xsl:otherwise>
	<type kind="scalar" type="char" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="gir:type[@name = 'gpointer']">
    <type kind="scalar" type="address" />
  </xsl:template>

  <xsl:template match="gir:type[@name = 'none']">
    <type kind="void" type="void" />
  </xsl:template>

  <xsl:template match="gir:type[boolean(//gir:enumeration[@name = current()/@name])]">
    <type kind="enum" type="{@name}" />
  </xsl:template>

  <xsl:template match="gir:type[boolean(//gir:alias[@name = current()/@name])]">
    <type kind="typedef" type="{@name}" />
  </xsl:template>

  <xsl:template match="gir:type[boolean(//gir:record[@name = current()/@name])]">
    <xsl:variable name="type-name" select="@name" />
    <!-- This is a fake for-each as there should be only one record with this name -->
    <xsl:for-each select="//gir:record[@name = $type-name]">
      <xsl:choose>
	<xsl:when test="boolean(gir:field)">
	  <type kind="struct" type="{@name}" />
	</xsl:when>
	<xsl:otherwise>
	  <type kind="class" type="{@name}" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="gir:type">
    <xsl:message terminate="yes">
      Error: could not match type name <xsl:value-of select="@name" />.
    </xsl:message>
  </xsl:template>

  <xsl:template match="gir:array[@zero-terminated = '0']">
    <xsl:variable name="const">
      <xsl:variable name="suffix" select="'_out'" />
      <xsl:variable name="suffix-length" select="string-length($suffix)" />
      <xsl:variable name="param-name" select="../@name" />
      <xsl:variable name="corresponding" select="substring($param-name, string-length($param-name) - $suffix-length + 1)" />
      <xsl:choose>
	<xsl:when test="$corresponding = $suffix">
	  <xsl:text>no</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>yes</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <type kind="array" size-param="label-{@length}" const="{$const}">
      <xsl:apply-templates />
    </type>
  </xsl:template>

  <xsl:template match="gir:array">
    <xsl:message terminate="yes">
      Error: could not match array type, (C type <xsl:value-of select="@c:type" />).
    </xsl:message>
  </xsl:template>

  <xsl:template match="gir:return-value[@transfer-ownership = 'none']">
    <return transfer="none">
      <xsl:apply-templates />
    </return>
  </xsl:template>

  <xsl:template match="gir:return-value[@transfer-ownership = 'full']">
    <return transfer="full">
      <xsl:apply-templates />
    </return>
  </xsl:template>

  <xsl:template match="gir:return-value">
    <xsl:message terminate="yes">
      Error: could not understand the ownership of the return value.
    </xsl:message>
  </xsl:template>

  <xsl:template match="gir:parameter[@direction = 'out']">
    <xsl:variable name="position"
		  select="count(preceding-sibling::gir:parameter)" />
    <xsl:variable name="array"
		  select="boolean(../gir:parameter[gir:array/@length = $position])" />
    <parameter name="{@name}"
	       label="label-{$position}"
	       direction="out"
	       transfer="{@transfer-ownership}"
	       size-placeholder="{$array}">
      <xsl:apply-templates />
    </parameter>
  </xsl:template>

  <xsl:template match="gir:parameter">
    <xsl:variable name="position"
		  select="count(preceding-sibling::gir:parameter)" />
    <xsl:variable name="array"
		  select="boolean(../gir:parameter[gir:array/@length = $position])" />
    <parameter name="{@name}"
	       label="label-{$position}"
	       direction="in"
	       transfer="none"
	       size-placeholder="{$array}">
      <xsl:apply-templates />
    </parameter>
  </xsl:template>

  <xsl:template match="gir:instance-parameter[@transfer-ownership = 'none']">
    <instance-parameter name="{@name}" label="this" direction="in" transfer="none">
      <xsl:apply-templates />
    </instance-parameter>
  </xsl:template>

  <xsl:template match="gir:instance-parameter">
    <xsl:message terminate="yes">
      Error: could not understand the ownership of the instance <xsl:value-of select="@name" />.
    </xsl:message>
  </xsl:template>

  <xsl:template match="gir:constant[gir:type/@name = 'gint']">
    <constant name="{@name}" value="{@value}" c-name="{@c:type}" />
  </xsl:template>

  <xsl:template match="gir:constant">
    <xsl:message terminate="yes">
      Error: this type of constants is not implemented.
    </xsl:message>
  </xsl:template>

  <xsl:template match="gir:doc" />
</xsl:stylesheet>
