# tarot implements the rules of the tarot game
# Copyright (C) 2019  Vivien Kraus

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AM_CPPFLAGS += -I $(srcdir)/%reldir% -I %reldir%

EXTRA_DIST += %reldir%/test_guile.c %reldir%/tarot/config.scm.in

%reldir%/tarot/config.scm: $(srcdir)/%reldir%/tarot/config.scm.in
	@$(MKDIR_P) %reldir%/tarot
	$(AM_V_GEN) sed 's|"RUNTIME_PREFIX_ENV"|"$(RUNTIME_PREFIX_ENV)"|g' < $(srcdir)/%reldir%/tarot/config.scm.in \
	  | sed 's|"SYSCONFDIR"|"$(sysconfdir)"|g' \
	  | sed 's|"LOCALEDIR"|"$(localedir)"|g' \
	  | sed 's|"DATADIR"|"$(datadir)"|g' \
	  | sed 's|"PACKAGE"|"$(PACKAGE)"|g' \
	  | sed 's|"PACKAGE_STRING"|"$(PACKAGE_STRING)"|g' \
	  | sed 's|"USER_DATA_DIR"|"$(USER_DATA_DIR)"|g' \
	  | sed 's|"TEXTDOMAIN_CODESET"|"$(TEXTDOMAIN_CODESET)"|g' \
	  > %reldir%/tarot/config.scm-t
	@mv %reldir%/tarot/config.scm-t %reldir%/tarot/config.scm

CLEANFILES += %reldir%/tarot/config.scm

moddir=$(datadir)/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
tarotmoddir=$(moddir)/tarot
tarotgodir=$(godir)/tarot

dist_mod_DATA =
go_DATA =
dist_tarotmod_DATA =
tarotmod_DATA =
tarotgo_DATA =

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-goDATA
$(guile_install_go_files): install-dist_modDATA
guile_install_tarotgo_files = install-tarotgoDATA
$(guile_install_tarotgo_files): install-dist_tarotmodDATA install-tarotmodDATA

CLEANFILES += $(go_DATA) $(tarotgo_DATA)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat
SUFFIXES += .scm .go
.scm.go:
	$(MAKE) $(AM_MAKEFLAGS) %reldir%/tarot/config.scm
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILD) compile $(GUILE_WARNINGS) -o "$@" "$<"

if HAVE_GUILE
dist_tarotmod_DATA +=
tarotmod_DATA += %reldir%/tarot/config.scm
tarotgo_DATA += %reldir%/tarot/config.go
dist_mod_DATA += %reldir%/tarot.scm
go_DATA += %reldir%/tarot.go
AM_CFLAGS += $(GUILE_CFLAGS)
AM_LIBS += $(GUILE_LIBS)
lib_LTLIBRARIES += %reldir%/libtarot-guile.la
%canon_reldir%_libtarot_guile_la_SOURCES = \
  src/libtarot/semantic-api/tarot_guile.c
%canon_reldir%_libtarot_guile_la_LIBADD = \
  src/libtarot/libtarot.la $(AM_LIBS)
%canon_reldir%_libtarot_guile_la_LDFLAGS = \
  -no-undefined \
  -version-info 0:0:0 \
  $(AM_LDADD)
check_PROGRAMS += %reldir%/test-guile
%canon_reldir%_test_guile_SOURCES = \
  %reldir%/test_guile.c
%canon_reldir%_test_guile_LDADD = \
  src/libtarot/libtarot.la %reldir%/libtarot-guile.la $(AM_LIBS)
TESTS += %reldir%/test-guile
%reldir%/tarot.go: %reldir%/tarot/config.scm
endif
