;; tarot implements the rules of the tarot game
;; Copyright (C) 2019  Vivien Kraus

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tarot)
  #:export (libtarot-version
		 ace jack knight queen king petit twentyone excuse
		 of decompose
		 string->card string->card/c card-parse card-parse/c
		 card->string card->string/c))

(use-modules (tarot config) (rnrs bytevectors) (ice-9 optargs))

(define tarot-lib (dynamic-link "libtarot-guile.so"))

(dynamic-call "tarot_guile_init" tarot-lib)

(define (init)
  (let ((datadir (string->utf8 absolute-datadir))
	(localedir (string->utf8 absolute-localedir)))
    (tarot-set-datadir datadir)
    (if (not (eq? (tarot-init localedir) 0))
	(error "Could not initialize libtarot."))))
(init)
(define (libtarot-version)
  (utf8->string (tarot-libtarot-version)))
(define ace 1)
(define jack 11)
(define knight 12)
(define queen 13)
(define king 14)
(define petit 56)
(define twentyone 76)
(define excuse 77)
(define (input-card what)
  (cond ((eq? what 'petit) petit)
	((eq? what 'twentyone) twentyone)
	((eq? what 'excuse) excuse)
	(else what)))
(define (input-number what)
  (cond ((eq? what 'ace) ace)
	((eq? what 'jack) jack)
	((eq? what 'knight) knight)
	((eq? what 'queen) queen)
	((eq? what 'king) king)
	(else what)))
(define (of number suit)
  (let ((ret (tarot-of (input-number number) suit)))
    (if (eq? (car ret) 0)
	(cadr ret)
	#f)))
(define (decompose card)
  (let ((ret (tarot-decompose (input-card card))))
    (if (eq? (car ret) 0)
	(cdr ret)
	#f)))
(define (string->card str)
  (let ((ret (tarot-string-to-card (string->utf8 str))))
    (if (eq? ret -1)
	#f
	ret)))
(define (string->card/c str)
  (let ((ret (tarot-string-to-card-c (string->utf8 str))))
    (if (eq? ret -1)
	#f
	ret)))
(define (card-parse str)
  (let ((ret (tarot-card-parse (string->utf8 str))))
    (let ((parsed (if (>= (car ret) 78) #f (car ret)))
	  (remaining (utf8->string (cadr ret))))
      (list parsed remaining))))
(define (card-parse/c str)
  (let ((ret (tarot-card-parse-c (string->utf8 str))))
    (let ((parsed (if (>= (car ret) 78) #f (car ret)))
	  (remaining (utf8->string (cadr ret))))
      (list parsed remaining))))
(define (card->string card)
  (utf8->string (tarot-card-to-string-alloc (input-card card))))
(define (card->string/c card)
  (utf8->string (tarot-card-to-string-c-alloc (input-card card))))
