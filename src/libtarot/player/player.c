/* tarot implements the rules of the tarot game
 * Copyright (C) 2018, 2019  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/player_private.h>
#include <stdlib.h>
#include <string.h>
#include "xalloc.h"
#include <assert.h>

TarotPlayer
tarot_string_to_player (const char *text)
{
  char *end = NULL;
  TarotPlayer ret = player_parse (text, &end);
  if (end == (char *) text || *end != '\0')
    {
      ret = (TarotPlayer) - 1;
    }
  return ret;
}

TarotPlayer
tarot_string_to_player_c (const char *text)
{
  char *end = NULL;
  TarotPlayer ret = player_parse_c (text, &end);
  if (end == (char *) text || *end != '\0')
    {
      ret = (TarotPlayer) - 1;
    }
  return ret;
}

TarotPlayer
tarot_player_parse (const char *text, char **end)
{
  return player_parse (text, end);
}

TarotPlayer
tarot_player_parse_c (const char *text, char **end)
{
  return player_parse_c (text, end);
}

size_t
tarot_player_to_string (TarotPlayer p, size_t max, char *dest)
{
  return player_to_string (p, max, dest);
}

char *
tarot_player_to_string_alloc (TarotPlayer p)
{
  size_t n = player_to_string (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t check = player_to_string (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}

size_t
tarot_player_to_string_c (TarotPlayer p, size_t max, char *dest)
{
  return player_to_string_c (p, max, dest);
}

char *
tarot_player_to_string_c_alloc (TarotPlayer p)
{
  size_t n = player_to_string_c (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  // GCC warns that we should not use strncpy since we don't truncate
  // anyway.  This is true, but we don't care.
  size_t check = player_to_string_c (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}

#include <tarot/player_private_impl.h>
