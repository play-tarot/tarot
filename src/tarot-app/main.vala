// tarot implements the rules of the tarot game
// Copyright (C) 2019, 2020  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

[CCode (cname="RUNTIME_PREFIX_ENV")]
extern const string RUNTIME_PREFIX_ENV;

[CCode (cname="SYSCONFDIR")]
extern const string SYSCONFDIR;

[CCode (cname="LOCALEDIR")]
extern const string LOCALEDIR;

[CCode (cname="DATADIR")]
extern const string DATADIR;

[CCode (cname="PACKAGE")]
extern const string PACKAGE;

[CCode (cname="PACKAGE_STRING")]
extern const string PACKAGE_STRING;

[CCode (cname="USER_DATA_DIR")]
extern const string USER_DATA_DIR;

[CCode (cname="TEXTDOMAIN_CODESET")]
extern const string TEXTDOMAIN_CODESET;

const string program_name = "tarot";

namespace Tarot {
	
	public static void setup_environment (string runtime_prefix_env,
										  string sysconfdir,
										  string localedir,
										  string datadir) {
		string runtime_prefix = "";
		if (runtime_prefix_env != "" && runtime_prefix_env != "no") {
			string? test = GLib.Environment.get_variable (runtime_prefix_env);
			if (test == null) {
				stderr.printf (_ ("ERROR: environment variable '%s' is not set.  Try reinstalling the program.\n"),
							   runtime_prefix_env);
				GLib.Process.abort ();
			} else {
				runtime_prefix = test;
			}
		}
		string absolute_sysconfdir = runtime_prefix + sysconfdir;
		string absolute_localedir = runtime_prefix + localedir;
		string absolute_datadir = runtime_prefix + datadir;
		string? test = GLib.Environment.get_variable (USER_DATA_DIR);
		if (test == null && USER_DATA_DIR == "XDG_DATA_HOME") {
			// By specification, default to $HOME/.local/share
			string? home = GLib.Environment.get_variable ("HOME");
			if (home == null) {
				stderr.printf (_ ("Error: XDG_DATA_HOME nor HOME are set.\n"));
				GLib.Process.abort ();
			}
			test = home + "/.local/share";
		} else if (test == null) {
			stderr.printf (_ ("Error: environment variable '%s' is not set.\n"),
						   USER_DATA_DIR);
			GLib.Process.abort ();
		}
		string absolute_localdatadir = test;
		GLib.Environment.set_variable ("SYSCONFDIR", absolute_sysconfdir, true);
		GLib.Environment.set_variable ("LOCALEDIR", absolute_localedir, true);
		GLib.Environment.set_variable ("DATADIR", absolute_datadir, true);
		stderr.printf ("Sysconfdir: '%s'\nLocaledir: '%s'\nDatadir: '%s'\nLocaldatadir: '%s'\n",
					   absolute_sysconfdir, absolute_localedir, absolute_datadir, absolute_localdatadir);
		Tarot.set_datadir (absolute_datadir);
		if (Tarot.init (absolute_localedir) != 0) {
			stderr.printf (_ ("ERROR: could not initialize libtarot.\n"));
			GLib.Process.abort ();
		}
	}

	public class GuiApplication: Gtk.Application {

		public GLib.OptionEntry[] set_options () {
			var opt_version = GLib.OptionEntry ();
			opt_version.arg = OptionArg.NONE;
			opt_version.arg_data = null;
			opt_version.arg_description = null;
			opt_version.description = _ ("Print version information and exit");
			opt_version.flags = 0;
			opt_version.long_name = _ ("version");
			opt_version.short_name = 'v';
			GLib.OptionEntry opt_final = { null };
			var options = new GLib.OptionEntry[0];
			options += opt_version;
			options += opt_final;
			return options;
		}

		public override int handle_local_options (VariantDict options) {
			bool print_version = options.contains (_ ("version"));
			if (print_version) {
				return run_version (PACKAGE_STRING);
			}
			return -1;
		}
		public static int run_version (string package_string) {
			stdout.printf (_ ("%s (libtarot %s)\n\nCopyright © 2019 Vivien Kraus\nThis is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"),
						   package_string,
						   Tarot.libtarot_version ());
			return 0;
		}

		construct {
			add_main_option_entries (set_options ());
			this.activate.connect (() => {
					var win = active_window;
					if (win == null) {
						win = new Tarot.Window (this);
					}
					win.present ();
				});

		}
	}
	
	public static int main (string[] args) {
		setup_environment (RUNTIME_PREFIX_ENV, SYSCONFDIR, LOCALEDIR, DATADIR);
		GLib.Intl.setlocale (LocaleCategory.ALL, "");
		GLib.Intl.bindtextdomain (PACKAGE, Environment.get_variable ("LOCALEDIR"));
		GLib.Intl.textdomain (PACKAGE);
		if (TEXTDOMAIN_CODESET != "no" && TEXTDOMAIN_CODESET != "") {
			stderr.printf ("Warning: translations codepage set to %s\n", TEXTDOMAIN_CODESET);
			GLib.Intl.bind_textdomain_codeset(PACKAGE, TEXTDOMAIN_CODESET);
		}
		Gtk.Application app = new GuiApplication ();
		int ret = app.run (args);
		Tarot.quit ();
		return ret;
	}
}