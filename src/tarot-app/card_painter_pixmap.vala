// tarot implements the rules of the tarot game
// Copyright (C) 2019, 2024  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Cairo;

namespace TarotCairo {

	public errordomain LoaderError {
		NO_CARD_ASSETS
	}
	
	public class PixmapCardPainter: GLib.Object, TarotCairo.CardPainter {
		private Cairo.ImageSurface images[78];
		public double card_width {
			get { return images[0].get_width () ; }
		}
		public double card_height {
			get { return images[0].get_height (); }
		}
		public void paint_card (Tarot.Card card, bool is_selected, bool is_selectable, Cairo.Context context) {
			uint index = card;
			if (index >= 78) {
				return;
			}
			var img = images[index];
			context.translate (- img.get_width () / 2, - img.get_height () / 2);
			context.set_source_surface (img, 0, 0);
			context.rectangle (0, 0, img.get_width (), img.get_height ());
			context.fill ();
			// Add the overlays
			if (is_selected) {
				context.set_source_rgba (0.988, 0.914, 0.31, 0.2);
				context.fill ();
			}
			if (!is_selectable) {
				context.set_source_rgba (0.827, 0.843, 0.812, 0.2);
				context.fill ();
			}
		}
		public PixmapCardPainter () throws LoaderError {
			var datadir = GLib.Environment.get_variable ("DATADIR");
			if (datadir == null) {
				stderr.printf ("datadir not set\n");
				throw new LoaderError.NO_CARD_ASSETS ("datadir not set");
			}
			for (int i = 0; i < 78; i++) {
				var filename = "%s/%s/cards/card_%d.png".printf (datadir, PACKAGE, i);
				try {
					images[i] = new Cairo.ImageSurface.from_png (filename);
					if (images[i].get_width () == 0) {
						// I expected from_png to throw...
						throw new LoaderError.NO_CARD_ASSETS ("could not load card %s".printf (filename));
					}
				} catch (GLib.Error e) {
					throw new LoaderError.NO_CARD_ASSETS ("could not load card %s".printf (filename));
				}
			}
		}
	}
}
