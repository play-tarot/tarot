/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "program_commons.h"

void
tarot_program_setup (void)
{
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);
  tarot_set_datadir (DATADIR);
  if (tarot_init (LOCALEDIR) != 0)
    {
      fprintf (stderr, _("ERROR: could not initialize libtarot.\n"));
    }
}
