/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_PROGRAM_COMMONS_INCLUDED
#define H_PROGRAM_COMMONS_INCLUDED
#include <config.h>
#include <tarot.h>
#include "gettext.h"
#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include "xalloc.h"
#include <assert.h>
#include "getopt.h"
#include <string.h>
#include <nettle/yarrow.h>

#define _(String) gettext (String)
#define N_(String) String

void tarot_program_setup (void);
#endif /* not H_PROGRAM_COMMONS_INCLUDED */
