// tarot implements the rules of the tarot game
// Copyright (C) 2017, 2018, 2019  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Cairo;

namespace TarotCairo {
	public interface CardPainter: GLib.Object {

		class Reference {
			public size_t index;

			public Reference (size_t i) {
				index = i;
			}
		}

		/**
		 * Paint a card to the cairo context.  Usually this is simply
		 * printing a pixmap of width given by the property
		 * card_width.  The cards from 78 and up should be painted as
		 * a card back.
		 */
		public abstract void paint_card (Tarot.Card card, bool is_selected, bool is_selectable, Cairo.Context context);
		public abstract double card_width { get; }
		public abstract double card_height { get; }
	}
}
