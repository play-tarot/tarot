// tarot implements the rules of the tarot game
// Copyright (C) 2017, 2018, 2019  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Cairo;

namespace TarotCairo {
	public class DefaultCardPainter: GLib.Object, TarotCairo.CardPainter {
		private double _card_width = 32;
		private double _card_height = 48;
		public double card_width {
			get { return _card_width; }
		}
		public double card_height {
			get { return _card_height; }
		}
		public double border_width = 1;
		private void paint_border (bool selected, bool selectable, Cairo.Context context) {
			context.translate (- card_width / 2, - card_height / 2);
			context.rectangle (0, 0, card_width, card_height);
			context.set_line_width (border_width);
			if (selected) {
				context.set_source_rgb (0.988, 0.914, 0.31);
			} else if (selectable) {
				context.set_source_rgb (0.933, 0.933, 0.925);
			} else {
				context.set_source_rgb (0.827, 0.843, 0.812);
			}
			context.fill ();
			context.move_to (0, 0);
			context.line_to (0, card_height);
			context.line_to (card_width, card_height);
			context.line_to (card_width, 0);
			context.line_to (0, 0);
			context.set_source_rgb (0.18, 0.204, 0.212);
			context.stroke ();
		}
		private void paint_back (Cairo.Context context) {
			paint_border (false, false, context);
			context.rectangle (0, 0, card_width, card_height);
			context.set_source_rgb (0.333, 0.341, 0.325);
			context.fill ();
		}
		private void paint_number_and_suit (Cairo.Context context, string number, string suit, int color) {
			switch (color) {
			case 0:
				context.set_source_rgb (0.18, 0.204, 0.212);
				break;
			case 1:
				context.set_source_rgb (0.643, 0, 0);
				break;
			case 2:
				context.set_source_rgb (0.125, 0.29, 0.529);
				break;
			}
			var font_size = 4;
			context.select_font_face ("Sans", Cairo.FontSlant.NORMAL, Cairo.FontWeight.BOLD);
			context.set_font_size (font_size);
			context.translate (1, font_size);
			context.show_text (number);
			context.move_to (0, font_size);
			context.show_text (suit);
			context.move_to (6, 12);
			context.set_font_size (14);
			context.show_text (number);
			context.move_to (6, 32);
			context.show_text (suit);
		}
		public void paint_card (Tarot.Card card, bool selected, bool selectable, Cairo.Context context) {
			// This is debugging information in case you want to debug
			// an empty deck; it prints the location of the card
			// centers in pixels on the device.

			// double x = 0, y = 0;
			// context.user_to_device (ref x, ref y);
			// stderr.printf ("Printing card %s at (%f, %f)\n", card.to_string (), x, y);
			if (card < 78) {
				paint_border (selected, selectable, context);
				Tarot.Number n;
				Tarot.Suit s;
				string number = _ ("EXC");
				string suit = "*";
				int color = 2;
				if (Tarot.decompose (card, out n, out s) == 0) {
					if (s == Tarot.Suit.TRUMPS) {
						suit = "#";
						color = 2;
						number = n.to_string ();
					} else {
						switch (s) {
						case Tarot.Suit.HEARTS:
							suit = "♥";
							color = 1;
							break;
						case Tarot.Suit.CLUBS:
							suit = "♣";
							color = 0;
							break;
						case Tarot.Suit.DIAMONDS:
							suit = "♦";
							color = 1;
							break;
						case Tarot.Suit.SPADES:
							suit = "♠";
							color = 0;
							break;
						}
						number = n.to_string ();
						switch (n) {
						case 11:
							number = _ ("J");
							break;
						case 12:
							number = _ ("C");
							break;
						case 13:
							number = _ ("Q");
							break;
						case 14:
							number = _ ("K");
							break;
						}
					}
				}
				paint_number_and_suit (context, number, suit, color);
			} else if (card == 78) {
				paint_back (context);
			}
		}
	}
}
