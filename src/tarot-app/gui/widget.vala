// tarot implements the rules of the tarot game
// Copyright (C) 2019, 2024  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace Tarot {
	[GtkTemplate (ui = "/eu/planete_kraus/Tarot/widget.ui")]
	public class Widget : Gtk.Box {

		[GtkChild]
		private unowned Tarot.TrickCardView trick_widget;

		[GtkChild]
		private unowned Tarot.CenterCardView center_widget;

		[GtkChild]
		private unowned Tarot.FanCardView hand_widget;

		[GtkChild]
		private unowned Gtk.Stack entry_stack;

		[GtkChild]
		private unowned Gtk.FlowBox widget_bid;

		[GtkChild]
		private unowned Gtk.Button button_take;

		[GtkChild]
		private unowned Gtk.Button button_push;

		[GtkChild]
		private unowned Gtk.Button button_straight_keep;

		[GtkChild]
		private unowned Gtk.Button button_double_keep;

		[GtkChild]
		private unowned Gtk.Label player_information_myself;

		[GtkChild]
		private unowned Gtk.Box widget_decl;

		[GtkChild]
		private unowned Gtk.Button button_slam;

		[GtkChild]
		private unowned Gtk.Box widget_call;

		[GtkChild]
		private unowned Tarot.CardSelection call_selection;

		[GtkChild]
		private unowned Gtk.Button button_call;

		[GtkChild]
		private unowned Gtk.Box widget_discard;

		[GtkChild]
		private unowned Tarot.CardSelection discard_selection;

		[GtkChild]
		private unowned Gtk.Button button_discard;

		[GtkChild]
		private unowned Gtk.Box widget_handful;

		[GtkChild]
		private unowned Tarot.CardSelection handful_selection;

		[GtkChild]
		private unowned Gtk.Button button_handful;

		public signal void select_bid (Tarot.Bid bid);
		public signal void select_declaration (bool decl);
		public signal void select_call (uint call);
		public signal void select_discard (uint[] discard);
		public signal void select_handful (uint[] handful);
		public signal void select_card (uint card);

		[GtkCallback]
		private void select_pass () {
			dont_ask ();
			select_bid (Tarot.Bid.PASS);
		}

		[GtkCallback]
		private void select_take () {
			dont_ask ();
			select_bid (Tarot.Bid.TAKE);
		}

		[GtkCallback]
		private void select_push () {
			dont_ask ();
			select_bid (Tarot.Bid.PUSH);
		}

		[GtkCallback]
		private void select_straight_keep () {
			dont_ask ();
			select_bid (Tarot.Bid.STRAIGHT_KEEP);
		}

		[GtkCallback]
		private void select_double_keep () {
			dont_ask ();
			select_bid (Tarot.Bid.DOUBLE_KEEP);
		}

		[GtkCallback]
		private void select_none () {
			dont_ask ();
			select_declaration (false);
		}

		[GtkCallback]
		private void select_slam () {
			dont_ask ();
			select_declaration (true);
		}

		[GtkCallback]
		private void button_call_clicked () {
			dont_ask ();
			select_call (call_selection.get_selection ()[0]);
		}

		[GtkCallback]
		private void valid_call_selection () {
			button_call.sensitive = true;
		}

		[GtkCallback]
		private void invalid_call_selection () {
			button_call.sensitive = false;
		}

		[GtkCallback]
		private void button_discard_clicked () {
			dont_ask ();
			select_discard (discard_selection.get_selection ());
		}

		[GtkCallback]
		private void valid_discard_selection () {
			button_discard.sensitive = true;
		}

		[GtkCallback]
		private void invalid_discard_selection () {
			button_discard.sensitive = false;
		}

		[GtkCallback]
		private void button_handful_clicked () {
			dont_ask ();
			select_handful (handful_selection.get_selection ());
		}

		// The widget does not know if a handful is valid or not, we
		// have to ask the game to check.

		[GtkCallback]
		private void valid_handful_selection () {
			Tarot.GameEvent e = new Tarot.GameEvent.alloc_handful (handful_selection.get_selection ());
			button_handful.sensitive = current_game.check_event (e);
		}

		[GtkCallback]
		private void invalid_handful_selection () {
			valid_handful_selection ();
		}

		private bool allow_play = true;

		public void ask_bid (Tarot.Bid minimum) {
			button_double_keep.sensitive = false;
			button_straight_keep.sensitive = false;
			button_push.sensitive = false;
			button_take.sensitive = false;
			switch (minimum) {
			case Tarot.Bid.PASS:
			case Tarot.Bid.TAKE:
				button_double_keep.sensitive = true;
				button_straight_keep.sensitive = true;
				button_push.sensitive = true;
				button_take.sensitive = true;
				break;
			case Tarot.Bid.PUSH:
				button_double_keep.sensitive = true;
				button_straight_keep.sensitive = true;
				button_push.sensitive = true;
				break;
			case Tarot.Bid.STRAIGHT_KEEP:
				button_double_keep.sensitive = true;
				button_straight_keep.sensitive = true;
				break;
			case Tarot.Bid.DOUBLE_KEEP:
				button_double_keep.sensitive = true;
				break;
			}
			entry_stack.visible_child = widget_bid;
			assert (entry_stack.visible_child == widget_bid);
			entry_stack.show ();
		}

		public void ask_declaration (bool allowed) {
			select_declaration (false);
		}

		public void ask_call (Tarot.Number mini) {
			Tarot.Suit[] suits = {
				Tarot.Suit.HEARTS,
				Tarot.Suit.CLUBS,
				Tarot.Suit.DIAMONDS,
				Tarot.Suit.SPADES
			};
			Tarot.Number jack = 10 + 1;
			Tarot.Number knight = jack + 1;
			Tarot.Number queen = knight + 1;
			Tarot.Number king = queen + 1;
			Tarot.Card[] possible = new Tarot.Card[0];
			for (Tarot.Number i = mini; i <= king; i++) {
				foreach (var s in suits) {
					Tarot.Card added;
					if (Tarot.of (i, s, out added) != 0) {
						assert (false);
					}
					possible += added;
				}
			}
			call_selection.set_forced ({});
			call_selection.set_optional (possible);
			call_selection.set_n_optional (1);
			entry_stack.visible_child = widget_call;
			assert (entry_stack.visible_child == widget_call);
			entry_stack.show ();
		}

		public void ask_discard (size_t n, Tarot.Card[] prio, Tarot.Card[] additional) {
			Tarot.Card[] forced = {};
			Tarot.Card[] optional = prio;
			size_t n_optional = n;
			if (n >= prio.length) {
				forced = prio;
				optional = additional;
				n_optional = n - prio.length;
			}
			discard_selection.set_forced (forced);
			discard_selection.set_optional (optional);
			discard_selection.set_n_optional (n_optional);
			entry_stack.visible_child = widget_discard;
			assert (entry_stack.visible_child == widget_discard);
			entry_stack.show ();
		}

		public void ask_handful (size_t n_simple, size_t n_double, size_t n_triple, Tarot.Card[] prio, Tarot.Card[] additional) {
			Tarot.Card[] forced = {};
			Tarot.Card[] optional = prio;
			foreach (var c in additional) {
				optional += c;
			}
			size_t n_optional = n_simple;
			// The selection widget cannot handle staged handfuls, so
			// we rely on the game to check the handful at each stage.
			handful_selection.set_forced (forced);
			handful_selection.set_optional (optional);
			handful_selection.set_n_optional (n_optional);
			entry_stack.visible_child = widget_handful;
			assert (entry_stack.visible_child == widget_handful);
			entry_stack.show ();
		}

		public void ask_play (Tarot.Card[] playable) {
			// No special widget; click on your hand.
			allow_play = true;
			var is_playable = new bool[78];
			for (size_t i = 0; i < 78; i++) {
				is_playable[i] = false;
			}
			foreach (var c in playable) {
				is_playable[(int) c] = true;
			}
			var i = new size_t[0];
			Tarot.Card[] all_cards = hand_widget.get_cards ();
			for (size_t j = 0; j < all_cards.length; j++) {
				if (is_playable[all_cards[j]]) {
					i += j;
				}
			}
			hand_widget.set_selectable (i);
		}

		public void dont_ask () {
			entry_stack.hide ();
			allow_play = false;
			var empty_selectable = new size_t[0];
			hand_widget.set_selectable (empty_selectable);
		}

		private Tarot.Game current_game = new Tarot.Game.alloc ();

		private string compute_my_status_with_score () {
			Tarot.Player myself;
			if (current_game.get_main_player (out myself) != Tarot.GameError.OK) {
				myself = 0;
			}
			int[] scores;
			if (current_game.get_scores_alloc (out scores) == Tarot.GameError.OK) {
				if (scores[myself] % 2 == 0) {
					return _ ("%s\nScore: %d").printf (compute_my_status (myself),
													   scores[myself] / 2);
				}
				return _ ("%s\nScore: %d.5").printf (compute_my_status (myself),
													 scores[myself] / 2);
			}
			return compute_my_status (myself);
		}

		private string compute_my_status (Tarot.Player myself) {
			Tarot.Player taker;
			current_game.get_taker (out taker);
			Tarot.Bid[] bids;
			Tarot.Player partner;
			if (current_game.get_bids_alloc (out bids) == Tarot.GameError.OK
				&& bids.length > myself) {
				if (taker == myself) {
					Tarot.Player declarant;
					if (current_game.get_declarant (out declarant) == Tarot.GameError.OK
						&& declarant == myself) {
						switch (bids[taker]) {
						case Tarot.Bid.PASS:
							return _ ("%s slams!").printf (myself.to_string_alloc ());
						case Tarot.Bid.TAKE:
							return _ ("%s takes with slam!").printf (myself.to_string_alloc ());
						case Tarot.Bid.PUSH:
							return _ ("%s pushes with slam!").printf (myself.to_string_alloc ());
						case Tarot.Bid.STRAIGHT_KEEP:
							return _ ("%s keeps with slam!").printf (myself.to_string_alloc ());
						case Tarot.Bid.DOUBLE_KEEP:
							return _ ("%s keeps (double, with slam)!").printf (myself.to_string_alloc ());
						}
					} else {
						switch (bids[taker]) {
						case Tarot.Bid.PASS:
							return _ ("%s (pass)").printf (myself.to_string_alloc ());
						case Tarot.Bid.TAKE:
							return _ ("%s takes.").printf (myself.to_string_alloc ());
						case Tarot.Bid.PUSH:
							return _ ("%s pushes.").printf (myself.to_string_alloc ());
						case Tarot.Bid.STRAIGHT_KEEP:
							return _ ("%s keeps.").printf (myself.to_string_alloc ());
						case Tarot.Bid.DOUBLE_KEEP:
							return _ ("%s keeps (double).").printf (myself.to_string_alloc ());
						}
					}
				} else if (current_game.get_partner (out partner) == Tarot.GameError.OK
						   && partner == myself) {
					Tarot.Player declarant;
					if (current_game.get_declarant (out declarant) == Tarot.GameError.OK
						&& declarant == myself) {
						return _ ("%s (called) slams!").printf (myself.to_string_alloc ());
					} else {
						return _ ("%s (called)").printf (myself.to_string_alloc ());
					}
				} else {
					Tarot.Player declarant;
					if (current_game.get_declarant (out declarant) == Tarot.GameError.OK
						&& declarant == myself) {
						switch (bids[myself]) {
						case Tarot.Bid.PASS:
							return _ ("%s slams!").printf (myself.to_string_alloc ());
						case Tarot.Bid.TAKE:
							return _ ("%s slams (take)!").printf (myself.to_string_alloc ());
						case Tarot.Bid.PUSH:
							return _ ("%s slams (push)!").printf (myself.to_string_alloc ());
						case Tarot.Bid.STRAIGHT_KEEP:
							return _ ("%s slams (str. keep)!").printf (myself.to_string_alloc ());
						case Tarot.Bid.DOUBLE_KEEP:
							return _ ("%s slams (dbl. keep)!").printf (myself.to_string_alloc ());
						}
					} else {
						switch (bids[myself]) {
						case Tarot.Bid.TAKE:
							return _ ("%s (take)").printf (myself.to_string_alloc ());
						case Tarot.Bid.PUSH:
							return _ ("%s (push)").printf (myself.to_string_alloc ());
						case Tarot.Bid.STRAIGHT_KEEP:
							return _ ("%s (str. keep)").printf (myself.to_string_alloc ());
						case Tarot.Bid.DOUBLE_KEEP:
							return _ ("%s (dbl. keep)").printf (myself.to_string_alloc ());
						}
					}
				}
			}
			return _ ("%s").printf (myself.to_string_alloc ());
		}

		[GtkChild]
		private unowned Gtk.TreeView player_information_others;

		private void update_header_model () {
			size_t n_players = current_game.n_players ();
			var types = new GLib.Type[n_players - 1];
			for (size_t i = 1; i < n_players; i++) {
				types[i - 1] = typeof (string);
			}
			var model = new Gtk.ListStore.newv (types);
			Gtk.TreeIter iter;
			Tarot.Player myself;
			if (current_game.get_main_player (out myself) != Tarot.GameError.OK) {
				myself = 0;
			}
			// Add a row for the bids
			Tarot.Bid[] bids;
			if (current_game.get_bids_alloc (out bids) == Tarot.GameError.OK) {
				model.append (out iter);
				for (size_t i = 1; i < n_players; i++) {
					Tarot.Player p = (Tarot.Player) ((myself + i) % n_players);
					if (p < bids.length) {
						switch (bids[p]) {
						case Tarot.Bid.PASS:
							if (bids.length < n_players) {
								model.@set (iter, i - 1, _ ("pass"));
							}
							break;
						case Tarot.Bid.TAKE:
							model.@set (iter, i - 1, _ ("take"));
							break;
						case Tarot.Bid.PUSH:
							model.@set (iter, i - 1, _ ("push"));
							break;
						case Tarot.Bid.STRAIGHT_KEEP:
							model.@set (iter, i - 1, _ ("straight keep"));
							break;
						case Tarot.Bid.DOUBLE_KEEP:
							model.@set (iter, i - 1, _ ("double keep"));
							break;
						}
					}
				}
			}
			// Add a row for the slam declarations if relevant
			Tarot.Player declarant;
			if (current_game.get_declarant (out declarant) == Tarot.GameError.OK
				&& declarant != myself) {
				model.append (out iter);
				for (size_t i = 1; i < n_players; i++) {
					Tarot.Player p = (Tarot.Player) ((myself + i) % n_players);
					if (p == declarant) {
						model.@set (iter, i - 1, _ ("slam"));
					}
				}
			}
			// Add a row for the score
			int[] scores;
			if (current_game.get_scores_alloc (out scores) == Tarot.GameError.OK) {
				model.append (out iter);
				for (size_t i = 1; i < n_players; i++) {
					Tarot.Player p = (Tarot.Player) ((myself + i) % n_players);
					if (scores[p] % 2 == 0) {
						model.@set (iter, i - 1, (scores[p] / 2).to_string ());
					} else {
						model.@set (iter, i - 1, "%d.5".printf (scores[p] / 2));
					}
				}
			}
			player_information_others.model = model;
			var columns = player_information_others.get_columns ();
			foreach (var c in columns) {
				player_information_others.remove_column (c);
			}
			Tarot.Player taker;
			current_game.get_taker (out taker);
			Tarot.Player partner;
			bool has_partner = (current_game.get_partner (out partner) == Tarot.GameError.OK);
			for (size_t i = 0; i + 1 < n_players; i++) {
				Tarot.Player p = (Tarot.Player) ((myself + i + 1) % n_players);
				var renderer = new Gtk.CellRendererText ();
				if (taker == p) {
					renderer.background = "#a40000";
					renderer.foreground = "#eeeeec";
				} else if (has_partner && partner == p) {
					renderer.background = "#c4a000";
					renderer.foreground = "#2e3436";
				}
				renderer.alignment = Pango.Alignment.CENTER;
				var column = new Gtk.TreeViewColumn.with_attributes (p.to_string_alloc (),
																	 renderer,
																	 "text", i,
																	 null);
				column.expand = true;
				column.alignment = (float) 0.5;
				player_information_others.append_column (column);
			}
			player_information_others.show ();
		}

		[GtkChild]
		private unowned Gtk.Label call_info;

		private Tarot.Card[] trick_content_aux (Tarot.Player view, out Tarot.Player trick_taker, size_t trick) {
			trick_taker = 42;
			size_t n_players = current_game.n_players ();
			var cards_trick = new Tarot.Card[n_players];
			var trick_content = new Tarot.GameCardAnalysis[0];
			Tarot.Player trick_leader;
			bool has_trick_content = (current_game.get_trick_cards_alloc (trick, out trick_content)
									  == Tarot.GameError.OK);
			bool has_trick_leader = (current_game.get_trick_leader (trick, out trick_leader)
									 == Tarot.GameError.OK);
			current_game.get_trick_taker (trick, out trick_taker);
			if (has_trick_content && has_trick_leader) {
				for (size_t i = 0; i < n_players; i++) {
					cards_trick[i] = 79; // Not painted
				}
				var card_of = new Tarot.Card[n_players];
				for (int i = 0; i < n_players; i++) {
					card_of[i] = 79;
				}
				assert (trick_content.length <= n_players);
				for (int i = 0; i < trick_content.length; i++) {
					var p = (trick_leader + i) % n_players;
					card_of[p] = trick_content[i].card;
				}
				for (int i = 0; i < n_players; i++) {
					Tarot.Player p = (Tarot.Player) (((size_t) (view + i)) % n_players);
					cards_trick[i] = card_of[p];
				}
				return cards_trick;
			} else {
				return new Tarot.Card[0];
			}
		}

		private Tarot.Card[] trick_content (Tarot.Player view, out Tarot.Player trick_taker) {
			trick_taker = 42;
			size_t current_trick;
			bool has_current_trick = (current_game.get_current_trick (out current_trick) == Tarot.GameError.OK);
			if (has_current_trick && current_trick >= current_game.n_tricks ()) {
				current_trick--;
			}
			if (!has_current_trick) {
				current_trick = current_game.n_tricks () - 1;
				has_current_trick = true;
			}
			var trick_content = new Tarot.GameCardAnalysis[0];
			bool has_trick_content = (has_current_trick
									  && (current_game.get_trick_cards_alloc (current_trick,
																			  out trick_content)
										  == Tarot.GameError.OK));
			if (has_trick_content && current_trick > 0 && trick_content.length == 0) {
				// uninteresting empty trick
				current_trick--;
			}
			Tarot.Player my_trick_taker;
			var ret = trick_content_aux (view, out my_trick_taker, current_trick);
			trick_taker = my_trick_taker;
			return ret;
		}

		private Tarot.Card[] center_content () {
			Tarot.Card[] foreground = new Tarot.Card[0];
			if (current_game.step () == Tarot.Step.DISCARD) {
				if (current_game.get_dog_alloc (out foreground) != Tarot.GameError.OK) {
					assert (false);
				}
			} else {
				size_t i_trick;
				Tarot.GameCardAnalysis[] trick_cards = {};
				if (current_game.get_current_trick (out i_trick) == Tarot.GameError.OK
					&& i_trick == 0
					&& current_game.get_trick_cards_alloc (i_trick, out trick_cards) == Tarot.GameError.OK
					&& trick_cards.length == 0
					&& current_game.get_public_discard_alloc (out foreground) == Tarot.GameError.OK) {
					// foreground is set to the public discard
				}
			}
			return foreground;
		}

		private void update () {
			Tarot.Player myself;
			if (current_game.get_main_player (out myself) != Tarot.GameError.OK) {
				myself = 0;
			}
			Tarot.Card[] my_cards;
			if (current_game.get_cards_alloc (myself, out my_cards) != Tarot.GameError.OK) {
				my_cards = new Tarot.Card[0];
			}
			Tarot.Player taker = 42;
			Tarot.Card[] dog = {};
			if (current_game.step () == Tarot.Step.DISCARD
				&& current_game.get_taker (out taker) == Tarot.GameError.OK
				&& taker == myself
				&& current_game.get_dog_alloc (out dog) == Tarot.GameError.OK) {
				// Add the dog to our cards
				// We have to sort the cards though.
				bool present[78];
				for (Tarot.Card i = 0; i < 78; i++) {
					present[i] = false;
				}
				foreach (var c in my_cards) {
					present[c] = true;
				}
				foreach (var c in dog) {
					present[c] = true;
				}
				my_cards = new Tarot.Card[0];
				for (Tarot.Card i = 0; i < 78; i++) {
					if (present[i]) {
						my_cards += i;
					}
				}
			}
			hand_widget.set_cards (my_cards);
			Tarot.Player trick_taker;
			trick_widget.set_cards (trick_content (myself, out trick_taker));
			size_t position = (trick_taker + current_game.n_players () - myself) % current_game.n_players ();
			size_t[] trick_selection = { position };
			if (position < trick_widget.get_cards ().length) {
				trick_widget.set_selection (trick_selection);
			} else {
				trick_widget.set_selection ({});
			}
			center_widget.set_cards (center_content ());
			if (center_widget.get_cards ().length == 0) {
				center_widget.hide ();
			} else {
				center_widget.show ();
			}
			current_game.get_taker (out taker);
			var taker_format = "%s";
			Tarot.Bid[] bids;
			if (current_game.get_bids_alloc (out bids) != Tarot.GameError.OK) {
				bids = new Tarot.Bid[0];
			}
			if (taker < bids.length) {
				switch (bids[taker]) {
				case Tarot.Bid.PASS:
					break;
				case Tarot.Bid.TAKE:
					taker_format = _ ("%s\ntakes");
					break;
				case Tarot.Bid.PUSH:
					taker_format = _ ("%s\npushes");
					break;
				case Tarot.Bid.STRAIGHT_KEEP:
					taker_format = _ ("%s\nkeeps");
					break;
				case Tarot.Bid.DOUBLE_KEEP:
					taker_format = _ ("%s\nkeeps (double)");
					break;
				}
			}
			player_information_myself.label = compute_my_status_with_score ();
			update_header_model ();
			Tarot.Card call;
			Tarot.Player partner;
			const string[] names = {
				N_ ("A♥"), N_ ("2♥"), N_ ("3♥"), N_ ("4♥"), N_ ("5♥"),
				N_ ("6♥"), N_ ("7♥"), N_ ("8♥"), N_ ("9♥"), N_ ("10♥"),
				N_ ("J♥"), N_ ("C♥"), N_ ("Q♥"), N_ ("K♥"),
				N_ ("A♣"), N_ ("2♣"), N_ ("3♣"), N_ ("4♣"), N_ ("5♣"),
				N_ ("6♣"), N_ ("7♣"), N_ ("8♣"), N_ ("9♣"), N_ ("10♣"),
				N_ ("J♣"), N_ ("C♣"), N_ ("Q♣"), N_ ("K♣"),
				N_ ("A♦"), N_ ("2♦"), N_ ("3♦"), N_ ("4♦"), N_ ("5♦"),
				N_ ("6♦"), N_ ("7♦"), N_ ("8♦"), N_ ("9♦"), N_ ("10♦"),
				N_ ("J♦"), N_ ("C♦"), N_ ("Q♦"), N_ ("K♦"),
				N_ ("A♠"), N_ ("2♠"), N_ ("3♠"), N_ ("4♠"), N_ ("5♠"),
				N_ ("6♠"), N_ ("7♠"), N_ ("8♠"), N_ ("9♠"), N_ ("10♠"),
				N_ ("J♠"), N_ ("C♠"), N_ ("Q♠"), N_ ("K♠"),
				N_ ("1★"), N_ ("2★"), N_ ("3★"), N_ ("4★"), N_ ("5★"),
				N_ ("6★"), N_ ("7★"), N_ ("8★"), N_ ("9★"), N_ ("10★"),
				N_ ("11★"), N_ ("12★"), N_ ("13★"), N_ ("14★"), N_ ("15★"),
				N_ ("16★"), N_ ("17★"), N_ ("18★"), N_ ("19★"), N_ ("20★"),
				N_ ("21★"),
				N_ ("EXC"),
				null
			};
			if (current_game.get_call (out call) == Tarot.GameError.OK
				&& current_game.get_partner (out partner) != Tarot.GameError.OK) {
				call_info.label = _ ("Call: %s").printf (_ (names[(int) call]));
				call_info.show ();
			} else if (current_game.get_partner (out partner) == Tarot.GameError.OK
					   && partner == myself) {
				// We need to show the info at the same level, not in
				// the bar downstairs!
				call_info.label = _ ("Call: you (%s)").printf (_ (names[(int) call]));
				call_info.show ();
			} else {
				call_info.hide ();
			}
		}

		public Tarot.Game game {
			get {
				return current_game;
			}
			set {
				current_game = value;
				update ();
			}
		}

		private void card_selected (size_t i_card) {
			Tarot.Card c = hand_widget.get_cards ()[i_card];
			Tarot.GameEvent e = new Tarot.GameEvent.alloc_card (c);
			if (current_game.check_event (e)) {
				// c is a playable card
				select_card (c);
			} else {
				no_card_selected ();
			}
		}

		private void no_card_selected () {
			size_t[] selection = { };
			hand_widget.set_selection (selection);
		}

		construct {
			hand_widget.motion_notify_event_card.connect ((motion, i_card) => {
					hand_widget.set_pointed (i_card);
				});
			hand_widget.motion_notify_event_no_card.connect ((motion) => {
					hand_widget.set_pointed (null);
				});
			trick_widget.motion_notify_event_card.connect ((motion, i_card) => {
					trick_widget.set_pointed (i_card);
				});
			trick_widget.motion_notify_event_no_card.connect ((motion) => {
					trick_widget.set_pointed (null);
				});
			center_widget.motion_notify_event_card.connect ((motion, i_card) => {
					center_widget.set_pointed (i_card);
				});
			center_widget.motion_notify_event_no_card.connect ((motion) => {
					center_widget.set_pointed (null);
				});
			hand_widget.button_press_event_card.connect ((button, i_card) => {
					if (allow_play) {
						stderr.printf ("allow_play = true\n");
						card_selected (i_card);
					}
					// With the mouse, we play the card directly.
				});
			hand_widget.button_press_event_no_card.connect ((button) => {
					no_card_selected ();
				});
			discard_selection.n_remaining_cards_to_select.connect ((n) => {
					if (n == 0) {
						button_discard.label = _ ("Discard");
					} else if (n < 0) {
						button_discard.label = _ ("%d cards in excess").printf (-n);
					} else {
						button_discard.label = _ ("%d...").printf (n);
					}
				});
			widget_bid.show ();
			widget_decl.show ();
			widget_call.show ();
			widget_discard.show ();
			widget_handful.show ();
			entry_stack.hide ();
		}
	}
}
