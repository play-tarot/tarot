// tarot implements the rules of the tarot game
// Copyright (C) 2019, 2020, 2024  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

[CCode (cname="VERSION")]
extern const string VERSION;

[CCode (cname="EXTERNAL_FILE_MANAGER_PROGRAM")]
extern const string EXTERNAL_FILE_MANAGER_PROGRAM;

namespace Tarot {

	[DBus (name = "org.freedesktop.FileManager1")]
	public interface FileManagerInterface: Object {
		public abstract void ShowFolders (string[] URIs, string StartupId) throws GLib.Error;
	}

	private class EventQueue {

		// If there are more than that number of events, an assert is
		// raised!
		private Tarot.GameEvent m_data[256];
		private size_t m_head;
		private size_t m_n;
		private bool timer_running = false;
		private bool deleted = false;
		private double next_delay = 0;

		public signal void new_event (Tarot.GameEvent e);
		public signal void catchup ();

		private void start_timer () {
			timer_running = true;
			double seconds = next_delay;
			next_delay = 1;
			switch (m_data[m_head].type ()) {
			case Tarot.GameEventT.SETUP_EVENT:
			case Tarot.GameEventT.DEAL_EVENT:
			case Tarot.GameEventT.BID_EVENT:
			case Tarot.GameEventT.DECL_EVENT:
				next_delay = 0;
				break;
			case Tarot.GameEventT.DOG_EVENT:
				next_delay = 3;
				break;
			case Tarot.GameEventT.HANDFUL_EVENT:
				var cards = new Tarot.Card[0];
				if (m_data[m_head].get_handful_alloc (out cards) != Tarot.GameEventError.OK) {
					assert (false);
				}
				if (cards.length != 0) {
					next_delay = 3;
				} else {
					next_delay = 0;
				}
				break;
			default:
				next_delay = 1;
				break;
			}
			GLib.Timeout.add ((uint) (1000 * seconds), () => {
					timer_running = false;
					if (!deleted) {
						dequeue ();
					}
					return false;
				});
		}

		private void dequeue () {
			assert (m_n > 0);
			new_event (m_data[m_head]);
			m_head ++;
			m_n --;
			m_head %= m_data.length;
			if (m_n > 0) {
				assert (!timer_running);
				start_timer ();
			} else {
				catchup ();
			}
		}

		public void enqueue (Tarot.GameEvent e) {
			assert (m_n < m_data.length);
			size_t i = (m_head + m_n) % m_data.length;
			m_data[i] = e;
			if (m_n++ == 0) {
				assert (!timer_running);
				start_timer ();
			}
		}

		public void close () {
			deleted = true;
		}

		public bool empty () {
			return (m_n == 0);
		}
	}

	[GtkTemplate (ui = "/eu/planete_kraus/Tarot/window.ui")]
	public class Window : Gtk.ApplicationWindow {

		[GtkChild]
		private unowned Gtk.Stack pages;

		[GtkChild]
		private unowned Tarot.Widget tarot_widget;

		[GtkChild]
		private unowned Gtk.ListBox page_select_players;

		[GtkChild]
		private unowned Gtk.Button button_new_game;

		[GtkChild]
		private unowned Gtk.PopoverMenu menu_popover;

		private Tarot.Solo my_game;
		private EventQueue m_queue;
		// Events transfered from my_game to the queue:
		private size_t n_events_pulled;
		private Tarot.Game game_shown;

		private void update_events () {
			size_t i = 0;
			var game = my_game.get_alloc ();
			foreach (var e in game) {
				if (i >= n_events_pulled) {
					m_queue.enqueue (e);
					n_events_pulled++;
				}
				i++;
			}
			check_if_next ();
		}

		private void on_server_event (Tarot.GameEvent ev) {
			if (game_shown.add_event (ev) != Tarot.GameError.OK) {
				assert (false);
			}
			update_events ();
		}

		[GtkCallback]
		private void show_new_game_page () {
			m_queue.close ();
			m_queue = new EventQueue ();
			m_queue.new_event.connect (on_server_event);
			m_queue.catchup.connect (() => { check_if_next (); });
			pages.visible_child = page_select_players;
			button_new_game.hide ();
		}

		[GtkCallback]
		private void new_game_3p () {
			start_new_game (3, false, new GLib.DateTime.now_utc ().to_string ());
		}

		[GtkCallback]
		private void new_game_4p () {
			start_new_game (4, false, new GLib.DateTime.now_utc ().to_string ());
		}

		[GtkCallback]
		private void new_game_5p () {
			start_new_game (5, true, new GLib.DateTime.now_utc ().to_string ());
		}

		[GtkCallback]
		private void open_info_dialog () {
			string[] artists = { "Individu Masqué", null };
			string[] authors = { "Vivien Kraus", null };
			var comments = "";
			var copyright = _ ("Copyright © 2019, 2020 Vivien Kraus");
			string[] documenters = { "Guess Who", null };
			var license_type = Gtk.License.GPL_3_0;
			var program_name = "eu.planete_kraus.Tarot";
			var version = VERSION;
			var website = "https://play-tarot.frama.io/tarot";
			var logo_icon_name = "eu.planete_kraus.Tarot";
			Gtk.show_about_dialog (this,
								   "artists", artists,
								   "authors", authors,
								   "comments", comments,
								   "copyright", copyright,
								   "documenters", documenters,
								   "license-type", license_type,
								   "program-name", program_name,
								   "version", version,
								   "website", website,
								   "logo-icon-name", logo_icon_name,
								   null);
			menu_popover.hide ();
		}

		[GtkCallback]
		private void show_menu_popover () {
			menu_popover.show_all ();
		}

		private void check_if_next () {
			tarot_widget.game = game_shown;
			tarot_widget.dont_ask ();
			if (!m_queue.empty ()) {
				// check_if_next is called frequently.  If we call it
				// while waiting for our event to be displayed, we
				// could play twice!
				return;
			}
			Tarot.Player myself = (Tarot.Player) (-1);
			if (game_shown.get_main_player (out myself) != Tarot.GameError.OK) {
				assert (false);
			}
			Tarot.Player next;
			Tarot.Bid mini;
			if (game_shown.get_hint_bid (out next, out mini) == Tarot.GameError.OK
				&& next == myself) {
				stderr.printf ("Asking bid >= %s\n".printf (mini.to_string ()));
				tarot_widget.ask_bid (mini);
			}
			bool allowed;
			if (game_shown.get_hint_decl (out next, out allowed) == Tarot.GameError.OK
				&& next == myself) {
				stderr.printf ("Asking declaration <= %s\n".printf (allowed.to_string ()));
				tarot_widget.ask_declaration (allowed);
			}
			Tarot.Number mini_call;
			if (game_shown.get_hint_call (out next, out mini_call) == Tarot.GameError.OK
				&& next == myself) {
				stderr.printf ("Asking call <= %s\n".printf (mini_call.to_string ()));
				tarot_widget.ask_call (mini_call);
			}
			size_t n_discard;
			Tarot.Card[] prio;
			Tarot.Card[] additional;
			if (game_shown.get_hint_discard_alloc (out next, out n_discard, out prio, out additional) == Tarot.GameError.OK
				&& next == myself) {
				stderr.printf ("Asking discard: %lu cards\n", n_discard);
				tarot_widget.ask_discard (n_discard, prio, additional);
			}
			size_t n_simple, n_double, n_triple;
			if (game_shown.get_hint_handful_alloc (out next, out n_simple, out n_double, out n_triple, out prio, out additional) == Tarot.GameError.OK
				&& next == myself
				&& n_simple <= prio.length + additional.length) {
				stderr.printf ("Asking handful: %lu, %lu or %lu cards\n", n_simple, n_double, n_triple);
				tarot_widget.ask_handful (n_simple, n_double, n_triple, prio, additional);
			}
			Tarot.Card[] playable;
			if (game_shown.get_hint_card_alloc (out next, out playable) == Tarot.GameError.OK
				&& next == myself) {
				stderr.printf ("Asking card");
				tarot_widget.ask_play (playable);
			}
		}

		construct {
			m_queue = new EventQueue ();
			tarot_widget.select_bid.connect ((bid) => {
					stderr.printf ("Selected bid %s\n", bid.to_string ());
					if (my_game.add (new Tarot.GameEvent.alloc_bid (bid)) != Tarot.GameError.OK) {
						assert (false);
					}
					update_events ();
				});
			tarot_widget.select_declaration.connect ((decl) => {
					stderr.printf ("Selected decl %s\n", decl.to_string ());
					if (my_game.add (new Tarot.GameEvent.alloc_decl (decl)) != Tarot.GameError.OK) {
						assert (false);
					}
					update_events ();
				});
			tarot_widget.select_call.connect ((card) => {
					stderr.printf ("Selected call %s\n", ((Tarot.Card) card).to_string_alloc ());
					if (my_game.add (new Tarot.GameEvent.alloc_call (card)) != Tarot.GameError.OK) {
						assert (false);
					}
					update_events ();
				});
			tarot_widget.select_discard.connect ((cards) => {
					stderr.printf ("Selected discard\n");
					var discard_cards = new Tarot.Card[cards.length];
					for (size_t i = 0; i < cards.length; i++) {
						discard_cards[i] = (Tarot.Card)(cards[i]);
					}
					if (my_game.add (new Tarot.GameEvent.alloc_discard (discard_cards)) != Tarot.GameError.OK) {
						assert (false);
					}
					update_events ();
				});
			tarot_widget.select_handful.connect ((cards) => {
					stderr.printf ("Selected handful\n");
					var handful_cards = new Tarot.Card[cards.length];
					for (size_t i = 0; i < cards.length; i++) {
						handful_cards[i] = (Tarot.Card)(cards[i]);
					}
					if (my_game.add (new Tarot.GameEvent.alloc_handful (handful_cards)) != Tarot.GameError.OK) {
						assert (false);
					}
					update_events ();
				});
			tarot_widget.select_card.connect ((card) => {
					stderr.printf ("Selected card %s\n", ((Tarot.Card) card).to_string_alloc ());
					if (my_game.add (new Tarot.GameEvent.alloc_card (card)) != Tarot.GameError.OK) {
						assert (false);
					}
					tarot_widget.dont_ask ();
					update_events ();
				});
		}

		private void start_new_game (size_t n_players, bool with_call, owned string seed) {
			n_events_pulled = 0;
			game_shown = new Tarot.Game.alloc ();
			size_t retry = 0;
			bool ok = false;
			while (!ok) {
				var my_seed = "%s (%lu)".printf (seed, retry);
				var bytes = new char[my_seed.length];
				for (long i = 0; i < my_seed.length; i++) {
					bytes[i] = my_seed[i];
				}
				my_game = new Tarot.Solo.alloc_fuzzy (0.05, bytes, null);
				retry++;
				ok = (my_game.setup_random (n_players, with_call, bytes) == Tarot.GameError.OK);
			}
			pages.visible_child = tarot_widget;
			button_new_game.show ();
			tarot_widget.dont_ask ();
			update_events ();
		}

		private static bool starts_with (string s, string prefix) {
			for (var i = 0; i < prefix.length; i++) {
				if (s[i] != prefix[i]) {
					return false;
				}
			}
			return true;
		}

		public Window (Gtk.Application app) {
			Object (application: app);
			var screen = Gdk.Screen.get_default ();
			var provider = new Gtk.CssProvider ();
			provider.load_from_resource ("/eu/planete_kraus/Tarot/style.css");
			Gtk.StyleContext.add_provider_for_screen (screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
			show_new_game_page ();
		}
	}
}
