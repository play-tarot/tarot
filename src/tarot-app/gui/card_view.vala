// tarot implements the rules of the tarot game
// Copyright (C) 2019  Vivien Kraus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace Tarot {
	public abstract class CardView: Gtk.DrawingArea {
		private Tarot.Card[] m_cards;
		private size_t[] m_selection;
		private size_t[] m_selectable;
		private TarotCairo.CardPainter m_card_painter;
		private size_t? m_pointed = null;

		public signal bool button_press_event_card (Gdk.EventButton button, size_t index);
		public signal bool button_press_event_no_card (Gdk.EventButton button);
		public signal bool button_release_event_card (Gdk.EventButton button, size_t index);
		public signal bool button_release_event_no_card (Gdk.EventButton button);
		public signal bool motion_notify_event_card (Gdk.EventMotion motion, size_t index);
		public signal bool motion_notify_event_no_card (Gdk.EventMotion motion);
		public signal bool touch_event_card (Gdk.EventTouch touch, size_t index);
		public signal bool touch_event_no_card (Gdk.EventTouch touch);

		public bool all_selectable = true;

		public Tarot.Card[] get_cards () {
			return m_cards;
		}

		public void set_cards (Tarot.Card[] value) {
			m_cards = value;
			m_selection = new size_t[0];
			m_selectable = new size_t[0];
			m_pointed = null;
			queue_draw ();
		}

		public size_t[] get_selection () {
			return m_selection;
		}

		public void set_selection (size_t[] value) {
			m_selection = value;
			foreach (var i in value) {
				assert (i < m_cards.length);
			}
			queue_draw ();
		}

		public size_t[] get_selectable () {
			if (all_selectable) {
				var ret = new size_t[m_cards.length];
				for (size_t i = 0; i < ret.length; i++) {
					ret[i] = i;
				}
				return ret;
			} else {
				return m_selectable;
			}
		}

		public void set_selectable (size_t[] value) {
			m_selectable = value;
			foreach (var i in value) {
				assert (i < m_cards.length);
			}
			all_selectable = false;
			queue_draw ();
		}

		public size_t? get_pointed () {
			return m_pointed;
		}

		public void set_pointed (size_t? value) {
			if (value != null) {
				assert (value < m_cards.length);
			}
			if ((value == null) != (m_pointed == null)
				|| (value != null && m_pointed != null
					&& value != m_pointed)) {
				m_pointed = value;
				queue_draw ();
			}
		}

		protected abstract Tarot.Position[] compute_positions (double view_w, double view_h, double card_w, double card_h, size_t n_cards);

		public Tarot.Position[] get_positions () {
			var width = get_allocated_width ();
			var height = get_allocated_height ();
			var card_width = m_card_painter.card_width;
			var card_height = m_card_painter.card_height;
			return compute_positions (width, height, card_width, card_height, get_cards ().length);
		}

		private delegate void PaintOne (size_t i, Tarot.Card which, bool selected, bool selectable, Cairo.Context ctx);

		private void paint_all (PaintOne paint, Cairo.Context context, bool repaint_pointed_card_last) {
			size_t i;
			Tarot.Position[] positions = get_positions ();
			bool[] is_selected = new bool[positions.length];
			bool[] is_selectable = new bool[positions.length];
			for (i = 0; i < positions.length; i++) {
				is_selected[i] = false;
				is_selectable[i] = false;
			}
			foreach (var pos in get_selection ()) {
				is_selected[pos] = true;
			}
			foreach (var pos in get_selectable ()) {
				is_selectable[pos] = true;
			}
			Tarot.Card[] cards = get_cards ();
			for (i = 0; i < positions.length; i++) {
				var p = positions[i];
				context.save ();
				context.translate (p.x, p.y);
				context.rotate (p.angle);
				context.scale (p.scale, -p.scale);
				paint (i, cards[i], is_selected[i], is_selectable[i], context);
				context.restore ();
			}
			size_t? pointed = get_pointed ();
			if (repaint_pointed_card_last && pointed != null) {
				i = pointed;
				var p = positions[i];
				context.save ();
				context.translate (p.x, p.y);
				context.rotate (p.angle);
				context.scale (p.scale, -p.scale);
				bool s = is_selected[i];
				bool sa = is_selectable[i];
				paint (i, cards[i], s, sa, context);
				context.restore ();
			}
		}

		public size_t? point_at (double x, double y) {
			int width = get_allocated_width ();
			int height = get_allocated_height ();
			var fakeimage = new Cairo.ImageSurface (Cairo.Format.A1, width, height);
			var context = new Cairo.Context (fakeimage);
			context.translate (width / 2.0, height / 2.0);
			context.scale (1, -1);
			size_t? ret = null;
			double card_width = m_card_painter.card_width;
			double card_height = m_card_painter.card_height;
			paint_all ((i, card, selected, selectable, context) => {
					double local_x = x;
					double local_y = y;
					context.device_to_user (ref local_x, ref local_y);
					local_x += card_width / 2;
					local_y += card_height / 2;
					if (local_x >= 0 && local_x <= card_width
						&& local_y >= 0 && local_y <= card_height) {
						ret = i;
					}
				}, context, false);
			return ret;
		}

		construct {
			this.expand = true;
			m_card_painter = new TarotCairo.DefaultCardPainter ();
			this.draw.connect ((context) => {
					uint width = get_allocated_width ();
					uint height = get_allocated_height ();
					context.set_source_rgba (0.306, 0.604, 0.024, 0.2);
					context.rectangle (0, 0, width, height);
					context.fill ();
					context.translate (width / 2.0, height / 2.0);
					context.scale (1, -1);
					paint_all ((i, card, selected, selectable, context) => {
							m_card_painter.paint_card (card, selected, selectable, context);
						}, context, true);
					return true;
				});
			add_events (Gdk.EventMask.BUTTON_PRESS_MASK
						| Gdk.EventMask.BUTTON_PRESS_MASK
						| Gdk.EventMask.POINTER_MOTION_MASK
						| Gdk.EventMask.TOUCH_MASK);
			this.button_press_event.connect ((button) => {
					var c = point_at (button.x, button.y);
					if (c == null) {
						return button_press_event_no_card (button);
					} else {
						return button_press_event_card (button, c);
					}
				});
			this.button_release_event.connect ((button) => {
					var c = point_at (button.x, button.y);
					if (c == null) {
						return button_release_event_no_card (button);
					} else {
						return button_release_event_card (button, c);
					}
				});
			this.motion_notify_event.connect ((motion) => {
					var c = point_at (motion.x, motion.y);
					if (c == null) {
						return motion_notify_event_no_card (motion);
					} else {
						return motion_notify_event_card (motion, c);
					}
				});
			this.touch_event.connect ((touch) => {
					var c = point_at (touch.touch.x, touch.touch.y);
					if (c == null) {
						return touch_event_no_card (touch.touch);
					} else {
						return touch_event_card (touch.touch, c);
					}
				});
		}
	}
	public class FanCardView: Tarot.CardView {

		protected override Tarot.Position[] compute_positions (double view_w, double view_h, double card_w, double card_h, size_t n_cards) {
			Tarot.Position[] ret;
			Tarot.layout_hand_alloc (view_w, view_h, card_w, card_h, n_cards, out ret);
			return ret;
		}
	}
	public class CenterCardView: Tarot.CardView {

		protected override Tarot.Position[] compute_positions (double view_w, double view_h, double card_w, double card_h, size_t n_cards) {
			Tarot.Position[] ret;
			Tarot.layout_center_alloc (view_w, view_h, card_w, card_h, n_cards, out ret);
			return ret;
		}
	}
	public class TrickCardView: Tarot.CardView {

		protected override Tarot.Position[] compute_positions (double view_w, double view_h, double card_w, double card_h, size_t n_cards) {
			Tarot.Position[] ret;
			Tarot.layout_trick_alloc (view_w, view_h, card_w, card_h, n_cards, out ret);
			return ret;
		}
	}
	public class CardSelection: Gtk.Box {
		private Tarot.CenterCardView forced = new Tarot.CenterCardView ();
		private Tarot.CenterCardView optional = new Tarot.CenterCardView ();
		private size_t n_optional = 0;

		public signal void valid_selection ();
		public signal void invalid_selection ();
		// n may be negative if we have too many cards selected
		public signal void n_remaining_cards_to_select (int n);

		public Tarot.Card[] get_selection () {
			Tarot.Card[] ret = forced.get_cards ();
			Tarot.Card[] optional_cards = optional.get_cards ();
			size_t[] selected_optional = optional.get_selection ();
			foreach (var i in selected_optional) {
				ret += optional_cards[i];
			}
			return ret;
		}

		public void set_n_optional (size_t n) {
			n_optional = n;
			if (n == 0) {
				optional.hide ();
			} else {
				optional.show ();
			}
			if (optional.get_selection ().length == n) {
				valid_selection ();
				n_remaining_cards_to_select (0);
			} else {
				invalid_selection ();
				n_remaining_cards_to_select ((int) n - optional.get_selection ().length);
			}
		}

		public void set_forced (Tarot.Card[] forced_cards) {
			if (forced_cards.length == 0) {
				forced.hide ();
			} else {
				forced.set_cards (forced_cards);
				forced.show ();
			}
		}

		public void set_optional (Tarot.Card[] optional_cards) {
			optional.set_cards (optional_cards);
			set_n_optional (n_optional);
			// We don't explicitly show / hide the widget here; it
			// will be updated when setting n_optional
		}

		private void clicked_on_optional (size_t i) {
			size_t[] new_selection = new size_t[0];
			if (n_optional == 1) {
				// Simply replace the selection.
				size_t[] i_arr = { i };
				new_selection = i_arr;
			} else {
				bool already_selected = false;
				foreach (var j in optional.get_selection ()) {
					if (j == i) {
						already_selected = true;
					}
				}
				if (already_selected) {
					// Remove i from the selection
					new_selection = new size_t[0];
					foreach (var old in optional.get_selection ()) {
						if (i != old) {
							new_selection += old;
						}
					}
				} else {
					// Add i to the selection
					new_selection = optional.get_selection ();
					new_selection += i;
				}
			}
			optional.set_selection (new_selection);
			set_n_optional (n_optional);
		}

		construct {
			orientation = Gtk.Orientation.HORIZONTAL;
			pack_start (forced);
			pack_start (optional);
			forced.motion_notify_event_card.connect ((motion, index) => {
					forced.set_pointed (index);
				});
			forced.motion_notify_event_no_card.connect ((motion) => {
					forced.set_pointed (null);
				});
			forced.touch_event_card.connect ((touch, index) => {
					forced.set_pointed (index);
				});
			forced.touch_event_no_card.connect ((touch) => {
					forced.set_pointed (null);
				});
			optional.motion_notify_event_card.connect ((motion, index) => {
					optional.set_pointed (index);
				});
			optional.motion_notify_event_no_card.connect ((motion) => {
					optional.set_pointed (null);
				});
			optional.touch_event_card.connect ((touch, index) => {
					optional.set_pointed (index);
					clicked_on_optional (index);
				});
			optional.touch_event_no_card.connect ((touch) => {
					optional.set_pointed (null);
				});
			optional.button_press_event_card.connect ((button, index) => {
					clicked_on_optional (index);
				});
			this.vexpand = false;
			this.height_request = 96;
		}
	}
}
