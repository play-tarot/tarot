/* tarot implements the rules of the tarot game
 * Copyright (C) 2019, 2020  Vivien Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "program_commons.h"
#include <math.h>

static size_t n_hidden_layers;
static size_t *hidden_sizes;
static double learning_rate;
static double exploration;
static int cont = 0;
static struct yarrow256_ctx generator;

static void parse_options (int argc, char *argv[]);

static void epoch (TarotPerceptron * perceptron);
static double self_learn (struct yarrow256_ctx *prng,
                          TarotPerceptron * perceptron);

int
main (int argc, char *argv[])
{
  TarotPerceptron *perceptron;
  size_t n_weights, i;
  double *weights;
  tarot_program_setup ();
  parse_options (argc, argv);
  if (cont)
    {
      perceptron = tarot_perceptron_static_default ();
      tarot_perceptron_set_learning_rate (perceptron, learning_rate);
    }
  else
    {
      perceptron =
        tarot_perceptron_alloc (n_hidden_layers, hidden_sizes, learning_rate);
      /* Initial weights are not initialized correctly */
      tarot_perceptron_save_alloc (perceptron, &n_weights, &weights);
      for (i = 0; i < n_weights; i++)
        {
          /* We also need to add some random noise, because the
           * initialization is weak */
          double mini = -2;
          double maxi = 2;
          size_t random_n;
          size_t random_max = (size_t) (-1);
          uint8_t random_data[sizeof (random_n)];
          double t = 0;
          yarrow256_random (&generator, sizeof (random_data), random_data);
          memcpy (&random_n, random_data, sizeof (random_data));
          t = ((double) random_n) / ((double) random_max);
          weights[i] = (mini * (1 - t) + maxi * t);
        }
      tarot_perceptron_load (perceptron, 0, n_weights, weights);
      free (weights);
    }
  fprintf (stderr,
           _("Metric across the epoch\tMaximum weight\tMaximum update\n"));
  while (1)
    {
      epoch (perceptron);
    }
  tarot_perceptron_free (perceptron);
  return EXIT_SUCCESS;
}

static void
parse_options (int argc, char *argv[])
{
  int c;
  char default_seed[] = "default";
  uint8_t *seed;
  char *end;
  exploration = 0.05;
  learning_rate = 1e-7;
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, strlen (default_seed), (void *) default_seed);
  while (1)
    {
      static struct option long_options[] = {
        {N_("help"), no_argument, 0, 'h'},
        {N_("version"), no_argument, 0, 'v'},
        {N_("learning-rate"), required_argument, 0, 'r'},
        {N_("exploration"), required_argument, 0, 'e'},
        {N_("continue"), no_argument, 0, 'c'},
        {N_("seed"), required_argument, 0, 's'},
        {0, 0, 0, 0}
      };
      int option_index = 0;
      for (option_index = 0; long_options[option_index].name != NULL;
           option_index++)
        {
          long_options[option_index].name =
            _(long_options[option_index].name);
        }
      option_index = 0;
      c = getopt_long (argc, argv, "hvr:e:cs:", long_options, &option_index);
      if (c == -1)
        break;
      switch (c)
        {
        case 0:
          break;

        case 'h':
          printf (_
                  ("Usage: tarot-perceptron-bootstrap [OPTIONS]... [HIDDEN_SIZE]...\n\
Learn to play by trial and error.  Options:\n\
- -h, --help: print this message and exit;\n\
- -v, --version: print the program version and exit;\n\
- -r, --learning-rate=LR: define the learning rate;\n\
- -e, --exploration=FRAC: define the probability to play at random;\n\
- -c, --continue: start from the already learnt model;\n\
- -s SEED, --seed=\"SEED\": set the seed for the random deal.  SEED\n\
   must not be empty.\n\
"));
          exit (EXIT_SUCCESS);
          break;

        case 'v':
          printf (_("%s (libtarot %s)\n\
\n\
Copyright © 2019 Vivien Kraus\nThis is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"), "tarot-perceptron-bootstrap", tarot_libtarot_version ());
          exit (EXIT_SUCCESS);
          break;

        case 'r':
          learning_rate = strtod (optarg, &end);
          if (learning_rate <= 0)
            {
              fprintf (stderr,
                       _
                       ("Error: the learning rate should be a positive number, not '%s'.\n"),
                       optarg);
              exit (EXIT_FAILURE);
            }
          break;

        case 'e':
          exploration = strtod (optarg, &end);
          if (exploration < 0 || exploration > 1)
            {
              fprintf (stderr,
                       _
                       ("Error: the exploration probability should be a probability (between 0 and 1), not '%s'.\n"),
                       optarg);
              exit (EXIT_FAILURE);
            }
          break;

        case 'c':
          cont = 1;
          break;

        case 's':
          seed = xmalloc (strlen (optarg) + 1);
          memcpy (seed, optarg, strlen (optarg) + 1);
          if (strlen (optarg) == 0)
            {
              fprintf (stderr, _("Error: the seed must not be empty.\n"));
              exit (EXIT_FAILURE);
            }
          yarrow256_seed (&generator, strlen (optarg), seed);
          free (seed);
          break;

        case '?':
          /* getopt_long already printed an error message. */
          exit (EXIT_FAILURE);
          break;

        default:
          abort ();
        }
    }
  n_hidden_layers = argc - optind;
  if (n_hidden_layers != 0 && cont)
    {
      fprintf (stderr,
               _
               ("Error: if you are continuing learning, do not specify the structure!\n"));
      exit (EXIT_FAILURE);
    }
  hidden_sizes = xmalloc (n_hidden_layers * sizeof (size_t));
  for (c = 0; (size_t) c < n_hidden_layers; c++)
    {
      hidden_sizes[c] = strtoul (argv[optind + c], &end, 10);
      if (strcmp (end, "") != 0)
        {
          hidden_sizes[c] = 0;
        }
      if (hidden_sizes[c] == 0)
        {
          fprintf (stderr,
                   _
                   ("Error: the %d-th dimension should be a strictly positive integer, not '%s'.\n"),
                   c, argv[optind + c]);
          exit (EXIT_FAILURE);
        }
    }
}

struct custom_ai_data
{
  double *sum_metric;
  double *n_metric;
  TarotPerceptron *perceptron;
};

static inline void
custom_ai_eval (void *data, const TarotGame * base, size_t n_candidates,
                TarotGameEvent ** candidates, size_t start, size_t max,
                double *scores)
{
  struct custom_ai_data *ai_data = (struct custom_ai_data *) data;
  TarotPerceptron *p = ai_data->perceptron;
  tarot_perceptron_eval (p, base, n_candidates, candidates, start, max,
                         scores);
}

static inline void
custom_ai_learn (void *data, const TarotGame * base,
                 const TarotGameEvent * event, double final_score)
{
  struct custom_ai_data *ai_data = (struct custom_ai_data *) data;
  TarotPerceptron *p = ai_data->perceptron;
  double *sum_metric = ai_data->sum_metric;
  double *n_metric = ai_data->n_metric;
  /* We need to validate before learning */
  double predicted;
  TarotGameEvent *evaluated = (TarotGameEvent *) event;
  double diff;
  tarot_perceptron_eval (p, base, 1, &evaluated, 0, 1, &predicted);
  diff = final_score - predicted;
  if (diff < 0)
    {
      diff = -diff;
    }
  *sum_metric += diff;
  *n_metric += 1;
  tarot_perceptron_learn (p, base, event, final_score);
}

static inline void *
custom_ai_dup (void *data)
{
  /* No memory management: we do not own anything and there is only
   * one occurence at a time. */
  return data;
}

static inline void
custom_ai_destruct (void *data)
{
  (void) data;
}

static inline TarotGame *
play_game (struct yarrow256_ctx *prng, double *metric, TarotPerceptron * p,
           size_t n_players, int with_call)
{
  uint8_t next_seed[256];
  double sum_metric = 0;
  double n_metric = 0;
  struct custom_ai_data data = {.sum_metric = &sum_metric,.n_metric =
      &n_metric,.perceptron = p
  };
  TarotAi *pure =
    tarot_ai_alloc (&data, custom_ai_eval, custom_ai_learn, custom_ai_dup,
                    custom_ai_destruct);
  TarotAi *fuzzy;
  TarotSolo *solo;
  int done = 0;
  TarotGame *ret = NULL;
  yarrow256_random (prng, sizeof (next_seed), next_seed);
  solo =
    tarot_solo_alloc_fuzzy (exploration, sizeof (next_seed), next_seed, pure);
  fuzzy =
    tarot_ai_alloc_fuzzy (exploration, sizeof (next_seed), next_seed, pure);
  tarot_ai_free (pure);
  yarrow256_random (prng, sizeof (next_seed), next_seed);
  if (tarot_solo_setup_random
      (solo, n_players, with_call, sizeof (next_seed),
       next_seed) != TAROT_GAME_OK)
    {
      tarot_solo_free (solo);
      ret = play_game (prng, metric, p, n_players, with_call);
      tarot_ai_free (fuzzy);
      tarot_solo_free (solo);
      return ret;
    }
  /* Play with fuzzy */
  while (!done)
    {
      TarotGame *state = tarot_solo_get_alloc (solo);
      if (tarot_game_step (state) == TAROT_END)
        {
          done = 1;
        }
      else
        {
          const TarotGameEvent *best;
          double predicted_score;
          best = tarot_ai_best (fuzzy, state, &predicted_score);
          if (best == NULL)
            {
              assert (0);
            }
          else
            {
              if (tarot_solo_add (solo, best) != TAROT_GAME_OK)
                {
                  assert (0);
                }
            }
        }
      tarot_game_free (state);
    }
  tarot_ai_free (fuzzy);
  ret = tarot_solo_get_alloc (solo);
  tarot_solo_free (solo);
  assert (n_metric != 0);
  *metric = sum_metric / n_metric;
  return ret;
}

static inline void
draw_options (struct yarrow256_ctx *prng, size_t *n_players, int *with_call)
{
  uint8_t option = 0;
  yarrow256_random (prng, sizeof (option), &option);
  if (option % 4 == 0)
    {
      *n_players = 3;
      *with_call = 0;
    }
  else if (option % 4 == 1)
    {
      *n_players = 4;
      *with_call = 0;
    }
  else if (option % 4 == 2)
    {
      *n_players = 5;
      *with_call = 0;
    }
  else
    {
      *n_players = 5;
      *with_call = 1;
    }
}

static inline TarotGame *
generate (struct yarrow256_ctx *prng, double *metric,
          TarotPerceptron * perceptron)
{
  size_t n_players;
  int with_call;
  TarotGame *played;
  draw_options (prng, &n_players, &with_call);
  played = play_game (prng, metric, perceptron, n_players, with_call);
  return played;
}

static void
epoch (TarotPerceptron * perceptron)
{
  size_t i = 0;
  double *initial_data;
  size_t n_weights;
  double *updates;
  double u_max = 0, p_max = 0;
  double sum_metric = 0;
  double n_metric = 0;
  tarot_perceptron_save_alloc (perceptron, &n_weights, &initial_data);
  updates = xcalloc (n_weights, sizeof (double));
#ifdef _OPENMP
#pragma omp parallel for
#endif /* _OPENMP */
  for (i = 0; i < 100; i++)
    {
      TarotPerceptron *my_perceptron = tarot_perceptron_dup (perceptron);
      double *final_data;
      size_t n_final_weights;
      size_t j;
      struct yarrow256_ctx prng;
      double metric;
      uint8_t seed[256];
      size_t seed_size = sizeof (seed) / sizeof (seed[0]);
#ifdef _OPENMP
#pragma omp critical
#endif /* _OPENMP */
      {
        yarrow256_random (&generator, seed_size, seed);
      }
      yarrow256_init (&prng, 0, NULL);
      yarrow256_seed (&prng, seed_size, seed);
      metric = self_learn (&prng, my_perceptron);
      tarot_perceptron_save_alloc (my_perceptron, &n_final_weights,
                                   &final_data);
      tarot_perceptron_free (my_perceptron);
      assert (n_weights == n_final_weights);
      /* Aggregation */
#ifdef _OPENMP
#pragma omp critical
#endif /* _OPENMP */
      {
        sum_metric += metric;
        n_metric += 1;
        for (j = 0; j < n_final_weights; j++)
          {
            double u = final_data[j] - initial_data[j];
            updates[j] += u;
          }
      }
      free (final_data);
    }
  for (i = 0; i < n_weights; i++)
    {
      double u_abs = updates[i];
      double p_abs = initial_data[i] + updates[i];
      initial_data[i] += updates[i];
      if (u_abs < 0)
        {
          u_abs = -u_abs;
        }
      if (p_abs < 0)
        {
          p_abs = -p_abs;
        }
      if (u_max < u_abs)
        {
          u_max = u_abs;
        }
      if (p_max < p_abs)
        {
          p_max = p_abs;
        }
      if (i != 0)
        {
          printf (", ");
        }
      printf ("%f", initial_data[i]);
    }
  free (updates);
  printf ("\n");
  fprintf (stderr, "%.6e\t%.6e\t%.6e\n", sum_metric / n_metric, p_max, u_max);
  tarot_perceptron_load (perceptron, 0, n_weights, initial_data);
  free (initial_data);
}

static double
self_learn (struct yarrow256_ctx *prng, TarotPerceptron * perceptron)
{
  double metric = 0;
  TarotGame *generated = generate (prng, &metric, perceptron);
  tarot_game_free (generated);
  return metric;
}
